﻿using AForge.Imaging;
using System.Drawing;

namespace Text_Image_Segmenter.LayoutAnalysis
{
    class BlobDetection
    {
        // detects blobs in image document using connected component labelling
        public Blob[] DetectBlobs(Bitmap image)
        {
            // create an instance of blob counter algorithm
            BlobCounterBase bc = new BlobCounter();

            // set filtering options
            bc.FilterBlobs = true;
            bc.MinWidth = 1;
            bc.MinHeight = 1;

            // set ordering options
            bc.ObjectsOrder = ObjectsOrder.XY;

            // process binary image
            bc.ProcessImage(image);
            Blob[] blobs = bc.GetObjectsInformation();

            // extract the biggest blob
            if (blobs.Length > 0)
            {
                bc.ExtractBlobsImage(image, blobs[0], true);
            }

            return blobs;
        }
    }
}
