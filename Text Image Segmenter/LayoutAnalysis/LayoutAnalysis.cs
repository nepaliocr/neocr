﻿using AForge.Imaging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Text_Image_Segmenter.LayoutAnalysis
{
    class LayoutAnalysis
    {
        float avg_height = 0;
        List<List<Blob>> sections = new List<List<Blob>>();

        public List<Rectangle> LayoutStart(Bitmap image)
        {
            Console.WriteLine("Starting Layout Analysis.......");

            // find blobs in image documents
            BlobDetection blobDetection = new BlobDetection();
            Blob [] blobList = blobDetection.DetectBlobs(image);

            Bitmap original = image;
            Brush whiteBrush = Brushes.White;
            Bitmap blockSeparatedImage = new Bitmap(original.Width, original.Height);
            Graphics g = Graphics.FromImage(blockSeparatedImage);
            g.Clear(Color.Black);

            using (var graphics = Graphics.FromImage(blockSeparatedImage))
            {
                foreach (Blob rList in blobList)
                {
                    graphics.FillRectangle(whiteBrush, rList.Rectangle);
                }
            }

            Blob [] blobs = blobDetection.DetectBlobs(blockSeparatedImage);

            // filter average ranging blobs on basis of average height
            List<Blob> avg_blobs = AverageBlobs(blobs);

            // layout finding
            List<Rectangle> layout = new List<Rectangle>();
            List<List<Blob>> columns = GetCols(avg_blobs);
            GetLayout(columns);

            layout = MakeRectangle(sections);

            return layout;
        }

        // find layout 
        public void GetLayout(List<List<Blob>> avg_blobs)
        {
            List<List<Blob>> columns = GetCols(avg_blobs);
            List<List<Blob>> columns_with_rows = new List<List<Blob>>();
            
            List<List<Blob>> rows = GetRowss(columns);
            
            List<List<Blob>> rows_with_columns = new List<List<Blob>>();

            foreach(List<Blob> row in rows)
            {
                if (checkColumns(row))
                {
                    rows_with_columns.Add(row);
                }else
                {
                    sections.Add(row);
                }
            }

            if(rows_with_columns.Count > 0)
            {
                GetLayout(rows_with_columns);
            }
        }

        public Boolean checkColumns(List<Blob> rows)
        {
            ColumnText col = new ColumnText();

            if(col.GetColumns(rows).Count > 1)
            {
                return true;
            }else
            {
                return false;
            }
        }

        public Boolean checkRows(List<Blob> columns)
        {
            RowText row = new RowText();

            if(row.GetRoz(columns,avg_height).Count > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // finds columns and returns columns
        public List<List<Blob>> GetCols(List<List<Blob>> columns)
        {
            List<List<Blob>> cols = new List<List<Blob>>();

            foreach (List<Blob> column in columns)
            {
                foreach (List<Blob> col in GetCols(column))
                {
                    cols.Add(col);
                }
            }

            return cols;
        }

        // find columns and returns columns
        public List<List<Blob>> GetCols(List<Blob> avg_blobs)
        {
            // columns 
            ColumnText col = new ColumnText();
            List<List<Blob>> columns = col.GetColumns(avg_blobs);

            return columns;
        }

        public List<List<Blob>> GetRowss(List<List<Blob>> columns)
        {
            // rows
            RowText row = new RowText();
            List<List<Blob>> rows = new List<List<Blob>>();

            foreach (List<Blob> column in columns)
            {
                List<List<Blob>> paragraph = row.GetRoz(column, avg_height);
                foreach (List<Blob> para in paragraph)
                {
                    rows.Add(para);
                }
            }

            return rows;
        }

        // make rectnagle
        public List<Rectangle> MakeRectangle(List<List<Blob>> rows)
        {
            List<Rectangle> layout = new List<Rectangle>();

            RowText rowText = new RowText();

            foreach(List<Blob> row in rows)
            {
                layout.Add(rowText.GetTextRectangle(row));
            }

            return layout;
        }


        // remove bigger and smaller blobs
        public List<Blob> AverageBlobs(Blob [] blobs)
        {
            //average height of blobs is calculated
            avg_height = AverageHeight(blobs);

            //find smaller blobs
            ArrayList smallblobs = RemoveSmallblobs(blobs);

            //find big blobs 
            ArrayList bigblobs = RemoveBigblobs(blobs);

            //convert Blob Array to List
            List<Blob> blobList = blobs.ToList();

            //remove smaller blobs
            foreach (Blob blob in smallblobs)
            {
                blobList.Remove(blob);
            }

            //remove bigger blobs
            foreach (Blob blob in bigblobs)
            {
                blobList.Remove(blob);
            }

            return blobList;
        }

        // This function calculates average height of blobs.
        public float AverageHeight(Blob[] blobs)
        {

            int sum = 0;

            for (int i = 0; i < blobs.Length; i++)
            {
                sum = sum + blobs[i].Rectangle.Height;
            }

            return sum / blobs.Length;

        }

        // this function removes small blobs.
        public ArrayList RemoveSmallblobs(Blob[] blobs)
        {
            //create smallblobs arraylist
            ArrayList smallblobs = new ArrayList();

            for (int i = 0; i < blobs.Length; i++)
            {

                if (blobs[i].Rectangle.Height < 0.5 * avg_height)
                {
                    smallblobs.Add(blobs[i]);
                }
            }

            return smallblobs;
        }

        
        // this function removes the big blobs. 
        public ArrayList RemoveBigblobs(Blob[] blobs)
        {
            //create bigblobs arraylist
            ArrayList bigblobs = new ArrayList();

            for (int i = 0; i < blobs.Length; i++)
            {

                if (blobs[i].Rectangle.Height > 2.8 * avg_height)
                {
                    bigblobs.Add(blobs[i]);
                }
            }

            return bigblobs;
        }

    }
}
