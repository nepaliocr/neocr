﻿using AForge.Imaging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Text_Image_Segmenter.LayoutAnalysis
{
    /*
     * This program detects textline in document.
     * The textline is formed using blobs in the document. 
    */
    class ColumnText
    {  
        // This function returns the collection of text lines.
        public List<List<Blob>> GetColumns(List<Blob> blobList)
        {
            //construct column blocks
            List<List<Blob>> column_blocks = ConstructColumns(blobList.OrderBy(b => b.Rectangle.X).ToList());

            return column_blocks;
        }

        // this function constructs text lines using blobs
        public List<List<Blob>> ConstructColumns(List<Blob> blobList)
        {
            //Column parameters
            int col_x1 = blobList[0].Rectangle.X;
            int col_x2 = col_x1 + blobList[0].Rectangle.Width;
            int col_y1 = blobList[0].Rectangle.Y;
            int col_y2 = col_y1 + blobList[0].Rectangle.Height;

            int inter_gap, intra_gap = 0;

            //Column list
            List<Blob> column_blobs = new List<Blob>();
            List<List<Blob>> columnList = new List<List<Blob>>();
            column_blobs.Add(blobList[0]);

            if (blobList.Count == 1)
            {    
                columnList.Add(column_blobs);
            }
            else
            {
                for (int i = 1; i < blobList.Count; i++)
                {
                    int next_col_x1 = blobList[i].Rectangle.X;
                    int next_col_x2 = next_col_x1 + blobList[i].Rectangle.Width;
                    int next_col_y1 = blobList[i].Rectangle.Y;
                    int next_col_y2 = next_col_y1 + blobList[i].Rectangle.Height;

                    intra_gap = blobList[i].Rectangle.Height;

                    if (next_col_x2 <= col_x2)
                    {
                        column_blobs.Add(blobList[i]);
                        if (next_col_y1 < col_y1)
                        {
                            col_y1 = next_col_y1;
                        }

                        if (next_col_y2 > col_y2)
                        {
                            col_y2 = next_col_y2;
                        }

                        if (i == blobList.Count - 1)
                        {
                            columnList.Add(column_blobs);
                        }
                    }
                    else if (next_col_x2 > col_x2 && next_col_x1 <= col_x2)
                    {
                        column_blobs.Add(blobList[i]);

                        col_x2 = next_col_x2;

                        if (next_col_y1 < col_y1)
                        {
                            col_y1 = next_col_y1;
                        }

                        if (next_col_y2 > col_y2)
                        {
                            col_y2 = next_col_y2;
                        }

                        if (i == blobList.Count - 1)
                        {
                            columnList.Add(column_blobs);
                        }
                    }
                    else
                    {
                        inter_gap = next_col_x1 - col_x2;

                        if (inter_gap > intra_gap)
                        {
                            columnList.Add(column_blobs);
                            column_blobs = new List<Blob>();

                            col_x1 = next_col_x1;
                            col_x2 = next_col_x2;
                            col_y1 = next_col_y1;
                            col_y2 = next_col_y2;
                        }
                        else
                        {
                            column_blobs.Add(blobList[i]);
                            col_x2 = next_col_x2;

                            if (next_col_y1 < col_y1)
                            {
                                col_y1 = next_col_y1;
                            }

                            if (next_col_y2 > col_y2)
                            {
                                col_y2 = next_col_y2;
                            }

                            if (i == blobList.Count - 1)
                            {
                                columnList.Add(column_blobs); 
                            }
                        }
                    }
                }
            }
            
            return columnList;
        }

    }
}
