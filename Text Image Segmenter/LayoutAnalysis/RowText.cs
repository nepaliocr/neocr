﻿using AForge.Imaging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Text_Image_Segmenter.LayoutAnalysis
{
    /*
     * This program detects textline in document.
     * The textline is formed using blobs in the document. 
    */
    class RowText
    {
        float avg_height = 0;

        // This function returns the collection of text lines.
        public List<List<Blob>> GetRoz(List<Blob> blobList, float average_height)
        {

            avg_height = average_height;

            // construct text lines
            List<Rectangle> textLines = TextLines(blobList.OrderBy(b => b.Rectangle.Y).ToList());
           
            // construct text blocks
            List <Rectangle> textBlocks = TextBlocks(textLines);

            List<List<Blob>> rows = GroupBlobs(textBlocks, blobList);

            return rows;
        }

        // constructs text lines using blobs
        public List<Rectangle> TextLines(List<Blob> blobList)
        {
            int blob_1_y = 0;
            int blob_2_y = 0;

            List<Rectangle> textLineList = new List<Rectangle>();
            List<Blob> wordsList = new List<Blob>();

            if (blobList.Count > 1)
            {
                for (int i = 0; i < blobList.Count; i++)
                {
                    if (i < blobList.Count - 1)
                    {
                        blob_1_y = blobList[i].Rectangle.Y;
                        blob_2_y = blobList[i+1].Rectangle.Y;
                    }
                    else
                    {
                        blob_1_y = blobList[i].Rectangle.Y;
                        blob_2_y = blobList[i - 1].Rectangle.Y;
                    }

                    if (Math.Abs(blob_1_y - blob_2_y) < avg_height)
                    { 
                        wordsList.Add(blobList[i]);

                        if(i == blobList.Count - 1)
                        {
                            Rectangle rec = GetTextRectangle(wordsList);
                            textLineList.Add(rec);
                        }
                    }
                    else
                    {
                        wordsList.Add(blobList[i]);

                        Rectangle rec = GetTextRectangle(wordsList);
                        textLineList.Add(rec);
                        wordsList = new List<Blob>();
                    }
                }
            }
            else if(blobList.Count == 1)
            {
                wordsList.Add(blobList[0]);
                Rectangle rec = GetTextRectangle(wordsList);
                textLineList.Add(rec);
            }

            return textLineList;
        }

        // construct rectangle to cover text line. 
        public Rectangle GetTextRectangle(List<Blob> wordList)
        {
            int line_x1 = wordList[0].Rectangle.X;
            int line_x2 = line_x1 + wordList[0].Rectangle.Width;
            int line_y1 = wordList[0].Rectangle.Y;
            int line_y2 = line_y1 + wordList[0].Rectangle.Height;

            if (wordList.Count > 1)
            {             
                for (int i = 1; i < wordList.Count; i++)
                {
                    if (wordList[i].Rectangle.X < line_x1)
                    { //finding line x1 coordinate
                        line_x1 = wordList[i].Rectangle.X;
                    }
                    if (wordList[i].Rectangle.X + wordList[i].Rectangle.Width > line_x2)
                    { //finding line x2 coordinate
                        line_x2 = wordList[i].Rectangle.X + wordList[i].Rectangle.Width;
                    }
                    if (wordList[i].Rectangle.Y < line_y1)
                    { //finding line y1 coordinate
                        line_y1 = wordList[i].Rectangle.Y;
                    }
                    if (wordList[i].Rectangle.Y + wordList[i].Rectangle.Height > line_y2)
                    { //finding line y2 coordinate
                        line_y2 = wordList[i].Rectangle.Y + wordList[i].Rectangle.Height;
                    }
                }       
            }
            else
            {
                return wordList[0].Rectangle;
            }

            return new Rectangle(line_x1, line_y1, line_x2 - line_x1, line_y2 - line_y1);
        }
        
        // construct text blocks        
        public List<Rectangle> TextBlocks(List<Rectangle> textLines)
        {
            int blob_1_y = 0;
            int blob_2_y = 0;

            List<Rectangle> textLineList = new List<Rectangle>();
            List<Rectangle> wordsList = new List<Rectangle>();

            if (textLines.Count > 1)
            {

                for (int i = 0; i < textLines.Count; i++)
                {

                    if (i < textLines.Count - 1)
                    {
                        blob_1_y = textLines[i].Y + textLines[i].Height;
                        blob_2_y = textLines[i + 1].Y;
                    }
                    else
                    {
                        blob_1_y = textLines[i].Y + textLines[i].Height;
                        blob_2_y = textLines[i - 1].Y;
                    }

                    if (Math.Abs(blob_1_y - blob_2_y) < avg_height)
                    {
                        //assumption of text line gap
                        wordsList.Add(textLines[i]);

                        if (i == textLines.Count - 1)
                        {
                            Rectangle rec = GetTextBlockRectangle(wordsList);
                            textLineList.Add(rec);
                        }
                    }
                    else
                    {
                        wordsList.Add(textLines[i]);

                        Rectangle rec = GetTextBlockRectangle(wordsList);
                        textLineList.Add(rec);
                        wordsList = new List<Rectangle>();
                    }
                }
            }
            else if(textLines.Count == 1)
            {
                wordsList.Add(textLines[0]);
                Rectangle rec = GetTextBlockRectangle(wordsList);
                textLineList.Add(rec);
            }

            return textLineList;

        }

        // construct rectangle enclosing text line
        public Rectangle GetTextBlockRectangle(List<Rectangle> wordList)
        {
            int line_x1 = wordList[0].X;
            int line_x2 = line_x1 + wordList[0].Width;
            int line_y1 = wordList[0].Y;
            int line_y2 = line_y1 + wordList[0].Height;

            if (wordList.Count > 1)
            {
                for (int i = 1; i < wordList.Count; i++)
                {
                    if (wordList[i].X < line_x1)
                    { //finding line x1 coordinate
                        line_x1 = wordList[i].X;
                    }
                    if (wordList[i].X + wordList[i].Width > line_x2)
                    { //finding line x2 coordinate
                        line_x2 = wordList[i].X + wordList[i].Width;
                    }
                    if (wordList[i].Y < line_y1)
                    { //finding line y1 coordinate
                        line_y1 = wordList[i].Y;
                    }
                    if (wordList[i].Y + wordList[i].Height > line_y2)
                    { //finding line y2 coordinate
                        line_y2 = wordList[i].Y + wordList[i].Height;
                    }
                }
            }
            else
            {
                return wordList[0];
            }

            return new Rectangle(line_x1, line_y1, line_x2 - line_x1, line_y2 - line_y1);
        }

        // groups all the blobs into rectangles
        public List<List<Blob>> GroupBlobs(List<Rectangle> rectangles, List<Blob> blobs)
        {
            List<Blob> row = new List<Blob>();

            List<List<Blob>> rows = new List<List<Blob>>();

           
            foreach (Rectangle rectangle in rectangles)
            {
                int rec_x1 = rectangle.X;
                int rec_x2 = rectangle.Width + rec_x1;
                int rec_y1 = rectangle.Y;
                int rec_y2 = rectangle.Height + rec_y1;

                foreach (Blob blob in blobs)
                {
                    int blob_x1 = blob.Rectangle.X;
                    int blob_x2 = blob.Rectangle.Width + blob_x1;
                    int blob_y1 = blob.Rectangle.Y;
                    int blob_y2 = blob.Rectangle.Height + blob_y1;                  

                    if (blob_x1 >= rec_x1 && blob_x2 <= rec_x2 && blob_y1 >= rec_y1 && blob_y2 <= rec_y2)
                    {
                        row.Add(blob);
                    }
                }
                rows.Add(row);

                foreach (Blob blob in row)
                {
                    blobs.Remove(blob);
                }

                row = new List<Blob>();
            }

            return rows;
        }
    }
}
