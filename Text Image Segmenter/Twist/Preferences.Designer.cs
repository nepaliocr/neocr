﻿namespace NeOCR.Twist
{
    partial class Preferences
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvLanguageData = new System.Windows.Forms.DataGridView();
            this.languageCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.languageName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataFilePrefix = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnRemoveLanguage = new System.Windows.Forms.Button();
            this.btnCancelDownload = new System.Windows.Forms.Button();
            this.btnDownload = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbDefaultFont = new System.Windows.Forms.CheckBox();
            this.btnSelectFont = new System.Windows.Forms.Button();
            this.tbFontName = new System.Windows.Forms.TextBox();
            this.tbFontSize = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbRecognizeOnLoad = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbOCRModes = new System.Windows.Forms.ComboBox();
            this.cbTessOCRLanguages = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbRestoreLayout = new System.Windows.Forms.CheckBox();
            this.btnSavePreferences = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLanguageData)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleDescription = "Manage OCR Language Data";
            this.groupBox1.Controls.Add(this.dgvLanguageData);
            this.groupBox1.Controls.Add(this.progressBar1);
            this.groupBox1.Controls.Add(this.btnRemoveLanguage);
            this.groupBox1.Controls.Add(this.btnCancelDownload);
            this.groupBox1.Controls.Add(this.btnDownload);
            this.groupBox1.Location = new System.Drawing.Point(12, 220);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(353, 150);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Manage Language Data";
            // 
            // dgvLanguageData
            // 
            this.dgvLanguageData.AllowUserToAddRows = false;
            this.dgvLanguageData.AllowUserToDeleteRows = false;
            this.dgvLanguageData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvLanguageData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLanguageData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.languageCode,
            this.languageName,
            this.dataFilePrefix});
            this.dgvLanguageData.Location = new System.Drawing.Point(6, 19);
            this.dgvLanguageData.Name = "dgvLanguageData";
            this.dgvLanguageData.ReadOnly = true;
            this.dgvLanguageData.Size = new System.Drawing.Size(260, 121);
            this.dgvLanguageData.TabIndex = 14;
            this.dgvLanguageData.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLanguageData_CellClick);
            // 
            // languageCode
            // 
            this.languageCode.HeaderText = "Language Code";
            this.languageCode.Name = "languageCode";
            this.languageCode.ReadOnly = true;
            // 
            // languageName
            // 
            this.languageName.HeaderText = "Name";
            this.languageName.Name = "languageName";
            this.languageName.ReadOnly = true;
            // 
            // dataFilePrefix
            // 
            this.dataFilePrefix.HeaderText = "Prefix";
            this.dataFilePrefix.Name = "dataFilePrefix";
            this.dataFilePrefix.ReadOnly = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(272, 117);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(75, 23);
            this.progressBar1.TabIndex = 13;
            // 
            // btnRemoveLanguage
            // 
            this.btnRemoveLanguage.AccessibleDescription = "Remove Language Data";
            this.btnRemoveLanguage.Location = new System.Drawing.Point(272, 78);
            this.btnRemoveLanguage.Name = "btnRemoveLanguage";
            this.btnRemoveLanguage.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveLanguage.TabIndex = 12;
            this.btnRemoveLanguage.Text = "Remove";
            this.btnRemoveLanguage.UseVisualStyleBackColor = true;
            this.btnRemoveLanguage.Click += new System.EventHandler(this.btnRemoveLanguage_Click);
            // 
            // btnCancelDownload
            // 
            this.btnCancelDownload.AccessibleDescription = "Cancel Downloading";
            this.btnCancelDownload.Location = new System.Drawing.Point(272, 48);
            this.btnCancelDownload.Name = "btnCancelDownload";
            this.btnCancelDownload.Size = new System.Drawing.Size(75, 23);
            this.btnCancelDownload.TabIndex = 11;
            this.btnCancelDownload.Text = "Cancel";
            this.btnCancelDownload.UseVisualStyleBackColor = true;
            this.btnCancelDownload.Click += new System.EventHandler(this.btnCancelDownload_Click);
            // 
            // btnDownload
            // 
            this.btnDownload.AccessibleDescription = "Download Language Data";
            this.btnDownload.Location = new System.Drawing.Point(272, 19);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(75, 23);
            this.btnDownload.TabIndex = 10;
            this.btnDownload.Text = "Download";
            this.btnDownload.UseVisualStyleBackColor = true;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // btnClose
            // 
            this.btnClose.AccessibleDescription = "Close";
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(284, 376);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 14;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Output pane font:";
            // 
            // cbDefaultFont
            // 
            this.cbDefaultFont.AccessibleDescription = "Output Pane Font";
            this.cbDefaultFont.AutoSize = true;
            this.cbDefaultFont.Checked = true;
            this.cbDefaultFont.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbDefaultFont.Location = new System.Drawing.Point(130, 12);
            this.cbDefaultFont.Name = "cbDefaultFont";
            this.cbDefaultFont.Size = new System.Drawing.Size(60, 17);
            this.cbDefaultFont.TabIndex = 0;
            this.cbDefaultFont.Text = "Default";
            this.cbDefaultFont.UseVisualStyleBackColor = true;
            this.cbDefaultFont.CheckedChanged += new System.EventHandler(this.cbDefaultFont_CheckedChanged);
            // 
            // btnSelectFont
            // 
            this.btnSelectFont.AccessibleDescription = "Select Font Button";
            this.btnSelectFont.Location = new System.Drawing.Point(269, 8);
            this.btnSelectFont.Name = "btnSelectFont";
            this.btnSelectFont.Size = new System.Drawing.Size(96, 23);
            this.btnSelectFont.TabIndex = 1;
            this.btnSelectFont.Text = "Select Font";
            this.btnSelectFont.UseVisualStyleBackColor = true;
            this.btnSelectFont.Click += new System.EventHandler(this.btnSelectFont_Click);
            // 
            // tbFontName
            // 
            this.tbFontName.AccessibleDescription = "Output Pane Font Name";
            this.tbFontName.Location = new System.Drawing.Point(130, 37);
            this.tbFontName.Name = "tbFontName";
            this.tbFontName.Size = new System.Drawing.Size(137, 20);
            this.tbFontName.TabIndex = 2;
            // 
            // tbFontSize
            // 
            this.tbFontSize.AccessibleDescription = "Output Pane Font Size";
            this.tbFontSize.Location = new System.Drawing.Point(269, 37);
            this.tbFontSize.Name = "tbFontSize";
            this.tbFontSize.Size = new System.Drawing.Size(96, 20);
            this.tbFontSize.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Recognition:";
            // 
            // cbRecognizeOnLoad
            // 
            this.cbRecognizeOnLoad.AccessibleDescription = "Recognize Documents on Load";
            this.cbRecognizeOnLoad.AutoSize = true;
            this.cbRecognizeOnLoad.Location = new System.Drawing.Point(130, 72);
            this.cbRecognizeOnLoad.Name = "cbRecognizeOnLoad";
            this.cbRecognizeOnLoad.Size = new System.Drawing.Size(119, 17);
            this.cbRecognizeOnLoad.TabIndex = 4;
            this.cbRecognizeOnLoad.Text = "Recognize on Load";
            this.cbRecognizeOnLoad.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AccessibleDescription = "Prefered Recognition Language";
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Language:";
            // 
            // label5
            // 
            this.label5.AccessibleDescription = "Prefered Recognition Mode";
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Mode:";
            // 
            // groupBox2
            // 
            this.groupBox2.AccessibleDescription = "Recognition Language and Mode";
            this.groupBox2.Controls.Add(this.cbOCRModes);
            this.groupBox2.Controls.Add(this.cbTessOCRLanguages);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cbRestoreLayout);
            this.groupBox2.Location = new System.Drawing.Point(12, 104);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(353, 110);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Recognition Language and Mode";
            // 
            // cbOCRModes
            // 
            this.cbOCRModes.AccessibleDescription = "OCR Mode";
            this.cbOCRModes.FormattingEnabled = true;
            this.cbOCRModes.Items.AddRange(new object[] {
            "Plain Text",
            "MS Word",
            "Searchable PDF"});
            this.cbOCRModes.Location = new System.Drawing.Point(186, 52);
            this.cbOCRModes.Name = "cbOCRModes";
            this.cbOCRModes.Size = new System.Drawing.Size(161, 21);
            this.cbOCRModes.TabIndex = 7;
            // 
            // cbTessOCRLanguages
            // 
            this.cbTessOCRLanguages.AccessibleDescription = "OCR Language";
            this.cbTessOCRLanguages.FormattingEnabled = true;
            this.cbTessOCRLanguages.Items.AddRange(new object[] {
            "Nepali (ne)",
            "English (en)"});
            this.cbTessOCRLanguages.Location = new System.Drawing.Point(186, 25);
            this.cbTessOCRLanguages.Name = "cbTessOCRLanguages";
            this.cbTessOCRLanguages.Size = new System.Drawing.Size(161, 21);
            this.cbTessOCRLanguages.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(134, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Restore Document Layout:";
            this.label6.Visible = false;
            // 
            // cbRestoreLayout
            // 
            this.cbRestoreLayout.AccessibleDescription = "Restore Document Layout";
            this.cbRestoreLayout.AutoSize = true;
            this.cbRestoreLayout.Location = new System.Drawing.Point(186, 83);
            this.cbRestoreLayout.Name = "cbRestoreLayout";
            this.cbRestoreLayout.Size = new System.Drawing.Size(63, 17);
            this.cbRestoreLayout.TabIndex = 8;
            this.cbRestoreLayout.Text = "Restore";
            this.cbRestoreLayout.UseVisualStyleBackColor = true;
            this.cbRestoreLayout.Visible = false;
            // 
            // btnSavePreferences
            // 
            this.btnSavePreferences.AccessibleDescription = "Save Preferences";
            this.btnSavePreferences.Location = new System.Drawing.Point(180, 376);
            this.btnSavePreferences.Name = "btnSavePreferences";
            this.btnSavePreferences.Size = new System.Drawing.Size(75, 23);
            this.btnSavePreferences.TabIndex = 13;
            this.btnSavePreferences.Text = "Save";
            this.btnSavePreferences.UseVisualStyleBackColor = true;
            this.btnSavePreferences.Click += new System.EventHandler(this.btnSavePreferences_Click);
            // 
            // Preferences
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(373, 411);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnSavePreferences);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.tbFontSize);
            this.Controls.Add(this.tbFontName);
            this.Controls.Add(this.btnSelectFont);
            this.Controls.Add(this.cbRecognizeOnLoad);
            this.Controls.Add(this.cbDefaultFont);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Preferences";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Preferences";
            this.Load += new System.EventHandler(this.Preferences_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLanguageData)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnRemoveLanguage;
        private System.Windows.Forms.Button btnCancelDownload;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbDefaultFont;
        private System.Windows.Forms.Button btnSelectFont;
        private System.Windows.Forms.TextBox tbFontName;
        private System.Windows.Forms.TextBox tbFontSize;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox cbRecognizeOnLoad;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cbOCRModes;
        private System.Windows.Forms.ComboBox cbTessOCRLanguages;
        private System.Windows.Forms.Button btnSavePreferences;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox cbRestoreLayout;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.DataGridView dgvLanguageData;
        private System.Windows.Forms.DataGridViewTextBoxColumn languageCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn languageName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataFilePrefix;
    }
}