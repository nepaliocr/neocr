﻿namespace NeOCR.Twist
{
    partial class ImageProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelSize = new System.Windows.Forms.Label();
            this.labelImageSource = new System.Windows.Forms.Label();
            this.labelResolution = new System.Windows.Forms.Label();
            this.labelBitDepth = new System.Windows.Forms.Label();
            this.labelColorMode = new System.Windows.Forms.Label();
            this.textBoxWidth = new System.Windows.Forms.TextBox();
            this.textBoxHeight = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxXResolution = new System.Windows.Forms.TextBox();
            this.textBoxBitDepth = new System.Windows.Forms.TextBox();
            this.textBoxColorMode = new System.Windows.Forms.TextBox();
            this.textBoxImageSource = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBoxYResolution = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelSize
            // 
            this.labelSize.AutoSize = true;
            this.labelSize.Location = new System.Drawing.Point(3, 10);
            this.labelSize.Name = "labelSize";
            this.labelSize.Size = new System.Drawing.Size(80, 13);
            this.labelSize.TabIndex = 0;
            this.labelSize.Text = "Width x Height:";
            // 
            // labelImageSource
            // 
            this.labelImageSource.AutoSize = true;
            this.labelImageSource.Location = new System.Drawing.Point(3, 185);
            this.labelImageSource.Name = "labelImageSource";
            this.labelImageSource.Size = new System.Drawing.Size(76, 13);
            this.labelImageSource.TabIndex = 0;
            this.labelImageSource.Text = "Image Source:";
            // 
            // labelResolution
            // 
            this.labelResolution.AutoSize = true;
            this.labelResolution.Location = new System.Drawing.Point(3, 47);
            this.labelResolution.Name = "labelResolution";
            this.labelResolution.Size = new System.Drawing.Size(70, 13);
            this.labelResolution.TabIndex = 0;
            this.labelResolution.Text = "X-Resolution:";
            // 
            // labelBitDepth
            // 
            this.labelBitDepth.AutoSize = true;
            this.labelBitDepth.Location = new System.Drawing.Point(3, 113);
            this.labelBitDepth.Name = "labelBitDepth";
            this.labelBitDepth.Size = new System.Drawing.Size(52, 13);
            this.labelBitDepth.TabIndex = 0;
            this.labelBitDepth.Text = "Bit depth:";
            // 
            // labelColorMode
            // 
            this.labelColorMode.AutoSize = true;
            this.labelColorMode.Location = new System.Drawing.Point(3, 148);
            this.labelColorMode.Name = "labelColorMode";
            this.labelColorMode.Size = new System.Drawing.Size(64, 13);
            this.labelColorMode.TabIndex = 0;
            this.labelColorMode.Text = "Color Mode:";
            // 
            // textBoxWidth
            // 
            this.textBoxWidth.AccessibleDescription = "Image Width";
            this.textBoxWidth.Location = new System.Drawing.Point(96, 7);
            this.textBoxWidth.Name = "textBoxWidth";
            this.textBoxWidth.ReadOnly = true;
            this.textBoxWidth.Size = new System.Drawing.Size(53, 20);
            this.textBoxWidth.TabIndex = 1;
            // 
            // textBoxHeight
            // 
            this.textBoxHeight.AccessibleDescription = "Image Height";
            this.textBoxHeight.Location = new System.Drawing.Point(173, 7);
            this.textBoxHeight.Name = "textBoxHeight";
            this.textBoxHeight.ReadOnly = true;
            this.textBoxHeight.Size = new System.Drawing.Size(53, 20);
            this.textBoxHeight.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(155, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "x";
            // 
            // textBoxXResolution
            // 
            this.textBoxXResolution.AccessibleDescription = "X-Resolution";
            this.textBoxXResolution.Location = new System.Drawing.Point(96, 44);
            this.textBoxXResolution.Name = "textBoxXResolution";
            this.textBoxXResolution.ReadOnly = true;
            this.textBoxXResolution.Size = new System.Drawing.Size(53, 20);
            this.textBoxXResolution.TabIndex = 3;
            // 
            // textBoxBitDepth
            // 
            this.textBoxBitDepth.AccessibleDescription = "Bit-Depth";
            this.textBoxBitDepth.Location = new System.Drawing.Point(96, 106);
            this.textBoxBitDepth.Name = "textBoxBitDepth";
            this.textBoxBitDepth.ReadOnly = true;
            this.textBoxBitDepth.Size = new System.Drawing.Size(53, 20);
            this.textBoxBitDepth.TabIndex = 5;
            // 
            // textBoxColorMode
            // 
            this.textBoxColorMode.AccessibleDescription = "Color Mode";
            this.textBoxColorMode.Location = new System.Drawing.Point(96, 141);
            this.textBoxColorMode.Name = "textBoxColorMode";
            this.textBoxColorMode.ReadOnly = true;
            this.textBoxColorMode.Size = new System.Drawing.Size(130, 20);
            this.textBoxColorMode.TabIndex = 6;
            // 
            // textBoxImageSource
            // 
            this.textBoxImageSource.AccessibleDescription = "Image Source";
            this.textBoxImageSource.Location = new System.Drawing.Point(96, 178);
            this.textBoxImageSource.Name = "textBoxImageSource";
            this.textBoxImageSource.ReadOnly = true;
            this.textBoxImageSource.Size = new System.Drawing.Size(254, 20);
            this.textBoxImageSource.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(232, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "pixels";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(155, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "DPI";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.labelSize);
            this.panel2.Controls.Add(this.textBoxWidth);
            this.panel2.Controls.Add(this.textBoxImageSource);
            this.panel2.Controls.Add(this.textBoxHeight);
            this.panel2.Controls.Add(this.labelImageSource);
            this.panel2.Controls.Add(this.textBoxColorMode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.textBoxBitDepth);
            this.panel2.Controls.Add(this.labelColorMode);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.textBoxYResolution);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.textBoxXResolution);
            this.panel2.Controls.Add(this.labelResolution);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.labelBitDepth);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(0, -1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(355, 209);
            this.panel2.TabIndex = 2;
            // 
            // textBoxYResolution
            // 
            this.textBoxYResolution.AccessibleDescription = "Y-Resolution";
            this.textBoxYResolution.Location = new System.Drawing.Point(96, 70);
            this.textBoxYResolution.Name = "textBoxYResolution";
            this.textBoxYResolution.ReadOnly = true;
            this.textBoxYResolution.Size = new System.Drawing.Size(53, 20);
            this.textBoxYResolution.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Y-Resolution:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(155, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "DPI";
            // 
            // btnClose
            // 
            this.btnClose.AccessibleDescription = "Close";
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(275, 214);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // ImageProperties
            // 
            this.AccessibleDescription = "Image Properties";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(358, 244);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImageProperties";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ImageProperties";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelSize;
        private System.Windows.Forms.Label labelImageSource;
        private System.Windows.Forms.Label labelResolution;
        private System.Windows.Forms.Label labelBitDepth;
        private System.Windows.Forms.Label labelColorMode;
        private System.Windows.Forms.TextBox textBoxWidth;
        private System.Windows.Forms.TextBox textBoxHeight;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxXResolution;
        private System.Windows.Forms.TextBox textBoxBitDepth;
        private System.Windows.Forms.TextBox textBoxColorMode;
        private System.Windows.Forms.TextBox textBoxImageSource;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textBoxYResolution;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnClose;
    }
}