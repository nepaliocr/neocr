﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NeOCR.Twist
{
    public partial class AlertForm : Form
    {
        #region PROPERTIES
        public string Message
        {
            set { labelMessage.Text = value; }
        }

        public int ProgressValue
        {
            set { progressBar1.Value = value; }
        }

        public bool IndeterminateProgressBar
        {
            set
            {
                if (value == true)
                {
                    progressBar1.Style = ProgressBarStyle.Marquee;
                }
            }
        }
        #endregion

        public AlertForm()
        {
            InitializeComponent();
        }

        // usage on txtSegmentProgress
        public void DisplayOCRProgress(String text)
        {
            Console.WriteLine(text);

            //Check if invoke requied if so return - as i will be recalled in correct thread
            if (ControlInvokeRequiredOCRProgress(this, () => DisplayOCRProgress(text))) return;
            this.Text = text;
        }

        /// <summary>
        /// Helper method to determin if invoke required, if so will rerun method on correct thread.
        /// if not do nothing.
        /// </summary>
        /// <param name="c">Control that might require invoking</param>
        /// <param name="a">action to preform on control thread if so.</param>
        /// <returns>true if invoke required</returns>
        public bool ControlInvokeRequiredOCRProgress(Control c, Action a)
        {
            if (c.InvokeRequired) c.Invoke(new MethodInvoker(delegate { a(); }));
            else return false;

            return true;
        }

        // usage on txtSegmentProgress
        public void SetOCRProgress(int progressVal)
        {
            //Check if invoke requied if so return - as i will be recalled in correct thread
            if (ControlInvokeRequiredOCRProgressVal(this, () => DisplayOCRProgressVal(progressVal))) return;
            progressBar1.Value = progressVal;
        }

        /// <summary>
        /// Helper method to determin if invoke required, if so will rerun method on correct thread.
        /// if not do nothing.
        /// </summary>
        /// <param name="c">Control that might require invoking</param>
        /// <param name="a">action to preform on control thread if so.</param>
        /// <returns>true if invoke required</returns>
        public bool ControlInvokeRequiredOCRProgressVal(Control c, Action a)
        {
            if (c.InvokeRequired) c.Invoke(new MethodInvoker(delegate { a(); }));
            else return false;

            return true;
        }

        // usage on txtSegmentProgress
        public void DisplayOCRProgressVal(int progressVal)
        {
            //Check if invoke requied if so return - as i will be recalled in correct thread
            if (ControlInvokeRequiredOCRProgress(this, () => DisplayOCRProgressVal(progressVal))) return;
            progressBar1.Value = progressVal;
        }
    }
}
