﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NeOCR.Twist
{
    public partial class ImageProperties : Form
    {
        #region PROPERTIES
        public String ImageWidth
        {
            set { textBoxWidth.Text = value; }
        }

        public String ImageHeight
        {
            set { textBoxHeight.Text = value; }
        }

        public String XResolution
        {
            set { textBoxXResolution.Text = value; }
        }

        public String YResolution
        {
            set { textBoxYResolution.Text = value; }
        }

        public String BitDepth
        {
            set { textBoxBitDepth.Text = value; }
        }

        public String ColorMode
        {
            set { textBoxColorMode.Text = value; }
        }

        public String ImageSource
        {
            set { textBoxImageSource.Text = value; }
        }
        #endregion
        public ImageProperties()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
