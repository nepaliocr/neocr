﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime;
using System.Runtime.InteropServices;
using System.Net.NetworkInformation;
using NeOCR.Libs;
using System.Net;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;

namespace NeOCR.Twist
{
    public partial class Preferences : Form
    {
        WebClient webClient = null;
        String sourceURL = "";
        String destinationPath = "";
        List<String> availLangs = null;

        #region--Properties Section
        private string opFontMode;
        private string outputPaneFont;
        private double opFontSize;
        private string recognizeOnLoad;
        private string ocrLang;
        private string ocrMode;
        private string restoreDocLayout;

        public string OPFont
        {
            get { return outputPaneFont; }
            set { outputPaneFont = value; }
        }

        public double OPFontSize
        {
            get { return opFontSize; }
            set { opFontSize = value; }
        }

        public string OPFontMode
        {
            get { return opFontMode; }
            set { opFontMode = value; }
        }

        public string RecognizeOnLoad
        {
            get { return recognizeOnLoad; }
            set { recognizeOnLoad = value; }
        }

        public string OCRLang
        {
            get { return ocrLang; }
            set { ocrLang = value; }
        }

        public string OCRMode
        {
            get { return ocrMode; }
            set { ocrMode = value; }
        }

        public string RestoreLayout
        {
            get { return restoreDocLayout; }
            set { restoreDocLayout = value; }
        }
        #endregion--End Properties Section

        public Preferences()
        {
            InitializeComponent();
        }

        private void btnSelectFont_Click(object sender, EventArgs e)
        {
            FontDialog fontDialog1 = new FontDialog();

            // Show the dialog.
            DialogResult result = fontDialog1.ShowDialog();
            // See if OK was pressed.
            if (result == DialogResult.OK)
            {
                // Get Font.
                Font font = fontDialog1.Font;
                // Set TextBox properties.
                tbFontName.Text = font.Name;
                tbFontSize.Text = font.Size.ToString();
            }
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            // get selected row
            int selectedRow = dgvLanguageData.SelectedCells[0].RowIndex;

            // get selected language prefix
            DataGridViewRow selectedDataRow = dgvLanguageData.Rows[selectedRow];
            String prefix = selectedDataRow.Cells[2].Value.ToString();

            // set tesseract directory path
            string tessDataDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\tesseract\\tessdata";
            tessDataDirPath = string.Format(tessDataDirPath.Replace("file:\\", ""));

            // check internet connection availability
            bool internetCheckStatus = NetworkInterface.GetIsNetworkAvailable();

            if (internetCheckStatus)
            {
                btnCancelDownload.Enabled = true;
                // source url and destination path of trainned data
                sourceURL = "https://github.com/tesseract-ocr/tessdata/blob/master/" + prefix + ".traineddata?raw=true";
                destinationPath = tessDataDirPath + "\\" + prefix + ".traineddata";

                webClient = new WebClient();
                webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(webClient_DownloadProgressChanged);
                webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(webClient_DownloadFileCompleted);

                webClient.DownloadFileAsync(new Uri(sourceURL), @destinationPath);

                while (webClient.IsBusy)    // while busy wait
                {
                    Application.DoEvents();
                }

                // make progress bar value 0
                progressBar1.Value = 0;
                // check download status and enable disable data related buttons
                checkLanguageDownloadStatus();
            }
            else
            {
                MessageBox.Show(null, "Could not connect to server. Please check your internet connection.", "Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// If downloading, cancels a download in progress.
        /// </summary>
        public virtual void Cancel()
        {
            if (this.webClient != null)
            {
                this.webClient.CancelAsync();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void cbDefaultFont_CheckedChanged(object sender, EventArgs e)
        {
            // disable buttons
            if(cbDefaultFont.Checked == true)
            {
                btnSelectFont.Enabled = false;
                tbFontName.Enabled = false;
                tbFontSize.Enabled = false;
            }
            else
            {
                btnSelectFont.Enabled = true;
            }
        }

        private void Preferences_Load(object sender, EventArgs e)
        {
            // disable buttons
            if (cbDefaultFont.Checked == true)
            {
                btnSelectFont.Enabled = false;
                tbFontName.Enabled = false;
                tbFontSize.Enabled = false;

                // Set TextBox properties. ||default font and font-size
                tbFontName.Text = "Kalimati";
                tbFontSize.Text = "12";
            }
            else
            {
                btnSelectFont.Enabled = true;
            }

            // OCR language and mode list
            getTesseractOCRLanguages();
            setOCRModes();

            getAvailableTessOCRLanguageList();

            // set preferences
            getPreferences();
            setPreferences();
        }

        private void webClient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            progressBar1.Maximum = (int)e.TotalBytesToReceive/100;
            progressBar1.Value = (int)e.BytesReceived/100;
            Console.Out.WriteLine((int)e.BytesReceived/100);
        }

        private void webClient_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                webClient.Dispose(); // Method that disposes the client and unhooks events

                // delete downloaded part file
                try
                {
                    if (File.Exists(destinationPath))
                    {
                        File.Delete(destinationPath);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(null, "tessdata Download Error: " + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                MessageBox.Show(null,"Download cancelled by user.","Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            // show download success message
            MessageBox.Show(null, "Download completed!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnCancelDownload_Click(object sender, EventArgs e)
        {
            this.Cancel(); // cancel tessdata downlod
        }


        private void getTesseractOCRLanguages()
        {
            cbTessOCRLanguages.Items.Clear();
            // get all tesseract languages
            String tessdata_path = "tesseract\\tessdata";
            // Process the list of files found in the directory.
            string[] listOfFiles = Directory.GetFiles(tessdata_path);

            availLangs = new List<string>();// initialize available languages list

            foreach (String file_item in listOfFiles)
            {

                if (File.Exists(file_item) && file_item.Substring(file_item.LastIndexOf(".")).Equals(".traineddata"))
                {
                    // get language name from language code
                    String lng = Path.GetFileName(file_item).Replace(".traineddata", "");
                    //String name = new CultureInfo(lng).DisplayName;
                    try
                    {
                        var langName = CultureInfo
                           .GetCultures(CultureTypes.NeutralCultures)
                           .Where(ci => string.Equals(ci.ThreeLetterISOLanguageName, lng, StringComparison.OrdinalIgnoreCase))
                           .FirstOrDefault().DisplayName;

                        if (lng.Equals("nep"))
                        {
                            cbTessOCRLanguages.Items.Insert(0, langName + " (" + lng + ")");
                        }
                        else
                        {
                            cbTessOCRLanguages.Items.Add(langName + " (" + lng + ")");
                        }

                        // add lang data prefix to availLandData list
                        availLangs.Add(lng);
                    }
                    catch (Exception ex)
                    {
                        // do something
                    }
                }
            }

            // If item count is > 0; set 0 as selected index
            if (cbTessOCRLanguages.Items.Count > 0)
            {
                // set selected index
                cbTessOCRLanguages.SelectedIndex = 0;
            }
        }

        private void setOCRModes()
        {
            // clear item list
            cbOCRModes.Items.Clear();
            // Process the list of files found in the directory.
            string[] ocrModes = { "Plain Text" };//, "Searchable PDF"};//, "hOCR"

            foreach (String mode in ocrModes)
            {
                cbOCRModes.Items.Add(mode);
            }

            // If item count is > 0; set 0 as selected index
            if (cbOCRModes.Items.Count > 0)
            {
                // set selected index
                cbOCRModes.SelectedIndex = 0;
            }
        }

        private void getAvailableTessOCRLanguageList()
        {
            string availLangFilePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\content\\avail-langs.xml";

            //available - langs.csv
            DataTable dtLangs = new DataTable();
            DataSet ds = new DataSet();
            ds.ReadXml(@availLangFilePath);
            dtLangs = ds.Tables[0];

            // set select whole row mode
            dgvLanguageData.MultiSelect = false;
            dgvLanguageData.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            // make columns NotSortable
            foreach (DataGridViewColumn column in dgvLanguageData.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            // clear dgv rows
            dgvLanguageData.Rows.Clear();
            // add languages to datagridview
            foreach(DataRow dr in dtLangs.Rows) {
                // add row to dgv
                this.dgvLanguageData.Rows.Add(dr[0],dr[1], dr[2]);
            }

            // check language status 
            checkLanguageDownloadStatus();
        }

        private void dgvLanguageData_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            checkLanguageDownloadStatus();
        }

        private void checkLanguageDownloadStatus()
        {
            // get selected row
            int selectedRow = dgvLanguageData.SelectedCells[0].RowIndex;

            // get selected language prefix
            DataGridViewRow selectedDataRow = dgvLanguageData.Rows[selectedRow];
            String prefix = selectedDataRow.Cells[2].Value.ToString();

            // check if already downloaded
            if (availLangs.Contains(prefix))
            {
                // disable download button, cancel button
                // enable remove button
                btnDownload.Enabled = false;
                btnCancelDownload.Enabled = false;

                btnRemoveLanguage.Enabled = true;
            }
            else
            {
                // enable download button and disable others
                btnRemoveLanguage.Enabled = false;
                btnCancelDownload.Enabled = false;

                btnDownload.Enabled = true;
            }
        }

        private void btnRemoveLanguage_Click(object sender, EventArgs e)
        {
            // get selected row
            int selectedRow = dgvLanguageData.SelectedCells[0].RowIndex;

            // get selected language prefix
            DataGridViewRow selectedDataRow = dgvLanguageData.Rows[selectedRow];
            String prefix = selectedDataRow.Cells[2].Value.ToString();

            // set tesseract directory path
            string tessDataDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\tesseract\\tessdata";
            tessDataDirPath = string.Format(tessDataDirPath.Replace("file:\\", ""));

            // confirmation dialog
            DialogResult dr = MessageBox.Show("Are you sure you want to delete this language data?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (dr == DialogResult.Yes)
            {
                // run OCR Process
                String destinationPath = tessDataDirPath + "\\" + prefix + ".traineddata";
                try
                {
                    // delete selected tessdata file
                    if (File.Exists(destinationPath)) { File.Delete(destinationPath); }
                    MessageBox.Show(null, "Selected tessdata removed successfully.\n", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                    getTesseractOCRLanguages(); // reload language list
                    checkLanguageDownloadStatus(); // check language download status
                }
                catch(Exception ex)
                {
                    MessageBox.Show(null, "Unable to delete language data.\n" + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnSavePreferences_Click(object sender, EventArgs e)
        {
            // get all configuration parameters
            // output pane font
            String opFont = (cbDefaultFont.Checked)?"default": tbFontName.Text;     // default font
            String fontSize = (cbDefaultFont.Checked) ? "default" : tbFontSize.Text;

            // recognition on load
            String recOnLoad = (cbRecognizeOnLoad.Checked) ? "TRUE" : "FALSE";

            // recognition language and mode
            String lang = cbTessOCRLanguages.Text;
            String mode = cbOCRModes.Text;
            //lang = Regex.Match(lang, @"\(([^)]*)\)").Groups[1].Value;

            // restore layout option
            String restoreLayout = (cbRestoreLayout.Checked) ? "YES" : "NO";

            // write to config file
            try
            {
                // get base directory path
                string baseDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                baseDirPath = @baseDirPath + "\\neocrapp.config";
                baseDirPath = string.Format(baseDirPath.Replace("file:\\", ""));

                // create datatable of preferences
                DataTable dtPreferences = new DataTable("Preferences");
                dtPreferences.Columns.Add("Font", typeof(String));
                dtPreferences.Columns.Add("FontSize", typeof(String));
                dtPreferences.Columns.Add("OnLoadRecognition", typeof(String));
                dtPreferences.Columns.Add("DefaultLang", typeof(String));
                dtPreferences.Columns.Add("Mode", typeof(String));
                dtPreferences.Columns.Add("LayoutRestore", typeof(String));
                dtPreferences.Rows.Add(opFont,fontSize, recOnLoad, lang, mode, restoreLayout);

                // write to xml
                DataSet dataSet = new DataSet();
                dataSet.Tables.Add(dtPreferences);
                // Save to disk
                dtPreferences.WriteXml(@baseDirPath,true);

                // show success message dialog
                MessageBox.Show(null, "Preferences saved successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Dispose(); // close window
            }
            catch(Exception ex)
            {
                MessageBox.Show(null, "Error while saving preferences." + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void getPreferences()
        {
            try
            {
                // get base directory path
                string baseDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                baseDirPath = @baseDirPath + "\\neocrapp.config";
                baseDirPath = string.Format(baseDirPath.Replace("file:\\", ""));
                // read preferences xml
                DataSet dataSet = new DataSet();
                dataSet.ReadXml(baseDirPath);

                DataTable pref = dataSet.Tables[0];
                this.OPFont = (pref.Rows[0][0].ToString().Equals("default")) ? "Kalimati" : pref.Rows[0][0].ToString(); // default font is "Kalimati"
                this.OPFontSize = (pref.Rows[0][1].ToString().Equals("default"))?12:Convert.ToDouble(pref.Rows[0][1]); // default font size is "12"
                this.OPFontMode = (pref.Rows[0][0].ToString().Equals("default")) ? "default" : "custom";
                this.RecognizeOnLoad = pref.Rows[0][2].ToString();
                this.OCRLang = pref.Rows[0][3].ToString();
                this.OCRMode = pref.Rows[0][4].ToString();
                this.RestoreLayout = pref.Rows[0][5].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(null, "Error while saving preferences." + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setPreferences()
        {
            // set value of each preferences control
            if (this.OPFontMode.Equals("default"))
            {
                cbDefaultFont.Checked = true;
                tbFontName.Text = "Kalimati";
                tbFontSize.Text = 12.ToString();
            }
            else
            {
                cbDefaultFont.Checked = false;
                tbFontName.Text = this.OPFont;
                tbFontSize.Text = this.OPFontSize.ToString();
            }

            if (this.RecognizeOnLoad.Equals("FALSE"))
            {
                cbRecognizeOnLoad.Checked = false;
            }
            else
            {
                cbRecognizeOnLoad.Checked = true;
            }

            cbTessOCRLanguages.SelectedIndex = cbTessOCRLanguages.FindStringExact(this.OCRLang);
            cbOCRModes.SelectedIndex = cbOCRModes.FindStringExact(this.OCRMode);

            if (this.RestoreLayout.Equals("NO"))
            {
                cbRestoreLayout.Checked = false;
            }
            else
            {
                cbRestoreLayout.Checked = true;
            }
        }
    }
}
