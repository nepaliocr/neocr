﻿using Accord.Imaging.Filters;
using AForge.Imaging;
using AForge.Imaging.Filters;
using Ghostscript.NET;
using Ghostscript.NET.Rasterizer;
using Layout;
using NeOCR.Libs;
using NeOCR.Register;
using NeOCR.Twist;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Text_Image_Segmenter.Libs;
using Text_Image_Segmenter.Libs.LayoutAnalysis;
using Text_Image_Segmenter.Wizards;
using WIA;

namespace Text_Image_Segmenter
{
    public partial class NeOCR_Main : Form
    {
        private List<string> _files = null;
        private int _imageSize = 80;
        private int _currentStartImageIndex = 0;
        private String _pdf_file = null;
        private String _language = null;
        private String _ocr_mode = null;
        private string _out_file_path;
        private List<string> _files_to_process = null;
        AlertForm alert;
        int recognizedPageCounter = 1;
        private bool RecognizeDocumentOnLoad = true;
        private bool RestoreDocumentLayout = false;

        // set work saved status
        bool workSaved = false;

        // This SoundPlayer plays a sound whenever the player hits a wall.
        System.Media.SoundPlayer startSoundPlayer = new System.Media.SoundPlayer(@"C:\Windows\Media\chord.wav");

        // This SoundPlayer plays a sound when the player finishes the game.
        System.Media.SoundPlayer finishSoundPlayer = new System.Media.SoundPlayer(@"C:\Windows\Media\tada.wav");

        public NeOCR_Main()
        {
            // form component initialization function
            InitializeComponent();

            /*
            #region -- Check registration --
            // create directory
            string main_path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string app_directory = main_path + "\\NepOCR";  // create app directory
            Directory.CreateDirectory(app_directory);

            // registration file
            string file_path = app_directory + "\\license.bin";

            // check registration file
            if (!File.Exists(file_path))
            {
                var registerform = new RegisterForm();
                registerform.MinimizeBox = false;
                registerform.MaximizeBox = false;
                registerform.StartPosition = FormStartPosition.CenterParent;
                registerform.ShowDialog(this);
            }
            #endregion
            */
        }

        public WIA.ImageFile scannedImage = null;

        // Property for Input image and Intermediary changed image storing
        public static class InputImage
        {
            private static List<System.Drawing.Image> _ProcessHistory = new List<System.Drawing.Image>();

            public static System.Drawing.Image Original { get; set; }
            public static System.Drawing.Image Changed { get; set; }
            public static List<System.Drawing.Image> ProcessHistory 
            { 
                get { return _ProcessHistory; } set { _ProcessHistory = value; } 
            }
        }

        // Property for storing text segments like lines, words, and characters
        public static class TextImageSegments
        {
            private static List<List<KeyValuePair<int, KeyValuePair<int, Rectangle>>>> _RefinedWords = new List<List<KeyValuePair<int, KeyValuePair<int, Rectangle>>>>();
            private static List<KeyValuePair<int, KeyValuePair<int, Rectangle>>> _CharacterSegments = new List<KeyValuePair<int, KeyValuePair<int, Rectangle>>>();

            public static List<List<KeyValuePair<int, KeyValuePair<int, Rectangle>>>> RefinedWords 
            {
                get { return _RefinedWords; }
                set { _RefinedWords = value; }
            }

            public static List<KeyValuePair<int, KeyValuePair<int, Rectangle>>> CharacterSegments
            {
                get { return _CharacterSegments; }
                set { _CharacterSegments = value; }            
            }
        }

        public static class PictureZoom
        {
            private static int _ZoomLevel = 10;

            public static int ZoomLevel { get{ return _ZoomLevel ;} set{ _ZoomLevel = value;} }
        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            // check for unsaved work
            if (rtbOCRedText.Text.Length > 0 && workSaved != true)
            {
                // confirmation dialog
                DialogResult dr = MessageBox.Show("You have unsaved work. Do you want to continue?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (dr == DialogResult.Yes)
                {
                    // call open image function
                    this.openImage();
                }
                else if (dr == DialogResult.No)
                {
                    //
                }
                //TODO: Stuff
            }
            else
            {
                // call open image function
                this.openImage();
            }
        }

        //Opens image file and correct the skewness of image by calling Hough Transform method
        public void openImage()
        {
            OpenFileDialog openImageDialog = new OpenFileDialog();
            // Select Image File to Recognize
            //openImageDialog.InitialDirectory = "D:\\";
            openImageDialog.Filter = "All Supported Image Types (*.bmp;*.jpg;*.jpeg;*.gif;*.png;*.tif)|" +
              "*.bmp;*.jpg;*.jpeg;*.gif;*.png;*.tif|Bitmap (*.bmp)|*.bmp|JPEG (*.jpeg;*.jpg)|*.jpeg;*.jpg|GIF (*.gif)|*.gif|PNG (*.png)|*.png|TIFF (*.tif;*.tiff)|*.tif;*.tiff";
            openImageDialog.FilterIndex = 1;
            openImageDialog.Multiselect = true;
            openImageDialog.RestoreDirectory = true;

            if (openImageDialog.ShowDialog() == DialogResult.OK)
            {
                // clear rtbOCRedText
                rtbOCRedText.Clear();

                /***/
                /*#clear temp directory #*/
                // clear dgvpages and picture box before acquiring images from new file
                // remove file from list and gridview
                // clear image in picture box
                inputPicBox.Visible = false;
                //Dispose images
                if(InputImage.Original != null) { InputImage.Original.Dispose(); InputImage.Original = null; }
                if (InputImage.Changed != null) { InputImage.Changed.Dispose(); InputImage.Changed = null; }

                // dispose picture box image
                if (inputPicBox.Image != null)
                {
                    inputPicBox.Image.Dispose();
                    inputPicBox.Image = null;
                }

                dgvPages.Rows.Clear();
                _files = null;

                // temporary file directory inside user's home directory
                String userHomeDir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                //Console.Out.WriteLine(userHomeDir);
                if(!Directory.Exists(userHomeDir + "\\.neocr\\")) // neocr data parent directory
                {
                    Directory.CreateDirectory(userHomeDir + "\\.neocr\\");
                }

                // check for child data dir
                if (!Directory.Exists(userHomeDir + "\\.neocr\\docs\\")) // neocr data parent directory
                {
                    Directory.CreateDirectory(userHomeDir + "\\.neocr\\docs\\");
                }

                String batDir = userHomeDir + "\\.neocr\\docs\\";
                /*
                //empty acquisition result directory
                // delete all segment image files
                string batDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\pdftoimage\\docs";
                string batDir = string.Format(@batDirPath.Replace("file:\\", "")); // location to temp files
                */

                // try temp file delete process
                System.IO.DirectoryInfo di = new DirectoryInfo(batDir);
                foreach (FileInfo file in di.GetFiles())
                {
                    try
                    {
                        String filePath = batDir + "\\" + file.Name;
                        File.Delete(filePath);
                        //file.Delete();                       
                    }
                    catch (Exception e)
                    {
                        Console.Out.WriteLine(e.ToString());
                    }
                }
                /***/
                // get selected files
                String[] files = openImageDialog.FileNames;

                /*#Region: Image/PDF selection#*/
                // if selected file is pdf then convert to image and load in gridview
                if (files.Length > 0 && Path.GetExtension(files[0]).ToLower() != ".pdf")
                {
                    try
                    {
                        // set first image in picture box
                        InputImage.Original = System.Drawing.Image.FromFile(files[0], true);
                        //set value to gobal bitmap variables orgImg and Img
                        InputImage.Changed = System.Drawing.Image.FromFile(openImageDialog.FileName, true);
                        
                        // make pic box visible
                        inputPicBox.Visible = true;
                        inputPicBox.Image = InputImage.Original;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                    }

                    // display preview of all pages (images)
                    _files = new List<string>(files);
                    LoadImages();
                }
                else
                {
                    // pdf is expected
                    _pdf_file = files[0];

                    // run ocr process
                    if (backgroundWorkerPDFToImage.IsBusy != true)
                    {
                        // create a new instance of the alert form
                        alert = new AlertForm();
                        alert.StartPosition = FormStartPosition.CenterScreen;//CenterParent
                        alert.Show(this);//.Show(this);
                        alert.IndeterminateProgressBar = true;

                        // Start the asynchronous operation.
                        backgroundWorkerPDFToImage.RunWorkerAsync();
                    }

                    // run image acquisition process
                    // backgroundWorkerPDFToImage.RunWorkerAsync();
                    // wait until process is busy
                    while (backgroundWorkerPDFToImage.IsBusy)
                        Application.DoEvents();
                    // load images
                    LoadImages();
                    //END
                }
                /*#Region End: Image/PDF selection#*/
            }
        }

        //Opens image file and correct the skewness of image by calling Hough Transform method
        public void openPDF()
        {
            OpenFileDialog openImageDialog = new OpenFileDialog();
            // Select Image File to Recognize
            //openImageDialog.InitialDirectory = "D:\\";
            openImageDialog.Filter = "PDF File (*.pdf)|*.pdf";
            openImageDialog.FilterIndex = 1;
            openImageDialog.Multiselect = false;
            openImageDialog.RestoreDirectory = true;

            if (openImageDialog.ShowDialog() == DialogResult.OK)
            {
                // clear rtbOCRedText
                rtbOCRedText.Clear();

                /***/
                /*#clear temp directory #*/
                // clear dgvpages and picture box before acquiring images from new file
                // remove file from list and gridview
                // clear image in picture box
                inputPicBox.Visible = false;
                //Dispose images
                if (InputImage.Original != null) { InputImage.Original.Dispose(); InputImage.Original = null; }
                if (InputImage.Changed != null) { InputImage.Changed.Dispose(); InputImage.Changed = null; }

                // dispose picture box image
                if (inputPicBox.Image != null)
                {
                    inputPicBox.Image.Dispose();
                    inputPicBox.Image = null;
                }

                dgvPages.Rows.Clear();
                _files = null;

                // temporary file directory inside user's home directory
                String userHomeDir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                //Console.Out.WriteLine(userHomeDir);
                if (!Directory.Exists(userHomeDir + "\\.neocr\\")) // neocr data parent directory
                {
                    Directory.CreateDirectory(userHomeDir + "\\.neocr\\");
                }

                // check for child data dir
                if (!Directory.Exists(userHomeDir + "\\.neocr\\docs\\")) // neocr data parent directory
                {
                    Directory.CreateDirectory(userHomeDir + "\\.neocr\\docs\\");
                }

                String batDir = userHomeDir + "\\.neocr\\docs\\";
                // try temp file delete process
                System.IO.DirectoryInfo di = new DirectoryInfo(batDir);
                foreach (FileInfo fileInfo in di.GetFiles())
                {
                    try
                    {
                        String filePath = batDir + "\\" + fileInfo.Name;
                        File.Delete(filePath);
                        //file.Delete();                       
                    }
                    catch (Exception e)
                    {
                        Console.Out.WriteLine(e.ToString());
                    }
                }

                /***/
                // get selected files
                String file = openImageDialog.FileName;

                /*#Region: Image/PDF selection#*/
                // if selected file is pdf then convert to image and load in gridview
                if (file.Length > 0 && Path.GetExtension(file).ToLower() == ".pdf")
                {
                    // pdf is expected
                    _pdf_file = file;

                    // run ocr process
                    if (backgroundWorkerPDFToImage.IsBusy != true)
                    {
                        // create a new instance of the alert form
                        alert = new AlertForm();
                        alert.StartPosition = FormStartPosition.CenterScreen;
                        alert.Show(this);;//.Show(this);
                        alert.IndeterminateProgressBar = true;

                        // Start the asynchronous operation.
                        backgroundWorkerPDFToImage.RunWorkerAsync();
                    }

                    // run image acquisition process
                    // backgroundWorkerPDFToImage.RunWorkerAsync();
                    // wait until process is busy
                    while (backgroundWorkerPDFToImage.IsBusy)
                        Application.DoEvents();
                    // load images
                    LoadImages();
                    //END
                }
                /*#Region End: Image/PDF selection#*/
            }
        }

        private void tsBtnDeskew_Click(object sender, EventArgs e)
        {
            if (inputPicBox.Image == null)
                return;

            HoughTransformationDeskew sk = new HoughTransformationDeskew((Bitmap)inputPicBox.Image);
            //imageDeskew sk = new imageDeskew(orgImg);
            double skewangle = sk.GetSkewAngle();
            Bitmap bmpOut = ImageLab.RotateImage((Bitmap)InputImage.Changed, -skewangle);
            inputPicBox.Image = bmpOut;

            //set value of variable Img
            InputImage.Changed = bmpOut;
            InputImage.ProcessHistory.Add(bmpOut);
        }

        private void tsBtnGrayscale_Click(object sender, EventArgs e)
        {
            if (inputPicBox.Image != null)
            {
                // use Otsu method to grayscale
                OtsuMethod ot = new OtsuMethod();
                Bitmap temp = ot.MakeGrayscale((Bitmap)inputPicBox.Image);
                //Bitmap temp = ot.MakeGrayscale(orgImg);
                inputPicBox.Image = temp;
                InputImage.Changed = temp;
                InputImage.ProcessHistory.Add(temp);
            }
            else
            {
                MessageBox.Show("Image not Selected");
            }
        }

        private void tsBtnThreshold_Click(object sender, EventArgs e)
        {
            //Get image from PictureBox
            if (inputPicBox.Image != null)
            {
                Bitmap bm = ImageLab.IterativeThreshold((Bitmap)inputPicBox.Image);

                inputPicBox.Image = bm;

                InputImage.Changed = bm;
                InputImage.ProcessHistory.Add(bm);
            }
            else
            {
                MessageBox.Show("Image not Selected");
            }
        }

        #region "-------------- croping the image----------"
        //CROPING THE IMAGE
        int cropX;
        int cropY;
        int cropWidth;

        int cropHeight;
        //int oCropX;
        //int oCropY;
        public bool Makeselection = false;
        public Pen cropPen;

        private void tsBtnCrop_Click(object sender, EventArgs e)
        {
            // if a portion of image is selected
            if (tsBtnSelect.Checked)
            {
                Cursor = Cursors.Default;
                try
                {
                    if (cropWidth < 1)
                    {
                        return;
                    }

                    //find the crop area in zoomed image
                    //MessageBox.Show("zoom level = "+ zoomLevel.ToString());
                    int cropX_restored = cropX;
                    int cropY_restored = cropY;
                    int cropWidth_restored = cropWidth;
                    int cropHeight_restored = cropHeight;

                    //Find in correct cropping position in original image
                    if (PictureZoom.ZoomLevel < 10 || PictureZoom.ZoomLevel > 10)
                    {
                        cropX_restored = (int)Math.Ceiling((double)cropX / PictureZoom.ZoomLevel * 10);
                        cropY_restored = (int)Math.Ceiling((double)cropY / PictureZoom.ZoomLevel * 10);
                        cropWidth_restored = (int)Math.Ceiling((double)cropWidth_restored / PictureZoom.ZoomLevel * 10);
                        cropHeight_restored = (int)Math.Ceiling((double)cropHeight_restored / PictureZoom.ZoomLevel * 10);
                    }

                    // Declare crop rectangle
                    Rectangle rect_restored = new Rectangle(cropX_restored, cropY_restored, cropWidth_restored, cropHeight_restored);

                    Bitmap OriginalImage = (Bitmap)InputImage.Changed;

                    // Crop the image:
                    Bitmap _img = OriginalImage.Clone(rect_restored, OriginalImage.PixelFormat);
                    inputPicBox.Image = _img;

                    //pnlImage.Location = new System.Drawing.Point(0, 0); //Sets panel location to (0, 0) i.e. initial position

                    PictureZoom.ZoomLevel = 10; //Reset zoomLevel

                    tsBtnCrop.Checked = false;
                    InputImage.Changed = _img;
                    InputImage.ProcessHistory.Add(_img);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                finally
                {
                    tsBtnSelect.Checked = false;
                }
            }
        }

        private void tsBtnSelect_Click(object sender, EventArgs e)
        {
            if(inputPicBox.Image != null)
            {
                Makeselection = true;
                tsBtnCrop.Enabled = true;
            }
            else
            {
                tsBtnSelect.Checked = false;
            }
        }

        private void PictureBoxLocation()
        {
            int _x = 0;
            int _y = 0;
            if (pnlImage.Width > inputPicBox.Width)
            {
                _x = (pnlImage.Width - inputPicBox.Width) / 2;
            }
            if (pnlImage.Height > inputPicBox.Height)
            {
                _y = (pnlImage.Height - inputPicBox.Height) / 2;
            }
            inputPicBox.Location = new System.Drawing.Point(_x, _y);
        }

        private void inputPicBox_MouseDown(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Default;
            if (Makeselection)
            {
                try
                {
                    if (e.Button == System.Windows.Forms.MouseButtons.Left)
                    {
                        Cursor = Cursors.Cross;
                        cropX = e.X;
                        cropY = e.Y;
                        cropPen = new Pen(Color.Black, 1);
                        cropPen.DashStyle = DashStyle.DashDotDot;
                    }
                    inputPicBox.Refresh();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void inputPicBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (Makeselection)
            {
                Cursor = Cursors.Default;
            }
        }

        private void inputPicBox_MouseMove(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Default;
            if (Makeselection)
            {
                try
                {
                    if (inputPicBox.Image == null)
                        return;

                    if (e.Button == System.Windows.Forms.MouseButtons.Left)
                    {
                        inputPicBox.Refresh();
                        cropWidth = e.X - cropX;
                        cropHeight = e.Y - cropY;
                        inputPicBox.CreateGraphics().DrawRectangle(cropPen, cropX, cropY, cropWidth, cropHeight);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        #endregion "------ crop image region ended --------"

        //ERAGE BUTTON ACTION
        private void tsBtnErase_Click(object sender, EventArgs e)
        {
            if (tsBtnSelect.Checked)
            {
                try
                {
                    if (cropWidth < 1)
                    {
                        return;
                    }
                    //Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
                    //First we define a rectangle with the help of already calculated points
                    Bitmap image = (Bitmap)inputPicBox.Image;

                    //Set selection rectangle range under canvas
                    int erasePixelHeight = cropY + cropHeight;
                    int erasePixelWidth = cropX + cropWidth;
                    if (erasePixelHeight > image.Height)
                        erasePixelHeight = image.Height;
                    if (erasePixelWidth > image.Width)
                        erasePixelWidth = image.Width;

                    //MessageBox.Show("cropX="+cropX+" cropY="+cropY+"pixel range height range="+ erasePixelHeight +" pixel range to erase width="+ erasePixelWidth +" Image Height ="+image.Height+" Image width ="+image.Width);
                    //Replace Selected Pixels to white pixels i.e. erase selected pixels
                    for (int y = cropY; y < erasePixelHeight; y++)
                        for (int x = cropX; x < erasePixelWidth; x++)
                        {
                            //Color oldColor = image.GetPixel(x, y);
                            Color newColor = Color.White;
                            //Color newColor = Color.FromArgb(oldColor.A - 1, oldColor.R - 1, oldColor.G - 1, oldColor.B - 1);
                            image.SetPixel(x, y, newColor);
                        }

                    inputPicBox.Image = image;
                    InputImage.Changed = image;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                finally
                {
                    tsBtnSelect.Checked = false;
                }
            }
        }

        //ZoomIn Button Click Action
        private void tsBtnZoomIn_Click(object sender, EventArgs e)
        {
            if (inputPicBox.Image != null && PictureZoom.ZoomLevel < 20)
            {
                PictureZoom.ZoomLevel += 1;
                Size newSize = new Size((int)(InputImage.Changed.Width * (PictureZoom.ZoomLevel * .1)), (int)(InputImage.Changed.Height * (PictureZoom.ZoomLevel * .1)));
                Bitmap bmp = new Bitmap(InputImage.Changed, newSize);
                inputPicBox.Image = bmp;
                //panel1.Location = new Point(0, 0);

                inputPicBox.Invalidate();
            }
        }

        //ZoomOut Button click Action
        private void tsBtnZoomOut_Click(object sender, EventArgs e)
        {
            if (inputPicBox.Image != null && PictureZoom.ZoomLevel > 2)
            {
                PictureZoom.ZoomLevel -= 1;
                Size newSize = new Size((int)(InputImage.Changed.Width * (PictureZoom.ZoomLevel * .1)), (int)(InputImage.Changed.Height * (PictureZoom.ZoomLevel * .1)));
                Bitmap bmp = new Bitmap(InputImage.Changed, newSize);
                inputPicBox.Image = bmp;
                //panel1.Location = new Point(0, 0);

                inputPicBox.Invalidate();
            }
        }

        //Call on PictureBox Resize...
        private void inputPicBox_Resize(object sender, EventArgs e)
        {
            //manageScrollbarOnPicBoxResize();
        }

        public void manageScrollbarOnPicBoxResize()
        {
            if (inputPicBox.Image == null)
                return;
        }

        private void tsBtnInvertImage_Click(object sender, EventArgs e)
        {
            InvertImageManually();
        }

        private void InvertImageManually()
        {
            //Method for inverting image -- this method uses simple technique of subtracting each of three pixel values 
            //of red, green and blue from 255 to get inverted color value
            /*
            if (MessageBox.Show("Are you sure? You are going to invert the image.", "Invert?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
            */
            //MessageBox.Show("Ok clicked");
            Bitmap bmp = (Bitmap)inputPicBox.Image;
            if (bmp != null)
            {
                Bitmap bm = ImageLab.Invert(bmp);
                // Bitmap bm = ImageLab.Invert(bmp);
                inputPicBox.Image = bm;

                InputImage.Changed = bm;
                InputImage.ProcessHistory.Add(bm);
            }
            else
            {
                MessageBox.Show("Image not found");
            }
            /*
            }
            */
        }

        //BRIGHTNESS CHANGE METHOD --- TO ADJUST BRIGHTNESS OF IMAGE
        public void adjustBrightness(int brightVal)
        {
            if (inputPicBox.Image != null)
            {
                float value = brightVal * 0.01f;
                float[][] colorMatrixElements = {
	                    new float[] {
		                    1,
		                    0,
		                    0,
		                    0,
		                    0
	                    },
	                    new float[] {
		                    0,
		                    1,
		                    0,
		                    0,
		                    0
	                    },
	                    new float[] {
		                    0,
		                    0,
		                    1,
		                    0,
		                    0
	                    },
	                    new float[] {
		                    0,
		                    0,
		                    0,
		                    1,
		                    0
	                    },
	                    new float[] {
		                    value,
		                    value,
		                    value,
		                    0,
		                    1
	                    }
                    };
                ColorMatrix colorMatrix = new ColorMatrix(colorMatrixElements);
                ImageAttributes imageAttributes = new ImageAttributes();

                imageAttributes.SetColorMatrix(colorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

                System.Drawing.Image _img = InputImage.Changed;
                //PictureBox1.Image
                Graphics _g = default(Graphics);
                Bitmap bm_dest = new Bitmap(Convert.ToInt32(_img.Width), Convert.ToInt32(_img.Height));
                _g = Graphics.FromImage(bm_dest);
                _g.DrawImage(_img, new Rectangle(0, 0, bm_dest.Width + 1, bm_dest.Height + 1), 0, 0, bm_dest.Width + 1, bm_dest.Height + 1, GraphicsUnit.Pixel, imageAttributes);
                this.inputPicBox.Image = bm_dest;
            }
            else
            {
                MessageBox.Show("Image not selected");
            }

        }

        public void ThresholdImage(int thresholdVal)
        {
            //Using Otsu Method
            //Get image from PictureBox
            if (inputPicBox.Image != null)
            {
                //orgImg = binarizedImg;
                Bitmap temp = (Bitmap)InputImage.Changed;
                //Bitmap temp = orgImg;
                //Otsu Thresholding works on grayscale images. So firstly, we have to convert our image into a gray scale one.
                temp = new OtsuMethod().MakeGrayscale(temp);
                //Show Slider Panel
                //thresholdValueTrackbar.Enabled = true;
                //thresholdValueTrackbar.Visible = true;
                //lblTrackBar.Text = "Move the slider to threshold image manually ...";
                //thresholdSlider.Show();
                //tbThresholdValue.Text = tsThresholdSlider.TrackBar.Value.ToString();
                Bitmap bmp = ImageLab.Threshold(temp, thresholdVal);
                //InputImage.Changed = bmp;

                inputPicBox.Image = bmp;
            }
            else
            {
                MessageBox.Show("Image not Selected");
            }
        }

        private void tsbtnBlurImage_Click(object sender, EventArgs e)
        {
            Bitmap bm = (Bitmap)inputPicBox.Image;
            Rectangle rect = new Rectangle(0, 0, bm.Width, bm.Height);

            Bitmap bmp = ImageLab.Blur(bm, rect, 2);
            inputPicBox.Image = bmp;

            InputImage.Changed = bmp;
            InputImage.ProcessHistory.Add(bmp);
        }

        private void tsbtnThinning_Click(object sender, EventArgs e)
        {
            Bitmap thinned = ImageLab.Thinning((Bitmap)inputPicBox.Image);

            inputPicBox.Image = thinned;

            InputImage.Changed = thinned;
            InputImage.ProcessHistory.Add(thinned);
        }

        private void tsbtnSkeleton_Click(object sender, EventArgs e)
        {
            System.Drawing.Image img = new ZhangSuenThinning().ZhangSuenThinningAlgorithm(inputPicBox.Image);

            inputPicBox.Image = img;

            InputImage.Changed = img;
            InputImage.ProcessHistory.Add(img);
        }

        private void tsbtnFillHoles_Click(object sender, EventArgs e)
        {
            // call fill holes method
            Bitmap holeFilledImage = ImageLab.FillHoles((Bitmap)inputPicBox.Image, 5, 5);
            inputPicBox.Image = holeFilledImage;

            InputImage.Changed = holeFilledImage;
            InputImage.ProcessHistory.Add(holeFilledImage);
        }

        private void pnlImage_Resize(object sender, EventArgs e)
        {
            // do something
        }

        private void SegmenterMain_Load(object sender, EventArgs e)
        {
            // invisible buttons
            tsbtnLayoutAnalysis.Visible = false;
            tsbtnBlobs.Visible = false;
            tsbtnDetectLines.Visible = false;
            inputPicBox.Visible = false;

            toolStripSeparator25.Visible = false;
            skinToolStripMenuItem.Visible = false;
            userInterfaceLanguageToolStripMenuItem.Visible = false;
            reportAProblemToolStripMenuItem1.Visible = false;
            provideASuggestionToolStripMenuItem.Visible = false;
            toolStripSeparator23.Visible = false;

            // get tesseract languages
            getTesseractOCRLanguages();
            // set ocr modes
            setOCRModes();

            /**Check Font Installation**/
            /*THIS TASK WILL BE HANDLED BY ADVANCED INSTALLER CREATOR***/
            /*
            if (!InstallFont.IsFontInstalled("Kalimati"))    // check for Kalimati font
            {
                InstallFont.RegisterFont("Kalimati.ttf");
            }
            //next font
            if (!InstallFont.IsFontInstalled("Madan2"))    // check for Kalimati font
            {
                InstallFont.RegisterFont("jagadamba.ttf");
            }
            */
            /****/

            // generate font list
            List<string> fonts = new List<string>();
            InstalledFontCollection installedFonts = new InstalledFontCollection();
            foreach (FontFamily font in installedFonts.Families)
            {
                fonts.Add(font.Name);
            }

            tscbFonts.Items.AddRange(fonts.ToArray());
            /*
            tscbFonts.SelectedIndex = fonts.IndexOf("Kalimati");
            // set font and font size on ocred text pane
            rtbOCRedText.Font = new Font("Kalimati", 12, FontStyle.Regular);
            rtbOCRedText.Invalidate();
            */

            // set font sizes
            String[] fontSizes = { "8", "9", "10", "11", "12", "14", "16", "18", "20", "22", "24", "26", "28", "36", "48", "72" };
            tscbFontSize.Items.AddRange(fontSizes);
            //tscbFontSize.SelectedIndex = 4;
            
            // call set preferences method
            SetPreferences();

            #region --Hide Image Preview Panel--
            // hide image preview panel
            // if disposed show
            if (splitContainer2.Panel1Collapsed)
            {
                splitContainer2.Panel1Collapsed = false;
                // Set the checkmark for the menuItemBlue menu item.
                imagePreviewPaneToolStripMenuItem.Checked = true;
            }
            // if shown dispose
            else if (!splitContainer2.Panel1Collapsed)
            {
                splitContainer2.Panel1Collapsed = true;
                // Set the checkmark for the menuItemBlue menu item.
                imagePreviewPaneToolStripMenuItem.Checked = false;
            }
            #endregion --Hide Image Preview Panel--
        }

        private void getTesseractOCRLanguages()
        {
            cbTessOCRLanguages.Items.Clear();
            // get all tesseract languages
            String tessdata_path = "tesseract\\tessdata";
            // Process the list of files found in the directory.
            string[] listOfFiles = Directory.GetFiles(tessdata_path);

            foreach (String file_item in listOfFiles)
            {

                if (File.Exists(file_item) && file_item.Substring(file_item.LastIndexOf(".")).Equals(".traineddata"))
                {
                    // get language name from language code
                    String lng = Path.GetFileName(file_item).Replace(".traineddata", "");
                    //String name = new CultureInfo(lng).DisplayName;
                    try
                    {
                        var langName = CultureInfo
                           .GetCultures(CultureTypes.NeutralCultures)
                           .Where(ci => string.Equals(ci.ThreeLetterISOLanguageName, lng, StringComparison.OrdinalIgnoreCase))
                           .FirstOrDefault().DisplayName;

                        if (lng.Equals("nep"))
                        {
                            if(cbTessOCRLanguages.Items.Count > 0)
                                cbTessOCRLanguages.Items.Insert(0, langName + " (" + lng + ")");
                            else
                                cbTessOCRLanguages.Items.Add(langName + " (" + lng + ")");
                        }
                        else
                        {
                            cbTessOCRLanguages.Items.Add(langName + " (" + lng + ")");
                        }
                    }
                    catch(Exception ex)
                    {
                        // do something
                    }
                }
            }

            // If item count is > 0; set 0 as selected index
            if (cbTessOCRLanguages.Items.Count > 0)
            {
                // set selected index
                cbTessOCRLanguages.SelectedIndex = 0;
            }
        }

        private void setOCRModes()
        {
            // Process the list of files found in the directory.
            string[] ocrModes = { "Plain Text" };//, "Searchable PDF"};//, "hOCR"

            foreach (String mode in ocrModes)
            {
                tscbOCRMode.Items.Add(mode);
            }

            // If item count is > 0; set 0 as selected index
            if (tscbOCRMode.Items.Count > 0)
            {
                // set selected index
                tscbOCRMode.SelectedIndex = 0;
            }
        }

        private void tsbtnAutoSegment_Click(object sender, EventArgs e)
        {
            if (inputPicBox.Image == null)
                return;

            // Read input Bitmap for input picture box
            Bitmap bm = (Bitmap)inputPicBox.Image;

            // Deskew input image
            txtSegmentProgress.Text = "Deskewing...";

            HoughTransformationDeskew sk = new HoughTransformationDeskew(bm);
            double skewangle = sk.GetSkewAngle();
            bm = ImageLab.RotateImage(bm, -skewangle);
            inputPicBox.Image = bm;

            // Grayscale Image
            txtSegmentProgress.Text = "Converting to grayscale image...";

            bm = new OtsuMethod().MakeGrayscale(bm);
            inputPicBox.Image = bm;

            // THRESHOLDING USING Accord.NET LIBRARY
            txtSegmentProgress.Text = "Thresholding...";

            bm = ImageLab.IterativeThreshold(bm);

            inputPicBox.Image = bm;

            // Word Segmentation
            txtSegmentProgress.Text = "Word level segmentation...";

            // Call Word Segmentation Method .. this will segment the test image and store the word's info in refinedWords global variable.
            PPWordSegmentation(bm);   

            // Character Segmentation
            txtSegmentProgress.Text = "Character level segmentation...";
            PPCharacterSegmentation(bm);

            // save word and character segments
            SaveWordSegments();
            SaveCharacterSegments();

            txtSegmentProgress.Text = "Success ...";

            txtSegmentProgress.Clear();

            #region // DRAW RECTANGLE BOXES .. SURROUNDING CHAR SEGMENT
            Bitmap charRecImg = (Bitmap)InputImage.Changed;

            Pen redPen = new Pen(Color.Red, 1);
            Pen bluePen = new Pen(Color.Blue, 1);
            //MessageBox.Show("Number of words: " + wordlist.Count.ToString());

            int counter = 1;
            using (var graphics = Graphics.FromImage(charRecImg))
            {
                foreach (var rec in TextImageSegments.CharacterSegments)
                {
                    if (counter % 2 != 0)
                        graphics.DrawRectangle(redPen, rec.Value.Value);
                    else
                        graphics.DrawRectangle(bluePen, rec.Value.Value);

                    counter++;
                }
            }

            inputPicBox.Image = charRecImg;
            #endregion  // DRAW RECTANGLE BOXES .. SURROUNDING CHAR SEGMENT

            MessageBox.Show(null, "Text-image segmentation success.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void tsBtnSegment_Click(object sender, EventArgs e)
        {
            if (inputPicBox.Image == null)
                return;

            Bitmap charRecImg = (Bitmap)InputImage.Changed;

            for (int i = 0; i < charRecImg.Height; i++)
            {
                for (int j = 0; j < charRecImg.Width; j++)
                {
                    Color color = charRecImg.GetPixel(j, i);

                    // String value "0" for Black and "255" for white
                    if (!((color.R.ToString().Equals("0") && color.G.ToString().Equals("0") && color.B.ToString().Equals("0"))
                        || (color.R.ToString().Equals("255") && color.G.ToString().Equals("255") && color.B.ToString().Equals("255"))))
                    {
                        // If image is not binarized, return
                        MessageBox.Show(null, "Binary Image is required to segment. Make grayscale image and then threshold to get segmentation results.","Image format error.",MessageBoxButtons.OK, MessageBoxIcon.Error); 
                        return;
                    }
                }
            }

            // Call Word Segmentation
            txtSegmentProgress.Text = "Performing word segmentation...";
            PPWordSegmentation(charRecImg);

            // Call Character Segmentation
            txtSegmentProgress.Text = "Performing character segmentation...";
            PPCharacterSegmentation(charRecImg);

            // DRAW RECTANGLE BOXES .. SURROUNDING CHAR SEGMENT
            Pen redPen = new Pen(Color.Red, 1);
            Pen bluePen = new Pen(Color.Blue, 1);
            //MessageBox.Show("Number of words: " + wordlist.Count.ToString());

            int counter = 1;
            using (var graphics = Graphics.FromImage((InputImage.Changed)))
            {
                foreach (var rec in TextImageSegments.CharacterSegments)
                {
                    if (counter % 2 != 0)
                        graphics.DrawRectangle(redPen, rec.Value.Value);
                    else
                        graphics.DrawRectangle(bluePen, rec.Value.Value);
                    //graphics.DrawRectangle(bluePen, rec.Value);
                    counter++;
                }
            }

            inputPicBox.Image = charRecImg;

            inputPicBox.Invalidate();

            txtSegmentProgress.Clear();

            MessageBox.Show(null, "Text-image segmentation success.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void PPWordSegmentation(Bitmap bimg)
        {
            // Word segmentation using projection profile method
            // Horizontal Projection to detect text lines
            //get number of black pixels in each row ------ using DrawHistogram class ... RowPixelCount Method
          
            List<int> pixels_list = new Histogram().RowPixelCount(bimg);

            #region //separate text lines

            List<List<int>> line_borders = new ProjectionProfile().LineSegmentation(pixels_list, bimg);   //List for storing line borders .. contains line tops and line bottoms

            List<int> line_top_list = line_borders[0]; //List of line top indicating rows
            List<int> line_bottom_list = line_borders[1]; //List of line bottom indicating rows            //List<List<int>> line_borders = new List<List<int>>();   //List for storing line borders .. contains line tops and line bottoms

            #endregion //***separate text lines section end***//

            /*************** SEPERATE WORDS ***************/
            //Declare list of rectangles. Rectangle represents the part of image covered by a single word
            var wordlist = new ProjectionProfile().WordSegmentation(line_borders, bimg);

            TextImageSegments.RefinedWords = new ProjectionProfile().WordRefine(wordlist, line_borders[0].Count);   //Call ImageSegmentation class's WordFerine method .. get refined words
            //MessageBox.Show("refined words length: " + refined_words[1].Count);        
        }

        public void PPCharacterSegmentation(Bitmap bimg)
        {
            // Character Segmentation Using Projection Profile Method
            List<KeyValuePair<int, Rectangle>> charSegListPerWord = new List<KeyValuePair<int, Rectangle>>();
            List<KeyValuePair<int, KeyValuePair<int, Rectangle>>> CharSegmentsList = new List<KeyValuePair<int, KeyValuePair<int, Rectangle>>>();

            int wordCount = 1;
            // loop for each word in word list
            foreach (var item in TextImageSegments.RefinedWords) // item is a line in list of lines
            {
                foreach (var word in item)    // ch is a word in each line
                {
                    // Call CharacterSegmentation
                    charSegListPerWord.Clear();
                    charSegListPerWord = new ProjectionProfile().CharacterSegmentationFromSingleWord((Bitmap)ImageLab.CropAtRectangle(bimg, word.Value.Value));

                    //foreach (var segment in charSegListPerWord)
                    for (int ch_seg = 0; ch_seg < charSegListPerWord.Count; ch_seg++)
                    {
                        var segment = charSegListPerWord[ch_seg];

                        //// if current segment height and width ratio is less than 3 then merge it with previous segment
                        //if (CharSegmentsList.Count > 0)
                        //{
                        //    if (ch_seg > 0)
                        //    {
                        //        if ((charSegListPerWord[ch_seg].Value.Height / charSegListPerWord[ch_seg].Value.Width) < 2)
                        //        {
                        //            var temp = CharSegmentsList[CharSegmentsList.Count - 1];
                        //            CharSegmentsList.RemoveAt(CharSegmentsList.Count - 1);

                        //            Rectangle next = new Rectangle(new Point(word.Value.Value.X + segment.Value.X, word.Value.Value.Y + segment.Value.Y), new Size(segment.Value.Width, segment.Value.Height));

                        //            // Compute position of character respective to original image
                        //            // Rectangle Box <Box Start Point, Box Size>
                        //            KeyValuePair<int, Rectangle> seg = new KeyValuePair<int, Rectangle>(segment.Key, Rectangle.Union(temp.Value.Value, next));
                        //            CharSegmentsList.Add(new KeyValuePair<int, KeyValuePair<int, Rectangle>>(wordCount, seg));

                        //            continue;
                        //        }
                        //    }
                        //}

                        // Compute position of character respective to original image
                        // Rectangle Box <Box Start Point, Box Size>
                        KeyValuePair<int, Rectangle> segm = new KeyValuePair<int, Rectangle>(segment.Key, new Rectangle(new Point(word.Value.Value.X + segment.Value.X, word.Value.Value.Y + segment.Value.Y), new Size(segment.Value.Width, segment.Value.Height)));

                        CharSegmentsList.Add(new KeyValuePair<int, KeyValuePair<int, Rectangle>>(wordCount, segm));
                    }

                    wordCount++;
                }
            }

            // Store character segments data to property
            TextImageSegments.CharacterSegments = CharSegmentsList;
        }

        public void SaveWordSegments()    // 2016-01-20 by Nirajan
        {
            // delete all segment image files
            System.IO.DirectoryInfo diword = new DirectoryInfo("E:\\OCR References\\OCR Package\\segmentationResult\\words\\");
            foreach (FileInfo fi in diword.GetFiles())
            {
                fi.Delete();
            }
            /******* END ***********/
            
            // Write the string to a file.
            System.IO.StreamWriter wordFile = new System.IO.StreamWriter("E:\\OCR References\\OCR Package\\Recognition Data\\wordSegments.wrdsegfile");

            Bitmap b = (Bitmap)InputImage.Changed;

            int count = 1;

            // loop for each word in word list
            foreach (var item in TextImageSegments.RefinedWords) // item is a line in list of lines
            {
                foreach (var word in item)    // ch is a word in each line
                {
                    if (word.Value.Value.Width == 0)
                        continue;

                    Bitmap bmp = ImageLab.CropAtRectangle(b, word.Value.Value);    // (image, rectangle)

                    UnmanagedImage image = UnmanagedImage.FromManagedImage(bmp);
                    UnmanagedImage grayImage = null;

                    if (image.PixelFormat == PixelFormat.Format8bppIndexed)
                    {
                        grayImage = image;
                    }
                    else
                    {
                        grayImage = UnmanagedImage.Create(image.Width, image.Height,
                            PixelFormat.Format8bppIndexed);
                        Grayscale.CommonAlgorithms.BT709.Apply(image, grayImage);
                    }

                    // apply thresholding
                    bmp = new AForge.Imaging.Filters.OtsuThreshold().Apply(grayImage).ToManagedImage();//.SISThreshold().Apply(grayImage).ToManagedImage();

                    // Remove surrounding white pixels
                    bmp = ImageLab.RemoveSurroundingWhitePixels(bmp);

                    // save image segment to temp file path
                    string tmppath = "E:\\OCR References\\OCR Package\\segmentationResult\\words\\word_" + count + ".Jpeg";
                    bmp.Save(tmppath, System.Drawing.Imaging.ImageFormat.Jpeg);

                    wordFile.WriteLine("word_" + count + ".Jpeg");

                    // increment counter
                    count++;
                    Application.DoEvents();
                }
            }

            // Close files
            wordFile.Close();
        }

        public void SaveCharacterSegments()
        {
            // delete all segment image files
            System.IO.DirectoryInfo dichar = new DirectoryInfo("E:\\OCR References\\OCR Package\\segmentationResult\\characters\\");
            foreach (FileInfo fi in dichar.GetFiles())
            {
                fi.Delete();
            }
            /******* END ***********/

            System.IO.StreamWriter charFile = new System.IO.StreamWriter("E:\\OCR References\\OCR Package\\Recognition Data\\characterSegments.chrsegfile");

            Bitmap b = (Bitmap)InputImage.Changed;

            int count = 1;

            foreach (var ch in TextImageSegments.CharacterSegments)
            {
                if (ch.Value.Value.Width == 0)
                    continue;

                Bitmap bmp = ImageLab.CropAtRectangle(b, ch.Value.Value);    // (image, rectangle)

                //Otsu Thresholding works on grayscale images. So firstly, we have to convert our image into a gray scale one.
                bmp = new OtsuMethod().MakeGrayscale(bmp);

                UnmanagedImage image = UnmanagedImage.FromManagedImage(bmp);
                UnmanagedImage grayImage = null;

                if (image.PixelFormat == PixelFormat.Format8bppIndexed)
                {
                    grayImage = image;
                }
                else
                {
                    grayImage = UnmanagedImage.Create(image.Width, image.Height,
                        PixelFormat.Format8bppIndexed);
                    Grayscale.CommonAlgorithms.BT709.Apply(image, grayImage);
                }

                // apply thresholding
                bmp = new AForge.Imaging.Filters.OtsuThreshold().Apply(grayImage).ToManagedImage();//.SISThreshold().Apply(grayImage).ToManagedImage();

                //Remove surrounding white pixels
                bmp = ImageLab.RemoveSurroundingWhitePixels(bmp);

                //bmp = new Bitmap(bmp, new Size(37, 58));

                // save image segment to temp file path
                string tmppath = "E:\\OCR References\\OCR Package\\segmentationResult\\characters\\" + ch.Key + "_" + count + ".Jpeg";
                bmp.Save(tmppath, System.Drawing.Imaging.ImageFormat.Jpeg);

                charFile.WriteLine(ch.Key + "_" + count + ".Jpeg");

                count++;
                Application.DoEvents();
            }

            charFile.Close();
        }

        private void inputPicBox_ClientSizeChanged(object sender, EventArgs e)
        {
            //manageScrollbarOnPicBoxResize();
        }

        private void tsbtnBlobs_Click(object sender, EventArgs e)
        {
            if (inputPicBox.Image == null)
                return;

            //
            // create instance of blob counter
            BlobCounter blobC = new BlobCounter();
            blobC.MinWidth = 2; // minimum blob size is 2x2
            blobC.MinHeight = 2;
            blobC.ObjectsOrder = ObjectsOrder.YX;

            Bitmap bmBlobs = (Bitmap)inputPicBox.Image;
            // process input image
            blobC.ProcessImage((Bitmap)inputPicBox.Image);
            // get information about detected objects
            Blob[] blobL = blobC.GetObjectsInformation();

            /* ... PLOT WORD BLOBS BOUNDARIES ... */
            /*
            using (Font font1 = new Font("Arial", 8, FontStyle.Bold, GraphicsUnit.Point))
            {
                using (var graphics = Graphics.FromImage((bmBlobs)))
                {
                    int counter = 1;
                    foreach (var blob in blobL) // item is a line in list of lines
                    {
                        graphics.DrawRectangle(new Pen(Color.Violet, 1), blob.Rectangle);
                        graphics.DrawString(counter.ToString(), font1, Brushes.Red, blob.Rectangle);
                        counter++;
                    }
                }
            }

            inputPicBox.Image = bmBlobs;
            //
            return;
            */

            
            List<KeyValuePair<int, KeyValuePair<int, Rectangle>>> wordList = new List<KeyValuePair<int, KeyValuePair<int, Rectangle>>>();
            List<List<KeyValuePair<int, KeyValuePair<int, Rectangle>>>> wordListLineWise = new List<List<KeyValuePair<int, KeyValuePair<int, Rectangle>>>>();
/*
            //Blob[] blobs = new Nepali_OCR.Source.BlobProcessing().BlobCounterBase((Bitmap)inputPicBox.Image);

            System.Drawing.Image charRecImg = InputImage.Changed;
            Bitmap notInverted = (Bitmap)InputImage.Changed;

            Pen redPen = new Pen(Color.Red, 1);
            Pen greenPen = new Pen(Color.Green, 1);
            Pen bluePen = new Pen(Color.Blue, 1);
            Pen goldPen = new Pen(Color.Gold, 1);
            Pen silverPen = new Pen(Color.Silver, 1);
            Pen darkRedPen = new Pen(Color.DarkRed, 1);
            Pen darkGreenPen = new Pen(Color.DarkGreen, 1);
            Pen darkBluePen = new Pen(Color.DarkBlue, 1);

            Pen[] pen = new Pen[] { redPen, greenPen, bluePen, goldPen, silverPen, darkRedPen, darkGreenPen, darkBluePen };
            int penCounter = 0;

            BlobCounter blobCounter = new BlobCounter();

            //// set ordering options
            //bc.ObjectsOrder = ObjectsOrder.Size;
            //blobCounter.ObjectsOrder = ObjectsOrder.XY;
            // set filtering options
            blobCounter.FilterBlobs = true;
            //blobCounter.MinWidth = 10;
            blobCounter.MinHeight = 10;

            txtSegmentProgress.Text = "Pre-processing image...";
            
            // Get image
            Bitmap toProcess = (Bitmap)inputPicBox.Image;   
            
            //// Apply blur
            toProcess = ImageLab.Blur(toProcess, new Rectangle(new Point(0,0), new Size(toProcess.Width, toProcess.Height)), 1);

            OtsuMethod ot = new OtsuMethod();
            toProcess = ot.MakeGrayscale(toProcess);

            // Skeletonization
            toProcess = (Bitmap)new ZhangSuenThinning().ZhangSuenThinningAlgorithm(toProcess);

            // Invert image before extracting blobs
            toProcess = ImageLab.Invert(toProcess);

            // change image pixel format
            AForge.Imaging.UnmanagedImage image = AForge.Imaging.UnmanagedImage.FromManagedImage(toProcess);
            AForge.Imaging.UnmanagedImage grayImage = null;

            if (image.PixelFormat == System.Drawing.Imaging.PixelFormat.Format8bppIndexed)
            {
                grayImage = image;
            }
            else
            {
                grayImage = AForge.Imaging.UnmanagedImage.Create(image.Width, image.Height,
                    System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
                AForge.Imaging.Filters.Grayscale.CommonAlgorithms.BT709.Apply(image, grayImage);
            }

            // create filter
            Dilatation filter = new Dilatation();
            // apply the filter
            toProcess = filter.Apply(grayImage).ToManagedImage();

            // Convert to non-indexed Pixel format
            toProcess = ImageLab.ConvertToNonIndexedImage(toProcess);

            txtSegmentProgress.Text = "Detecting Word Blobs...";

            blobCounter.ProcessImage(toProcess);
            Blob[] blobs = blobCounter.GetObjectsInformation();
            */
            //int[] ints = new[] { 10, 20, 10, 34, 113 };

            //List<int> lst = ints.OfType<int>().ToList(); // this isn't going to be fast.

            txtSegmentProgress.Text = "Processing Blobs to detect lines and order of words...";

            //List<Blob> blobList = blobs.OfType<Blob>().ToList(); // convert blobs array to list of blobs
            List<Blob> blobList = blobL.OfType<Blob>().ToList(); // convert blobs array to list of blobs

            // Sort blobs ... first OrderBy x and then ThenBy y
            List<Blob> sortBlobs = blobList.OrderBy(b => b.Rectangle.Y).ToList();

            // get average of word height
            double avgHeight = 0.0;
            double aggr = 0.0;
            foreach (var blb in sortBlobs)
            {
                aggr += blb.Rectangle.Height;
            }

            avgHeight = aggr / sortBlobs.Count;

            //****** SELECT BIG BLOGS AND TRY TO RE-EXTRACT BLOBS *********// Will be completed later
            List<Blob> bigBlobs = sortBlobs
                .Where((data, index) => data.Rectangle.Height > avgHeight * 1.8)
                .ToList();

            //****** REMOVE SMALL BLOBS ****//
            sortBlobs = sortBlobs
                .Where(data => (data.Rectangle.Height > avgHeight / 2) 
                    && data.Rectangle.Height < avgHeight*1.8)
                .ToList();
            
            // check overlapping blobs
            //CheckOverlappingBlobs(ref sortBlobs, avgHeight);

            // resort blobs
            List<Blob> sortedBlobs = sortBlobs.OrderBy(b => b.Rectangle.Y).ToList();

            // get sequential difference of y values (a,b,c,d) => sequential difference : (a-b,b-c,c-d)
            List<int> seqDiff = new List<int>();

            for (int i = 0; i < (sortedBlobs.Count - 1); i++)
            {
                seqDiff.Add(sortedBlobs[i + 1].Rectangle.Y - sortedBlobs[i].Rectangle.Y);
            }

            // find discontuinity of sequence .. using sequential difference
            List<int> discontuinity = new List<int>();
            discontuinity.Add(0);
            for (int j = 0; j < seqDiff.Count; j++)// seqDiff.Count -1
            {
                if (seqDiff[j] > avgHeight * 80 / 100)
                {
                    discontuinity.Add(j);//Add(j + 1)
                }
            }
            //discontuinity.Add(seqDiff.Count - 1);

            discontuinity.Add(sortedBlobs.Count - 1);

            // separate lists 
            List<Blob> sor = new List<Blob>();

            for (int i = 0; i < (discontuinity.Count - 1); i++)
            {
                List<Blob> myRange = new List<Blob>();
                if (i == 0)
                    myRange = sortedBlobs.GetRange(discontuinity[i], discontuinity[i + 1] - discontuinity[i] + 1);
                else
                    myRange = sortedBlobs.GetRange(discontuinity[i] + 1, discontuinity[i + 1] - discontuinity[i]);

                // Sort blobs ... first OrderBy x and then ThenBy y
                List<Blob> soBlobs = myRange.OrderBy(b => b.Rectangle.X).ToList();

                #region <<-- Extract Blobs from text line image -->>
                // Calculate bounding box of detected word blobs in a line
                Rectangle boundingBox = CalculateBoundingBoxOfRectangles(soBlobs);

                // Draw Text Line Image part bounding box
                //using (var graphics = Graphics.FromImage((notInverted)))
                //{
                //    graphics.DrawRectangle(new Pen(Color.Green, 1), boundingBox);
                //}

                // Chop text line image from original image
                Bitmap textLineImage = ImageLab.CropAtRectangle((Bitmap)InputImage.Changed, boundingBox);

                // Extract word blobs from textLineimage
                List<Blob> blobsTextLineImg = ExtractBlobsFromTextImage(textLineImage);

                // Get relative position of blobs with respect to original image
                for (int bCount = 0; bCount < blobsTextLineImg.Count; bCount++)
                {
                    Rectangle wordR = new Rectangle(
                        new Point(boundingBox.X + blobsTextLineImg[bCount].Rectangle.X, boundingBox.Y + blobsTextLineImg[bCount].Rectangle.Y), 
                        new System.Drawing.Size(blobsTextLineImg[bCount].Rectangle.Width, blobsTextLineImg[bCount].Rectangle.Height));
                    wordList.Add(new KeyValuePair<int, KeyValuePair<int, Rectangle>>(bCount + 1, new KeyValuePair<int, Rectangle>(bCount + 1, wordR)));
                }

                // add word list to line wise word list
                wordListLineWise.Add(new List<KeyValuePair<int, KeyValuePair<int, Rectangle>>>(wordList));

                #endregion <<-- Extract Blobs from text line image -->>

                sor.AddRange(soBlobs);// add list content to main list
            }

            // STORE WORDLISTLINEWISE TO PROPERTY
            TextImageSegments.RefinedWords = wordListLineWise;

            using (Font font1 = new Font("Arial", 8, FontStyle.Bold, GraphicsUnit.Point))
            {
                using (var graphics = Graphics.FromImage((bmBlobs)))
                {
                    int counter = 1;
                    foreach (var line in wordListLineWise) // item is a line in list of lines
                    {
                        foreach (var word in line) // item is a line in list of lines
                        {
                            graphics.DrawRectangle(new Pen(Color.Violet, 1), word.Value.Value);
                            graphics.DrawString(counter.ToString(), font1, Brushes.Red, word.Value.Value);
                            counter++;
                        }
                    }
                }
            }

            /* ... PLOT WORD BLOBS BOUNDARIES ... */
            //using (var graphics = Graphics.FromImage((notInverted)))
            //{
            //    foreach (var line in TextImageSegments.RefinedWords) // item is a line in list of lines
            //    {
            //        foreach (var word in line)    // ch is a word in each line
            //        {
            //            graphics.DrawRectangle(new Pen(Color.Violet, 1), word.Value.Value);
            //        }
            //    }
            //}

            /*
            #region <-- Character Segmentation -->
            txtSegmentProgress.Text = "Performing Character Segmentation...";
            
            BlobMethodCharacterSegmentation(wordList);

            #endregion

            #region // DRAW RECTANGLE BOXES .. SURROUNDING CHAR SEGMENT

            //int counter = 1;
            //using (var graphics = Graphics.FromImage(notInverted))
            //{
            //    foreach (var rec in TextImageSegments.CharacterSegments)
            //    {
            //        if (counter % 2 != 0)
            //            graphics.DrawRectangle(redPen, rec.Value.Value);
            //        else
            //            graphics.DrawRectangle(bluePen, rec.Value.Value);

            //        counter++;
            //    }
            //}

            #endregion  // DRAW RECTANGLE BOXES .. SURROUNDING CHAR SEGMENT
            
            inputPicBox.Image = notInverted;

            txtSegmentProgress.Text = "Saving segmentation result images...";

            SaveWordSegments();
            SaveCharacterSegments();

            inputPicBox.Invalidate();

            txtSegmentProgress.Clear();

            MessageBox.Show(null, "Text-image segmentation success.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            */
        }

        public void BlobMethodCharacterSegmentation(List<KeyValuePair<int, KeyValuePair<int, Rectangle>>> wordList)
        {
            // Character Segmentation Using Projection Profile Method
            List<KeyValuePair<int, Rectangle>> charSegListPerWord = new List<KeyValuePair<int, Rectangle>>();
            List<KeyValuePair<int, KeyValuePair<int, Rectangle>>> CharSegmentsList = new List<KeyValuePair<int, KeyValuePair<int, Rectangle>>>();

            Bitmap b = (Bitmap)inputPicBox.Image;//(Bitmap)InputImage.Changed;

            // loop for each word in word list

            for (int i = 0; i < wordList.Count; i++)
            {            
                // Call CharacterSegmentation
                charSegListPerWord.Clear();
                charSegListPerWord = new ProjectionProfile().CharacterSegmentationFromSingleWord((Bitmap)ImageLab.CropAtRectangle(b, wordList[i].Value.Value));

                foreach (var segment in charSegListPerWord)
                {
                    // Compute position of character respective to original image
                    // Rectangle Box <Box Start Point, Box Size>
                    KeyValuePair<int, Rectangle> seg = new KeyValuePair<int, Rectangle>(segment.Key, new Rectangle(new Point(wordList[i].Value.Value.X + segment.Value.X, wordList[i].Value.Value.Y + segment.Value.Y), new Size(segment.Value.Width, segment.Value.Height)));

                    CharSegmentsList.Add(new KeyValuePair<int, KeyValuePair<int, Rectangle>>(i+1, seg));
                }
            }

            // Store character segments data to property
            TextImageSegments.CharacterSegments = CharSegmentsList;
        }

        public void CheckOverlappingBlobs(ref List<Blob> blobs, Double avgHeight)
        {
            /*
             * Method to check overlapping blobs and correct the errors by removig fake blobs.
             */

            List<int> fakeBlobs = new List<int>();
            fakeBlobs.Clear();

            for (int i = 0; i < blobs.Count; i++)
            {
                //if (fakeBlobs.Contains(i))//int result = list.Find(item => item > 20);
                if (fakeBlobs.Find(item => item == i) < 0)
                    continue;
                for (int j = i + 1; j < blobs.Count; j++) //for (int j = 0; j < blobs.Count; j++)
                {
                    if ((blobs[j].Rectangle.X > blobs[i].Rectangle.X) &&
                        (blobs[j].Rectangle.Y > blobs[i].Rectangle.Y) &&
                        ((blobs[j].Rectangle.X + blobs[j].Rectangle.Width) < (blobs[i].Rectangle.X + blobs[i].Rectangle.Width)) &&
                        ((blobs[j].Rectangle.Y + blobs[j].Rectangle.Height) < (blobs[i].Rectangle.Y + blobs[i].Rectangle.Height)))
                    {
                        fakeBlobs.Add(j);
                    }


                    //Rectangle intersect = Rectangle.Intersect(blobs[j].Rectangle, blobs[i].Rectangle);

                    //if(intersect.Width * intersect.Height == blobs[j].Rectangle.Width*blobs[j].Rectangle.Height)
                    //{
                    //    // union
                    //    fakeBlobs.Add(j);
                    //}

                    //else if ((blobs[i].Rectangle.Y + blobs[i].Rectangle.Height) < blobs[j].Rectangle.Y)
                    //{
                    //    break;
                    //}
                    //else
                    //    break;
                }
            }

            fakeBlobs.Sort();
            fakeBlobs.Reverse();
            List<int> fakeBlobsL = fakeBlobs.Distinct().ToList();

            for (int k = (fakeBlobsL.Count - 1); k > 0; k--)
            {
                if (fakeBlobsL[k] < blobs.Count)
                {
                    if (blobs[k].Rectangle.Height < avgHeight * 80 / 100)
                    blobs.RemoveAt(fakeBlobsL[k]);
                }
            }
        }

        public Rectangle CalculateBoundingBoxOfRectangles(List<Blob> rectanglesList)
        {
            /*
             * Method to get Bounding box of words ... i.e. bounding box covering all of the words in a line
             */

            if(rectanglesList.Count < 0 )
                return new Rectangle(new Point(0,0), new Size(0,0)); // return null rectangle

            // rectangleList[0] is leftmost word in a line
            int x_cor = rectanglesList[0].Rectangle.X;  // x_cor does not changes in a line
            int y_cor = rectanglesList[0].Rectangle.Y;
            int width = rectanglesList[0].Rectangle.Width;
            int height = rectanglesList[0].Rectangle.Height;

            foreach (var blob in rectanglesList)
            {
                if (y_cor > blob.Rectangle.Y)
                    y_cor = blob.Rectangle.Y;

                width = blob.Rectangle.X - x_cor + blob.Rectangle.Width;

                if(height < blob.Rectangle.Height)
                {
                    height = blob.Rectangle.Height;                    
                }
            }

            return new Rectangle(new Point(x_cor, y_cor), new Size(width, height));
        }

        public List<Blob> ExtractBlobsFromTextImage(Bitmap textLineImage)
        {
            /*
             * Method to extract blobs (words) from line
             */
            
            BlobCounter blobCounter = new BlobCounter();

            //// set ordering options
            //bc.ObjectsOrder = ObjectsOrder.Size;
            //blobCounter.ObjectsOrder = ObjectsOrder.XY;
            // set filtering options
            blobCounter.FilterBlobs = true;
            //blobCounter.MinWidth = 10;
            blobCounter.MinHeight = 5;

            // Get image
            Bitmap toProcess = textLineImage;   
            
            //// Apply blur
            //toProcess = ImageLab.Blur(toProcess, new Rectangle(new Point(0,0), new Size(toProcess.Width, toProcess.Height)), 1);

            OtsuMethod ot = new OtsuMethod();
            toProcess = ot.MakeGrayscale(toProcess);

            // Skeletonization
            toProcess = (Bitmap)new ZhangSuenThinning().ZhangSuenThinningAlgorithm(toProcess);

            // Invert image before extracting blobs
            toProcess = ImageLab.Invert(toProcess);

            // change image pixel format
            AForge.Imaging.UnmanagedImage image = AForge.Imaging.UnmanagedImage.FromManagedImage(toProcess);
            AForge.Imaging.UnmanagedImage grayImage = null;

            if (image.PixelFormat == System.Drawing.Imaging.PixelFormat.Format8bppIndexed)
            {
                grayImage = image;
            }
            else
            {
                grayImage = AForge.Imaging.UnmanagedImage.Create(image.Width, image.Height,
                    System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
                AForge.Imaging.Filters.Grayscale.CommonAlgorithms.BT709.Apply(image, grayImage);
            }

            // create filter
            Dilatation filter = new Dilatation();
            // apply the filter
            toProcess = filter.Apply(grayImage).ToManagedImage();

            // Convert to non-indexed Pixel format
            toProcess = ImageLab.ConvertToNonIndexedImage(toProcess);

            blobCounter.ProcessImage(toProcess);
            Blob[] blobs = blobCounter.GetObjectsInformation();

            List<Blob> blobList = blobs.OfType<Blob>().ToList(); // convert blobs array to list of blobs

            // Sort blobs ... first OrderBy x and then ThenBy y
            List<Blob> sortBlobs = blobList.OrderBy(b => b.Rectangle.X).ToList();

            // get average of word height
            double avgHeight = 0.0;
            double aggr = 0.0;
            foreach (var blb in sortBlobs)
            {
                aggr += blb.Rectangle.Height;
            }

            avgHeight = aggr / sortBlobs.Count;

            //****** SELECT BIG BLOGS AND TRY TO RE-EXTRACT BLOBS *********// Will be completed later
            //List<Blob> bigBlobs = sortBlobs
            //    .Where((data, index) => data.Rectangle.Height > avgHeight * 1.8)
            //    .ToList();

            //****** REMOVE SMALL BLOBS ****//
            sortBlobs = sortBlobs
                .Where(data => (data.Rectangle.Height > avgHeight / 2)
                    && data.Rectangle.Height < avgHeight * 1.8)
                .ToList();

            return sortBlobs;
        }

        //Rotate image in inputPicBox by 90 degree clockwise
        private void toolMnuRotateCW_Click(object sender, EventArgs e)
        {
            Rotate90CW();
        }

        private void Rotate90CW()
        {
            if (inputPicBox.Image != null)
            {
                Bitmap bitmap = (Bitmap)inputPicBox.Image;
                bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
                inputPicBox.Image = bitmap;
                InputImage.Changed = bitmap;
            }
        }

        //Rotate image in inputPicBox by 90 degree counterclockwise
        private void toolMnuRotateCCW_Click(object sender, EventArgs e)
        {
            Rotate90CCW();
        }

        private void Rotate90CCW()
        {
            if (inputPicBox.Image != null)
            {
                Bitmap bitmap = (Bitmap)inputPicBox.Image;
                bitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
                inputPicBox.Image = bitmap;
                InputImage.Changed = bitmap;
            }
        }

        //Flip image Horizontal
        private void toolMnuFlipHorizontal_Click(object sender, EventArgs e)
        {
            FlipHorizontal();
        }

        private void FlipHorizontal()
        {
            if (inputPicBox.Image != null)
            {
                Bitmap bitmap = (Bitmap)inputPicBox.Image;
                bitmap.RotateFlip(RotateFlipType.RotateNoneFlipX);
                inputPicBox.Image = bitmap;
                InputImage.Changed = bitmap;
            }
        }

        //Flip image vertical
        private void toolMnuFlipVertical_Click(object sender, EventArgs e)
        {
            FlipVertical();
        }

        private void FlipVertical()
        {
            if (inputPicBox.Image != null)
            {
                Bitmap bitmap = (Bitmap)inputPicBox.Image;
                bitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);
                inputPicBox.Image = bitmap;
                InputImage.Changed = bitmap;
            }
        }

        //Rotate Image in inputPicBox by 180 degree clockwise
        private void toolMnuRotate180_Click(object sender, EventArgs e)
        {
            Rotate180();
        }

        private void Rotate180()
        {
            if (inputPicBox.Image != null)
            {
                Bitmap bitmap = (Bitmap)inputPicBox.Image;
                bitmap.RotateFlip(RotateFlipType.Rotate180FlipNone);
                inputPicBox.Image = bitmap;
                InputImage.Changed = bitmap;
            }
        }

        //Rotate Image by an arbitrary angle
        private void toolMnuRotateArbitrary_Click(object sender, EventArgs e)
        {
            RotateArbitrary();
        }

        private void RotateArbitrary()
        {
            if (inputPicBox.Image != null)
            {
                int inputAngle = 5;

                DialogResult dr = InputBox("Rotate by", "Rotate image by angle:", ref inputAngle);

                if (dr == DialogResult.OK)
                {
                    Bitmap bitmap = (Bitmap)inputPicBox.Image;

                    bitmap = ImageLab.RotateImage(bitmap, Convert.ToInt16(inputAngle), Color.FromArgb(255, 255, 255));

                    InputImage.Changed = bitmap;

                    inputPicBox.Image = bitmap;
                }
            }

        }

        public static DialogResult InputBox(string title, string promptText, ref int value)
        {
            /*
             * Source: http://www.dreamincode.net/forums/topic/285294-why-havent-c%23-a-inputbox/ [Date Accessed: Mar 17, 2016]
             */
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            NumericUpDown numericUpDown = new NumericUpDown();

            form.Text = title;
            label.Text = promptText;
            numericUpDown.Value = value;
            numericUpDown.Minimum = -360;
            numericUpDown.Maximum = 360;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            numericUpDown.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            numericUpDown.Anchor = numericUpDown.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new Control[] { label, numericUpDown, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();
            value = Convert.ToInt16(numericUpDown.Value);
            return dialogResult;
        }

        private void tsbtnSaveSegmentResults_Click(object sender, EventArgs e)
        {
            if (TextImageSegments.RefinedWords.Count > 0 && TextImageSegments.CharacterSegments.Count > 0)
            {
                try
                {
                    txtSegmentProgress.Text = "Saving word segment images...";
                    SaveWordSegments();
                    txtSegmentProgress.Text = "Saving character segment images...";
                    SaveCharacterSegments();

                    MessageBox.Show(null, "Saved successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(null, "Saved with errors." + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(null, "No data to save. Perform Segmentation before saving.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsbtnLayoutAnalysis_Click(object sender, EventArgs e)
        {
            LibLayout.StartLayoutDetection((Bitmap)inputPicBox.Image, 0.5F, 0.5F);
            return;


            //inputPicBox.Image = ImageLab.HorizontalBlurring((Bitmap)inputPicBox.Image,3);
            Bitmap bm = (Bitmap)inputPicBox.Image;

            // noise removal; removes 3x3 black pixels block
            // inputPicBox.Image = BorderNoiseRemoval.RemoveBorderNoise(bm);

            int blockDimension = 8;
            //List<List<int>> whiteBoxRecs = new LayoutAnalyzer().GetWhitespaceRectangles(bm);
            List<List<Rectangle>> whiteBoxRecs = new LayoutAnalyzer().GetWhitespaceRectangles(bm);

            /*
            int recCount1 = 1;
            Pen redPenTool1 = new Pen(Color.Red, 1);
            Brush penTool1 = Brushes.Red;

            Bitmap blockSeparatedImage1 = bm;
            using (var graphics = Graphics.FromImage(blockSeparatedImage1))
            {
                foreach (List<Rectangle> rList in whiteBoxRecs)
                {
                    //if(recCount > finalRects[0].Count){
                    //    recCount = 1;
                    //    penTool = Brushes.Brown;
                    //}

                    foreach (Rectangle r in rList)
                    {
                        //graphics.FillRectangle(penTool, r);
                        graphics.FillRectangle(penTool1, r);
                        //graphics.DrawString(recCount.ToString(), font1, penTool, r);
                        recCount1++;
                    }
                }
            }

            return;
            */
            //List<List<Point>> clusterBoundingPolygons =new LayoutAnalyzer().DissectIntoRectangularBlocks(bm);
            // Divide image into 8x8 matrices
            int numOfRows = (int)Math.Ceiling((decimal)(bm.Height / blockDimension));
            int numOfColumns = (int)Math.Ceiling((decimal)(bm.Width / blockDimension));

            // Scale image to make it perfectly divisible into square grids
            bm = ImageLab.ScaleBitmap(bm, new Size(numOfColumns * blockDimension, numOfRows * blockDimension));

            Bitmap blockSeparatedImage = bm;
            Pen redPenTool = new Pen(Color.Red, 1);

            List<Rectangle> sortedRecsVert = whiteBoxRecs[0];
            List<Rectangle> sortedRecsHori = whiteBoxRecs[1];
            List<List<Rectangle>> finalRects = new List<List<Rectangle>>();

            #region <-- Remove those white space rectangle with low possibility of being block separator -->
            // If any rectangle crosses only no rectangles or, one rectangle 
            // provided that is does not touch image coundaries has less possibility of being section separator
            /*int verRecCount = sortedRecsVert.Count - 1;
            for (int verRecNo = verRecCount; verRecNo >=0; verRecNo--)
            {
                int crossNum = 0;
                // check if any vertical space rectangle crosses two or more horizontal white space rectangles or not
                for (int horRecNo = sortedRecsHori.Count - 1; horRecNo >= 0; horRecNo--)
                {
                    // check for crossing
                    if (sortedRecsVert[verRecNo].IntersectsWith(sortedRecsHori[horRecNo]))
                    {
                        crossNum++;
                    }
                }

                // If crossNum is less than 2, check if it touches image border or not
                if (crossNum < 2)
                {
                    // Check for image boundary touch
                    if (!(sortedRecsVert[verRecNo].Y == 0 || sortedRecsVert[verRecNo].Y == bm.Height))
                    {
                        sortedRecsVert.RemoveAt(verRecNo);
                    }
                }
            }*/
            #endregion

            #region <-- Remove those white space rectangle with low possibility of being block separator -->
            // If any rectangle crosses only no rectangles or, one rectangle 
            // provided that is does not touch image coundaries has less possibility of being section separator
            /*int horRecCount = sortedRecsHori.Count - 1;
            for (int horRecNo = horRecCount; horRecNo >= 0; horRecNo--)
            {
                int crossNum = 0;
                // check if any vertical space rectangle crosses two or more horizontal white space rectangles or not
                for (int verRecNo = sortedRecsVert.Count - 1; verRecNo >= 0; verRecNo--)
                {
                    // check for crossing
                    if (sortedRecsHori[horRecNo].IntersectsWith(sortedRecsVert[verRecNo]))
                    {
                        crossNum++;
                    }
                }

                // If crossNum is less than 2, check if it touches image border or not
                if (crossNum < 2)
                {
                    // Check for image boundary touch
                    if (!(sortedRecsHori[horRecNo].Y == 0 || sortedRecsHori[horRecNo].Y == bm.Height))
                    {
                        sortedRecsHori.RemoveAt(horRecNo);
                    }
                }
            }*/
            #endregion

            #region<-- Find white space rectangles that separates whole document into complete columns -->
            // Remove all blocks that has less height than image
            for (int x = sortedRecsVert.Count - 1; x >=0; x--)
            {
                if(sortedRecsVert[x].Height < bm.Height)
                {
                    sortedRecsVert.RemoveAt(x);
                }
            }

            List<Rectangle> mainColumns = new List<Rectangle>();
            for (int x = 0; x <= sortedRecsVert.Count - 1;x++)
            {
                Rectangle left = sortedRecsVert[x];
                Rectangle right = new Rectangle();

                if (x+1 >= sortedRecsVert.Count)
                {
                    right = new Rectangle(bm.Width - 1, 0, 1, bm.Height);
                }
                else
                {
                    right = sortedRecsVert[x + 1];
                }

                int xCor = left.Right;
                int yCor = 0;
                int width = right.Left - xCor;

                mainColumns.Add(new Rectangle(xCor, yCor, width, bm.Height));
            }

            // Foreach mainColumns find horizontal text regions
            foreach(Rectangle r in mainColumns)
            {
                // Find white space rectangles with width equal to width of column
                // Get Horizontal blocks


                // Find vertical blocks in each horizontal blocks


            }
            
            // Draw columns
            using (var graphics = Graphics.FromImage(blockSeparatedImage))
            {
                foreach (Rectangle r in mainColumns)
                {
                    if(r.Width <= 0)
                    {
                        continue;
                    }

                    Bitmap bmpImage = new Bitmap(bm);
                    Bitmap nb = bmpImage.Clone(r, bmpImage.PixelFormat);

                    Rectangle rec = ImageLab.SurroundingWhitePixelsRemovedRectangle(nb);
                    graphics.DrawRectangle(redPenTool, new Rectangle(r.X + rec.X, r.Y+rec.Y, rec.Width, rec.Height));
                }
            }

            inputPicBox.Image = blockSeparatedImage;

            return;
            #endregion

            #region<-- Find white space rectangles that separates whole document into complete horizontal blocks -->
            // Remove all blocks that has less width than image
            for (int x = sortedRecsHori.Count - 1; x >= 0; x--)
            {
                if (sortedRecsHori[x].Width < bm.Width)
                {
                    sortedRecsHori.RemoveAt(x);
                }
            }

            #endregion

            finalRects.Add(sortedRecsVert);
            finalRects.Add(sortedRecsHori);

            int recCount = 1;
            Brush penTool = Brushes.Red;

            using (Font font1 = new Font("Arial", 8, FontStyle.Bold, GraphicsUnit.Point))
            {
                using (var graphics = Graphics.FromImage(blockSeparatedImage))
                {
                    foreach (List<Rectangle> rList in finalRects)
                    {
                        //if(recCount > finalRects[0].Count){
                        //    recCount = 1;
                        //    penTool = Brushes.Brown;
                        //}

                        foreach (Rectangle r in rList){
                            //graphics.FillRectangle(penTool, r);
                            graphics.DrawRectangle(redPenTool, r);
                            //graphics.DrawString(recCount.ToString(), font1, penTool, r);
                            recCount++;
                        }
                    }
                }
            }

            //using (var graphics = Graphics.FromImage(blockSeparatedImage))
            //{
            //    foreach (List<Point> polygon in clusterBoundingPolygons)
            //    {
            //        graphics.DrawPolygon(redPenTool, polygon.ToArray());
            //        //Point topPoint = polygon[0];
            //        //Point bottomPoint = polygon[0];
            //        //foreach(Point p in polygon)
            //        //{
            //        //    if(topPoint.X > p.X || topPoint.Y > p.Y)
            //        //    {
            //        //        topPoint = p;
            //        //    }

            //        //    if (bottomPoint.X < p.X || bottomPoint.Y < p.Y)
            //        //    {
            //        //        bottomPoint = p;
            //        //    }
            //        //}

            //        //Rectangle r = new Rectangle(topPoint.X, topPoint.Y, bottomPoint.X - topPoint.X, bottomPoint.Y - topPoint.Y);
            //        //graphics.DrawRectangle(redPenTool, r);
            //    }
            //}

            for (int x = 0; x < blockSeparatedImage.Width; x++)
            {
                for(int y = 0; y < blockSeparatedImage.Height; y++)
                {
                    Color color = blockSeparatedImage.GetPixel(x, y);
                    //Console.Out.WriteLine(color.ToString());
                    if(! (color.R.ToString() == "255" && color.G.ToString() == "0" && color.B.ToString() == "0"))
                    { 
                        blockSeparatedImage.SetPixel(x, y, Color.Black);
                    }
                }
            }


            inputPicBox.Image = blockSeparatedImage;

            return;

            // Find the horizontal histogram on document
            List<int> horizontalHistogram = new Histogram().RowPixelCount((Bitmap)inputPicBox.Image);

            // Locate all the minima in the histogram
            List<int> vallies = new Histogram().LocateMinimaInTheHistogram(horizontalHistogram, 20);

            List<int> clusterBorders = new Histogram().ClusterHistogram(vallies);

            #region // DRAW RECTANGLE BOXES .. SURROUNDING CHAR SEGMENT
            Bitmap charRecImg = (Bitmap)inputPicBox.Image;

            Pen redPen = new Pen(Color.Red, 1);
            Pen bluePen = new Pen(Color.Blue, 1);
            //MessageBox.Show("Number of words: " + wordlist.Count.ToString());

            int counter = 1;
            using (var graphics = Graphics.FromImage(charRecImg))
            {
                for (int j=0;j<clusterBorders.Count-1;j++)
                {
                    if (counter % 2 != 0)
                        graphics.DrawRectangle(redPen, new Rectangle(0,clusterBorders[j],charRecImg.Width, clusterBorders[j+1]- clusterBorders[j]));
                    else
                        graphics.DrawRectangle(redPen, new Rectangle(0, clusterBorders[j], charRecImg.Width, clusterBorders[j + 1] - clusterBorders[j]));

                    counter++;
                }
            }

            inputPicBox.Image = charRecImg;
            #endregion  // DRAW RECTANGLE BOXES .. SURROUNDING CHAR SEGMENT

        }

        const string WIA_DEVICE_PROPERTY_PAGES_ID = "3096";
        const string WIA_HORIZONTAL_SCAN_RESOLUTION_DPI = "6147";
        const string WIA_VERTICAL_SCAN_RESOLUTION_DPI = "6148";
        const string WIA_HORIZONTAL_SCAN_START_PIXEL = "6149";
        const string WIA_VERTICAL_SCAN_START_PIXEL = "6150";
        const string WIA_HORIZONTAL_SCAN_SIZE_PIXELS = "6151";
        const string WIA_VERTICAL_SCAN_SIZE_PIXELS = "6152";
        const string WIA_SCAN_BRIGHTNESS_PERCENTS = "6154";
        const string WIA_SCAN_CONTRAST_PERCENTS = "6155";
        const int widthA4at300dpi = 2480;
        const int heightA4at300dpi = 3508;

        private void btnScan_Click(object sender, EventArgs e)
        {
            // close all document before scanning
            // get selected cell position
            if (dgvPages.Rows.Count > 0)
            {
                // clear image in picture box
                if (inputPicBox.Image != null)
                {
                    inputPicBox.Image.Dispose();
                    inputPicBox.Image = null;
                }

                inputPicBox.Visible = false;
                // remove file from list and gridview
                _files.Clear();
                dgvPages.Rows.Clear();

                // clear recognized text box
                rtbOCRedText.Clear();
            }

            // call scan document function
            ScanDocument();
        }

        /// <summary>
        /// function to scan document using scanner
        /// </summary>
        public void ScanDocument()
        {
            scannedImage = null;
            try
            {
                // Save the image
                //string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Data\scan.tif");
                // temporary file directory inside user's home directory
                String userHomeDir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                // generate name
                DirectoryInfo taskDirectory = new DirectoryInfo(userHomeDir + "\\.neocr\\docs\\");
                FileInfo[] taskFiles = taskDirectory.GetFiles("scan_*.tif");
                List<int> nums = new List<int>();
                foreach (FileInfo fileInfo in taskFiles)
                {
                    String[] splits = fileInfo.Name.Split('_');
                    nums.Add(Convert.ToInt16(splits[splits.Length - 1].Replace(".tif","")));
                }
                
                // get max counter
                int maxCounter = 0;
                if(nums.Count > 0)
                {
                    maxCounter = nums.Max();    // geet maximum of number list
                }
                
                // create file path
                String path = userHomeDir + "\\.neocr\\docs\\scan_" + (maxCounter + 1).ToString("D4") + ".tif";
                // load scanner wizard
                new ScanWizard().ShowDialog();

                // delete existing image
                if (File.Exists(path) && GlobalVariables.SCAN_STATUS == true)
                {
                    // add scanned file path to list
                    if (_files != null && _files.Count > 0)
                    {
                        _files.Insert(0, path);
                    }
                    else
                    {
                        _files = new List<string>();
                        _files.Add(path);
                    }
                    // reload images in gridview    
                    LoadImages();   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(null, ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private static void SetA4(WIA.IProperties properties, int dpi)
        {
            int width = (int)((widthA4at300dpi / 300.0) * dpi);
            int height = (int)((heightA4at300dpi / 300.0) * dpi);

            SetWIAProperty(properties, WIA_HORIZONTAL_SCAN_RESOLUTION_DPI, dpi);
            SetWIAProperty(properties, WIA_VERTICAL_SCAN_RESOLUTION_DPI, dpi);
            SetWIAProperty(properties, WIA_HORIZONTAL_SCAN_START_PIXEL, 0);
            SetWIAProperty(properties, WIA_VERTICAL_SCAN_START_PIXEL, 0);
            SetWIAProperty(properties, WIA_HORIZONTAL_SCAN_SIZE_PIXELS, width);
            SetWIAProperty(properties, WIA_VERTICAL_SCAN_SIZE_PIXELS, height);
        }

        private static void SetWIAProperty(WIA.IProperties properties, object propName, object propValue)
        {
            WIA.Property prop = properties.get_Item(ref propName);
            prop.set_Value(ref propValue);
        }

        private static void SaveImageToPNGFile(ImageFile image, string fileName)
        {
            ImageProcess imgProcess = new ImageProcess();
            object convertFilter = "Convert";
            string convertFilterID = imgProcess.FilterInfos.get_Item(ref convertFilter).FilterID;
            imgProcess.Filters.Add(convertFilterID, 0);
            SetWIAProperty(imgProcess.Filters[imgProcess.Filters.Count].Properties, "FormatID", WIA.FormatID.wiaFormatPNG);
            image = imgProcess.Apply(image);
            image.SaveFile(fileName);
        }

        private void tsbtnManuallyThresholdImage_Click(object sender, EventArgs e)
        {
            ThresholdImageManually();
        }

        private void ThresholdImageManually()
        {
            //Get image from PictureBox
            if (inputPicBox.Image != null)
            {
                Bitmap bm = ImageLab.IterativeThreshold((Bitmap)inputPicBox.Image);

                inputPicBox.Image = bm;

                InputImage.Changed = bm;
                InputImage.ProcessHistory.Add(bm);
            }
            else
            {
                MessageBox.Show("Image not Selected");
            }
        }

        private void tsbtnGrayscaleImage_Click(object sender, EventArgs e)
        {
            GrayscaleImage();
        }

        public void GrayscaleImage()
        {
            if (inputPicBox.Image != null)
            {
                // use Otsu method to grayscale
                OtsuMethod ot = new OtsuMethod();
                Bitmap temp = ot.MakeGrayscale((Bitmap)inputPicBox.Image);
                //Bitmap temp = ot.MakeGrayscale(orgImg);
                inputPicBox.Image = temp;
                InputImage.Changed = temp;
                InputImage.ProcessHistory.Add(temp);
            }
            else
            {
                MessageBox.Show(null, "Image not selected.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void backgroundWorkerPDFToImage_DoWork(object sender, DoWorkEventArgs e)
        {
            // call pdf to image acquisition process
            PDFToImageAcquisition(_pdf_file, 0, 0);
        }

        public void PDFToImageAcquisition(String file, int start_page, int end_page)
        {
            // path to pdf to image converter
            string batDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\pdftoimage";
            string batDir = string.Format(@batDirPath.Replace("file:\\", ""));

            // temporary file directory inside user's home directory
            String userHomeDir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            String tempDir = userHomeDir + "\\.neocr\\docs";

            #region -- perform pre-processing --
            // generate temporary image file path
            String tempFilePath = tempDir + "\\neocr-" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + Path.GetExtension(file);
            if (File.Exists(tempFilePath)) { File.Delete(tempFilePath); }
            //save file as temp file
            File.Copy(file, tempFilePath);
            #endregion

            // set arguments 
            String arguments = "-Xms128m -Xmx1024m -jar pdftoimage_01.jar";
            if(file != null)
            {
                arguments = "-Xms128m -Xmx1024m -jar pdftoimage_01.jar \"" + tempFilePath + "\"" + " \"" + tempDir + "\"";
            }

            // create image acquisition process
            var proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    WorkingDirectory = batDir,
                    FileName = "javaw ",
                    Arguments = arguments,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };

            // run acquisition process
            proc.Start();
            while (!proc.StandardOutput.EndOfStream)
            {
                string line = proc.StandardOutput.ReadLine();
                // do something with line
                UpdateProgress(line);

                // set progress in Alert form title
                alert.DisplayOCRProgress(line);
            }

            // Close process by sending a close message to its main window.
            // proc.CloseMainWindow();
            // Free resources associated with process.
            proc.Dispose();
            proc.Close();
            // PDF Acquisition END

            //delete temp file
            if (File.Exists(tempFilePath)) {
                File.Delete(tempFilePath);  // delete temporary file
            }
        }

        // usage on txtSegmentProgress
        public void UpdateProgress(String text)
        {
            //Check if invoke requied if so return - as i will be recalled in correct thread
            if (ControlInvokeRequired(txtSegmentProgress, () => UpdateProgress(text))) return;
            txtSegmentProgress.Text = text;
            // Set focus to the control, if it can receive focus.
            //txtSegmentProgress.Focus();
            //if(txtSegmentProgress.SelectionLength > 0)
            //{
                txtSegmentProgress.DeselectAll();
                txtSegmentProgress.SelectAll();
            //}
        }

        /// <summary>
        /// Helper method to determin if invoke required, if so will rerun method on correct thread.
        /// if not do nothing.
        /// </summary>
        /// <param name="c">Control that might require invoking</param>
        /// <param name="a">action to preform on control thread if so.</param>
        /// <returns>true if invoke required</returns>
        public bool ControlInvokeRequired(Control c, Action a)
        {
            if (c.InvokeRequired) c.Invoke(new MethodInvoker(delegate { a(); }));
            else return false;

            return true;
        }

        /// <summary>
        /// Performs the actual DataGridView image loading with image resizing
        /// </summary>
        private void LoadImages()
        {
            try
            {
                if (_files == null)
                {
                    return;
                }

                // clear rows and columns
                dgvPages.Rows.Clear();
                dgvPages.Columns.Clear();

                // initialize variables
                int numColumnsForWidth = 1;
                int numImagesRequired = _files.Count;
                int numRows = numImagesRequired;

                // add column to datagridview
                DataGridViewImageColumn dataGridViewColumn = new DataGridViewImageColumn();
                dgvPages.Columns.Add(dataGridViewColumn);

                // Create the rows
                for (int index = 0; index < numRows; index++)
                {
                    dgvPages.Rows.Add();
                    dgvPages.Rows[index].Height = _imageSize + 20;
                }

                int columnIndex = 0;
                int rowIndex = 0;
                try
                {
                    for (int index = _currentStartImageIndex; index < _currentStartImageIndex + numImagesRequired; index++)
                    {
                        // Load the image from the file and add to the DataGridView
                        System.Drawing.Image image = Helper.ResizeImage(_files[index], _imageSize, _imageSize, false);
                        dgvPages.Rows[rowIndex].Cells[columnIndex].Value = image;
                        dgvPages.Rows[rowIndex].Cells[columnIndex].ToolTipText = Path.GetFileName(_files[index]);
                        dgvPages.Rows[rowIndex].Cells[columnIndex].Tag = _files[index];

                        /*
                        // set background color to gridview cells
                        DataGridViewCellStyle style = new DataGridViewCellStyle();
                        style.BackColor = Color.FromArgb(Color.DarkGray.ToArgb());
                        style.ForeColor = Color.Black;
                        dgvPages.Rows[rowIndex].Cells[columnIndex].Style = style;
                        */

                        // Have we reached the end column? if so then start on the next row
                        if (columnIndex == numColumnsForWidth - 1)
                        {
                            rowIndex++;
                            columnIndex = 0;
                        }
                        else
                        {
                            columnIndex++;
                        }
                    }

                    // Perform OCR Automatically
                    #region -- call OCR --
                    // process all pages
                    if (RecognizeDocumentOnLoad)
                    {
                        RunMultipageOCR(false);
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    Console.Out.WriteLine(ex.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// Performs the actual DataGridView image loading with image resizing
        /// </summary>
        private void ImagesListInPreviewPane()
        {
            try
            {
                if (_files == null)
                {
                    return;
                }

                // clear rows and columns
                dgvPages.Rows.Clear();
                dgvPages.Columns.Clear();

                // initialize variables
                int numColumnsForWidth = 1;
                int numImagesRequired = _files.Count;
                int numRows = numImagesRequired;

                // add column to datagridview
                DataGridViewColumn dataGridViewColumn = new DataGridViewColumn();
                DataGridViewCell dataGridViewCell = new DataGridViewTextBoxCell();
                dataGridViewColumn.CellTemplate = dataGridViewCell;
                dgvPages.Columns.Add(dataGridViewColumn);

                // Create the rows
                for (int index = 0; index < numRows; index++)
                {
                    dgvPages.Rows.Add(index + 1);
                    dgvPages.Rows[index].Height = 20;
                }

                int columnIndex = 0;
                int rowIndex = 0;

                for (int index = _currentStartImageIndex; index < _currentStartImageIndex + numImagesRequired; index++)
                {
                    // Load the image from the file and add to the DataGridView
                    dgvPages.Rows[rowIndex].Cells[columnIndex].Value = Path.GetFileName(_files[index]);
                    dgvPages.Rows[rowIndex].Cells[columnIndex].ToolTipText = Path.GetFileName(_files[index]);
                    dgvPages.Rows[rowIndex].Cells[columnIndex].Tag = _files[index];

                    // Have we reached the end column? if so then start on the next row
                    if (columnIndex == numColumnsForWidth - 1)
                    {
                        rowIndex++;
                        columnIndex = 0;
                    }
                    else
                    {
                        columnIndex++;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                for(int x= dgvPages.Rows.Count - 1; x > 0; x--)
                {
                    if(dgvPages.Rows[x].Cells[0].Value == null)
                    {
                        dgvPages.Rows.RemoveAt(x);
                    }
                }
            }
        }

        private void backgroundWorkerPDFToImage_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            /*
            // Load images to preview pane
            // load images to preview datagridview
            // get list of files inside temp acquisition directory
            string batDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\pdftoimage\\docs";
            string batDir = string.Format(@batDirPath.Replace("file:\\", ""));
            */

            // check pdf image acquition process status
            if (backgroundWorkerPDFToImage.IsBusy != true)
            {
                // close alert form
                alert.Close();
            }

            // temporary file directory inside user's home directory
            String userHomeDir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            String batDir = userHomeDir + "\\.neocr\\docs";

            // get all files and order by creation date
            DirectoryInfo DirInfo = new DirectoryInfo(@batDir);
            var filesInOrder = from f in DirInfo.EnumerateFiles()
                               orderby f.CreationTime
                               select f;
            // create list of filenames
            _files = new List<string>();
            foreach (var item in filesInOrder)
            {
                _files.Add(item.FullName);
            }
            // call load images functions
            // LoadImages();

            // show first image in canvas 
            try
            {
                // set first image in picture box
                InputImage.Original = System.Drawing.Image.FromFile(_files[0], true);
                //set value to gobal bitmap variables orgImg and Img
                InputImage.Changed = System.Drawing.Image.FromFile(_files[0], true);
                inputPicBox.Image = InputImage.Original;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
            }

            _pdf_file = null;
            // END
        }

        private void tsbtnOCR_Click(object sender, EventArgs e)
        {
            rtbOCRedText.Clear();
            #region <!-- tesseract.net3.05 sample -->
            /*
            // run ocr process
            var testImagePath = "D:\\Devanagari Text Images\\Nepali Text Images\\himlab_2 -Dika Fragmented.jpg";
            try
            {
                Console.Out.WriteLine("X");
                using (var engine = new TesseractEngine(@"./tesseract/tessdata", "nep", EngineMode.Default))
                {
                    Console.Out.WriteLine("A");
                    using (var img = Pix.LoadFromFile(testImagePath))
                    {
                        Console.Out.WriteLine("B");
                        using (var page = engine.Process(img))
                        {
                            Console.Out.WriteLine("C");
                            var text = page.GetText();
                            Console.WriteLine("Mean confidence: {0}", page.GetMeanConfidence());
                            Console.WriteLine("Text (GetText): \r\n{0}", text);
                            Console.WriteLine("Text (iterator):");

                            using (var iter = page.GetIterator())
                            {
                                iter.Begin();
                                do
                                {
                                    do
                                    {
                                        do
                                        {
                                            do
                                            {
                                                if (iter.IsAtBeginningOf(PageIteratorLevel.Block))
                                                {
                                                    Console.WriteLine("<BLOCK>");
                                                }

                                                Console.Write(iter.GetText(PageIteratorLevel.Word));
                                                Console.Write(" ");

                                                if (iter.IsAtFinalOf(PageIteratorLevel.TextLine, PageIteratorLevel.Word))
                                                {
                                                    Console.WriteLine();
                                                }
                                            } while (iter.Next(PageIteratorLevel.TextLine, PageIteratorLevel.Word));

                                            if (iter.IsAtFinalOf(PageIteratorLevel.Para, PageIteratorLevel.TextLine))
                                            {
                                                Console.WriteLine();
                                            }
                                        } while (iter.Next(PageIteratorLevel.Para, PageIteratorLevel.TextLine));
                                    } while (iter.Next(PageIteratorLevel.Block, PageIteratorLevel.Para));
                                } while (iter.Next(PageIteratorLevel.Block));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(e.ToString());
                Console.WriteLine("Unexpected Error: " + ex.Message);
                Console.WriteLine("Details: ");
                Console.WriteLine(e.ToString());
            }

            //Console.Write("Press any key to continue . . . ");
            //Console.ReadKey(true);
            */
            #endregion

            // check for unsaved work
            if (rtbOCRedText.Text.Length > 0 && workSaved != true)
            {
                // confirmation dialog
                DialogResult dr = MessageBox.Show("You have unsaved work. Do you want to continue?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (dr == DialogResult.Yes)
                {
                    // run OCR Process
                    RunOCR();
                }
                else if (dr == DialogResult.No)
                {
                    //
                }
                //TODO: Stuff
            }
            else
            {
                // run OCR Process
                //RunOCR();
                ParserStart();
            }
            //ParserStart();
        }

        /// <summary>
        /// function to trigger OCR action
        /// </summary>
        public void RunOCR()
        {
            // Check if image is opened or not
            if (inputPicBox.Image == null)
            {
                MessageBox.Show(null, "Image not selected.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // get language
            _language = Regex.Match(cbTessOCRLanguages.Items[cbTessOCRLanguages.SelectedIndex].ToString(), @"\(([^)]*)\)").Groups[1].Value;
            // get ocr mode
            String ocr_mode = tscbOCRMode.Items[tscbOCRMode.SelectedIndex].ToString();
            if (ocr_mode.Equals("Plain Text"))
            {
                _ocr_mode = "";
            }
            else if (ocr_mode.Equals("Searchable PDF"))
            {
                _ocr_mode = "pdf";
            }
            /*
             *else if (ocr_mode.Equals("hOCR"))
            {
                _ocr_mode = "hocr";
            }
            */
            // clear OCRed Result text box
            rtbOCRedText.Clear();

            // run ocr process
            if (backgroundWorkerOCRUsingTesseract.IsBusy != true)
            {
                // create a new instance of the alert form
                alert = new AlertForm();
                alert.StartPosition = FormStartPosition.CenterScreen;//CenterParent
                alert.Show(this);;//.Show(this);
                alert.IndeterminateProgressBar = true;
                //this.ActiveControl = alert;

                // Start the asynchronous operation.
                backgroundWorkerOCRUsingTesseract.RunWorkerAsync();

                // wait until process is busy
                while (backgroundWorkerOCRUsingTesseract.IsBusy)
                    Application.DoEvents();
            }

            //backgroundWorkerOCRUsingTesseract.RunWorkerAsync();
            //END

            // focus on ocred text pane
            this.ActiveControl = rtbOCRedText;
        }

        /// <summary>
        /// function to perform OCR using tesseract 
        /// </summary>
        public void OCRUsingTesseract(String file_path, String language)
        {
            // update progress
            UpdateProgress("Recognizing...");
            
            // set tesseract directory path
            string tessractDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\tesseract";
            tessractDirPath = string.Format(tessractDirPath.Replace("file:\\", ""));
            
            // if file_path is not provided, set selected file from image preview pane
            if(file_path == null)   
            {
                // get selected cell position
                int selectedRow = 0;
                if (dgvPages.SelectedCells.Count > 0)
                {
                    // save image in picturebox as temporary file
                    selectedRow = dgvPages.SelectedCells[0].RowIndex;
                    //file_path = Path.GetTempPath() + Path.GetFileName(_files[selectedRow]);//_files[selectedRow];
                    String milliseconds = DateTimeOffset.Now.Millisecond.ToString();
                    file_path = Path.Combine(Path.GetTempPath(),  Path.GetFileName("ocr_doc_" + milliseconds + Path.GetExtension(_files[selectedRow])));

                    // Perform Pre-processing
                    Bitmap preprocessedImage = PreprocessImage((Bitmap)inputPicBox.Image);

                    // save image to temp path
                    preprocessedImage.Save(file_path);

                    // set progress
                    alert.DisplayOCRProgress("Recognizing... 5%");
                }
                else
                {
                    return;
                }
            }

            // get temp file path
            _out_file_path = Path.Combine(Path.GetTempPath(), Path.GetFileName(file_path));

            // set Nepali + English language mode
            if (language == "nep")
            {
                //language += "+eng";
            }

            String tessdataDirPath = tessractDirPath + "\\tessdata";
            // set arguments 
            String arguments = "\"" + file_path + "\" \"" + _out_file_path + "\" -l " + language + " --psm 1 --oem 1 hocr";

            // if pdf output mode change arguments accordingly
            if(_ocr_mode == "pdf")
            {
                arguments = "\"" + file_path + "\" \"" + _out_file_path + "\" -l " + language + " --psm 1 --oem 1 pdf";
            }

            // set progress
            alert.DisplayOCRProgress("Recognizing... (10%)");

            // Perform a time consuming operation and report progress.
            //backgroundWorkerOCRUsingTesseract.ReportProgress(5);

            // create image acquisition process
            var proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    WorkingDirectory = tessractDirPath,
                    FileName = tessractDirPath + "\\tesseract.exe",
                    Arguments = arguments,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };

            // run acquisition process
            proc.Start();

            // set progress
            alert.DisplayOCRProgress("Recognizing... (20%)");

            // Perform a time consuming operation and report progress.
            //backgroundWorkerOCRUsingTesseract.ReportProgress(50);
            int progress = 20;
            while (!proc.StandardOutput.EndOfStream)
            {
                string line = proc.StandardOutput.ReadLine();
                //Console.Out.WriteLine(line);
                UpdateProgress(line);
                // do something with line
                // set progress
                alert.DisplayOCRProgress("Recognizing... (" + (progress + 10) + "%)");
            }

            // Free resources associated with process.
            proc.Close();
            // Recognition END

            // set progress
            alert.DisplayOCRProgress("Recognizing... (80%)");
            // OCR process end
        }

        public Bitmap PreprocessImage(Bitmap bmp)
        {
            // use Otsu method to grayscale
            OtsuMethod ot = new OtsuMethod();
            Bitmap grayImage = ot.MakeGrayscale(bmp);

            HoughTransformationDeskew sk = new HoughTransformationDeskew(grayImage);
            //imageDeskew sk = new imageDeskew(orgImg);
            double skewangle = sk.GetSkewAngle();
            Bitmap deskewedImage = ImageLab.RotateImage(grayImage, -skewangle);

            return deskewedImage;
        }

        private void dgvPages_SelectionChanged(object sender, EventArgs e)
        {
            DisplayImage();
        }

        /// <summary>
        /// function to display selected image in picturebox
        /// </summary>
        public void DisplayImage()
        {
            // get selected cell position
            int selectedRow = 0;
            if (dgvPages.Rows.Count > 0 && dgvPages.SelectedCells.Count > 0)
            {
                selectedRow = dgvPages.SelectedCells[0].RowIndex;
                if(selectedRow < dgvPages.Rows.Count)
                {
                    // show first image in canvas 
                    try
                    {
                        // dispose images
                        if (InputImage.Original != null) { InputImage.Original.Dispose(); InputImage.Original = null; }
                        if (InputImage.Changed != null) { InputImage.Changed.Dispose(); InputImage.Changed = null; }

                        // set first image in picture box
                        InputImage.Original = System.Drawing.Image.FromFile(_files[selectedRow]);
                        //set value to gobal bitmap variables orgImg and Img
                        InputImage.Changed = System.Drawing.Image.FromFile(_files[selectedRow]);
                        // set image to picturebox
                        if(inputPicBox.Image != null) 
                        {
                            inputPicBox.Image.Dispose();    // dispose picture box before assigning new image
                            //inputPicBox.Image = null;
                        }

                        inputPicBox.Image = InputImage.Original;
                        if (inputPicBox.Visible == false)
                        {
                            inputPicBox.Visible = true;
                        }

                        // set default zoom level
                        PictureZoom.ZoomLevel = 10;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                    }
                }
            }
        }

        private void backgroundWorkerOCRUsingTesseract_DoWork(object sender, DoWorkEventArgs e)
        {
            // if screen reader is running play sound
            /*
            if (ScreenReader.IsRunning)
            {
                startSoundPlayer.PlayLooping();
            }
            */
            // run ocr process
            OCRUsingTesseract(null, _language);

            // Perform a time consuming operation and report progress.
            //backgroundWorkerOCRUsingTesseract.ReportProgress(100);
        }

        private void backgroundWorkerOCRUsingTesseract_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // set progress
            alert.DisplayOCRProgress("Recognizing... (90%)");

            // run ocr process
            if (backgroundWorkerOCRUsingTesseract.IsBusy != true)
            {
                // close alert form
                alert.Close();
            }

            _language = null;

            string tessractDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\tesseract";
            tessractDirPath = string.Format(tessractDirPath.Replace("file:\\", ""));

            // read all text from file and display
            try
            {
                // if ocr mode is plain text
                if (_ocr_mode == "")
                {
                    // Open the text file using a stream reader.
                    using (StreamReader sr = new StreamReader(_out_file_path + ".hocr"))
                    {
                        // Read the stream to a string, and write the string to the console.
                        String line = sr.ReadToEnd();
                        //DisplayOCRedText(line);
                        String text_only = GetTextFromHOCR(line);
                        DisplayOCRedText(text_only);
                    }

                    // delete file // free resources
                    File.Delete(_out_file_path + ".hocr");
                }

                // if ocr mode is searchable PDF
                if (_ocr_mode == "pdf")
                {
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                    saveFileDialog1.Title = "Save Searchable PDF";
                    saveFileDialog1.Filter = "PDF files (*.pdf)|*.pdf";
                    saveFileDialog1.FilterIndex = 1;
                    saveFileDialog1.RestoreDirectory = true;

                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        String saveFileName = saveFileDialog1.FileName;
                        File.Copy(_out_file_path + ".pdf", saveFileName);
                    }

                    // delete file // free resources
                    File.Delete(_out_file_path + ".txt");
                }

                /*
                // play finish sound
                if (ScreenReader.IsRunning)
                {
                    finishSoundPlayer.Play();
                }
                */

                // delete processed temp file 
                int selectedRow = 0;
                if (dgvPages.SelectedCells.Count > 0)
                {
                    // save image in picturebox as temporary file
                    selectedRow = dgvPages.SelectedCells[0].RowIndex;
                    String processedFile = Path.Combine(Path.GetTempPath(), Path.GetFileName(_files[selectedRow]));
                    if (File.Exists(processedFile))
                    {
                        File.Delete(processedFile);
                    }
                }

                // display success message
                UpdateProgress("Recognition succedeed.");
                // show success message box
                MessageBox.Show(null, "Recognition Completed.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                UpdateProgress("Some error occured. The file could not be read: \n" + ex.Message);
            }
        }

        // usage on txtSegmentProgress
        public void DisplayOCRedText(String text)
        {
            //Check if invoke requied if so return - as i will be recalled in correct thread
            if (ControlInvokeRequiredOCRedText(rtbOCRedText, () => DisplayOCRedText(text))) return;
            rtbOCRedText.Text = text;

            // set work saved status
            workSaved = false;
        }

        /// <summary>
        /// Helper method to determin if invoke required, if so will rerun method on correct thread.
        /// if not do nothing.
        /// </summary>
        /// <param name="c">Control that might require invoking</param>
        /// <param name="a">action to preform on control thread if so.</param>
        /// <returns>true if invoke required</returns>
        public bool ControlInvokeRequiredOCRedText(Control c, Action a)
        {
            if (c.InvokeRequired) c.Invoke(new MethodInvoker(delegate { a(); }));
            else return false;

            return true;
        }

        private void tsbtnClearText_Click(object sender, EventArgs e)
        {
            // check for unsaved work
            if (rtbOCRedText.Text.Length > 0 && workSaved != true)
            {
                // confirmation dialog
                DialogResult dr = MessageBox.Show("You have unsaved work. Do you want to continue?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (dr == DialogResult.Yes)
                {
                    // clear ocred text box
                    rtbOCRedText.Text = "";
                }
                else if (dr == DialogResult.No)
                {
                    //
                }
                //TODO: Stuff
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if(rtbOCRedText.Text.Trim().Length > 0)
            {
                Clipboard.SetText(rtbOCRedText.Text);
                MessageBox.Show(null, "Text copied to clipboard.", "Copy", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void tsbtnExportAsPlainText_Click(object sender, EventArgs e)
        {
            SaveOutputAsPlainText();
        }

        public void SaveOutputAsPlainText()
        {
            // check if text is available
            if (rtbOCRedText.Text.Trim().Length <= 0)
            {
                return;
            }
            // create save dialog box
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Title = "Save text";
            saveFileDialog1.CheckPathExists = true;
            saveFileDialog1.DefaultExt = "txt";
            saveFileDialog1.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                // Compose a string that consists of three lines.
                string lines = rtbOCRedText.Text;
                // Write the string to a file.
                System.IO.StreamWriter file = new System.IO.StreamWriter(saveFileDialog1.FileName);
                file.WriteLine(lines);
                file.Close();

                MessageBox.Show(null, "Text saved successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

                // set work saved status
                workSaved = true;
            }
        }

        private void tsbtnExportToWordDoc_Click(object sender, EventArgs e)
        {
            SaveOutputAsMSWordDocument();
        }

        /// <summary>
        /// function to save OCR output as MS Word Document
        /// </summary>
        public void SaveOutputAsMSWordDocument()
        {
            // check if text is available
            if(rtbOCRedText.Text.Trim().Length <= 0)
            {
                return;
            }
            // create save dialog box
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Title = "Save text";
            saveFileDialog1.CheckPathExists = true;
            saveFileDialog1.DefaultExt = "docx";
            saveFileDialog1.Filter = "Microsoft Word files (*.docx)|*.docx|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                // Compose a string that consists of three lines.
                string lines = rtbOCRedText.Text;

                // Write the string to a file.
                /**WRITE TO WORD FILE**/
                //Create an instance for word app
                Microsoft.Office.Interop.Word.Application winword = new Microsoft.Office.Interop.Word.Application();
                //Set status for word application is to be visible or not.
                winword.Visible = false;
                //Create a missing variable for missing value
                object missing = System.Reflection.Missing.Value;
                //Create a new document
                Microsoft.Office.Interop.Word.Document document = winword.Documents.Add(ref missing, ref missing, ref missing, ref missing);
                document.PageSetup.PaperSize = Microsoft.Office.Interop.Word.WdPaperSize.wdPaperA4;

                // Create a paragraph.
                Microsoft.Office.Interop.Word.Paragraph para = document.Paragraphs.Add(ref missing);
                para.Range.Text = lines;

                object filename = @saveFileDialog1.FileName;
                document.SaveAs(ref filename);
                document.Close(ref missing, ref missing, ref missing);
                document = null;
                winword.Quit(ref missing, ref missing, ref missing);
                winword = null;
                /**END**/

                // saved information dialog
                MessageBox.Show(null, "Microsoft Word File saved successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

                // set work saved status
                workSaved = true;
            }
        }

        private void tsbtnDeskewImage_Click(object sender, EventArgs e)
        {
            CorrectSkewness();
        }

        public void CorrectSkewness()
        {
            // Check if image is opened or not
            if (inputPicBox.Image == null)
            {
                MessageBox.Show(null, "Image not selected.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            HoughTransformationDeskew sk = new HoughTransformationDeskew((Bitmap)inputPicBox.Image);
            //imageDeskew sk = new imageDeskew(orgImg);
            double skewangle = sk.GetSkewAngle();
            Bitmap bmpOut = ImageLab.RotateImage((Bitmap)InputImage.Changed, -skewangle);
            inputPicBox.Image = bmpOut;

            //set value of variable Img
            InputImage.Changed = bmpOut;
            InputImage.ProcessHistory.Add(bmpOut);
        }

        private void tsbtnDetectLines_Click(object sender, EventArgs e)
        {
            Bitmap img = new TestOrientation().DoEverything((Bitmap)inputPicBox.Image);
            // your code here...
            inputPicBox.Image = img;
        }

        private void mnuOpenImage_Click(object sender, EventArgs e)
        {
            //this.openImage();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AboutBox().ShowDialog();
        }

        private void skewnessCorrectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CorrectSkewness();
        }

        private void grayscaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GrayscaleImage();
        }

        private void runOCRToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // check for unsaved work
            if (rtbOCRedText.Text.Length > 0 && workSaved != true)
            {
                // confirmation dialog
                DialogResult dr = MessageBox.Show("You have unsaved work. Do you want to continue?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (dr == DialogResult.Yes)
                {
                    // run OCR Process
                    RunOCR();
                }
                else if (dr == DialogResult.No)
                {
                    //
                }
                //TODO: Stuff
            }
            else
            {
                // run OCR Process
                RunOCR();
            }
        }

        private void scanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // close all document before scanning
            // get selected cell position
            if (dgvPages.Rows.Count > 0)
            {
                // clear image in picture box
                if (inputPicBox.Image != null)
                {
                    inputPicBox.Image.Dispose();
                    inputPicBox.Image = null;
                }

                inputPicBox.Visible = false;
                // remove file from list and gridview
                _files.Clear();
                dgvPages.Rows.Clear();

                // clear recognized text box
                rtbOCRedText.Clear();
            }

            // call scan document function
            ScanDocument();
        }

        private void dgvPages_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DisplayImage();
        }

        private void recognitionWizardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new RecognitionWizard().ShowDialog();
        }

        private void exportAsMicrosoftWordFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveOutputAsMSWordDocument();
        }

        private void exportAsPlainTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveOutputAsPlainText();
        }

        private void pageListPaneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // if disposed show
            if (splitContainer1.Panel1Collapsed)
            {
                splitContainer1.Panel1Collapsed = false;
                // Set the checkmark for the menuItemBlue menu item.
                pageListPaneToolStripMenuItem.Checked = true;
            }
            // if shown dispose
            else if (!splitContainer1.Panel1Collapsed)
            {
                splitContainer1.Panel1Collapsed = true;
                // Set the checkmark for the menuItemBlue menu item.
                pageListPaneToolStripMenuItem.Checked = false;
            }
        }

        private void imagePreviewPaneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // if disposed show
            if (splitContainer2.Panel1Collapsed)
            {
                splitContainer2.Panel1Collapsed = false;
                // Set the checkmark for the menuItemBlue menu item.
                imagePreviewPaneToolStripMenuItem.Checked = true;
            }
            // if shown dispose
            else if (!splitContainer2.Panel1Collapsed)
            {
                splitContainer2.Panel1Collapsed = true;
                // Set the checkmark for the menuItemBlue menu item.
                imagePreviewPaneToolStripMenuItem.Checked = false;
            }
        }

        private void rtbOCRedText_MouseDown(object sender, MouseEventArgs e)
        {
            // if right mouse click display popupmenu
            if (e.Button == MouseButtons.Right)
            {               
                Control control = (Control)sender;
                // Calculate the startPoint by using the PointToScreen 
                // method.
                var startPoint = control.PointToScreen(new Point(e.X, e.Y));
                contextMenuStripOCRedText.Show(startPoint);
            }
        }

        private void rtbOCRedText_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Apps)
            {
                Control control = (Control)sender;
                // Calculate the startPoint by using the PointToScreen 
                // method.
                var startPoint = control.PointToScreen(new Point(rtbOCRedText.Width / 4, rtbOCRedText.Height / 3));
                contextMenuStripOCRedText.Show(startPoint);
            }

            // paste text without formatting
            if (e.Control && e.KeyCode == Keys.V)
            {
                ((RichTextBox)sender).Paste(DataFormats.GetFormat("Text"));
                e.Handled = true;
            }
        }

        private void clearContentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // check for unsaved work
            if (rtbOCRedText.Text.Length > 0 && workSaved != true)
            {
                // confirmation dialog
                DialogResult dr = MessageBox.Show("You have unsaved work. Do you want to continue?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (dr == DialogResult.Yes)
                {
                    // clear ocred text box
                    rtbOCRedText.Text = "";
                }
                else if (dr == DialogResult.No)
                {
                    //
                }
                //TODO: Stuff
            }
        }

        private void tsbtnSpellCheck_Click(object sender, EventArgs e)
        {

        }

        private void copyToClipboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (rtbOCRedText.Text.Trim().Length > 0)
            {
                Clipboard.SetText(rtbOCRedText.Text);
                MessageBox.Show(null, "Text copied to clipboard.", "Copy", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void exportAsPlainTextToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SaveOutputAsPlainText();
        }

        private void exportAsMicrosoftWordDocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveOutputAsMSWordDocument();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (rtbOCRedText.SelectedText != null && rtbOCRedText.SelectedText != "")
            {
                Clipboard.SetText(rtbOCRedText.SelectedText);
            }
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // This call will select the entire contents of the RichTextBox.
            rtbOCRedText.SelectAll();
        }

        private void copyAllTextToClipboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (rtbOCRedText.Text.Trim().Length > 0)
            {
                Clipboard.SetText(rtbOCRedText.Text);
                MessageBox.Show(null, "Text copied to clipboard.", "Copy", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (rtbOCRedText.SelectedText != null && rtbOCRedText.SelectedText != "")
            {
                rtbOCRedText.Cut();
            }
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Clipboard.ContainsText())
            {
                int selstart = rtbOCRedText.SelectionStart;
                if (rtbOCRedText.SelectedText != null && rtbOCRedText.SelectedText != "")
                {
                    rtbOCRedText.Text = rtbOCRedText.Text.Remove(selstart, rtbOCRedText.SelectionLength);
                }

                string clip = Clipboard.GetText(TextDataFormat.Text).ToString();
                rtbOCRedText.Text = rtbOCRedText.Text.Insert(selstart, clip);
                rtbOCRedText.SelectionStart = selstart + clip.Length;
            }
        }

        private void correctSpellingsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private Color cbBorderColor = Color.Gray;
        private Pen cbBorderPen = new Pen(SystemColors.Window);
        private void toolStrip6_Paint(object sender, PaintEventArgs e)
        {
            /*
            foreach (var item in toolStrip6.Items)
            {
                var asComboBox = item as ToolStripComboBox;
                if (asComboBox != null)
                {
                    Rectangle r = new Rectangle(
                        asComboBox.ComboBox.Location.X - 1,
                        asComboBox.ComboBox.Location.Y - 1,
                        asComboBox.ComboBox.Size.Width + 1,
                        asComboBox.ComboBox.Size.Height + 1);

                    cbBorderPen.Color = cbBorderColor;
                    e.Graphics.DrawRectangle(cbBorderPen, r);
                }
            }
            */
        }

        private void tsbtnIncreaseFontSize_Click(object sender, EventArgs e)
        {
            // increase font size
            if (tscbFontSize.SelectedIndex < tscbFontSize.Items.Count - 1)
            {
                tscbFontSize.SelectedIndex = (tscbFontSize.SelectedIndex + 1);
                // change font size of rtbOCRedText
                // set font and font size on ocred text pane
                rtbOCRedText.Font = new Font(tscbFonts.SelectedItem.ToString(), Convert.ToInt16(tscbFontSize.SelectedItem.ToString()), FontStyle.Regular);
            }
        }

        private void tsbtnDecreaseFontSize_Click(object sender, EventArgs e)
        {
            // increase font size
            if (tscbFontSize.SelectedIndex > 1)
            {
                tscbFontSize.SelectedIndex = (tscbFontSize.SelectedIndex - 1);
                // change font size of rtbOCRedText
                // set font and font size on ocred text pane
                rtbOCRedText.Font = new Font(tscbFonts.SelectedItem.ToString(), Convert.ToInt16(tscbFontSize.SelectedItem.ToString()), FontStyle.Regular);
            }
        }

        private void tscbFontSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            // change font size of rtbOCRedText
            // set font and font size on ocred text pane
            if (tscbFontSize.SelectedItem != null && tscbFonts.SelectedItem != null)
            {
                rtbOCRedText.Font = new Font(tscbFonts.SelectedItem.ToString(), Convert.ToInt16(tscbFontSize.SelectedItem.ToString()), FontStyle.Regular);
            }
        }

        private void tscbFonts_SelectedIndexChanged(object sender, EventArgs e)
        {
            // change font size of rtbOCRedText
            // set font and font size on ocred text pane
            if(tscbFontSize.SelectedItem != null && tscbFonts.SelectedItem != null)
            {              
                rtbOCRedText.Font = new Font(tscbFonts.SelectedItem.ToString(), Convert.ToInt16(tscbFontSize.SelectedItem.ToString()), FontStyle.Regular);
            }
        }

        private void tsbtnReadImage_Click(object sender, EventArgs e)
        {
            //run ocr
            // check for unsaved work
            if (rtbOCRedText.Text.Length > 0 && workSaved != true)
            {
                // confirmation dialog
                DialogResult dr = MessageBox.Show("You have unsaved work. Do you want to continue?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (dr == DialogResult.Yes)
                {
                    // run OCR Process
                    RunOCR();
                }
                else if (dr == DialogResult.No)
                {
                    //
                }
                //TODO: Stuff
            }
            else
            {
                // run OCR Process
                RunOCR();
            }
        }

        private void tsbtnSaveText_Click(object sender, EventArgs e)
        {
            // save as ms word document
            SaveOutputAsMSWordDocument();
        }

        private void removeLinesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// function to trigger OCR multipage OCR action
        /// </summary>
        public void RunMultipageOCR(Boolean selectedPagesOnly)
        {
            // get language
            _language = Regex.Match(cbTessOCRLanguages.Items[cbTessOCRLanguages.SelectedIndex].ToString(), @"\(([^)]*)\)").Groups[1].Value;
            
            // set Nepali + English language mode
            if (_language == "nep")
            {
                //_language += "+eng";
            }

            // get ocr mode
            String ocr_mode = tscbOCRMode.Items[tscbOCRMode.SelectedIndex].ToString();
            if (ocr_mode.Equals("Plain Text"))
            {
                _ocr_mode = "";
            }
            else if (ocr_mode.Equals("Searchable PDF"))
            {
                _ocr_mode = "pdf";
            }
            /*
             *else if (ocr_mode.Equals("hOCR"))
            {
                _ocr_mode = "hocr";
            }
            */

            if (_files == null || _files.Count <= 0)
            {
                MessageBox.Show(null, "No files has found. Please browse files to recognize.", "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // run ocr process
            if (backgroundWorkerOCRAllPagesUsingTesseract.IsBusy != true)
            {
                //clear rtbOCRedText box
                rtbOCRedText.Clear();

                // create a new instance of the alert form
                alert = new AlertForm();
                alert.StartPosition = FormStartPosition.CenterScreen;//CenterParent
                alert.Show(this);;//.Show(this);
                alert.IndeterminateProgressBar = true;

                // Start the asynchronous operation.
                backgroundWorkerOCRAllPagesUsingTesseract.RunWorkerAsync();
            }

            // focus on ocred text pane
            this.ActiveControl = rtbOCRedText;
        }

        private void oCRAllPagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // check for unsaved work
            if (rtbOCRedText.Text.Length > 0 && workSaved != true)
            {
                // confirmation dialog
                DialogResult dr = MessageBox.Show("You have unsaved work. Do you want to continue?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (dr == DialogResult.Yes)
                {
                    // process all pages
                    RunMultipageOCR(false);
                }
                else if (dr == DialogResult.No)
                {
                    //
                }
                //TODO: Stuff
            }
            else
            {
                // process all pages
                RunMultipageOCR(false);
            }
        }

        private void backgroundWorkerOCRAllPagesUsingTesseract_DoWork(object sender, DoWorkEventArgs e)
        {
            // if screen reader is running play sound
            /*
            if (ScreenReader.IsRunning)
            {
                startSoundPlayer.PlayLooping();
            }
            */
            if (_files != null && _files.Count > 0)
            {
                _files_to_process = _files;
            }
            else
            {
                MessageBox.Show(null, "No files has been selected.", "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // run ocr process
            // OCRMultiPagesUsingTesseract(_language);
            if(_ocr_mode == "")
            {
                OCRMultiPagesUsingTesseractIBI(_language);
            }
            // if pdf output mode change arguments accordingly
            else if (_ocr_mode == "pdf")
            {
                OCRMultiPagesUsingTesseractPDF(_language);
            }
            //END
        }

        /// <summary>
        /// function to perform OCR multiple pages using tesseract 
        /// </summary>
        Process process;
        public void OCRMultiPagesUsingTesseract(String language)
        {   
            // recognized page counter
            recognizedPageCounter = 1;
            int totalPages = _files_to_process.Count;

            String ocrFileList = Path.GetTempPath() + "toocr-neocr.txt";
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@ocrFileList))
            {
                foreach (string file_path in _files_to_process)
                {
                    file.WriteLine(file_path);
                }
            }

            // recognize all pages
            // Perform a time consuming operation and report progress.
            //backgroundWorkerOCRAllPagesUsingTesseract.ReportProgress(5);
            //alert.Message = "Recognizing page " + pageCounter + " of " + totalPages;
              
            // set tesseract directory path
            string tessractDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\tesseract";
            tessractDirPath = string.Format(tessractDirPath.Replace("file:\\", ""));

            // get temp file path
            _out_file_path = Path.Combine(Path.GetTempPath(), "ocred-text-neocr");//Path.GetFileName(file_path);
            if (File.Exists(_out_file_path + ".txt")) { File.Delete(_out_file_path + ".txt"); }

            String tessdataDirPath = tessractDirPath + "\\tessdata";
            // set arguments 
            String arguments = "\"" + ocrFileList + "\" \"" + _out_file_path + "\" -l " + language + " --psm 1 --oem 1";

            // if pdf output mode change arguments accordingly
            if (_ocr_mode == "pdf")
            {
                arguments = "\"" + ocrFileList + "\" \"" + _out_file_path + "\" -l " + language + " --psm 1 --oem 1 pdf";
            }

            // create image recognition process
            var proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    WorkingDirectory = tessractDirPath,
                    FileName = tessractDirPath + "\\tesseract.exe",
                    Arguments = arguments,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                    CreateNoWindow = true
                }
            };

            proc.OutputDataReceived += new DataReceivedEventHandler(
                (s, e) =>
                {
                    Console.Out.WriteLine(e.Data);
                }
            );
            proc.ErrorDataReceived += new DataReceivedEventHandler((s, e) => {
                Console.Out.WriteLine(e.Data);
            });

            proc.Start();
            proc.BeginOutputReadLine();

            proc.WaitForExit();
            // Recognition END

            // Perform a time consuming operation and report progress.
            backgroundWorkerOCRAllPagesUsingTesseract.ReportProgress(100);

            // delete temporary image list file
            if (File.Exists(ocrFileList))
            {
                File.Delete(ocrFileList);
            }
        }

        /// <summary>
        /// function to perform OCR multiple pages using tesseract; Images one by one
        /// </summary>
        public void OCRMultiPagesUsingTesseractIBI1(String language)
        {
            // recognized page counter
            recognizedPageCounter = 1;  // recognized pages counter initialization
            int totalPages = _files_to_process.Count;   // total number of pages to ocr in single run
            String ocredText = "";  // string to store OCRed Text

            // recognize all pages
            // Perform a time consuming operation and report progress.
            //backgroundWorkerOCRAllPagesUsingTesseract.ReportProgress(5);

            // set tesseract directory path
            string tessractDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\tesseract";
            tessractDirPath = string.Format(tessractDirPath.Replace("file:\\", ""));

            // temporary file directory inside user's home directory
            String userHomeDir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            // get temp file path
            //_out_file_path = Path.GetTempPath() + "\\ocred-text-neocr";//Path.GetFileName(file_path);
            _out_file_path = userHomeDir + "\\.neocr\\docs\\ocred-text-neocr";

            if (File.Exists(_out_file_path + ".txt")) { File.Delete(_out_file_path + ".txt"); }

            int pageCounter = 1;
            int totalPagesCount = _files_to_process.Count;

            // recognize all selected images in single run
            foreach (String filePath in _files_to_process)
            {
                // ocr progress
                //alert.Message = "Recognizing page " + recognizedPageCounter + " of " + totalPages;
                UpdateProgress("Recognizing page " + recognizedPageCounter + " of " + totalPages);

                // set progress
                alert.DisplayOCRProgress("Recognizing Page... (" + pageCounter + " of " + totalPagesCount + ")");
                //alert.SetOCRProgress((int)(recognizedPageCounter / (float)totalPages * 100));

                #region -- perform pre-processing --
                // Perform Pre-processing
                // preprocess and save image temporarily
                Bitmap preproImage = (Bitmap)System.Drawing.Image.FromFile(filePath);
                Bitmap preprocessedImage = PreprocessImage(preproImage);


                // generate temporary image file path
                //String tempImgPath = Path.GetTempPath() + "neocr-"+ DateTime.Now.ToString("yyyyMMddHHmmssfff") + Path.GetExtension(filePath)

                String tempImgPath = userHomeDir + "\\.neocr\\docs\\" + "neocr-" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + Path.GetExtension(filePath);

                if (File.Exists(tempImgPath)) { File.Delete(tempImgPath); }
                //save image
                preprocessedImage.Save(tempImgPath);
                #endregion

                String tessdataDirPath = tessractDirPath + "\\tessdata";
                // set arguments // output file will be of format _out_file_path + "_rs"
                String arguments = "\"" + tempImgPath + "\" \"" + _out_file_path + "_rs\" -l " + language + " --psm 1 --oem 1";

                // create image recognition process
                var proc = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        WorkingDirectory = tessractDirPath,
                        FileName = tessractDirPath + "\\tesseract.exe",
                        Arguments = arguments,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        RedirectStandardInput = true,
                        CreateNoWindow = true
                    }
                };

                proc.OutputDataReceived += new DataReceivedEventHandler(
                    (s, e) =>
                    {
                        Console.Out.WriteLine(e.Data);
                    }
                );
                proc.ErrorDataReceived += new DataReceivedEventHandler((s, e) =>
                {
                    Console.Out.WriteLine(e.Data);
                });

                proc.Start();
                proc.BeginOutputReadLine();

                proc.WaitForExit();
                // Recognition END

                #region -- delete temporary image file --
                if (File.Exists(tempImgPath)) { File.Delete(tempImgPath); }
                #endregion

                // Perform a time consuming operation and report progress.
                //backgroundWorkerOCRAllPagesUsingTesseract.ReportProgress(100);

                // This text is always added, making the file longer over time
                // if it is not deleted.
                if (File.Exists(_out_file_path + "_rs.txt"))
                {
                    using (StreamWriter sw = File.AppendText(_out_file_path + ".txt"))
                    {
                        String text = File.ReadAllText(_out_file_path + "_rs.txt");   // read all content of files and append
                        sw.WriteLine(text);
                        //delete temporary file
                        File.Delete(_out_file_path + "_rs.txt");
                    }
                }

                // increase recognied pages counter
                recognizedPageCounter++;

                pageCounter++;
            }
        }

        /// <summary>
        /// function to perform OCR multiple pages using tesseract; Images one by one
        /// </summary>
        public void OCRMultiPagesUsingTesseractIBI(String language)
        {
            // recognized page counter
            recognizedPageCounter = 1;  // recognized pages counter initialization
            int totalPages = _files_to_process.Count;   // total number of pages to ocr in single run

            // recognize all pages
            // Perform a time consuming operation and report progress.
            //backgroundWorkerOCRAllPagesUsingTesseract.ReportProgress(5);

            // set tesseract directory path
            string tessractDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\tesseract";
            tessractDirPath = string.Format(tessractDirPath.Replace("file:\\", ""));

            // temporary file directory inside user's home directory
            String userHomeDir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            // get temp file path
            //_out_file_path = Path.GetTempPath() + "\\ocred-text-neocr";//Path.GetFileName(file_path);
            _out_file_path = userHomeDir + "\\.neocr\\docs\\ocred-text-neocr";

            if (File.Exists(_out_file_path + ".hocr")) { File.Delete(_out_file_path + ".hocr"); }

            int pageCounter = 1;
            int totalPagesCount = _files_to_process.Count;

            // recognize all selected images in single run
            foreach (String filePath in _files_to_process)
            {
                // ocr progress
                //alert.Message = "Recognizing page " + recognizedPageCounter + " of " + totalPages;
                UpdateProgress("Recognizing page " + recognizedPageCounter + " of " + totalPages);

                // set progress
                alert.DisplayOCRProgress("Recognizing Page... (" + pageCounter + " of " + totalPagesCount + ")");
                //alert.SetOCRProgress((int)(recognizedPageCounter / (float)totalPages * 100));

                #region -- perform pre-processing --
                // Perform Pre-processing
                // preprocess and save image temporarily
                Bitmap preproImage = (Bitmap)System.Drawing.Image.FromFile(filePath);
                Bitmap preprocessedImage = PreprocessImage(preproImage);

                // generate temporary image file path
                //String tempImgPath = Path.GetTempPath() + "neocr-"+ DateTime.Now.ToString("yyyyMMddHHmmssfff") + Path.GetExtension(filePath)

                String tempImgPath = userHomeDir + "\\.neocr\\docs\\" + "neocr-" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + Path.GetExtension(filePath);

                if (File.Exists(tempImgPath)) { File.Delete(tempImgPath); }
                //save image
                preprocessedImage.Save(tempImgPath);
                #endregion

                String tessdataDirPath = tessractDirPath + "\\tessdata";
                // set arguments // output file will be of format _out_file_path + "_rs"
                String arguments = "\"" + tempImgPath + "\" \"" + _out_file_path + "_rs\" -l " + language + " --psm 1 --oem 1 hocr";

                // create image recognition process
                var proc = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        WorkingDirectory = tessractDirPath,
                        FileName = tessractDirPath + "\\tesseract.exe",
                        Arguments = arguments,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        RedirectStandardInput = true,
                        CreateNoWindow = true
                    }
                };

                proc.OutputDataReceived += new DataReceivedEventHandler(
                    (s, e) =>
                    {
                        Console.Out.WriteLine(e.Data);
                    }
                );
                proc.ErrorDataReceived += new DataReceivedEventHandler((s, e) =>
                {
                    Console.Out.WriteLine(e.Data);
                });

                proc.Start();
                proc.BeginOutputReadLine();

                proc.WaitForExit();
                // Recognition END

                #region -- delete temporary image file --
                if (File.Exists(tempImgPath)) { File.Delete(tempImgPath); }
                #endregion

                // Perform a time consuming operation and report progress.
                //backgroundWorkerOCRAllPagesUsingTesseract.ReportProgress(100);

                // This text is always added, making the file longer over time
                // if it is not deleted.
                if (File.Exists(_out_file_path + "_rs.hocr"))
                {
                    using (StreamWriter sw = File.AppendText(_out_file_path + ".hocr"))
                    {
                        String text = File.ReadAllText(_out_file_path + "_rs.hocr");   // read all content of files and append
                        sw.WriteLine(text);
                        //delete temporary file
                        File.Delete(_out_file_path + "_rs.hocr");
                    }
                }

                // increase recognied pages counter
                recognizedPageCounter++;

                pageCounter++;
            }
        }

        /// <summary>
        /// function to perform OCR multiple pages using tesseract; creates single searchable PDF
        /// </summary>
        public void OCRMultiPagesUsingTesseractPDF(String language)
        {
            // write processing message
            UpdateProgress("Recognizing images to make searchable PDF...");

            // recognized page counter
            recognizedPageCounter = 1;
            int totalPages = _files_to_process.Count;

            String ocrFileList = Path.GetTempPath() + "toocr-neocr.txt";
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@ocrFileList))
            {
                foreach (string file_path in _files_to_process)
                {
                    file.WriteLine(file_path);
                }
            }

            // recognize all pages
            // Perform a time consuming operation and report progress.
            //backgroundWorkerOCRAllPagesUsingTesseract.ReportProgress(5);
            //alert.Message = "Recognizing page " + pageCounter + " of " + totalPages;

            // set tesseract directory path
            string tessractDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\tesseract";
            tessractDirPath = string.Format(tessractDirPath.Replace("file:\\", ""));

            // get temp file path
            _out_file_path = Path.GetTempPath() + "\\ocred-text-neocr";//Path.GetFileName(file_path);
            if (File.Exists(_out_file_path + ".txt")) { File.Delete(_out_file_path + ".txt"); }

            String tessdataDirPath = tessractDirPath + "\\tessdata";
            // set arguments 
            String arguments = "\"" + ocrFileList + "\" \"" + _out_file_path + "\" -l " + language + " --psm 1 --oem 1 pdf";

            // create image recognition process
            var proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    WorkingDirectory = tessractDirPath,
                    FileName = tessractDirPath + "\\tesseract.exe",
                    Arguments = arguments,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                    CreateNoWindow = true
                }
            };

            proc.OutputDataReceived += new DataReceivedEventHandler(
                (s, e) =>
                {
                    Console.Out.WriteLine(e.Data);
                }
            );
            proc.ErrorDataReceived += new DataReceivedEventHandler((s, e) => {
                Console.Out.WriteLine(e.Data);
            });

            proc.Start();
            proc.BeginOutputReadLine();

            proc.WaitForExit();
            // Recognition END

            // Perform a time consuming operation and report progress.
            // backgroundWorkerOCRAllPagesUsingTesseract.ReportProgress(100);

            // delete temporary image list file
            if (File.Exists(ocrFileList))
            {
                File.Delete(ocrFileList);
            }
        }

        private class ReadOutput
        {
            private StreamReader _reader;
            private ManualResetEvent _complete;

            public ReadOutput(StreamReader reader, ManualResetEvent complete)
            {
                _reader = reader;
                _complete = complete;
                Thread t = new Thread(ReadAll);
                t.Start();
            }

            void ReadAll()
            {
                int ch;
                while (-1 != (ch = _reader.Read()))
                {
                    Console.Write((char)ch);
                }
                _complete.Set();
            }
        }

        private void backgroundWorkerOCRAllPagesUsingTesseract_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // check ocr process status
            if (backgroundWorkerOCRAllPagesUsingTesseract.IsBusy != true)
            {
                // close alert form
                alert.Close();
            }

            // read all text from file and display
            try
            {
                // if ocr mode is plain text
                if (_ocr_mode == "")
                {
                    // Open the text file using a stream reader.
                    using (StreamReader sr = new StreamReader(_out_file_path + ".hocr"))
                    {
                        // Read the stream to a string, and write the string to the console.
                        String line = sr.ReadToEnd();
                        //DisplayOCRedText(line);
                        // get text only from HOCR text
                        String text_only = GetTextFromHOCR(line);
                        DisplayOCRedText(text_only);
                    }

                    // delete file // free resources
                    File.Delete(_out_file_path + ".hocr");

                    // play finish sound
                    if (ScreenReader.IsRunning)
                    {
                        finishSoundPlayer.Play();
                    }

                    // write completion message
                    UpdateProgress("Recognition succeeded.");

                    // show success message box
                    MessageBox.Show(null, "Recognitin completed successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                // if ocr mode is searchable PDF
                if (_ocr_mode == "pdf")
                {
                    // write processing message
                    UpdateProgress("Saving searchable PDF...");

                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                    saveFileDialog1.Title = "Save Searchable PDF";
                    saveFileDialog1.Filter = "PDF files (*.pdf)|*.pdf";
                    saveFileDialog1.FilterIndex = 1;
                    saveFileDialog1.RestoreDirectory = true;

                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        String saveFileName = saveFileDialog1.FileName;
                        File.Copy(_out_file_path + ".pdf", saveFileName);
                    }

                    // delete file // free resources
                    File.Delete(_out_file_path + ".pdf");

                    // write processing message
                    UpdateProgress("Searchable PDF saved successfully...");

                    // play finish sound
                    if (ScreenReader.IsRunning)
                    {
                        finishSoundPlayer.Play();
                    }

                    // show success message box
                    MessageBox.Show(null, "Searchable PDF saved successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                UpdateProgress("Some error occured. The file could not be read: \n" + ex.Message);
            }
        }

        private void removeLineBreaksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //replace single occurences of \n 
            string result = Regex.Replace(rtbOCRedText.Text, @"\r\n?|\n(?!\n)", " ");

            // display resultant text
            rtbOCRedText.Text = result;
        }

        private void fontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FontDialog fontDialog1 = new FontDialog();
            
            // Show the dialog.
            DialogResult result = fontDialog1.ShowDialog();
            // See if OK was pressed.
            if (result == DialogResult.OK)
            {
                // Get Font.
                Font font = fontDialog1.Font;
                // Set TextBox properties.
                rtbOCRedText.Font = font;

                // set font and font size dropdown values
                tscbFonts.SelectedIndex = tscbFonts.Items.IndexOf(font.Name);
                //tscbFontSize.SelectedIndex = tscbFontSize.Items.IndexOf(Convert.ToInt16(font.Size.ToString()));
                tscbFontSize.Text = font.Size.ToString();
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // get selected cell position
            int selectedRow = 0;
            if (dgvPages.SelectedCells.Count > 0)
            {
                selectedRow = dgvPages.SelectedCells[0].RowIndex;
                // clear image in picture box
                if (inputPicBox.Image != null)
                {
                    inputPicBox.Image.Dispose();
                    inputPicBox.Image = null;
                }

                inputPicBox.Visible = false;

                /*
                try
                {
                    ////////////////
                    // Delete all temporary scanned documents
                    // get file name to close
                    String fileToClose = _files[selectedRow];
                    String[] fileSplits = fileToClose.Split('_');

                    String userHomeDir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                    // generate name
                    DirectoryInfo taskDirectory = new DirectoryInfo(userHomeDir + "\\.neocr\\docs\\");

                    // if scanned file
                    if (fileSplits[fileSplits.Length - 1].StartsWith("scan_") && Path.GetDirectoryName(fileToClose).Equals(@taskDirectory))
                    {
                        File.Delete(fileToClose);
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                */

                // remove file from list and gridview
                _files.RemoveAt(selectedRow);
                dgvPages.Rows.RemoveAt(selectedRow);
                // clear recognized text box
                rtbOCRedText.Clear();
            }
            else
            {
                return;
            }
        }

        private void closeAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // get selected cell position
            if (dgvPages.Rows.Count > 0)
            {
                // clear image in picture box
                if (inputPicBox.Image != null)
                {
                    inputPicBox.Image.Dispose();
                    inputPicBox.Image = null;
                }

                inputPicBox.Visible = false;

                // remove file from list and gridview
                _files.Clear();
                dgvPages.Rows.Clear();

                // clear recognized text box
                rtbOCRedText.Clear();

                /*
                try
                {
                    ////////////////
                    // Delete all temporary scanned documents
                    String userHomeDir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                    // generate name
                    DirectoryInfo taskDirectory = new DirectoryInfo(userHomeDir + "\\.neocr\\docs\\");
                    FileInfo[] taskFiles = taskDirectory.GetFiles("scan_*.tif");
                    List<int> nums = new List<int>();
                    foreach (FileInfo fileInfo in taskFiles)
                    {
                        fileInfo.Delete();
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                */
            }
            else
            {
                return;
            }
        }

        private void backgroundWorkerOCRUsingTesseract_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // Pass the progress to AlertForm label and progressbar
            alert.Message = "In progress, please wait... " + e.ProgressPercentage.ToString() + "%";
            alert.ProgressValue = e.ProgressPercentage;
        }

        private void backgroundWorkerOCRAllPagesUsingTesseract_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // Pass the progress to AlertForm label and progressbar
            alert.Message = "Recognizing page " + recognizedPageCounter + " of " + _files_to_process.Count;
            alert.ProgressValue = e.ProgressPercentage;
        }

        private void imagePropertiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvPages.SelectedCells.Count > 0)
            {
                // create image properties form object
                ImageProperties ip = new ImageProperties();
                // get selected file
                int selectedRow = dgvPages.SelectedCells[0].RowIndex;
                String fileName = _files[selectedRow];

                // get and set image properties
                System.Drawing.Image img = System.Drawing.Image.FromFile(fileName);
                //System.Drawing.Imaging.ImageFormat format = img.RawFormat;

                ip.ImageWidth = img.Width.ToString();
                ip.ImageHeight = img.Height.ToString();
                ip.XResolution = img.VerticalResolution.ToString();
                ip.YResolution = img.HorizontalResolution.ToString();
                ip.BitDepth = System.Drawing.Image.GetPixelFormatSize(img.PixelFormat).ToString();

                // check for grayscale and color image
                if (isGrayScale((Bitmap)img))
                {
                    ip.ColorMode = "Grayscale Image";
                }
                else
                {
                    ip.ColorMode = "Color Image";
                }

                ip.ImageSource = fileName;
                ip.ShowDialog();
            }
            else
            {
                return;
            }
        }

        private static Boolean isGrayScale(Bitmap img)
        {
            Boolean result = true;
            for (Int32 h = 0; h < img.Height; h++)
                for (Int32 w = 0; w < img.Width; w++)
                {
                    Color color = img.GetPixel(w, h);
                    if ((color.R != color.G || color.G != color.B || color.R != color.B) && color.A != 0)
                    {
                        result = false;
                        return result;
                    }
                }
            return result;
        }

        private void tsbtnListImageNames_Click(object sender, EventArgs e)
        {
            // call list image names in image preview pane function 
            ImagesListInPreviewPane();
        }

        private void tsbtnListImageThumbnail_Click(object sender, EventArgs e)
        {
            LoadImagesOnly();
        }

        private void nextPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // get selected cell position
            int selectedRow = 0;
            if (dgvPages.SelectedCells.Count > 0)
            {   
                // select next row in dgvpages
                selectedRow = dgvPages.SelectedCells[0].RowIndex;
                if(dgvPages.Rows.Count > (selectedRow + 1))
                {
                    dgvPages.CurrentCell = dgvPages.Rows[selectedRow + 1].Cells[0];
                }
            }
            else
            {
                return;
            }
        }

        private void previousPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // get selected cell position
            int selectedRow = 0;
            if (dgvPages.SelectedCells.Count > 0)
            {
                // select next row in dgvpages
                selectedRow = dgvPages.SelectedCells[0].RowIndex;
                if ((selectedRow - 1) >= 0)
                {
                    dgvPages.CurrentCell = dgvPages.Rows[selectedRow - 1].Cells[0];
                }
            }
            else
            {
                return;
            }
        }

        private void preferencesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Preferences().ShowDialog();
            // call set preferences method; some preferences might be changed
            SetPreferences();
        }

        private void txtSegmentProgress_TextChanged(object sender, EventArgs e)
        {

        }

        private void goToToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ActiveControl = rtbOCRedText;
        }

        private void tsmnuOpenImageFiles_Click(object sender, EventArgs e)
        {
            // check for unsaved work
            if (rtbOCRedText.Text.Length > 0 && workSaved != true)
            {
                // confirmation dialog
                DialogResult dr = MessageBox.Show("You have unsaved work. Do you want to continue?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (dr == DialogResult.Yes)
                {
                    // call open image function
                    this.openImage();
                }
                else if (dr == DialogResult.No)
                {
                    //
                }
                //TODO: Stuff
            }
            else
            {
                // call open image function
                this.openImage();
            }
        }

        private void tsmnuOpenPDFFile_Click(object sender, EventArgs e)
        {
            //this.openPDF();
            // check for unsaved work
            if (rtbOCRedText.Text.Length > 0 && workSaved != true)
            {
                // confirmation dialog
                DialogResult dr = MessageBox.Show("You have unsaved work. Do you want to continue?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (dr == DialogResult.Yes)
                {
                    // call recognize pdf function
                    this.recognizePDF();
                }
                else if (dr == DialogResult.No)
                {
                    //
                }
                //TODO: Stuff
            }
            else
            {
                // call recognize pdf function
                this.recognizePDF();
            }
        }

        private void tsbtnOpenPDF_Click(object sender, EventArgs e)
        {
            // check for unsaved work
            if (rtbOCRedText.Text.Length > 0 && workSaved != true)
            {
                // confirmation dialog
                DialogResult dr = MessageBox.Show("You have unsaved work. Do you want to continue?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (dr == DialogResult.Yes)
                {
                    // call recognize pdf function
                    this.recognizePDF();
                }
                else if (dr == DialogResult.No)
                {
                    //
                }
                //TODO: Stuff
            }
            else
            {
                // call recognize pdf function
                this.recognizePDF();
            }
            //this.openPDF();
        }

        //Opens image file and correct the skewness of image by calling Hough Transform method
        public void recognizePDF()
        {
            OpenFileDialog openImageDialog = new OpenFileDialog();
            // Select Image File to Recognize
            //openImageDialog.InitialDirectory = "D:\\";
            openImageDialog.Filter = "PDF File (*.pdf)|*.pdf";
            openImageDialog.FilterIndex = 1;
            openImageDialog.Multiselect = false;
            openImageDialog.RestoreDirectory = true;

            if (openImageDialog.ShowDialog() == DialogResult.OK)
            {
                // clear rtbOCRedText
                rtbOCRedText.Clear();

                // get language
                _language = Regex.Match(cbTessOCRLanguages.Items[cbTessOCRLanguages.SelectedIndex].ToString(), @"\(([^)]*)\)").Groups[1].Value;

                // set Nepali + English language mode
                if (_language == "nep")
                {
                    //_language += "+eng";
                }

                // get ocr mode
                String ocr_mode = tscbOCRMode.Items[tscbOCRMode.SelectedIndex].ToString();
                if (ocr_mode.Equals("Plain Text"))
                {
                    _ocr_mode = "";
                }
                else if (ocr_mode.Equals("Searchable PDF"))
                {
                    _ocr_mode = "pdf";
                }
                /*
                 *else if (ocr_mode.Equals("hOCR"))
                {
                    _ocr_mode = "hocr";
                }
                */

                if(openImageDialog.FileName.Length <= 0)
                {
                    MessageBox.Show(null, "No files has found. Please browse files to recognize.", "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                /***/
                /*#clear temp directory #*/
                // clear dgvpages and picture box before acquiring images from new file
                // remove file from list and gridview
                // clear image in picture box
                inputPicBox.Visible = false;
                //Dispose images
                if (InputImage.Original != null) { InputImage.Original.Dispose(); InputImage.Original = null; }
                if (InputImage.Changed != null) { InputImage.Changed.Dispose(); InputImage.Changed = null; }

                // dispose picture box image
                if (inputPicBox.Image != null)
                {
                    inputPicBox.Image.Dispose();
                    inputPicBox.Image = null;
                }

                dgvPages.Rows.Clear();
                _files = null;

                // temporary file directory inside user's home directory
                String userHomeDir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                //Console.Out.WriteLine(userHomeDir);
                if (!Directory.Exists(userHomeDir + "\\.neocr\\")) // neocr data parent directory
                {
                    Directory.CreateDirectory(userHomeDir + "\\.neocr\\");
                }

                // check for child data dir
                if (!Directory.Exists(userHomeDir + "\\.neocr\\docs\\")) // neocr data parent directory
                {
                    Directory.CreateDirectory(userHomeDir + "\\.neocr\\docs\\");
                }

                String batDir = userHomeDir + "\\.neocr\\docs\\";
                // try temp file delete process
                System.IO.DirectoryInfo di = new DirectoryInfo(batDir);
                foreach (FileInfo fileInfo in di.GetFiles())
                {
                    try
                    {
                        String filePath = batDir + "\\" + fileInfo.Name;
                        File.Delete(filePath);
                        //file.Delete();                       
                    }
                    catch (Exception e)
                    {
                        Console.Out.WriteLine(e.ToString());
                    }
                }

                /***/
                // get selected files
                String file = openImageDialog.FileName;

                /*#Region: Image/PDF selection#*/
                // if selected file is pdf then convert to image and load in gridview
                if (file.Length > 0 && Path.GetExtension(file).ToLower() == ".pdf")
                {
                    // pdf is expected
                    _pdf_file = file;

                    // run ocr process
                    if (backgroundWorkerRecognizePDF.IsBusy != true)
                    {
                        // create a new instance of the alert form
                        alert = new AlertForm();
                        alert.StartPosition = FormStartPosition.CenterScreen;
                        alert.Show(this);;//.Show(this);
                        alert.IndeterminateProgressBar = true;

                        // Start the asynchronous operation.
                        backgroundWorkerRecognizePDF.RunWorkerAsync();
                    }

                    // run image acquisition process
                    // backgroundWorkerPDFToImage.RunWorkerAsync();
                    // wait until process is busy
                    while (backgroundWorkerRecognizePDF.IsBusy)
                        Application.DoEvents();

                    //END
                    LoadImagesOnly();
                    // focus on ocred text pane
                    this.ActiveControl = rtbOCRedText;
                }
                /*#Region End: Image/PDF selection#*/
            }
        }

        // load images to image list pane
        private void LoadImagesOnly()
        {
            try
            {
                if (_files == null)
                {
                    return;
                }

                // clear rows and columns
                dgvPages.Rows.Clear();
                dgvPages.Columns.Clear();

                // initialize variables
                int numColumnsForWidth = 1;
                int numImagesRequired = _files.Count;
                int numRows = numImagesRequired;

                // add column to datagridview
                DataGridViewImageColumn dataGridViewColumn = new DataGridViewImageColumn();
                dgvPages.Columns.Add(dataGridViewColumn);

                // Create the rows
                for (int index = 0; index < numRows; index++)
                {
                    dgvPages.Rows.Add();
                    dgvPages.Rows[index].Height = _imageSize + 20;
                }

                int columnIndex = 0;
                int rowIndex = 0;
                try
                {
                    for (int index = _currentStartImageIndex; index < _currentStartImageIndex + numImagesRequired; index++)
                    {
                        // Load the image from the file and add to the DataGridView
                        System.Drawing.Image image = Helper.ResizeImage(_files[index], _imageSize, _imageSize, false);
                        dgvPages.Rows[rowIndex].Cells[columnIndex].Value = image;
                        dgvPages.Rows[rowIndex].Cells[columnIndex].ToolTipText = Path.GetFileName(_files[index]);
                        dgvPages.Rows[rowIndex].Cells[columnIndex].Tag = _files[index];

                        // Have we reached the end column? if so then start on the next row
                        if (columnIndex == numColumnsForWidth - 1)
                        {
                            rowIndex++;
                            columnIndex = 0;
                        }
                        else
                        {
                            columnIndex++;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.Out.WriteLine("Error while loading image\n" + ex.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("Error while loading image\n" + ex.ToString());
            }
        }

        private void backgroundWorkerRecognizePDF_DoWork(object sender, DoWorkEventArgs e)
        {
            // check pdf file availability
            if (_pdf_file == null && _pdf_file.Length <= 0)
            {
                MessageBox.Show(null, "No files has been selected.", "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // run ocr process
            // OCRMultiPagesUsingTesseract(_language);
            if (_ocr_mode == "")
            {
                OCRPDFUsingTesseractIBI(_language);
            }

            /*
            // if pdf output mode change arguments accordingly
            else if (_ocr_mode == "pdf")
            {
                OCRMultiPagesUsingTesseractPDF(_language);
            }
            */
            //END
        }

        /// <summary>
        /// function to perform OCR multiple pages using tesseract; Images one by one
        /// </summary>
        public void OCRPDFUsingTesseractIBI(String language)
        {
            // temporary file directory inside user's home directory
            String userHomeDir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            //Console.Out.WriteLine(userHomeDir);
            if (!Directory.Exists(userHomeDir + "\\.neocr\\")) // neocr data parent directory
            {
                Directory.CreateDirectory(userHomeDir + "\\.neocr\\");
            }

            // check for child data dir
            if (!Directory.Exists(userHomeDir + "\\.neocr\\docs\\")) // neocr data parent directory
            {
                Directory.CreateDirectory(userHomeDir + "\\.neocr\\docs\\");
            }

            // temporary file storage space on disk
            String tempDir = userHomeDir + "\\.neocr\\docs\\";

            string _pdf_temp_path = tempDir + "\\tmp_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
            File.Copy(@_pdf_file, @_pdf_temp_path);

            try
            {
                /**Get number of pages in PDF document**/
                // ghostscript variables
                GhostscriptVersionInfo _lastInstalledVersion = GhostscriptVersionInfo.GetLastInstalledVersion(
                    GhostscriptLicense.GPL | GhostscriptLicense.AFPL,
                    GhostscriptLicense.GPL);
                GhostscriptRasterizer _rasterizer = new GhostscriptRasterizer();
                // get number of pages
                int totalPagesCount = GSRasterizer.GetNumberOfPages(_lastInstalledVersion, _rasterizer, _pdf_temp_path);

                // recognized page counter
                recognizedPageCounter = 1;  // recognized pages counter initialization

                // recognize all pages
                // set tesseract directory path
                string tessractDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\tesseract";
                tessractDirPath = string.Format(tessractDirPath.Replace("file:\\", ""));

                // get temp file path
                _out_file_path = tempDir + "\\ocred-text-neocr";
                if (File.Exists(_out_file_path + ".txt")) { File.Delete(_out_file_path + ".txt"); }

                // clear files to process array
                if (_files_to_process != null) { _files_to_process.Clear(); } else { _files_to_process = new List<string>(); }
                if (_files != null) { _files.Clear(); } else { _files = new List<string>(); }

                // recognize all selected images in single run  // _files_to_process
                for (int pageNum = 1; pageNum <= totalPagesCount; pageNum++)
                {
                    // rasterize pdf page
                    String filePath = GSRasterizer.RenderPDFToImage(_lastInstalledVersion, _rasterizer, _pdf_temp_path, tempDir, pageNum, 300, 300);
                    // add file to files list
                    _files.Add(filePath);   // later will be used to display images in list

                    String tempImgPath = "";
                    if (RecognizeDocumentOnLoad) { 
                        #region -- recognition
                        // ocr progress
                        //alert.Message = "Recognizing page " + recognizedPageCounter + " of " + totalPages;
                        UpdateProgress("Recognizing page " + recognizedPageCounter + " of " + totalPagesCount);

                        // set progress
                        alert.DisplayOCRProgress("Recognizing Page... (" + recognizedPageCounter + " of " + totalPagesCount + ")");

                        #region -- perform pre-processing --
                        // Perform Pre-processing
                        // preprocess and save image temporarily
                        Bitmap preproImage = (Bitmap)System.Drawing.Image.FromFile(filePath);
                        Bitmap preprocessedImage = PreprocessImage(preproImage);
                        // generate temporary image file path
                        tempImgPath = tempDir + "\\neocr-" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + Path.GetExtension(filePath);
                        if (File.Exists(tempImgPath)) { File.Delete(tempImgPath); }
                        //save image
                        preprocessedImage.Save(tempImgPath);
                        #endregion

                        String tessdataDirPath = tessractDirPath + "\\tessdata";
                        // set arguments // output file will be of format _out_file_path + "_rs"
                        String arguments = "\"" + tempImgPath + "\" \"" + _out_file_path + "_rs\" -l " + language + " --psm 1 --oem 1";

                        // create image recognition process
                        var proc = new Process
                        {
                            StartInfo = new ProcessStartInfo
                            {
                                WorkingDirectory = tessractDirPath,
                                FileName = tessractDirPath + "\\tesseract.exe",
                                Arguments = arguments,
                                UseShellExecute = false,
                                RedirectStandardOutput = true,
                                RedirectStandardError = true,
                                RedirectStandardInput = true,
                                CreateNoWindow = true
                            }
                        };

                        proc.OutputDataReceived += new DataReceivedEventHandler(
                            (s, e) =>
                            {
                                Console.Out.WriteLine(e.Data);
                            }
                        );
                        proc.ErrorDataReceived += new DataReceivedEventHandler((s, e) =>
                        {
                            Console.Out.WriteLine(e.Data);
                        });

                        proc.Start();
                        proc.BeginOutputReadLine();

                        proc.WaitForExit();
                        // Recognition END
                        #endregion --recognition end
                    }else{
                        // display acquisition message
                        UpdateProgress("Acquiring page " + recognizedPageCounter + " of " + totalPagesCount);
                        // set progress
                        alert.DisplayOCRProgress("Acquiring Page... (" + recognizedPageCounter + " of " + totalPagesCount + ")");
                    }

                    #region -- delete temporary image file --
                    if (File.Exists(tempImgPath)) { File.Delete(tempImgPath); }
                    #endregion

                    // Perform a time consuming operation and report progress.
                    //backgroundWorkerOCRAllPagesUsingTesseract.ReportProgress(100);

                    // This text is always added, making the file longer over time
                    // if it is not deleted.
                    if (File.Exists(_out_file_path + "_rs.txt"))
                    {
                        using (StreamWriter sw = File.AppendText(_out_file_path + ".txt"))
                        {
                            String text = File.ReadAllText(_out_file_path + "_rs.txt");   // read all content of files and append
                            sw.WriteLine(text);
                            //delete temporary file
                            File.Delete(_out_file_path + "_rs.txt");
                        }
                    }

                    // increase recognied pages counter
                    recognizedPageCounter++;
                }

                _rasterizer.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("GS Error: " + ex.ToString());
            }

            // delete temp file
            if (File.Exists(_pdf_temp_path))
            {
                File.Delete(_pdf_temp_path);
            }
        }

        private void backgroundWorkerRecognizePDF_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // check ocr process status
            if (backgroundWorkerRecognizePDF.IsBusy != true)
            {
                // close alert form
                alert.Close();
            }

            // if recognize on load is false for PDF do not try to access resultant test file as it is not available to read; no recognition is performed at all.
            if (RecognizeDocumentOnLoad)
            {
                // read all text from file and display
                try
                {
                    // if ocr mode is plain text
                    if (_ocr_mode == "")
                    {
                        // Open the text file using a stream reader.
                        using (StreamReader sr = new StreamReader(_out_file_path + ".txt"))
                        {
                            // Read the stream to a string, and write the string to the console.
                            String line = sr.ReadToEnd();
                            DisplayOCRedText(line);
                        }

                        // delete file // free resources
                        File.Delete(_out_file_path + ".txt");

                        // play finish sound    //Should be put as Optional task
                        if (ScreenReader.IsRunning)
                        {
                            finishSoundPlayer.Play();
                        }

                        // write completion message
                        UpdateProgress("Recognition succeeded.");

                        // show success message box
                        MessageBox.Show(null, "Recognitin completed successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    // if ocr mode is searchable PDF
                    if (_ocr_mode == "pdf")
                    {
                        // write processing message
                        UpdateProgress("Saving searchable PDF...");

                        SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                        saveFileDialog1.Title = "Save Searchable PDF";
                        saveFileDialog1.Filter = "PDF files (*.pdf)|*.pdf";
                        saveFileDialog1.FilterIndex = 1;
                        saveFileDialog1.RestoreDirectory = true;

                        if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            String saveFileName = saveFileDialog1.FileName;
                            File.Copy(_out_file_path + ".pdf", saveFileName);
                        }

                        // delete file // free resources
                        File.Delete(_out_file_path + ".pdf");

                        // write processing message
                        UpdateProgress("Searchable PDF saved successfully...");

                        // play finish sound
                        if (ScreenReader.IsRunning)
                        {
                            finishSoundPlayer.Play();
                        }

                        // show success message box
                        MessageBox.Show(null, "Searchable PDF saved successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    UpdateProgress("Some error occured. The file could not be read: \n" + ex.Message);
                }
            }
        }

        private void registerTool_Click(object sender, EventArgs e)
        {
            string main_path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string file_path = main_path + "\\NepOCR\\license.bin";

            var registeredInfo = new RegisteredInfo();
            registeredInfo.MinimizeBox = false;
            registeredInfo.MaximizeBox = false;
            registeredInfo.StartPosition = FormStartPosition.CenterParent;

            if (File.Exists(file_path))
            {
                try
                {
                    using (Stream stream = File.Open(file_path, FileMode.Open))
                    {
                        BinaryFormatter bin = new BinaryFormatter();

                        var license_info = (List<string>)bin.Deserialize(stream);
                        registeredInfo.SetTextForLabel("This product is licensed to:\n\nFirst Name : " + license_info[0] + "\nLast Name : " + license_info[1] + "\nEmail Address : " + license_info[2] + "\nOrganization's Name : " + license_info[3] + "\nCountry : " + license_info[4]);
                        registeredInfo.hideRegister();
                    }
                }
                catch (IOException ex)
                {
                    Console.WriteLine(ex);
                }

            }
            else
            {
                registeredInfo.SetTextForLabel("This product is not registered. Click on Register button to register. Registration is Free!");
            }

            registeredInfo.ShowDialog(this);
        }

        private void tsmnuGrayscaleImage_Click(object sender, EventArgs e)
        {
            GrayscaleImage();
        }

        private void tsmnuThresholdImage_Click(object sender, EventArgs e)
        {
            ThresholdImageManually();
        }

        private void tsmnuInvertImage_Click(object sender, EventArgs e)
        {
            InvertImageManually();
        }

        private void tsmnuRotate90CW_Click(object sender, EventArgs e)
        {
            Rotate90CW();
        }

        private void tsmnuRotate90CCW_Click(object sender, EventArgs e)
        {
            Rotate90CCW();
        }

        private void tsmnuFlipHorizontal_Click(object sender, EventArgs e)
        {
            FlipHorizontal();
        }

        private void tsmnuFlipVertical_Click(object sender, EventArgs e)
        {
            FlipVertical();
        }

        private void tsmnuRotate180_Click(object sender, EventArgs e)
        {
            Rotate180();
        }

        private void tsmnuRotateArbitrarily_Click(object sender, EventArgs e)
        {
            RotateArbitrary();
        }

        private void helpContentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string batDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\content";
            string batDir = string.Format(@batDirPath.Replace("file:\\", ""));
            // open help content html page
            Process.Start(Path.Combine(batDir, "NepOCR 1.0.0 Manual.html"));
        }

        public void SetPreferences()
        {
            try
            {
                // get base directory path
                string baseDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                baseDirPath = @baseDirPath + "\\neocrapp.config";
                baseDirPath = string.Format(baseDirPath.Replace("file:\\", ""));
                // read preferences xml
                DataSet dataSet = new DataSet();
                dataSet.ReadXml(baseDirPath);

                DataTable pref = dataSet.Tables[0];
                string OPFont = (pref.Rows[0][0].ToString().Equals("default")) ? "Madan2" : pref.Rows[0][0].ToString(); // default font is "Kalimati"
                double OPFontSize = (pref.Rows[0][1].ToString().Equals("default")) ? 12 : Convert.ToDouble(pref.Rows[0][1]); // default font size is "12"
                string OPFontMode = (pref.Rows[0][0].ToString().Equals("default")) ? "default" : "custom";
                string RecognizeOnLoad = pref.Rows[0][2].ToString();
                string OCRLang = pref.Rows[0][3].ToString();
                string OCRMode = pref.Rows[0][4].ToString();
                string RestoreLayout = pref.Rows[0][5].ToString();

                // set value of each preferences control
                if (OPFontMode.Equals("default")) {
                    tscbFonts.SelectedIndex = tscbFonts.FindStringExact("Madan2");
                    tscbFontSize.SelectedIndex = tscbFontSize.FindStringExact("12");

                    // change font size of rtbOCRedText
                    // set font and font size on ocred text pane
                    rtbOCRedText.Font = new Font("Madan2", 12, FontStyle.Regular);
                    rtbOCRedText.Invalidate();
                }
                else{
                    tscbFonts.SelectedIndex = tscbFonts.FindStringExact(OPFont);
                    tscbFontSize.SelectedIndex = tscbFontSize.FindStringExact(OPFontSize.ToString());

                    // change font size of rtbOCRedText
                    // set font and font size on ocred text pane
                    rtbOCRedText.Font = new Font(OPFont, Convert.ToInt16(OPFontSize.ToString()), FontStyle.Regular);
                    rtbOCRedText.Invalidate();
                }

                if (RecognizeOnLoad.Equals("FALSE")) {
                    RecognizeDocumentOnLoad = false;
                }else{
                    RecognizeDocumentOnLoad = true;
                }

                // re-load ocr languages list and set preference
                getTesseractOCRLanguages();
                cbTessOCRLanguages.SelectedIndex = cbTessOCRLanguages.FindStringExact(OCRLang);
                tscbOCRMode.SelectedIndex = tscbOCRMode.FindStringExact(OCRMode);

                if (RestoreLayout.Equals("NO")) {
                    RestoreDocumentLayout = false;
                }else{
                    RestoreDocumentLayout = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(null, "Error while setting preferences." + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
