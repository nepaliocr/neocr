﻿namespace NeOCR.Register
{
    partial class RegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.register_panel = new System.Windows.Forms.Panel();
            this.country_textbox = new System.Windows.Forms.TextBox();
            this.country = new System.Windows.Forms.Label();
            this.org_textbox = new System.Windows.Forms.TextBox();
            this.org_name = new System.Windows.Forms.Label();
            this.email_textbox = new System.Windows.Forms.TextBox();
            this.lastname_textbox = new System.Windows.Forms.TextBox();
            this.firstname_textbox = new System.Windows.Forms.TextBox();
            this.email_address = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.last_name = new System.Windows.Forms.Label();
            this.first_name = new System.Windows.Forms.Label();
            this.cancel_button = new System.Windows.Forms.Button();
            this.OK_button = new System.Windows.Forms.Button();
            this.instruction_label = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.register_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // register_panel
            // 
            this.register_panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.register_panel.Controls.Add(this.country_textbox);
            this.register_panel.Controls.Add(this.country);
            this.register_panel.Controls.Add(this.org_textbox);
            this.register_panel.Controls.Add(this.org_name);
            this.register_panel.Controls.Add(this.email_textbox);
            this.register_panel.Controls.Add(this.lastname_textbox);
            this.register_panel.Controls.Add(this.firstname_textbox);
            this.register_panel.Controls.Add(this.email_address);
            this.register_panel.Controls.Add(this.label4);
            this.register_panel.Controls.Add(this.last_name);
            this.register_panel.Controls.Add(this.first_name);
            this.register_panel.Location = new System.Drawing.Point(16, 96);
            this.register_panel.Name = "register_panel";
            this.register_panel.Size = new System.Drawing.Size(452, 155);
            this.register_panel.TabIndex = 4;
            // 
            // country_textbox
            // 
            this.country_textbox.AccessibleDescription = "Country";
            this.country_textbox.Location = new System.Drawing.Point(135, 123);
            this.country_textbox.Name = "country_textbox";
            this.country_textbox.Size = new System.Drawing.Size(150, 20);
            this.country_textbox.TabIndex = 9;
            // 
            // country
            // 
            this.country.AutoSize = true;
            this.country.Location = new System.Drawing.Point(3, 126);
            this.country.Name = "country";
            this.country.Size = new System.Drawing.Size(43, 13);
            this.country.TabIndex = 8;
            this.country.Text = "Country";
            // 
            // org_textbox
            // 
            this.org_textbox.AccessibleDescription = "Organization\'s Name";
            this.org_textbox.Location = new System.Drawing.Point(135, 88);
            this.org_textbox.Name = "org_textbox";
            this.org_textbox.Size = new System.Drawing.Size(303, 20);
            this.org_textbox.TabIndex = 7;
            // 
            // org_name
            // 
            this.org_name.AutoSize = true;
            this.org_name.Location = new System.Drawing.Point(3, 91);
            this.org_name.Name = "org_name";
            this.org_name.Size = new System.Drawing.Size(104, 13);
            this.org_name.TabIndex = 6;
            this.org_name.Text = "Organization\'s Name";
            // 
            // email_textbox
            // 
            this.email_textbox.AccessibleDescription = "Email Address";
            this.email_textbox.Location = new System.Drawing.Point(135, 53);
            this.email_textbox.Name = "email_textbox";
            this.email_textbox.Size = new System.Drawing.Size(303, 20);
            this.email_textbox.TabIndex = 5;
            // 
            // lastname_textbox
            // 
            this.lastname_textbox.AccessibleDescription = "Last Name";
            this.lastname_textbox.Location = new System.Drawing.Point(302, 6);
            this.lastname_textbox.Name = "lastname_textbox";
            this.lastname_textbox.Size = new System.Drawing.Size(136, 20);
            this.lastname_textbox.TabIndex = 4;
            // 
            // firstname_textbox
            // 
            this.firstname_textbox.AccessibleDescription = "First Name";
            this.firstname_textbox.Location = new System.Drawing.Point(135, 6);
            this.firstname_textbox.Name = "firstname_textbox";
            this.firstname_textbox.Size = new System.Drawing.Size(150, 20);
            this.firstname_textbox.TabIndex = 3;
            // 
            // email_address
            // 
            this.email_address.AutoSize = true;
            this.email_address.Location = new System.Drawing.Point(3, 56);
            this.email_address.Name = "email_address";
            this.email_address.Size = new System.Drawing.Size(73, 13);
            this.email_address.TabIndex = 2;
            this.email_address.Text = "Email Address";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(299, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Last Name";
            // 
            // last_name
            // 
            this.last_name.AutoSize = true;
            this.last_name.Location = new System.Drawing.Point(132, 29);
            this.last_name.Name = "last_name";
            this.last_name.Size = new System.Drawing.Size(57, 13);
            this.last_name.TabIndex = 1;
            this.last_name.Text = "First Name";
            // 
            // first_name
            // 
            this.first_name.AutoSize = true;
            this.first_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.first_name.Location = new System.Drawing.Point(3, 9);
            this.first_name.Name = "first_name";
            this.first_name.Size = new System.Drawing.Size(35, 13);
            this.first_name.TabIndex = 0;
            this.first_name.Text = "Name";
            // 
            // cancel_button
            // 
            this.cancel_button.Location = new System.Drawing.Point(368, 267);
            this.cancel_button.Name = "cancel_button";
            this.cancel_button.Size = new System.Drawing.Size(100, 23);
            this.cancel_button.TabIndex = 7;
            this.cancel_button.Text = "Cancel";
            this.cancel_button.UseVisualStyleBackColor = true;
            this.cancel_button.Click += new System.EventHandler(this.cancel_button_Click);
            // 
            // OK_button
            // 
            this.OK_button.Location = new System.Drawing.Point(227, 267);
            this.OK_button.Name = "OK_button";
            this.OK_button.Size = new System.Drawing.Size(107, 23);
            this.OK_button.TabIndex = 6;
            this.OK_button.Text = "OK";
            this.OK_button.UseVisualStyleBackColor = true;
            this.OK_button.Click += new System.EventHandler(this.OK_button_Click);
            // 
            // instruction_label
            // 
            this.instruction_label.AccessibleDescription = "";
            this.instruction_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.instruction_label.Location = new System.Drawing.Point(13, 47);
            this.instruction_label.Name = "instruction_label";
            this.instruction_label.Size = new System.Drawing.Size(382, 46);
            this.instruction_label.TabIndex = 0;
            this.instruction_label.Text = "Please enter your registration details in corresponding fields and click on OK bu" +
    "tton";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(48, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 24);
            this.label1.TabIndex = 8;
            this.label1.Text = "NeOCR";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(130, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "v1.0.0";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Image = global::NeOCR.Properties.Resources.OCR_icon;
            this.label3.Location = new System.Drawing.Point(2, -1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 38);
            this.label3.TabIndex = 8;
            // 
            // RegisterForm
            // 
            this.AccessibleDescription = "Please enter your registration details in corresponding fields and click on OK bu" +
    "tton";
            this.AccessibleName = "Product Registration Form";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 296);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.register_panel);
            this.Controls.Add(this.cancel_button);
            this.Controls.Add(this.OK_button);
            this.Controls.Add(this.instruction_label);
            this.Name = "RegisterForm";
            this.Text = "Product Registration Form";
            this.Load += new System.EventHandler(this.RegisterForm_Load);
            this.register_panel.ResumeLayout(false);
            this.register_panel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel register_panel;
        private System.Windows.Forms.TextBox country_textbox;
        private System.Windows.Forms.Label country;
        private System.Windows.Forms.TextBox org_textbox;
        private System.Windows.Forms.Label org_name;
        private System.Windows.Forms.TextBox email_textbox;
        private System.Windows.Forms.TextBox lastname_textbox;
        private System.Windows.Forms.TextBox firstname_textbox;
        private System.Windows.Forms.Label email_address;
        private System.Windows.Forms.Label last_name;
        private System.Windows.Forms.Label first_name;
        private System.Windows.Forms.Button cancel_button;
        private System.Windows.Forms.Button OK_button;
        private System.Windows.Forms.Label instruction_label;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}