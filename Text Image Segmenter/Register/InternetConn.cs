﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NeOCR.Register
{
    public partial class RegisterForm
    {
        // check internet connection availability
        public bool CheckForIC()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        // creates bin files for registration
        public bool CreateFile(string file_path, List<string> info)
        {
            string fileName = file_path;

            try
            {
                using (Stream stream = File.Open(file_path, FileMode.Create))
                {
                    BinaryFormatter bin = new BinaryFormatter();
                    bin.Serialize(stream, info);
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex);
                return false;
            }

            return true;
        }

        // Access server for new registration
        public void NewRegistration(List<string> info_list)
        {
            string URI = "http://192.168.0.100:80/NeOCR/registration.php";
            string myParameters = "register=yes&fname=" + info_list[0] + "&lname=" + info_list[1] +
                "&email=" + info_list[2] + "&orgname=" + info_list[3] + "&country=" + info_list[4] +
                "&macaddress=" + GetMacAddress();

            try
            {
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    string HtmlResult = wc.UploadString(URI, "POST", myParameters);

                    if (HtmlResult.Contains("error"))
                    {
                        MessageBox.Show("Sorry! Cound not connect to server.");
                        return;
                    }
                    else if (HtmlResult != null)
                    {
                        string main_path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

                        string file_path = main_path + "\\NepOCR\\license.bin";
                        info_list.Add(HtmlResult);

                        if (CreateFile(file_path, info_list))
                        {
                            MessageBox.Show("Registration Successful! Thank you for your support.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }


        }

        // get physical address of machine
        public PhysicalAddress GetMacAddress()
        {
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet &&
                    nic.OperationalStatus == OperationalStatus.Up)
                {
                    return nic.GetPhysicalAddress();
                }
            }
            return null;
        }

        // checks email validity
        public bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }

}
