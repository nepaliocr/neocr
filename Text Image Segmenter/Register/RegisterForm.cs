﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NeOCR.Register
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void OK_button_Click(object sender, EventArgs e)
        {
            string first_name = firstname_textbox.Text;
            string last_name = lastname_textbox.Text;
            string email_address = email_textbox.Text;
            string organization_name = org_textbox.Text;
            string country = country_textbox.Text;

            if (CheckForIC() && IsValidEmail(email_address))
            {
                List<string> info_list = new List<string>();
                info_list.Add(first_name);
                info_list.Add(last_name);
                info_list.Add(email_address);
                info_list.Add(organization_name);
                info_list.Add(country);

                NewRegistration(info_list);

                Close();
            }
            else
            {
                MessageBox.Show("Please check your internet connection or email address");
            }
        }

        private void cancel_button_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RegisterForm_Load(object sender, EventArgs e)
        {
            this.ActiveControl = instruction_label;
        }
    }
}
