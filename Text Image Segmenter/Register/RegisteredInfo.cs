﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NeOCR.Register
{
    public partial class RegisteredInfo : Form
    {
        public RegisteredInfo()
        {
            InitializeComponent();
        }

        private void register_button_Click(object sender, EventArgs e)
        {
            string main_path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string file_path = main_path + "\\NepOCR\\license.bin";

            var registerform = new RegisterForm();
            registerform.MinimizeBox = false;
            registerform.MaximizeBox = false;
            registerform.StartPosition = FormStartPosition.CenterParent;
            registerform.ShowDialog(this);

            this.Close();
        }

        public void SetTextForLabel(string msgText)
        {
            //this.info_label.Text = msgText;
            this.lblRegisterStatusMsg.Text = msgText;
            this.AccessibleDescription = msgText;
        }

        public void hideRegister()
        {
            register_button.Visible = false;
        }

        private void RegisteredInfo_Load(object sender, EventArgs e)
        {
            this.ActiveControl = lblRegisterStatusMsg;
        }
    }
}
