﻿namespace NeOCR.Register
{
    partial class RegisteredInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.info_label = new System.Windows.Forms.Label();
            this.register_button = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblRegisterStatusMsg = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // info_label
            // 
            this.info_label.AutoSize = true;
            this.info_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.info_label.Location = new System.Drawing.Point(193, 124);
            this.info_label.Name = "info_label";
            this.info_label.Size = new System.Drawing.Size(0, 16);
            this.info_label.TabIndex = 6;
            // 
            // register_button
            // 
            this.register_button.AccessibleDescription = "Register Product";
            this.register_button.Location = new System.Drawing.Point(15, 154);
            this.register_button.Name = "register_button";
            this.register_button.Size = new System.Drawing.Size(110, 23);
            this.register_button.TabIndex = 5;
            this.register_button.Text = "Register";
            this.register_button.UseVisualStyleBackColor = true;
            this.register_button.Click += new System.EventHandler(this.register_button_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(130, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 16);
            this.label2.TabIndex = 9;
            this.label2.Text = "v1.0.0";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Image = global::NeOCR.Properties.Resources.OCR_icon;
            this.label3.Location = new System.Drawing.Point(2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 38);
            this.label3.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(48, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 24);
            this.label1.TabIndex = 11;
            this.label1.Text = "NeOCR";
            // 
            // lblRegisterStatusMsg
            // 
            this.lblRegisterStatusMsg.AccessibleDescription = "This Product is not registered. Click on Register button to Register. Registratio" +
    "n is free!";
            this.lblRegisterStatusMsg.Location = new System.Drawing.Point(12, 50);
            this.lblRegisterStatusMsg.Name = "lblRegisterStatusMsg";
            this.lblRegisterStatusMsg.Size = new System.Drawing.Size(323, 101);
            this.lblRegisterStatusMsg.TabIndex = 12;
            this.lblRegisterStatusMsg.Text = "This Product is not registered. Click on Register button to Register. Registratio" +
    "n is free!";
            // 
            // RegisteredInfo
            // 
            this.AccessibleDescription = "Registration Info";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 187);
            this.Controls.Add(this.lblRegisterStatusMsg);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.info_label);
            this.Controls.Add(this.register_button);
            this.Name = "RegisteredInfo";
            this.Text = "Registration Info";
            this.Load += new System.EventHandler(this.RegisteredInfo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label info_label;
        private System.Windows.Forms.Button register_button;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblRegisterStatusMsg;
    }
}