﻿namespace Text_Image_Segmenter
{
    partial class NeOCR_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NeOCR_Main));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pageListPaneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imagePreviewPaneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.nextPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.previousPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.imagePropertiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runOCRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oCRAllPagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recognitionWizardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.skewnessCorrectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filtersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmnuGrayscaleImage = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmnuThresholdImage = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmnuInvertImage = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmnuRotate90CW = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmnuRotate90CCW = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmnuFlipHorizontal = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmnuFlipVertical = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmnuRotate180 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmnuRotateArbitrarily = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator22 = new System.Windows.Forms.ToolStripSeparator();
            this.preferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.skinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.regularToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nightModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userInterfaceLanguageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator25 = new System.Windows.Forms.ToolStripSeparator();
            this.fontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeLineBreaksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpContentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registerTool = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator24 = new System.Windows.Forms.ToolStripSeparator();
            this.reportAProblemToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.provideASuggestionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator23 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.tscbOCRMode = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.cbTessOCRLanguages = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtSegmentProgress = new System.Windows.Forms.TextBox();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.pnlImage = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.toolStrip5 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator21 = new System.Windows.Forms.ToolStripSeparator();
            this.pnlOCRedText = new System.Windows.Forms.Panel();
            this.toolStrip6 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel6 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator20 = new System.Windows.Forms.ToolStripSeparator();
            this.tscbFonts = new System.Windows.Forms.ToolStripComboBox();
            this.tscbFontSize = new System.Windows.Forms.ToolStripComboBox();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel5 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator19 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.toolStrip4 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.dgvPages = new System.Windows.Forms.DataGridView();
            this.backgroundWorkerPDFToImage = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerOCRUsingTesseract = new System.ComponentModel.BackgroundWorker();
            this.contextMenuStripOCRedText = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyAllTextToClipboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.panel4 = new System.Windows.Forms.Panel();
            this.backgroundWorkerOCRAllPagesUsingTesseract = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerRecognizePDF = new System.ComponentModel.BackgroundWorker();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnOpenFile = new System.Windows.Forms.ToolStripButton();
            this.tsbtnOpenPDF = new System.Windows.Forms.ToolStripButton();
            this.btnScan = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tsbtnOCR = new System.Windows.Forms.ToolStripButton();
            this.tsbtnLayoutAnalysis = new System.Windows.Forms.ToolStripButton();
            this.tsbtnBlobs = new System.Windows.Forms.ToolStripButton();
            this.tsbtnDetectLines = new System.Windows.Forms.ToolStripButton();
            this.tsbtnListImageThumbnail = new System.Windows.Forms.ToolStripButton();
            this.tsbtnListImageNames = new System.Windows.Forms.ToolStripButton();
            this.tsbtnReadImage = new System.Windows.Forms.ToolStripButton();
            this.tsbtnSaveText = new System.Windows.Forms.ToolStripButton();
            this.inputPicBox = new System.Windows.Forms.PictureBox();
            this.tsBtnSelect = new System.Windows.Forms.ToolStripButton();
            this.tsBtnCrop = new System.Windows.Forms.ToolStripButton();
            this.tsBtnErase = new System.Windows.Forms.ToolStripButton();
            this.tsBtnZoomIn = new System.Windows.Forms.ToolStripButton();
            this.tsBtnZoomOut = new System.Windows.Forms.ToolStripButton();
            this.tsBtnRotateImage = new System.Windows.Forms.ToolStripSplitButton();
            this.toolMnuRotateCW = new System.Windows.Forms.ToolStripMenuItem();
            this.toolMnuRotateCCW = new System.Windows.Forms.ToolStripMenuItem();
            this.toolMnuFlipHorizontal = new System.Windows.Forms.ToolStripMenuItem();
            this.toolMnuFlipVertical = new System.Windows.Forms.ToolStripMenuItem();
            this.toolMnuRotate180 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolMnuRotateArbitrary = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbtnDeskewImage = new System.Windows.Forms.ToolStripButton();
            this.tsbtnGrayscaleImage = new System.Windows.Forms.ToolStripButton();
            this.tsbtnManuallyThresholdImage = new System.Windows.Forms.ToolStripButton();
            this.tsBtnInvertImage = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel7 = new System.Windows.Forms.ToolStripLabel();
            this.tsbtnIncreaseFontSize = new System.Windows.Forms.ToolStripButton();
            this.tsbtnDecreaseFontSize = new System.Windows.Forms.ToolStripButton();
            this.tsbtnClearText = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.tsbtnExportAsPlainText = new System.Windows.Forms.ToolStripButton();
            this.tsbtnExportToWordDoc = new System.Windows.Forms.ToolStripButton();
            this.mnuOpenImage = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmnuOpenImageFiles = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmnuOpenPDFFile = new System.Windows.Forms.ToolStripMenuItem();
            this.scanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportAsPlainTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportAsMicrosoftWordFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearContentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportAsPlainTextToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exportAsMicrosoftWordDocumentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rtbOCRedText = new System.Windows.Forms.RichTextBox();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.pnlImage.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.toolStrip5.SuspendLayout();
            this.pnlOCRedText.SuspendLayout();
            this.toolStrip6.SuspendLayout();
            this.toolStrip3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.toolStrip4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPages)).BeginInit();
            this.contextMenuStripOCRedText.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputPicBox)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Window;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.formatToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1090, 25);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "Main Menu";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuOpenImage,
            this.scanToolStripMenuItem,
            this.saveOutputToolStripMenuItem,
            this.toolStripSeparator12,
            this.closeToolStripMenuItem,
            this.closeAllToolStripMenuItem,
            this.toolStripSeparator13,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(39, 21);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(206, 6);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.closeToolStripMenuItem.Text = "&Close";
            this.closeToolStripMenuItem.ToolTipText = "Close Document";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // closeAllToolStripMenuItem
            // 
            this.closeAllToolStripMenuItem.Name = "closeAllToolStripMenuItem";
            this.closeAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.W)));
            this.closeAllToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.closeAllToolStripMenuItem.Text = "Clos&e All";
            this.closeAllToolStripMenuItem.ToolTipText = "Close All Opend Document";
            this.closeAllToolStripMenuItem.Click += new System.EventHandler(this.closeAllToolStripMenuItem_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(206, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pageListPaneToolStripMenuItem,
            this.imagePreviewPaneToolStripMenuItem,
            this.goToToolStripMenuItem,
            this.toolStripSeparator14,
            this.nextPageToolStripMenuItem,
            this.previousPageToolStripMenuItem,
            this.toolStripSeparator15,
            this.imagePropertiesToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(47, 21);
            this.viewToolStripMenuItem.Text = "&View";
            // 
            // pageListPaneToolStripMenuItem
            // 
            this.pageListPaneToolStripMenuItem.Checked = true;
            this.pageListPaneToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.pageListPaneToolStripMenuItem.Name = "pageListPaneToolStripMenuItem";
            this.pageListPaneToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.L)));
            this.pageListPaneToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.pageListPaneToolStripMenuItem.Text = "Page &List Pane";
            this.pageListPaneToolStripMenuItem.Click += new System.EventHandler(this.pageListPaneToolStripMenuItem_Click);
            // 
            // imagePreviewPaneToolStripMenuItem
            // 
            this.imagePreviewPaneToolStripMenuItem.CheckOnClick = true;
            this.imagePreviewPaneToolStripMenuItem.Name = "imagePreviewPaneToolStripMenuItem";
            this.imagePreviewPaneToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.M)));
            this.imagePreviewPaneToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.imagePreviewPaneToolStripMenuItem.Text = "&Image Preview Pane";
            this.imagePreviewPaneToolStripMenuItem.Click += new System.EventHandler(this.imagePreviewPaneToolStripMenuItem_Click);
            // 
            // goToToolStripMenuItem
            // 
            this.goToToolStripMenuItem.Name = "goToToolStripMenuItem";
            this.goToToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this.goToToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.goToToolStripMenuItem.Text = "&Go to Recognition Result";
            this.goToToolStripMenuItem.Click += new System.EventHandler(this.goToToolStripMenuItem_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(264, 6);
            // 
            // nextPageToolStripMenuItem
            // 
            this.nextPageToolStripMenuItem.Name = "nextPageToolStripMenuItem";
            this.nextPageToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.nextPageToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.nextPageToolStripMenuItem.Text = "Ne&xt Page";
            this.nextPageToolStripMenuItem.ToolTipText = "Next Task";
            this.nextPageToolStripMenuItem.Click += new System.EventHandler(this.nextPageToolStripMenuItem_Click);
            // 
            // previousPageToolStripMenuItem
            // 
            this.previousPageToolStripMenuItem.Name = "previousPageToolStripMenuItem";
            this.previousPageToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.H)));
            this.previousPageToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.previousPageToolStripMenuItem.Text = "P&revious Page";
            this.previousPageToolStripMenuItem.Click += new System.EventHandler(this.previousPageToolStripMenuItem_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(264, 6);
            // 
            // imagePropertiesToolStripMenuItem
            // 
            this.imagePropertiesToolStripMenuItem.Name = "imagePropertiesToolStripMenuItem";
            this.imagePropertiesToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.imagePropertiesToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.imagePropertiesToolStripMenuItem.Text = "Image &Properties";
            this.imagePropertiesToolStripMenuItem.Click += new System.EventHandler(this.imagePropertiesToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.runOCRToolStripMenuItem,
            this.oCRAllPagesToolStripMenuItem,
            this.recognitionWizardToolStripMenuItem,
            this.toolStripSeparator3,
            this.skewnessCorrectionToolStripMenuItem,
            this.filtersToolStripMenuItem,
            this.rotateImageToolStripMenuItem,
            this.toolStripSeparator22,
            this.preferencesToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(51, 21);
            this.toolsToolStripMenuItem.Text = "&Tools";
            this.toolsToolStripMenuItem.ToolTipText = "Tools";
            // 
            // runOCRToolStripMenuItem
            // 
            this.runOCRToolStripMenuItem.Name = "runOCRToolStripMenuItem";
            this.runOCRToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.runOCRToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.runOCRToolStripMenuItem.Text = "Run &OCR";
            this.runOCRToolStripMenuItem.ToolTipText = "Run OCR";
            this.runOCRToolStripMenuItem.Click += new System.EventHandler(this.runOCRToolStripMenuItem_Click);
            // 
            // oCRAllPagesToolStripMenuItem
            // 
            this.oCRAllPagesToolStripMenuItem.Name = "oCRAllPagesToolStripMenuItem";
            this.oCRAllPagesToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.oCRAllPagesToolStripMenuItem.Text = "OCR &All Pages";
            this.oCRAllPagesToolStripMenuItem.ToolTipText = "OCR All Pages";
            this.oCRAllPagesToolStripMenuItem.Click += new System.EventHandler(this.oCRAllPagesToolStripMenuItem_Click);
            // 
            // recognitionWizardToolStripMenuItem
            // 
            this.recognitionWizardToolStripMenuItem.Name = "recognitionWizardToolStripMenuItem";
            this.recognitionWizardToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.recognitionWizardToolStripMenuItem.Text = "Recognition &Wizard";
            this.recognitionWizardToolStripMenuItem.Click += new System.EventHandler(this.recognitionWizardToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(193, 6);
            // 
            // skewnessCorrectionToolStripMenuItem
            // 
            this.skewnessCorrectionToolStripMenuItem.Name = "skewnessCorrectionToolStripMenuItem";
            this.skewnessCorrectionToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.skewnessCorrectionToolStripMenuItem.Text = "S&kewness Correction";
            this.skewnessCorrectionToolStripMenuItem.Click += new System.EventHandler(this.skewnessCorrectionToolStripMenuItem_Click);
            // 
            // filtersToolStripMenuItem
            // 
            this.filtersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmnuGrayscaleImage,
            this.tsmnuThresholdImage,
            this.tsmnuInvertImage});
            this.filtersToolStripMenuItem.Name = "filtersToolStripMenuItem";
            this.filtersToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.filtersToolStripMenuItem.Text = "&Filters";
            this.filtersToolStripMenuItem.ToolTipText = "Filters";
            // 
            // tsmnuGrayscaleImage
            // 
            this.tsmnuGrayscaleImage.Name = "tsmnuGrayscaleImage";
            this.tsmnuGrayscaleImage.Size = new System.Drawing.Size(134, 22);
            this.tsmnuGrayscaleImage.Text = "&Grayscale";
            this.tsmnuGrayscaleImage.ToolTipText = "Grayscale";
            this.tsmnuGrayscaleImage.Click += new System.EventHandler(this.tsmnuGrayscaleImage_Click);
            // 
            // tsmnuThresholdImage
            // 
            this.tsmnuThresholdImage.Name = "tsmnuThresholdImage";
            this.tsmnuThresholdImage.Size = new System.Drawing.Size(134, 22);
            this.tsmnuThresholdImage.Text = "&Threshold";
            this.tsmnuThresholdImage.ToolTipText = "Threshold";
            this.tsmnuThresholdImage.Click += new System.EventHandler(this.tsmnuThresholdImage_Click);
            // 
            // tsmnuInvertImage
            // 
            this.tsmnuInvertImage.Name = "tsmnuInvertImage";
            this.tsmnuInvertImage.Size = new System.Drawing.Size(134, 22);
            this.tsmnuInvertImage.Text = "&Invert";
            this.tsmnuInvertImage.ToolTipText = "Invert";
            this.tsmnuInvertImage.Click += new System.EventHandler(this.tsmnuInvertImage_Click);
            // 
            // rotateImageToolStripMenuItem
            // 
            this.rotateImageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmnuRotate90CW,
            this.tsmnuRotate90CCW,
            this.tsmnuFlipHorizontal,
            this.tsmnuFlipVertical,
            this.tsmnuRotate180,
            this.tsmnuRotateArbitrarily});
            this.rotateImageToolStripMenuItem.Name = "rotateImageToolStripMenuItem";
            this.rotateImageToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.rotateImageToolStripMenuItem.Text = "&Rotate Image";
            // 
            // tsmnuRotate90CW
            // 
            this.tsmnuRotate90CW.Name = "tsmnuRotate90CW";
            this.tsmnuRotate90CW.Size = new System.Drawing.Size(164, 22);
            this.tsmnuRotate90CW.Text = "90° CW";
            this.tsmnuRotate90CW.Click += new System.EventHandler(this.tsmnuRotate90CW_Click);
            // 
            // tsmnuRotate90CCW
            // 
            this.tsmnuRotate90CCW.Name = "tsmnuRotate90CCW";
            this.tsmnuRotate90CCW.Size = new System.Drawing.Size(164, 22);
            this.tsmnuRotate90CCW.Text = "90° CCW";
            this.tsmnuRotate90CCW.Click += new System.EventHandler(this.tsmnuRotate90CCW_Click);
            // 
            // tsmnuFlipHorizontal
            // 
            this.tsmnuFlipHorizontal.Name = "tsmnuFlipHorizontal";
            this.tsmnuFlipHorizontal.Size = new System.Drawing.Size(164, 22);
            this.tsmnuFlipHorizontal.Text = "Flip Horizontal";
            this.tsmnuFlipHorizontal.Click += new System.EventHandler(this.tsmnuFlipHorizontal_Click);
            // 
            // tsmnuFlipVertical
            // 
            this.tsmnuFlipVertical.Name = "tsmnuFlipVertical";
            this.tsmnuFlipVertical.Size = new System.Drawing.Size(164, 22);
            this.tsmnuFlipVertical.Text = "Flip Vertical";
            this.tsmnuFlipVertical.Click += new System.EventHandler(this.tsmnuFlipVertical_Click);
            // 
            // tsmnuRotate180
            // 
            this.tsmnuRotate180.Name = "tsmnuRotate180";
            this.tsmnuRotate180.Size = new System.Drawing.Size(164, 22);
            this.tsmnuRotate180.Text = "Rotate 180° ";
            this.tsmnuRotate180.Click += new System.EventHandler(this.tsmnuRotate180_Click);
            // 
            // tsmnuRotateArbitrarily
            // 
            this.tsmnuRotateArbitrarily.Name = "tsmnuRotateArbitrarily";
            this.tsmnuRotateArbitrarily.Size = new System.Drawing.Size(164, 22);
            this.tsmnuRotateArbitrarily.Text = "Arbitrary Angle";
            this.tsmnuRotateArbitrarily.Click += new System.EventHandler(this.tsmnuRotateArbitrarily_Click);
            // 
            // toolStripSeparator22
            // 
            this.toolStripSeparator22.Name = "toolStripSeparator22";
            this.toolStripSeparator22.Size = new System.Drawing.Size(193, 6);
            // 
            // preferencesToolStripMenuItem
            // 
            this.preferencesToolStripMenuItem.Name = "preferencesToolStripMenuItem";
            this.preferencesToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.preferencesToolStripMenuItem.Text = "&Preferences";
            this.preferencesToolStripMenuItem.Click += new System.EventHandler(this.preferencesToolStripMenuItem_Click);
            // 
            // formatToolStripMenuItem
            // 
            this.formatToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.skinToolStripMenuItem,
            this.userInterfaceLanguageToolStripMenuItem,
            this.toolStripSeparator25,
            this.fontToolStripMenuItem,
            this.removeLineBreaksToolStripMenuItem});
            this.formatToolStripMenuItem.Name = "formatToolStripMenuItem";
            this.formatToolStripMenuItem.Size = new System.Drawing.Size(61, 21);
            this.formatToolStripMenuItem.Text = "&Format";
            // 
            // skinToolStripMenuItem
            // 
            this.skinToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.regularToolStripMenuItem,
            this.nightModeToolStripMenuItem});
            this.skinToolStripMenuItem.Name = "skinToolStripMenuItem";
            this.skinToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.skinToolStripMenuItem.Text = "&Theme";
            // 
            // regularToolStripMenuItem
            // 
            this.regularToolStripMenuItem.Name = "regularToolStripMenuItem";
            this.regularToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.regularToolStripMenuItem.Text = "&Regular";
            // 
            // nightModeToolStripMenuItem
            // 
            this.nightModeToolStripMenuItem.Name = "nightModeToolStripMenuItem";
            this.nightModeToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.nightModeToolStripMenuItem.Text = "&Night Mode";
            // 
            // userInterfaceLanguageToolStripMenuItem
            // 
            this.userInterfaceLanguageToolStripMenuItem.Name = "userInterfaceLanguageToolStripMenuItem";
            this.userInterfaceLanguageToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.userInterfaceLanguageToolStripMenuItem.Text = "User Interface &Language";
            // 
            // toolStripSeparator25
            // 
            this.toolStripSeparator25.Name = "toolStripSeparator25";
            this.toolStripSeparator25.Size = new System.Drawing.Size(234, 6);
            // 
            // fontToolStripMenuItem
            // 
            this.fontToolStripMenuItem.Name = "fontToolStripMenuItem";
            this.fontToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F)));
            this.fontToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.fontToolStripMenuItem.Text = "&Font...";
            this.fontToolStripMenuItem.Click += new System.EventHandler(this.fontToolStripMenuItem_Click);
            // 
            // removeLineBreaksToolStripMenuItem
            // 
            this.removeLineBreaksToolStripMenuItem.Name = "removeLineBreaksToolStripMenuItem";
            this.removeLineBreaksToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.removeLineBreaksToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.removeLineBreaksToolStripMenuItem.Text = "&Remove Line Breaks";
            this.removeLineBreaksToolStripMenuItem.Click += new System.EventHandler(this.removeLineBreaksToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpContentsToolStripMenuItem,
            this.registerTool,
            this.toolStripSeparator24,
            this.reportAProblemToolStripMenuItem1,
            this.provideASuggestionToolStripMenuItem,
            this.toolStripSeparator23,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(47, 21);
            this.helpToolStripMenuItem.Text = "&Help";
            this.helpToolStripMenuItem.ToolTipText = "Help";
            // 
            // helpContentsToolStripMenuItem
            // 
            this.helpContentsToolStripMenuItem.Name = "helpContentsToolStripMenuItem";
            this.helpContentsToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.helpContentsToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.helpContentsToolStripMenuItem.Text = "H&elp Contents";
            this.helpContentsToolStripMenuItem.ToolTipText = "Help Contents";
            this.helpContentsToolStripMenuItem.Click += new System.EventHandler(this.helpContentsToolStripMenuItem_Click);
            // 
            // registerTool
            // 
            this.registerTool.Name = "registerTool";
            this.registerTool.Size = new System.Drawing.Size(209, 22);
            this.registerTool.Text = "&Register";
            this.registerTool.Click += new System.EventHandler(this.registerTool_Click);
            // 
            // toolStripSeparator24
            // 
            this.toolStripSeparator24.Name = "toolStripSeparator24";
            this.toolStripSeparator24.Size = new System.Drawing.Size(206, 6);
            // 
            // reportAProblemToolStripMenuItem1
            // 
            this.reportAProblemToolStripMenuItem1.Name = "reportAProblemToolStripMenuItem1";
            this.reportAProblemToolStripMenuItem1.Size = new System.Drawing.Size(209, 22);
            this.reportAProblemToolStripMenuItem1.Text = "Report a &Problem...";
            // 
            // provideASuggestionToolStripMenuItem
            // 
            this.provideASuggestionToolStripMenuItem.Name = "provideASuggestionToolStripMenuItem";
            this.provideASuggestionToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.provideASuggestionToolStripMenuItem.Text = "Provide a Suggestion...";
            // 
            // toolStripSeparator23
            // 
            this.toolStripSeparator23.Name = "toolStripSeparator23";
            this.toolStripSeparator23.Size = new System.Drawing.Size(206, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.aboutToolStripMenuItem.Text = "&About NeOCR";
            this.aboutToolStripMenuItem.ToolTipText = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.GhostWhite;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnOpenFile,
            this.tsbtnOpenPDF,
            this.btnScan,
            this.toolStripSeparator1,
            this.toolStripLabel2,
            this.tscbOCRMode,
            this.toolStripSeparator7,
            this.toolStripLabel1,
            this.cbTessOCRLanguages,
            this.toolStripSeparator11,
            this.tsbtnOCR,
            this.tsbtnLayoutAnalysis,
            this.toolStripSeparator5,
            this.tsbtnBlobs,
            this.tsbtnDetectLines});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.MinimumSize = new System.Drawing.Size(0, 48);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1086, 48);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "Main Actions Toolstrip";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Margin = new System.Windows.Forms.Padding(5);
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 38);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(65, 45);
            this.toolStripLabel2.Text = "OCR Mode";
            // 
            // tscbOCRMode
            // 
            this.tscbOCRMode.AccessibleDescription = "Select OCR Mode";
            this.tscbOCRMode.AccessibleName = "OCR Mode Selection Box";
            this.tscbOCRMode.BackColor = System.Drawing.SystemColors.Window;
            this.tscbOCRMode.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.tscbOCRMode.Name = "tscbOCRMode";
            this.tscbOCRMode.Size = new System.Drawing.Size(90, 48);
            this.tscbOCRMode.Tag = "";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Margin = new System.Windows.Forms.Padding(5);
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 38);
            // 
            // cbTessOCRLanguages
            // 
            this.cbTessOCRLanguages.AccessibleDescription = "Select OCR Language";
            this.cbTessOCRLanguages.AccessibleName = "OCR Language Selection Box";
            this.cbTessOCRLanguages.BackColor = System.Drawing.SystemColors.Window;
            this.cbTessOCRLanguages.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.cbTessOCRLanguages.Name = "cbTessOCRLanguages";
            this.cbTessOCRLanguages.Size = new System.Drawing.Size(90, 48);
            this.cbTessOCRLanguages.Tag = "";
            this.cbTessOCRLanguages.ToolTipText = "OCR Languages";
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 48);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 48);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1090, 480);
            this.panel1.TabIndex = 3;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.textBox1);
            this.panel3.Controls.Add(this.txtSegmentProgress);
            this.panel3.Location = new System.Drawing.Point(0, 443);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1088, 35);
            this.panel3.TabIndex = 9;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(946, 7);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(140, 20);
            this.textBox1.TabIndex = 9;
            this.textBox1.Text = "V1.0";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSegmentProgress
            // 
            this.txtSegmentProgress.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSegmentProgress.Enabled = false;
            this.txtSegmentProgress.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSegmentProgress.Location = new System.Drawing.Point(3, 7);
            this.txtSegmentProgress.Multiline = true;
            this.txtSegmentProgress.Name = "txtSegmentProgress";
            this.txtSegmentProgress.ReadOnly = true;
            this.txtSegmentProgress.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSegmentProgress.Size = new System.Drawing.Size(937, 20);
            this.txtSegmentProgress.TabIndex = 8;
            this.txtSegmentProgress.Text = "Select an image to recognize...";
            this.txtSegmentProgress.TextChanged += new System.EventHandler(this.txtSegmentProgress_TextChanged);
            // 
            // toolStrip2
            // 
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel4,
            this.toolStripSeparator6,
            this.tsBtnSelect,
            this.tsBtnCrop,
            this.tsBtnErase,
            this.toolStripSeparator2,
            this.tsBtnZoomIn,
            this.tsBtnZoomOut,
            this.tsBtnRotateImage,
            this.toolStripSeparator4,
            this.tsbtnDeskewImage,
            this.tsbtnGrayscaleImage,
            this.tsbtnManuallyThresholdImage,
            this.tsBtnInvertImage,
            this.toolStripSeparator18});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip2.Size = new System.Drawing.Size(46, 389);
            this.toolStrip2.TabIndex = 4;
            this.toolStrip2.Text = "Image Actions Toolstrip";
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Padding = new System.Windows.Forms.Padding(0, 15, 0, 0);
            this.toolStripLabel4.Size = new System.Drawing.Size(43, 30);
            this.toolStripLabel4.Text = "TOOLS";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(43, 6);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(43, 6);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(43, 6);
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(43, 6);
            // 
            // pnlImage
            // 
            this.pnlImage.AccessibleDescription = "Image Preview Pane";
            this.pnlImage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlImage.AutoScroll = true;
            this.pnlImage.BackColor = System.Drawing.Color.DimGray;
            this.pnlImage.Controls.Add(this.inputPicBox);
            this.pnlImage.Location = new System.Drawing.Point(53, 36);
            this.pnlImage.Name = "pnlImage";
            this.pnlImage.Padding = new System.Windows.Forms.Padding(5);
            this.pnlImage.Size = new System.Drawing.Size(426, 350);
            this.pnlImage.TabIndex = 5;
            this.pnlImage.Resize += new System.EventHandler(this.pnlImage_Resize);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.splitContainer2);
            this.panel2.Controls.Add(this.splitter1);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(956, 389);
            this.panel2.TabIndex = 8;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.AccessibleDescription = "Image Preview Panel";
            this.splitContainer2.Panel1.Controls.Add(this.toolStrip5);
            this.splitContainer2.Panel1.Controls.Add(this.pnlImage);
            this.splitContainer2.Panel1.Controls.Add(this.toolStrip2);
            this.splitContainer2.Panel1MinSize = 5;
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.AccessibleDescription = "Display Recognized Text Panel";
            this.splitContainer2.Panel2.Controls.Add(this.pnlOCRedText);
            this.splitContainer2.Panel2MinSize = 5;
            this.splitContainer2.Size = new System.Drawing.Size(953, 389);
            this.splitContainer2.SplitterDistance = 482;
            this.splitContainer2.TabIndex = 7;
            // 
            // toolStrip5
            // 
            this.toolStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel3,
            this.toolStripSeparator21,
            this.tsbtnReadImage,
            this.tsbtnSaveText});
            this.toolStrip5.Location = new System.Drawing.Point(46, 0);
            this.toolStrip5.Name = "toolStrip5";
            this.toolStrip5.Size = new System.Drawing.Size(436, 33);
            this.toolStrip5.TabIndex = 6;
            this.toolStrip5.Text = "toolStrip5";
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel3.Margin = new System.Windows.Forms.Padding(5);
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(44, 23);
            this.toolStripLabel3.Text = "IMAGE";
            // 
            // toolStripSeparator21
            // 
            this.toolStripSeparator21.Name = "toolStripSeparator21";
            this.toolStripSeparator21.Size = new System.Drawing.Size(6, 33);
            // 
            // pnlOCRedText
            // 
            this.pnlOCRedText.AutoScroll = true;
            this.pnlOCRedText.BackColor = System.Drawing.SystemColors.Control;
            this.pnlOCRedText.Controls.Add(this.rtbOCRedText);
            this.pnlOCRedText.Controls.Add(this.toolStrip6);
            this.pnlOCRedText.Controls.Add(this.toolStrip3);
            this.pnlOCRedText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlOCRedText.Location = new System.Drawing.Point(0, 0);
            this.pnlOCRedText.Name = "pnlOCRedText";
            this.pnlOCRedText.Size = new System.Drawing.Size(467, 389);
            this.pnlOCRedText.TabIndex = 0;
            // 
            // toolStrip6
            // 
            this.toolStrip6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel6,
            this.toolStripSeparator20,
            this.toolStripLabel7,
            this.tscbFonts,
            this.tscbFontSize,
            this.tsbtnIncreaseFontSize,
            this.tsbtnDecreaseFontSize});
            this.toolStrip6.Location = new System.Drawing.Point(44, 0);
            this.toolStrip6.Name = "toolStrip6";
            this.toolStrip6.Size = new System.Drawing.Size(423, 25);
            this.toolStrip6.TabIndex = 8;
            this.toolStrip6.Text = "OCR Result Textbox Actions Toolstrip";
            this.toolStrip6.Paint += new System.Windows.Forms.PaintEventHandler(this.toolStrip6_Paint);
            // 
            // toolStripLabel6
            // 
            this.toolStripLabel6.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel6.Margin = new System.Windows.Forms.Padding(5);
            this.toolStripLabel6.Name = "toolStripLabel6";
            this.toolStripLabel6.Size = new System.Drawing.Size(34, 15);
            this.toolStripLabel6.Text = "TEXT";
            // 
            // toolStripSeparator20
            // 
            this.toolStripSeparator20.Name = "toolStripSeparator20";
            this.toolStripSeparator20.Size = new System.Drawing.Size(6, 25);
            // 
            // tscbFonts
            // 
            this.tscbFonts.AccessibleDescription = "Font Selection Combobox";
            this.tscbFonts.DropDownWidth = 70;
            this.tscbFonts.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.tscbFonts.Name = "tscbFonts";
            this.tscbFonts.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.tscbFonts.Size = new System.Drawing.Size(75, 25);
            this.tscbFonts.SelectedIndexChanged += new System.EventHandler(this.tscbFonts_SelectedIndexChanged);
            // 
            // tscbFontSize
            // 
            this.tscbFontSize.AccessibleDescription = "Font Size Selection Combobox";
            this.tscbFontSize.DropDownWidth = 75;
            this.tscbFontSize.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.tscbFontSize.Name = "tscbFontSize";
            this.tscbFontSize.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.tscbFontSize.Size = new System.Drawing.Size(75, 25);
            this.tscbFontSize.SelectedIndexChanged += new System.EventHandler(this.tscbFontSize_SelectedIndexChanged);
            // 
            // toolStrip3
            // 
            this.toolStrip3.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip3.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel5,
            this.toolStripSeparator19,
            this.tsbtnClearText,
            this.toolStripSeparator8,
            this.toolStripButton1,
            this.toolStripSeparator10,
            this.tsbtnExportAsPlainText,
            this.tsbtnExportToWordDoc,
            this.toolStripSeparator9});
            this.toolStrip3.Location = new System.Drawing.Point(0, 0);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip3.Size = new System.Drawing.Size(44, 389);
            this.toolStrip3.TabIndex = 7;
            this.toolStrip3.Text = "Text Actions Toolstrip";
            // 
            // toolStripLabel5
            // 
            this.toolStripLabel5.Name = "toolStripLabel5";
            this.toolStripLabel5.Padding = new System.Windows.Forms.Padding(0, 15, 0, 0);
            this.toolStripLabel5.Size = new System.Drawing.Size(41, 30);
            this.toolStripLabel5.Text = "TOOLS";
            // 
            // toolStripSeparator19
            // 
            this.toolStripSeparator19.Name = "toolStripSeparator19";
            this.toolStripSeparator19.Size = new System.Drawing.Size(41, 6);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(41, 6);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(41, 6);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(41, 6);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 389);
            this.splitter1.TabIndex = 6;
            this.splitter1.TabStop = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Location = new System.Drawing.Point(2, 77);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AccessibleName = "Image List Panel";
            this.splitContainer1.Panel1.Controls.Add(this.toolStrip4);
            this.splitContainer1.Panel1.Controls.Add(this.dgvPages);
            this.splitContainer1.Panel1MinSize = 5;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.panel2);
            this.splitContainer1.Panel2MinSize = 5;
            this.splitContainer1.Size = new System.Drawing.Size(1088, 391);
            this.splitContainer1.SplitterDistance = 126;
            this.splitContainer1.TabIndex = 9;
            // 
            // toolStrip4
            // 
            this.toolStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtnListImageThumbnail,
            this.tsbtnListImageNames,
            this.toolStripSeparator17});
            this.toolStrip4.Location = new System.Drawing.Point(0, 0);
            this.toolStrip4.Name = "toolStrip4";
            this.toolStrip4.Size = new System.Drawing.Size(124, 25);
            this.toolStrip4.TabIndex = 4;
            this.toolStrip4.Text = "Image List Pane Toostrip";
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(6, 25);
            // 
            // dgvPages
            // 
            this.dgvPages.AccessibleDescription = "Image List/Preview Table";
            this.dgvPages.AllowUserToAddRows = false;
            this.dgvPages.AllowUserToDeleteRows = false;
            this.dgvPages.AllowUserToResizeColumns = false;
            this.dgvPages.AllowUserToResizeRows = false;
            this.dgvPages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPages.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPages.BackgroundColor = System.Drawing.Color.White;
            this.dgvPages.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvPages.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            this.dgvPages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPages.ColumnHeadersVisible = false;
            this.dgvPages.Location = new System.Drawing.Point(0, 30);
            this.dgvPages.Margin = new System.Windows.Forms.Padding(5);
            this.dgvPages.Name = "dgvPages";
            this.dgvPages.ReadOnly = true;
            this.dgvPages.RowHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvPages.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPages.Size = new System.Drawing.Size(124, 359);
            this.dgvPages.TabIndex = 3;
            this.dgvPages.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPages_CellClick);
            this.dgvPages.SelectionChanged += new System.EventHandler(this.dgvPages_SelectionChanged);
            // 
            // backgroundWorkerPDFToImage
            // 
            this.backgroundWorkerPDFToImage.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerPDFToImage_DoWork);
            this.backgroundWorkerPDFToImage.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerPDFToImage_RunWorkerCompleted);
            // 
            // backgroundWorkerOCRUsingTesseract
            // 
            this.backgroundWorkerOCRUsingTesseract.WorkerReportsProgress = true;
            this.backgroundWorkerOCRUsingTesseract.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerOCRUsingTesseract_DoWork);
            this.backgroundWorkerOCRUsingTesseract.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerOCRUsingTesseract_ProgressChanged);
            this.backgroundWorkerOCRUsingTesseract.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerOCRUsingTesseract_RunWorkerCompleted);
            // 
            // contextMenuStripOCRedText
            // 
            this.contextMenuStripOCRedText.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.selectAllToolStripMenuItem,
            this.copyAllTextToClipboardToolStripMenuItem,
            this.toolStripSeparator16,
            this.clearContentToolStripMenuItem,
            this.exportAsPlainTextToolStripMenuItem1,
            this.exportAsMicrosoftWordDocumentToolStripMenuItem});
            this.contextMenuStripOCRedText.Name = "contextMenuStripOCRedText";
            this.contextMenuStripOCRedText.Size = new System.Drawing.Size(267, 186);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.selectAllToolStripMenuItem.Text = "Select All";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // copyAllTextToClipboardToolStripMenuItem
            // 
            this.copyAllTextToClipboardToolStripMenuItem.Name = "copyAllTextToClipboardToolStripMenuItem";
            this.copyAllTextToClipboardToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.copyAllTextToClipboardToolStripMenuItem.Text = "Copy All Text to Clipboard";
            this.copyAllTextToClipboardToolStripMenuItem.Click += new System.EventHandler(this.copyAllTextToClipboardToolStripMenuItem_Click);
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(263, 6);
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.toolStrip1);
            this.panel4.Location = new System.Drawing.Point(2, 25);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1088, 49);
            this.panel4.TabIndex = 10;
            // 
            // backgroundWorkerOCRAllPagesUsingTesseract
            // 
            this.backgroundWorkerOCRAllPagesUsingTesseract.WorkerReportsProgress = true;
            this.backgroundWorkerOCRAllPagesUsingTesseract.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerOCRAllPagesUsingTesseract_DoWork);
            this.backgroundWorkerOCRAllPagesUsingTesseract.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerOCRAllPagesUsingTesseract_ProgressChanged);
            this.backgroundWorkerOCRAllPagesUsingTesseract.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerOCRAllPagesUsingTesseract_RunWorkerCompleted);
            // 
            // backgroundWorkerRecognizePDF
            // 
            this.backgroundWorkerRecognizePDF.WorkerReportsProgress = true;
            this.backgroundWorkerRecognizePDF.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerRecognizePDF_DoWork);
            this.backgroundWorkerRecognizePDF.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerRecognizePDF_RunWorkerCompleted);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Image = global::NeOCR.Properties.Resources.if_image_272710;
            this.btnOpenFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(76, 45);
            this.btnOpenFile.Text = "Open Image";
            this.btnOpenFile.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnOpenFile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnOpenFile.ToolTipText = "Open Image";
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // tsbtnOpenPDF
            // 
            this.tsbtnOpenPDF.Image = global::NeOCR.Properties.Resources.application_pdf;
            this.tsbtnOpenPDF.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnOpenPDF.Name = "tsbtnOpenPDF";
            this.tsbtnOpenPDF.Size = new System.Drawing.Size(64, 45);
            this.tsbtnOpenPDF.Text = "Open PDF";
            this.tsbtnOpenPDF.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnOpenPDF.ToolTipText = "Open PDF File";
            this.tsbtnOpenPDF.Click += new System.EventHandler(this.tsbtnOpenPDF_Click);
            // 
            // btnScan
            // 
            this.btnScan.Image = ((System.Drawing.Image)(resources.GetObject("btnScan.Image")));
            this.btnScan.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(36, 45);
            this.btnScan.Text = "Scan";
            this.btnScan.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnScan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Image = global::NeOCR.Properties.Resources.icon350x3501;
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(110, 45);
            this.toolStripLabel1.Text = "OCR Language";
            // 
            // tsbtnOCR
            // 
            this.tsbtnOCR.AccessibleDescription = "Perform OCR";
            this.tsbtnOCR.AccessibleName = "OCR";
            this.tsbtnOCR.Image = global::NeOCR.Properties.Resources.OCR_icon;
            this.tsbtnOCR.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnOCR.Name = "tsbtnOCR";
            this.tsbtnOCR.Size = new System.Drawing.Size(35, 45);
            this.tsbtnOCR.Text = "OCR";
            this.tsbtnOCR.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnOCR.Click += new System.EventHandler(this.tsbtnOCR_Click);
            // 
            // tsbtnLayoutAnalysis
            // 
            this.tsbtnLayoutAnalysis.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnLayoutAnalysis.Image")));
            this.tsbtnLayoutAnalysis.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnLayoutAnalysis.Name = "tsbtnLayoutAnalysis";
            this.tsbtnLayoutAnalysis.Size = new System.Drawing.Size(91, 45);
            this.tsbtnLayoutAnalysis.Text = "Analyze Layout";
            this.tsbtnLayoutAnalysis.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnLayoutAnalysis.Click += new System.EventHandler(this.tsbtnLayoutAnalysis_Click);
            // 
            // tsbtnBlobs
            // 
            this.tsbtnBlobs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnBlobs.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnBlobs.Image")));
            this.tsbtnBlobs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnBlobs.Name = "tsbtnBlobs";
            this.tsbtnBlobs.Size = new System.Drawing.Size(28, 45);
            this.tsbtnBlobs.Text = "toolStripButton2";
            this.tsbtnBlobs.Click += new System.EventHandler(this.tsbtnBlobs_Click);
            // 
            // tsbtnDetectLines
            // 
            this.tsbtnDetectLines.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnDetectLines.Image")));
            this.tsbtnDetectLines.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnDetectLines.Name = "tsbtnDetectLines";
            this.tsbtnDetectLines.Size = new System.Drawing.Size(38, 45);
            this.tsbtnDetectLines.Text = "Lines";
            this.tsbtnDetectLines.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnDetectLines.Click += new System.EventHandler(this.tsbtnDetectLines_Click);
            // 
            // tsbtnListImageThumbnail
            // 
            this.tsbtnListImageThumbnail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnListImageThumbnail.Image = global::NeOCR.Properties.Resources.if_interface_50_809253__1_;
            this.tsbtnListImageThumbnail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnListImageThumbnail.Name = "tsbtnListImageThumbnail";
            this.tsbtnListImageThumbnail.Size = new System.Drawing.Size(23, 22);
            this.tsbtnListImageThumbnail.Text = "Thumbnail Preview of Selected Items";
            this.tsbtnListImageThumbnail.Click += new System.EventHandler(this.tsbtnListImageThumbnail_Click);
            // 
            // tsbtnListImageNames
            // 
            this.tsbtnListImageNames.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnListImageNames.Image = global::NeOCR.Properties.Resources.if_list_173044;
            this.tsbtnListImageNames.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnListImageNames.Name = "tsbtnListImageNames";
            this.tsbtnListImageNames.Size = new System.Drawing.Size(23, 22);
            this.tsbtnListImageNames.Text = "List Preview of Selected Items";
            this.tsbtnListImageNames.Click += new System.EventHandler(this.tsbtnListImageNames_Click);
            // 
            // tsbtnReadImage
            // 
            this.tsbtnReadImage.Image = global::NeOCR.Properties.Resources.if_edit_find_118922;
            this.tsbtnReadImage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnReadImage.Name = "tsbtnReadImage";
            this.tsbtnReadImage.Padding = new System.Windows.Forms.Padding(5);
            this.tsbtnReadImage.Size = new System.Drawing.Size(99, 30);
            this.tsbtnReadImage.Text = "Read Image";
            this.tsbtnReadImage.Click += new System.EventHandler(this.tsbtnReadImage_Click);
            // 
            // tsbtnSaveText
            // 
            this.tsbtnSaveText.Image = global::NeOCR.Properties.Resources.if_page_save_36261;
            this.tsbtnSaveText.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnSaveText.Name = "tsbtnSaveText";
            this.tsbtnSaveText.Padding = new System.Windows.Forms.Padding(5);
            this.tsbtnSaveText.Size = new System.Drawing.Size(85, 30);
            this.tsbtnSaveText.Text = "Save Text";
            this.tsbtnSaveText.Click += new System.EventHandler(this.tsbtnSaveText_Click);
            // 
            // inputPicBox
            // 
            this.inputPicBox.AccessibleDescription = "Picture Box";
            this.inputPicBox.BackColor = System.Drawing.Color.LightGray;
            this.inputPicBox.ImageLocation = "";
            this.inputPicBox.Location = new System.Drawing.Point(10, 10);
            this.inputPicBox.Margin = new System.Windows.Forms.Padding(5);
            this.inputPicBox.Name = "inputPicBox";
            this.inputPicBox.Padding = new System.Windows.Forms.Padding(5);
            this.inputPicBox.Size = new System.Drawing.Size(298, 320);
            this.inputPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.inputPicBox.TabIndex = 0;
            this.inputPicBox.TabStop = false;
            this.inputPicBox.ClientSizeChanged += new System.EventHandler(this.inputPicBox_ClientSizeChanged);
            this.inputPicBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.inputPicBox_MouseDown);
            this.inputPicBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.inputPicBox_MouseMove);
            this.inputPicBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.inputPicBox_MouseUp);
            this.inputPicBox.Resize += new System.EventHandler(this.inputPicBox_Resize);
            // 
            // tsBtnSelect
            // 
            this.tsBtnSelect.CheckOnClick = true;
            this.tsBtnSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnSelect.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnSelect.Image")));
            this.tsBtnSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnSelect.Name = "tsBtnSelect";
            this.tsBtnSelect.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.tsBtnSelect.Size = new System.Drawing.Size(43, 29);
            this.tsBtnSelect.Text = "Select";
            this.tsBtnSelect.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.tsBtnSelect.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsBtnSelect.Click += new System.EventHandler(this.tsBtnSelect_Click);
            // 
            // tsBtnCrop
            // 
            this.tsBtnCrop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnCrop.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnCrop.Image")));
            this.tsBtnCrop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnCrop.Name = "tsBtnCrop";
            this.tsBtnCrop.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.tsBtnCrop.Size = new System.Drawing.Size(43, 29);
            this.tsBtnCrop.Text = "Crop";
            this.tsBtnCrop.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.tsBtnCrop.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsBtnCrop.Click += new System.EventHandler(this.tsBtnCrop_Click);
            // 
            // tsBtnErase
            // 
            this.tsBtnErase.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnErase.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnErase.Image")));
            this.tsBtnErase.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnErase.Name = "tsBtnErase";
            this.tsBtnErase.Size = new System.Drawing.Size(43, 24);
            this.tsBtnErase.Text = "Eraser";
            this.tsBtnErase.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.tsBtnErase.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsBtnErase.Click += new System.EventHandler(this.tsBtnErase_Click);
            // 
            // tsBtnZoomIn
            // 
            this.tsBtnZoomIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnZoomIn.Image")));
            this.tsBtnZoomIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnZoomIn.Name = "tsBtnZoomIn";
            this.tsBtnZoomIn.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.tsBtnZoomIn.Size = new System.Drawing.Size(43, 29);
            this.tsBtnZoomIn.Text = "Zoom In";
            this.tsBtnZoomIn.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.tsBtnZoomIn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsBtnZoomIn.Click += new System.EventHandler(this.tsBtnZoomIn_Click);
            // 
            // tsBtnZoomOut
            // 
            this.tsBtnZoomOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnZoomOut.Image")));
            this.tsBtnZoomOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnZoomOut.Name = "tsBtnZoomOut";
            this.tsBtnZoomOut.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.tsBtnZoomOut.Size = new System.Drawing.Size(43, 29);
            this.tsBtnZoomOut.Text = "Zoom Out";
            this.tsBtnZoomOut.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.tsBtnZoomOut.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsBtnZoomOut.Click += new System.EventHandler(this.tsBtnZoomOut_Click);
            // 
            // tsBtnRotateImage
            // 
            this.tsBtnRotateImage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnRotateImage.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolMnuRotateCW,
            this.toolMnuRotateCCW,
            this.toolMnuFlipHorizontal,
            this.toolMnuFlipVertical,
            this.toolMnuRotate180,
            this.toolMnuRotateArbitrary});
            this.tsBtnRotateImage.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnRotateImage.Image")));
            this.tsBtnRotateImage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnRotateImage.Name = "tsBtnRotateImage";
            this.tsBtnRotateImage.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.tsBtnRotateImage.Size = new System.Drawing.Size(43, 24);
            this.tsBtnRotateImage.Text = "Rotate Image";
            this.tsBtnRotateImage.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolMnuRotateCW
            // 
            this.toolMnuRotateCW.Image = ((System.Drawing.Image)(resources.GetObject("toolMnuRotateCW.Image")));
            this.toolMnuRotateCW.Name = "toolMnuRotateCW";
            this.toolMnuRotateCW.Size = new System.Drawing.Size(151, 22);
            this.toolMnuRotateCW.Text = "90° CW";
            this.toolMnuRotateCW.Click += new System.EventHandler(this.toolMnuRotateCW_Click);
            // 
            // toolMnuRotateCCW
            // 
            this.toolMnuRotateCCW.Image = ((System.Drawing.Image)(resources.GetObject("toolMnuRotateCCW.Image")));
            this.toolMnuRotateCCW.Name = "toolMnuRotateCCW";
            this.toolMnuRotateCCW.Size = new System.Drawing.Size(151, 22);
            this.toolMnuRotateCCW.Text = "90° CCW";
            this.toolMnuRotateCCW.Click += new System.EventHandler(this.toolMnuRotateCCW_Click);
            // 
            // toolMnuFlipHorizontal
            // 
            this.toolMnuFlipHorizontal.Image = ((System.Drawing.Image)(resources.GetObject("toolMnuFlipHorizontal.Image")));
            this.toolMnuFlipHorizontal.Name = "toolMnuFlipHorizontal";
            this.toolMnuFlipHorizontal.Size = new System.Drawing.Size(151, 22);
            this.toolMnuFlipHorizontal.Text = "Flip Horizontal";
            this.toolMnuFlipHorizontal.Click += new System.EventHandler(this.toolMnuFlipHorizontal_Click);
            // 
            // toolMnuFlipVertical
            // 
            this.toolMnuFlipVertical.Image = ((System.Drawing.Image)(resources.GetObject("toolMnuFlipVertical.Image")));
            this.toolMnuFlipVertical.Name = "toolMnuFlipVertical";
            this.toolMnuFlipVertical.Size = new System.Drawing.Size(151, 22);
            this.toolMnuFlipVertical.Text = "Flip Vertical";
            this.toolMnuFlipVertical.Click += new System.EventHandler(this.toolMnuFlipVertical_Click);
            // 
            // toolMnuRotate180
            // 
            this.toolMnuRotate180.Image = ((System.Drawing.Image)(resources.GetObject("toolMnuRotate180.Image")));
            this.toolMnuRotate180.Name = "toolMnuRotate180";
            this.toolMnuRotate180.Size = new System.Drawing.Size(151, 22);
            this.toolMnuRotate180.Text = "Rotate 180°";
            this.toolMnuRotate180.Click += new System.EventHandler(this.toolMnuRotate180_Click);
            // 
            // toolMnuRotateArbitrary
            // 
            this.toolMnuRotateArbitrary.Image = ((System.Drawing.Image)(resources.GetObject("toolMnuRotateArbitrary.Image")));
            this.toolMnuRotateArbitrary.Name = "toolMnuRotateArbitrary";
            this.toolMnuRotateArbitrary.Size = new System.Drawing.Size(151, 22);
            this.toolMnuRotateArbitrary.Text = "Arbitrary";
            this.toolMnuRotateArbitrary.Click += new System.EventHandler(this.toolMnuRotateArbitrary_Click);
            // 
            // tsbtnDeskewImage
            // 
            this.tsbtnDeskewImage.AccessibleName = "Deskew Image Button";
            this.tsbtnDeskewImage.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnDeskewImage.Image")));
            this.tsbtnDeskewImage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnDeskewImage.Name = "tsbtnDeskewImage";
            this.tsbtnDeskewImage.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.tsbtnDeskewImage.Size = new System.Drawing.Size(43, 29);
            this.tsbtnDeskewImage.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnDeskewImage.ToolTipText = "Deskew Image";
            this.tsbtnDeskewImage.Click += new System.EventHandler(this.tsbtnDeskewImage_Click);
            // 
            // tsbtnGrayscaleImage
            // 
            this.tsbtnGrayscaleImage.AccessibleName = "Grayscale Image Button";
            this.tsbtnGrayscaleImage.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnGrayscaleImage.Image")));
            this.tsbtnGrayscaleImage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnGrayscaleImage.Name = "tsbtnGrayscaleImage";
            this.tsbtnGrayscaleImage.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.tsbtnGrayscaleImage.Size = new System.Drawing.Size(43, 29);
            this.tsbtnGrayscaleImage.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnGrayscaleImage.ToolTipText = "Create Grayscale Image";
            this.tsbtnGrayscaleImage.Click += new System.EventHandler(this.tsbtnGrayscaleImage_Click);
            // 
            // tsbtnManuallyThresholdImage
            // 
            this.tsbtnManuallyThresholdImage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnManuallyThresholdImage.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnManuallyThresholdImage.Image")));
            this.tsbtnManuallyThresholdImage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnManuallyThresholdImage.Name = "tsbtnManuallyThresholdImage";
            this.tsbtnManuallyThresholdImage.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.tsbtnManuallyThresholdImage.Size = new System.Drawing.Size(43, 29);
            this.tsbtnManuallyThresholdImage.Text = "Threshold Image Manually";
            this.tsbtnManuallyThresholdImage.Click += new System.EventHandler(this.tsbtnManuallyThresholdImage_Click);
            // 
            // tsBtnInvertImage
            // 
            this.tsBtnInvertImage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnInvertImage.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnInvertImage.Image")));
            this.tsBtnInvertImage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnInvertImage.Name = "tsBtnInvertImage";
            this.tsBtnInvertImage.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.tsBtnInvertImage.Size = new System.Drawing.Size(43, 29);
            this.tsBtnInvertImage.Text = "Invert Image";
            this.tsBtnInvertImage.Click += new System.EventHandler(this.tsBtnInvertImage_Click);
            // 
            // toolStripLabel7
            // 
            this.toolStripLabel7.Image = global::NeOCR.Properties.Resources.if_font_1608890;
            this.toolStripLabel7.Name = "toolStripLabel7";
            this.toolStripLabel7.Size = new System.Drawing.Size(16, 22);
            // 
            // tsbtnIncreaseFontSize
            // 
            this.tsbtnIncreaseFontSize.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnIncreaseFontSize.Image = global::NeOCR.Properties.Resources.icons8_Increase_Font_26;
            this.tsbtnIncreaseFontSize.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnIncreaseFontSize.Name = "tsbtnIncreaseFontSize";
            this.tsbtnIncreaseFontSize.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.tsbtnIncreaseFontSize.Size = new System.Drawing.Size(30, 22);
            this.tsbtnIncreaseFontSize.Text = "Font Size Increase Button";
            this.tsbtnIncreaseFontSize.Click += new System.EventHandler(this.tsbtnIncreaseFontSize_Click);
            // 
            // tsbtnDecreaseFontSize
            // 
            this.tsbtnDecreaseFontSize.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnDecreaseFontSize.Image = global::NeOCR.Properties.Resources.icons8_Decrease_Font_26;
            this.tsbtnDecreaseFontSize.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnDecreaseFontSize.Name = "tsbtnDecreaseFontSize";
            this.tsbtnDecreaseFontSize.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.tsbtnDecreaseFontSize.Size = new System.Drawing.Size(30, 22);
            this.tsbtnDecreaseFontSize.Text = "Font Size Decrease Button";
            this.tsbtnDecreaseFontSize.Click += new System.EventHandler(this.tsbtnDecreaseFontSize_Click);
            // 
            // tsbtnClearText
            // 
            this.tsbtnClearText.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnClearText.Image = global::NeOCR.Properties.Resources.if_Gnome_Edit_Clear_64_55586;
            this.tsbtnClearText.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnClearText.Margin = new System.Windows.Forms.Padding(5);
            this.tsbtnClearText.Name = "tsbtnClearText";
            this.tsbtnClearText.Size = new System.Drawing.Size(31, 24);
            this.tsbtnClearText.Text = "Clear Text";
            this.tsbtnClearText.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.tsbtnClearText.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnClearText.Click += new System.EventHandler(this.tsbtnClearText_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::NeOCR.Properties.Resources.edit_copy;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Margin = new System.Windows.Forms.Padding(5);
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(31, 24);
            this.toolStripButton1.Text = "Copy to Clipboard";
            this.toolStripButton1.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // tsbtnExportAsPlainText
            // 
            this.tsbtnExportAsPlainText.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnExportAsPlainText.Image = global::NeOCR.Properties.Resources.doc_export;
            this.tsbtnExportAsPlainText.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnExportAsPlainText.Margin = new System.Windows.Forms.Padding(5);
            this.tsbtnExportAsPlainText.Name = "tsbtnExportAsPlainText";
            this.tsbtnExportAsPlainText.Size = new System.Drawing.Size(31, 24);
            this.tsbtnExportAsPlainText.Text = "Export as Plain Text";
            this.tsbtnExportAsPlainText.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.tsbtnExportAsPlainText.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnExportAsPlainText.Click += new System.EventHandler(this.tsbtnExportAsPlainText_Click);
            // 
            // tsbtnExportToWordDoc
            // 
            this.tsbtnExportToWordDoc.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnExportToWordDoc.Image = global::NeOCR.Properties.Resources.word_2013;
            this.tsbtnExportToWordDoc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnExportToWordDoc.Margin = new System.Windows.Forms.Padding(5);
            this.tsbtnExportToWordDoc.Name = "tsbtnExportToWordDoc";
            this.tsbtnExportToWordDoc.Size = new System.Drawing.Size(31, 24);
            this.tsbtnExportToWordDoc.Text = "Export as Microsoft Word Document";
            this.tsbtnExportToWordDoc.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.tsbtnExportToWordDoc.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnExportToWordDoc.Click += new System.EventHandler(this.tsbtnExportToWordDoc_Click);
            // 
            // mnuOpenImage
            // 
            this.mnuOpenImage.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmnuOpenImageFiles,
            this.tsmnuOpenPDFFile});
            this.mnuOpenImage.Image = global::NeOCR.Properties.Resources.if_Folder___Default_32040;
            this.mnuOpenImage.Name = "mnuOpenImage";
            this.mnuOpenImage.Size = new System.Drawing.Size(209, 22);
            this.mnuOpenImage.Text = "&Open";
            this.mnuOpenImage.ToolTipText = "Open File";
            this.mnuOpenImage.Click += new System.EventHandler(this.mnuOpenImage_Click);
            // 
            // tsmnuOpenImageFiles
            // 
            this.tsmnuOpenImageFiles.Image = global::NeOCR.Properties.Resources.image;
            this.tsmnuOpenImageFiles.Name = "tsmnuOpenImageFiles";
            this.tsmnuOpenImageFiles.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.tsmnuOpenImageFiles.Size = new System.Drawing.Size(202, 22);
            this.tsmnuOpenImageFiles.Text = "Image Files";
            this.tsmnuOpenImageFiles.Click += new System.EventHandler(this.tsmnuOpenImageFiles_Click);
            // 
            // tsmnuOpenPDFFile
            // 
            this.tsmnuOpenPDFFile.Image = global::NeOCR.Properties.Resources.pdf;
            this.tsmnuOpenPDFFile.Name = "tsmnuOpenPDFFile";
            this.tsmnuOpenPDFFile.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.O)));
            this.tsmnuOpenPDFFile.Size = new System.Drawing.Size(202, 22);
            this.tsmnuOpenPDFFile.Text = "PDF File";
            this.tsmnuOpenPDFFile.Click += new System.EventHandler(this.tsmnuOpenPDFFile_Click);
            // 
            // scanToolStripMenuItem
            // 
            this.scanToolStripMenuItem.Image = global::NeOCR.Properties.Resources.scanner_318_10055;
            this.scanToolStripMenuItem.Name = "scanToolStripMenuItem";
            this.scanToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F9;
            this.scanToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.scanToolStripMenuItem.Text = "S&can";
            this.scanToolStripMenuItem.ToolTipText = "Scan Document";
            this.scanToolStripMenuItem.Click += new System.EventHandler(this.scanToolStripMenuItem_Click);
            // 
            // saveOutputToolStripMenuItem
            // 
            this.saveOutputToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportAsPlainTextToolStripMenuItem,
            this.exportAsMicrosoftWordFileToolStripMenuItem});
            this.saveOutputToolStripMenuItem.Image = global::NeOCR.Properties.Resources.if_page_save_36261;
            this.saveOutputToolStripMenuItem.Name = "saveOutputToolStripMenuItem";
            this.saveOutputToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.saveOutputToolStripMenuItem.Text = "&Save Document As";
            this.saveOutputToolStripMenuItem.ToolTipText = "Save Recognized Text";
            // 
            // exportAsPlainTextToolStripMenuItem
            // 
            this.exportAsPlainTextToolStripMenuItem.AccessibleDescription = "Save as Plain Text";
            this.exportAsPlainTextToolStripMenuItem.Image = global::NeOCR.Properties.Resources.doc_export;
            this.exportAsPlainTextToolStripMenuItem.Name = "exportAsPlainTextToolStripMenuItem";
            this.exportAsPlainTextToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.exportAsPlainTextToolStripMenuItem.Text = "Save as Plain T&ext";
            this.exportAsPlainTextToolStripMenuItem.Click += new System.EventHandler(this.exportAsPlainTextToolStripMenuItem_Click);
            // 
            // exportAsMicrosoftWordFileToolStripMenuItem
            // 
            this.exportAsMicrosoftWordFileToolStripMenuItem.AccessibleDescription = "Save as Microsoft Word File";
            this.exportAsMicrosoftWordFileToolStripMenuItem.Image = global::NeOCR.Properties.Resources.word_2013;
            this.exportAsMicrosoftWordFileToolStripMenuItem.Name = "exportAsMicrosoftWordFileToolStripMenuItem";
            this.exportAsMicrosoftWordFileToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.exportAsMicrosoftWordFileToolStripMenuItem.Text = "Save as Microsoft W&ord File";
            this.exportAsMicrosoftWordFileToolStripMenuItem.Click += new System.EventHandler(this.exportAsMicrosoftWordFileToolStripMenuItem_Click);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Image = global::NeOCR.Properties.Resources.if_content_cut_326596;
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.cutToolStripMenuItem.Text = "Cut";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Image = global::NeOCR.Properties.Resources.edit_copy;
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Image = global::NeOCR.Properties.Resources.if_Paste_1493290;
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.pasteToolStripMenuItem.Text = "Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // clearContentToolStripMenuItem
            // 
            this.clearContentToolStripMenuItem.Image = global::NeOCR.Properties.Resources.if_media_playlist_clear_9410;
            this.clearContentToolStripMenuItem.Name = "clearContentToolStripMenuItem";
            this.clearContentToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.clearContentToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.clearContentToolStripMenuItem.Text = "C&lear Content";
            this.clearContentToolStripMenuItem.Click += new System.EventHandler(this.clearContentToolStripMenuItem_Click);
            // 
            // exportAsPlainTextToolStripMenuItem1
            // 
            this.exportAsPlainTextToolStripMenuItem1.Image = global::NeOCR.Properties.Resources.doc_export;
            this.exportAsPlainTextToolStripMenuItem1.Name = "exportAsPlainTextToolStripMenuItem1";
            this.exportAsPlainTextToolStripMenuItem1.Size = new System.Drawing.Size(266, 22);
            this.exportAsPlainTextToolStripMenuItem1.Text = "Export as Plain &Text";
            this.exportAsPlainTextToolStripMenuItem1.Click += new System.EventHandler(this.exportAsPlainTextToolStripMenuItem1_Click);
            // 
            // exportAsMicrosoftWordDocumentToolStripMenuItem
            // 
            this.exportAsMicrosoftWordDocumentToolStripMenuItem.Image = global::NeOCR.Properties.Resources.word_2013;
            this.exportAsMicrosoftWordDocumentToolStripMenuItem.Name = "exportAsMicrosoftWordDocumentToolStripMenuItem";
            this.exportAsMicrosoftWordDocumentToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.exportAsMicrosoftWordDocumentToolStripMenuItem.Text = "Export as Microsoft &Word Document";
            this.exportAsMicrosoftWordDocumentToolStripMenuItem.Click += new System.EventHandler(this.exportAsMicrosoftWordDocumentToolStripMenuItem_Click);
            // 
            // rtbOCRedText
            // 
            this.rtbOCRedText.AccessibleDescription = "Recognized Text display box";
            this.rtbOCRedText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbOCRedText.Location = new System.Drawing.Point(47, 28);
            this.rtbOCRedText.Name = "rtbOCRedText";
            this.rtbOCRedText.Size = new System.Drawing.Size(417, 358);
            this.rtbOCRedText.TabIndex = 9;
            this.rtbOCRedText.Text = "";
            // 
            // NeOCR_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1090, 505);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NeOCR_Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NeOCR Version 1.0 Alpha";
            this.Load += new System.EventHandler(this.SegmenterMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.pnlImage.ResumeLayout(false);
            this.pnlImage.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.toolStrip5.ResumeLayout(false);
            this.toolStrip5.PerformLayout();
            this.pnlOCRedText.ResumeLayout(false);
            this.pnlOCRedText.PerformLayout();
            this.toolStrip6.ResumeLayout(false);
            this.toolStrip6.PerformLayout();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.toolStrip4.ResumeLayout(false);
            this.toolStrip4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPages)).EndInit();
            this.contextMenuStripOCRedText.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputPicBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuOpenImage;
        private System.Windows.Forms.ToolStripMenuItem saveOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem skewnessCorrectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpContentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnOpenFile;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtSegmentProgress;
        private System.Windows.Forms.Panel pnlImage;
        private System.Windows.Forms.PictureBox inputPicBox;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton tsBtnSelect;
        private System.Windows.Forms.ToolStripButton tsBtnCrop;
        private System.Windows.Forms.ToolStripButton tsBtnErase;
        private System.Windows.Forms.ToolStripButton tsBtnZoomIn;
        private System.Windows.Forms.ToolStripButton tsBtnZoomOut;
        private System.Windows.Forms.ToolStripSplitButton tsBtnRotateImage;
        private System.Windows.Forms.ToolStripMenuItem toolMnuRotateCW;
        private System.Windows.Forms.ToolStripMenuItem toolMnuRotateCCW;
        private System.Windows.Forms.ToolStripMenuItem toolMnuFlipHorizontal;
        private System.Windows.Forms.ToolStripMenuItem toolMnuFlipVertical;
        private System.Windows.Forms.ToolStripMenuItem toolMnuRotate180;
        private System.Windows.Forms.ToolStripButton tsBtnInvertImage;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem toolMnuRotateArbitrary;
        private System.Windows.Forms.ToolStripButton tsbtnLayoutAnalysis;
        private System.Windows.Forms.ToolStripButton btnScan;
        private System.Windows.Forms.ToolStripButton tsbtnOCR;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStripButton tsbtnManuallyThresholdImage;
        private System.Windows.Forms.ToolStripButton tsbtnGrayscaleImage;
        private System.Windows.Forms.ToolStripButton tsbtnDeskewImage;
        private System.Windows.Forms.ToolStripComboBox tscbOCRMode;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.DataGridView dgvPages;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.ComponentModel.BackgroundWorker backgroundWorkerPDFToImage;
        private System.ComponentModel.BackgroundWorker backgroundWorkerOCRUsingTesseract;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Panel pnlOCRedText;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton tsbtnClearText;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripButton tsbtnExportToWordDoc;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton tsbtnExportAsPlainText;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton tsbtnBlobs;
        private System.Windows.Forms.ToolStripButton tsbtnDetectLines;
        private System.Windows.Forms.ToolStripMenuItem scanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runOCRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oCRAllPagesToolStripMenuItem;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripComboBox cbTessOCRLanguages;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem recognitionWizardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportAsPlainTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportAsMicrosoftWordFileToolStripMenuItem;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pageListPaneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imagePreviewPaneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripMenuItem nextPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem previousPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem imagePropertiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fontToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeLineBreaksToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripOCRedText;
        private System.Windows.Forms.ToolStripMenuItem clearContentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportAsPlainTextToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exportAsMicrosoftWordDocumentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripMenuItem copyAllTextToClipboardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip4;
        private System.Windows.Forms.ToolStripButton tsbtnListImageThumbnail;
        private System.Windows.Forms.ToolStripButton tsbtnListImageNames;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStrip toolStrip5;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripButton tsbtnReadImage;
        private System.Windows.Forms.ToolStripButton tsbtnSaveText;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStrip toolStrip6;
        private System.Windows.Forms.ToolStripLabel toolStripLabel5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator19;
        private System.Windows.Forms.ToolStripLabel toolStripLabel6;
        private System.Windows.Forms.ToolStripLabel toolStripLabel7;
        private System.Windows.Forms.ToolStripComboBox tscbFonts;
        private System.Windows.Forms.ToolStripComboBox tscbFontSize;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator21;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator20;
        private System.Windows.Forms.ToolStripButton tsbtnIncreaseFontSize;
        private System.Windows.Forms.ToolStripButton tsbtnDecreaseFontSize;
        private System.Windows.Forms.Panel panel4;
        private System.ComponentModel.BackgroundWorker backgroundWorkerOCRAllPagesUsingTesseract;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator22;
        private System.Windows.Forms.ToolStripMenuItem preferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goToToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmnuOpenImageFiles;
        private System.Windows.Forms.ToolStripMenuItem tsmnuOpenPDFFile;
        private System.Windows.Forms.ToolStripButton tsbtnOpenPDF;
        private System.ComponentModel.BackgroundWorker backgroundWorkerRecognizePDF;
        private System.Windows.Forms.ToolStripMenuItem registerTool;
        private System.Windows.Forms.ToolStripMenuItem filtersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmnuInvertImage;
        private System.Windows.Forms.ToolStripMenuItem tsmnuThresholdImage;
        private System.Windows.Forms.ToolStripMenuItem tsmnuGrayscaleImage;
        private System.Windows.Forms.ToolStripMenuItem rotateImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmnuRotate90CW;
        private System.Windows.Forms.ToolStripMenuItem tsmnuRotate90CCW;
        private System.Windows.Forms.ToolStripMenuItem tsmnuFlipHorizontal;
        private System.Windows.Forms.ToolStripMenuItem tsmnuFlipVertical;
        private System.Windows.Forms.ToolStripMenuItem tsmnuRotate180;
        private System.Windows.Forms.ToolStripMenuItem tsmnuRotateArbitrarily;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator24;
        private System.Windows.Forms.ToolStripMenuItem reportAProblemToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem provideASuggestionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator23;
        private System.Windows.Forms.ToolStripMenuItem skinToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem regularToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nightModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userInterfaceLanguageToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator25;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.RichTextBox rtbOCRedText;
    }
}

