﻿namespace Text_Image_Segmenter.Wizards
{
    partial class RecognitionWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RecognitionWizard));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rbSaveInSelectedDir = new System.Windows.Forms.RadioButton();
            this.rbSaveToSourceDir = new System.Windows.Forms.RadioButton();
            this.btnSaveAsBrowse = new System.Windows.Forms.Button();
            this.tbSaveAsFileLocation = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbPlainText = new System.Windows.Forms.RadioButton();
            this.rbMSWord = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.tbSelectedFile = new System.Windows.Forms.TextBox();
            this.btnBrowseFile = new System.Windows.Forms.Button();
            this.cbOCRLanguages = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnRecognize = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.backgroundWorkerOCRUsingTesseract = new System.ComponentModel.BackgroundWorker();
            this.pbRecognitionProgress = new System.Windows.Forms.ProgressBar();
            this.backgroundWorkerRecognizaImage = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerRecognizePDF = new System.ComponentModel.BackgroundWorker();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.progressBar1);
            this.panel1.Controls.Add(this.tbSelectedFile);
            this.panel1.Controls.Add(this.btnBrowseFile);
            this.panel1.Controls.Add(this.cbOCRLanguages);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Location = new System.Drawing.Point(12, 90);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(710, 315);
            this.panel1.TabIndex = 21;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.rbSaveInSelectedDir);
            this.panel3.Controls.Add(this.rbSaveToSourceDir);
            this.panel3.Controls.Add(this.btnSaveAsBrowse);
            this.panel3.Controls.Add(this.tbSaveAsFileLocation);
            this.panel3.Location = new System.Drawing.Point(55, 257);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(650, 51);
            this.panel3.TabIndex = 27;
            // 
            // rbSaveInSelectedDir
            // 
            this.rbSaveInSelectedDir.AccessibleName = "Select output directory - Save in location";
            this.rbSaveInSelectedDir.AutoSize = true;
            this.rbSaveInSelectedDir.Location = new System.Drawing.Point(200, 15);
            this.rbSaveInSelectedDir.Name = "rbSaveInSelectedDir";
            this.rbSaveInSelectedDir.Size = new System.Drawing.Size(61, 17);
            this.rbSaveInSelectedDir.TabIndex = 21;
            this.rbSaveInSelectedDir.TabStop = true;
            this.rbSaveInSelectedDir.Text = "Save in";
            this.rbSaveInSelectedDir.UseVisualStyleBackColor = true;
            this.rbSaveInSelectedDir.CheckedChanged += new System.EventHandler(this.rbSaveInSelectedDir_CheckedChanged);
            // 
            // rbSaveToSourceDir
            // 
            this.rbSaveToSourceDir.AccessibleDescription = "Save in source directory";
            this.rbSaveToSourceDir.AccessibleName = "Select output directory - Save in source directory";
            this.rbSaveToSourceDir.AutoSize = true;
            this.rbSaveToSourceDir.Checked = true;
            this.rbSaveToSourceDir.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSaveToSourceDir.Location = new System.Drawing.Point(4, 13);
            this.rbSaveToSourceDir.Name = "rbSaveToSourceDir";
            this.rbSaveToSourceDir.Size = new System.Drawing.Size(154, 19);
            this.rbSaveToSourceDir.TabIndex = 20;
            this.rbSaveToSourceDir.TabStop = true;
            this.rbSaveToSourceDir.Text = "Save in source directory";
            this.rbSaveToSourceDir.UseVisualStyleBackColor = true;
            // 
            // btnSaveAsBrowse
            // 
            this.btnSaveAsBrowse.AccessibleName = "Select output directory - Browse location to save";
            this.btnSaveAsBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveAsBrowse.Location = new System.Drawing.Point(297, 11);
            this.btnSaveAsBrowse.Name = "btnSaveAsBrowse";
            this.btnSaveAsBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnSaveAsBrowse.TabIndex = 22;
            this.btnSaveAsBrowse.Text = "Browse...";
            this.btnSaveAsBrowse.UseVisualStyleBackColor = true;
            this.btnSaveAsBrowse.Click += new System.EventHandler(this.btnSaveAsBrowse_Click);
            // 
            // tbSaveAsFileLocation
            // 
            this.tbSaveAsFileLocation.Enabled = false;
            this.tbSaveAsFileLocation.Location = new System.Drawing.Point(378, 14);
            this.tbSaveAsFileLocation.Name = "tbSaveAsFileLocation";
            this.tbSaveAsFileLocation.Size = new System.Drawing.Size(269, 20);
            this.tbSaveAsFileLocation.TabIndex = 23;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rbPlainText);
            this.panel2.Controls.Add(this.rbMSWord);
            this.panel2.Location = new System.Drawing.Point(56, 155);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(650, 67);
            this.panel2.TabIndex = 26;
            // 
            // rbPlainText
            // 
            this.rbPlainText.AccessibleDescription = "Plain Text";
            this.rbPlainText.AccessibleName = "Select an output format - Plain Text";
            this.rbPlainText.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbPlainText.AutoSize = true;
            this.rbPlainText.Image = ((System.Drawing.Image)(resources.GetObject("rbPlainText.Image")));
            this.rbPlainText.Location = new System.Drawing.Point(126, 6);
            this.rbPlainText.Name = "rbPlainText";
            this.rbPlainText.Size = new System.Drawing.Size(64, 55);
            this.rbPlainText.TabIndex = 18;
            this.rbPlainText.Text = "Plain Text";
            this.rbPlainText.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.rbPlainText.UseVisualStyleBackColor = true;
            // 
            // rbMSWord
            // 
            this.rbMSWord.AccessibleDescription = "Microsoft Word";
            this.rbMSWord.AccessibleName = "Select and Output format - Microsoft Word";
            this.rbMSWord.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbMSWord.AutoSize = true;
            this.rbMSWord.Checked = true;
            this.rbMSWord.Image = ((System.Drawing.Image)(resources.GetObject("rbMSWord.Image")));
            this.rbMSWord.Location = new System.Drawing.Point(3, 6);
            this.rbMSWord.Name = "rbMSWord";
            this.rbMSWord.Size = new System.Drawing.Size(89, 55);
            this.rbMSWord.TabIndex = 17;
            this.rbMSWord.TabStop = true;
            this.rbMSWord.Text = "Microsoft Word";
            this.rbMSWord.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.rbMSWord.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(52, 234);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(168, 20);
            this.label10.TabIndex = 24;
            this.label10.Text = "Select output directory";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(10, 234);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(24, 20);
            this.label11.TabIndex = 23;
            this.label11.Text = "4.";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(329, 423);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(378, 23);
            this.progressBar1.TabIndex = 20;
            // 
            // tbSelectedFile
            // 
            this.tbSelectedFile.Enabled = false;
            this.tbSelectedFile.Location = new System.Drawing.Point(148, 40);
            this.tbSelectedFile.Name = "tbSelectedFile";
            this.tbSelectedFile.Size = new System.Drawing.Size(292, 20);
            this.tbSelectedFile.TabIndex = 15;
            // 
            // btnBrowseFile
            // 
            this.btnBrowseFile.AccessibleDescription = "Browse file to recognize";
            this.btnBrowseFile.AccessibleName = "Browse File Button";
            this.btnBrowseFile.Location = new System.Drawing.Point(56, 36);
            this.btnBrowseFile.Name = "btnBrowseFile";
            this.btnBrowseFile.Size = new System.Drawing.Size(75, 27);
            this.btnBrowseFile.TabIndex = 14;
            this.btnBrowseFile.Text = "Browse...";
            this.btnBrowseFile.UseVisualStyleBackColor = true;
            this.btnBrowseFile.Click += new System.EventHandler(this.btnBrowseFile_Click);
            // 
            // cbOCRLanguages
            // 
            this.cbOCRLanguages.AccessibleDescription = "Select Language of your Document";
            this.cbOCRLanguages.AccessibleName = "OCR Languages Selection Box";
            this.cbOCRLanguages.FormattingEnabled = true;
            this.cbOCRLanguages.Location = new System.Drawing.Point(56, 96);
            this.cbOCRLanguages.Name = "cbOCRLanguages";
            this.cbOCRLanguages.Size = new System.Drawing.Size(190, 21);
            this.cbOCRLanguages.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(56, 132);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(176, 20);
            this.label9.TabIndex = 9;
            this.label9.Text = "Select an output format";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(52, 73);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(278, 20);
            this.label8.TabIndex = 8;
            this.label8.Text = "Select the language of your document";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(52, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(177, 20);
            this.label7.TabIndex = 7;
            this.label7.Text = "Browse file to recognize";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(10, 132);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 20);
            this.label6.TabIndex = 6;
            this.label6.Text = "3.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "2.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "1.";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(678, 36);
            this.label3.TabIndex = 3;
            this.label3.Text = "This wizard helps recognize pictures from your scanner or computer and PDF files " +
    "to your computer using Optical Character Recogtion (OCR).";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(264, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Welcome to Recognition Wizard";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 418);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "To continue, click Recognize.";
            // 
            // btnRecognize
            // 
            this.btnRecognize.Location = new System.Drawing.Point(566, 413);
            this.btnRecognize.Name = "btnRecognize";
            this.btnRecognize.Size = new System.Drawing.Size(76, 27);
            this.btnRecognize.TabIndex = 24;
            this.btnRecognize.Text = "&Recognize";
            this.btnRecognize.UseVisualStyleBackColor = true;
            this.btnRecognize.Click += new System.EventHandler(this.btnRecognize_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(666, 413);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(56, 27);
            this.btnCancel.TabIndex = 25;
            this.btnCancel.Text = "Close";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Location = new System.Drawing.Point(12, 12);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(710, 72);
            this.panel4.TabIndex = 22;
            // 
            // backgroundWorkerOCRUsingTesseract
            // 
            this.backgroundWorkerOCRUsingTesseract.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerOCRUsingTesseract_DoWork);
            this.backgroundWorkerOCRUsingTesseract.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerOCRUsingTesseract_RunWorkerCompleted);
            // 
            // pbRecognitionProgress
            // 
            this.pbRecognitionProgress.Location = new System.Drawing.Point(184, 411);
            this.pbRecognitionProgress.Name = "pbRecognitionProgress";
            this.pbRecognitionProgress.Size = new System.Drawing.Size(364, 27);
            this.pbRecognitionProgress.TabIndex = 26;
            this.pbRecognitionProgress.Visible = false;
            // 
            // backgroundWorkerRecognizaImage
            // 
            this.backgroundWorkerRecognizaImage.WorkerReportsProgress = true;
            this.backgroundWorkerRecognizaImage.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerRecognizaImage_DoWork);
            this.backgroundWorkerRecognizaImage.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerRecognizaImage_RunWorkerCompleted);
            // 
            // backgroundWorkerRecognizePDF
            // 
            this.backgroundWorkerRecognizePDF.WorkerReportsProgress = true;
            this.backgroundWorkerRecognizePDF.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerRecognizePDF_DoWork);
            this.backgroundWorkerRecognizePDF.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerRecognizePDF_RunWorkerCompleted);
            // 
            // RecognitionWizard
            // 
            this.AccessibleDescription = "This wizard helps recognize pictures from your scanner or computer and PDF files " +
    "to your computer using Optical Character Recogtion (OCR).";
            this.AccessibleName = "Welcome to Recognition Wizard";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(734, 447);
            this.Controls.Add(this.pbRecognitionProgress);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnRecognize);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RecognitionWizard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Recognition Wizard";
            this.Load += new System.EventHandler(this.RecognitionWizard_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRecognize;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbSelectedFile;
        private System.Windows.Forms.Button btnBrowseFile;
        private System.Windows.Forms.ComboBox cbOCRLanguages;
        private System.Windows.Forms.RadioButton rbPlainText;
        private System.Windows.Forms.RadioButton rbMSWord;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnSaveAsBrowse;
        private System.Windows.Forms.TextBox tbSaveAsFileLocation;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RadioButton rbSaveInSelectedDir;
        private System.Windows.Forms.RadioButton rbSaveToSourceDir;
        private System.ComponentModel.BackgroundWorker backgroundWorkerOCRUsingTesseract;
        private System.Windows.Forms.ProgressBar pbRecognitionProgress;
        private System.ComponentModel.BackgroundWorker backgroundWorkerRecognizaImage;
        private System.ComponentModel.BackgroundWorker backgroundWorkerRecognizePDF;
    }
}