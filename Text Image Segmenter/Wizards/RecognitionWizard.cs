﻿using Ghostscript.NET;
using Ghostscript.NET.Rasterizer;
using NeOCR.Libs;
using NeOCR.Twist;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Text_Image_Segmenter.Libs;

namespace Text_Image_Segmenter.Wizards
{
    public partial class RecognitionWizard : Form
    {
        private String _selectedFile = null;
        private String _saveTo = null;
        private String _language = "nep";   // default language is Nepali
        private String _out_dir = "";
        AlertForm alert;
        private String out_file_path;

        enum OutputFormat
        {
            MS_WORD=1,
            PLAIN_TEXT=2,
            PDF=3
        }

        private OutputFormat selectedFormat = OutputFormat.MS_WORD;

        public RecognitionWizard()
        {
            InitializeComponent();
        }

        private void btnBrowseFile_Click(object sender, EventArgs e)
        {
            OpenFile();
        }

        //Opens image file and correct the skewness of image by calling Hough Transform method
        public void OpenFile()
        {
            OpenFileDialog openImageDialog = new OpenFileDialog();
            // Select Image File to Recognize
            //openImageDialog.InitialDirectory = "D:\\";
            openImageDialog.Filter = "All Supported Image Types (*.bmp;*.jpg;*.jpeg;*.gif;*.png;*.tif)|" +
              "*.bmp;*.jpg;*.jpeg;*.gif;*.png;*.tif|PDF Files (*.pdf)|*.pdf";
            openImageDialog.FilterIndex = 1;
            openImageDialog.Multiselect = false;
            openImageDialog.RestoreDirectory = true;

            if (openImageDialog.ShowDialog() == DialogResult.OK)
            {
                // get selected files
                String file = openImageDialog.FileName;

                // if selected file is pdf then convert to image and load in gridview
                if (file.Length > 0)
                {
                    _selectedFile = file;
                    tbSelectedFile.Text = file;
                }
            }
        }

        private void RecognitionWizard_Load(object sender, EventArgs e)
        {
            // disable folder browse button
            btnSaveAsBrowse.Enabled = false;
            // get tesseract languages
            getTesseractOCRLanguages();
        }

        private void getTesseractOCRLanguages()
        {
            cbOCRLanguages.Items.Clear();
            // get all tesseract languages
            String tessdata_path = "tesseract\\tessdata";
            // Process the list of files found in the directory.
            string[] listOfFiles = Directory.GetFiles(tessdata_path);

            foreach (String file_item in listOfFiles)
            {

                if (File.Exists(file_item) && file_item.Substring(file_item.LastIndexOf(".")).Equals(".traineddata"))
                {
                    // get language name from language code
                    String lng = Path.GetFileName(file_item).Replace(".traineddata", "");
                    //String name = new CultureInfo(lng).DisplayName;
                    try
                    {
                        var langName = CultureInfo
                           .GetCultures(CultureTypes.NeutralCultures)
                           .Where(ci => string.Equals(ci.ThreeLetterISOLanguageName, lng, StringComparison.OrdinalIgnoreCase))
                           .FirstOrDefault().DisplayName;

                        if (lng.Equals("nep"))
                        {
                            cbOCRLanguages.Items.Insert(0, langName + " (" + lng + ")");
                        }
                        else
                        {
                            cbOCRLanguages.Items.Add(langName + " (" + lng + ")");
                        }
                    }
                    catch (Exception ex)
                    {
                        // do something
                    }
                }
            }

            // If item count is > 0; set 0 as selected index
            if (cbOCRLanguages.Items.Count > 0)
            {
                // set selected index
                cbOCRLanguages.SelectedIndex = 0;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnRecognize_Click(object sender, EventArgs e)
        {
            // check file selected status
            if(_selectedFile == null)
            {
                MessageBox.Show(null, "File not selected. Please select file to recognize.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // get OCR language
            _language = Regex.Match(cbOCRLanguages.Items[cbOCRLanguages.SelectedIndex].ToString(), @"\(([^)]*)\)").Groups[1].Value;

            // get output format
            if (rbMSWord.Checked == true)
            {
                selectedFormat = OutputFormat.MS_WORD;
            }
            else if(rbPlainText.Checked == true)
            {
                selectedFormat = OutputFormat.PLAIN_TEXT;
            }

            //output dir
            if(rbSaveToSourceDir.Checked == true)
            {
                _out_dir = Path.GetDirectoryName(_selectedFile);
            }
            else if (rbSaveInSelectedDir.Checked == true)
            {
                if(_saveTo != null)
                {
                    _out_dir = _saveTo;
                }
                else
                {
                    return;
                }
            }

            #region <-- Recognition -->
            // start progressbar
            //pbRecognitionProgress.Visible = true;
            //pbRecognitionProgress.Style = ProgressBarStyle.Marquee;

            if (_selectedFile.Length > 0 && Path.GetExtension(_selectedFile).ToLower() != ".pdf")
            {
                // selected file is image
                // call open image function
                this.recognizemage();
            }
            else
            {
                // file is pdf
                // call recognize pdf function
                this.recognizePDF();
            }

            // close Wizard window
            this.Dispose();
            #endregion
        }

        //Recognize image file and correct the skewness of image by calling Hough Transform method
        public void recognizemage()
        {
            // run ocr process
            if (backgroundWorkerRecognizaImage.IsBusy != true)
            {
                // create a new instance of the alert form
                alert = new AlertForm();
                alert.StartPosition = FormStartPosition.CenterScreen;//CenterParent
                alert.Show(this); ;//.Show(this);
                alert.IndeterminateProgressBar = true;

                // Start the asynchronous operation.
                backgroundWorkerRecognizaImage.RunWorkerAsync();

                // run image acquisition process
                // backgroundWorkerRecognizaImage.RunWorkerAsync();
                // wait until process is busy
                while (backgroundWorkerRecognizaImage.IsBusy)
                    Application.DoEvents();
            }
        }

        //Opens image file and correct the skewness of image by calling Hough Transform method
        public void recognizePDF()
        {
            /*#Region: Image/PDF selection#*/
            // if selected file is pdf then convert to image and load in gridview
            if (_selectedFile.Length > 0 && Path.GetExtension(_selectedFile).ToLower() == ".pdf")
            {
                // pdf is expected
                // run ocr process
                if (backgroundWorkerRecognizePDF.IsBusy != true)
                {
                    // create a new instance of the alert form
                    alert = new AlertForm();
                    alert.StartPosition = FormStartPosition.CenterScreen;
                    alert.Show(this); ;//.Show(this);
                    alert.IndeterminateProgressBar = true;

                    // Start the asynchronous operation.
                    backgroundWorkerRecognizePDF.RunWorkerAsync();
                }

                // run image acquisition process
                // backgroundWorkerRecognizePDF.RunWorkerAsync();
                // wait until process is busy
                while (backgroundWorkerRecognizePDF.IsBusy)
                    Application.DoEvents();
            }
        }

        private void btnSaveAsBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            // Show the FolderBrowserDialog.
            DialogResult result = folderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                // get selected folder path
                _saveTo = folderBrowserDialog.SelectedPath;
                tbSaveAsFileLocation.Text = _saveTo;
            }
        }

        private void rbSaveInSelectedDir_CheckedChanged(object sender, EventArgs e)
        {
            if(rbSaveInSelectedDir.Checked == true)
            {
                btnSaveAsBrowse.Enabled = true;
            }
            else if(rbSaveInSelectedDir.Checked == false)
            {
                btnSaveAsBrowse.Enabled = false;
            }
        }

        /// <summary>
        /// function to perform OCR using tesseract 
        /// </summary>
        public void OCRUsingTesseract()
        {
            string tessractDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\tesseract";
            tessractDirPath = string.Format(tessractDirPath.Replace("file:\\", ""));
            // tesseract data path
            String tessdataDirPath = tessractDirPath + "\\tessdata";

            // get temp file path
            String out_file_path = Path.Combine(Path.GetTempPath(), Path.GetFileName(_selectedFile));

            // selected file type and ocr call
            if (Path.GetExtension(_selectedFile) != ".pdf")
            {
                // set arguments 
                //String arguments = "\"" + _selectedFile + "\"" + " \"" + tessractDirPath + "\\out.txt" + "\"" + " --tessdata-dir " + "\"" + tessdataDirPath + "\"" + " -l " + _language + " --oem 1";
                // set arguments 
                String arguments = "\"" + _selectedFile + "\" \"" + out_file_path + "\" -l " + _language + " --psm 1 --oem 1";

                // try recognition process
                try
                {
                    Process proc = new System.Diagnostics.Process();
                    proc.StartInfo.FileName = tessractDirPath + @"\\tesseract.exe";
                    proc.StartInfo.Arguments = arguments;
                    proc.StartInfo.WorkingDirectory = tessractDirPath;
                    proc.StartInfo.UseShellExecute = false;
                    proc.StartInfo.CreateNoWindow = true;
                    proc.StartInfo.RedirectStandardInput = true;
                    proc.StartInfo.RedirectStandardOutput = true;
                    proc.StartInfo.RedirectStandardError = true;

                    // run OCR process
                    proc.Start();
                    while (!proc.StandardOutput.EndOfStream)
                    {
                        string line = proc.StandardOutput.ReadLine();
                        Console.Out.WriteLine(line);
                        //UpdateProgress(line,0,true);
                        // do something with line
                    }

                    // Free resources associated with process.
                    proc.Close();
                    // OCR process END
                }
                catch (Exception ex)
                {
                    Console.Out.WriteLine(ex.ToString());
                }

                String saveFileName = "";
                // read all text from file and display
                try
                {
                    // Open the text file using a stream reader.
                    using (StreamReader sr = new StreamReader(out_file_path + ".txt"))
                    {
                        // Read the stream to a string, and write the string to the console.
                        String lines = sr.ReadToEnd();

                        if (rbMSWord.Checked == true)
                        {
                            saveFileName = _out_dir + "//" + Path.GetFileName(_selectedFile) + ".docx";

                            // Write the string to a file.
                            /**WRITE TO WORD FILE**/
                            //Create an instance for word app
                            Microsoft.Office.Interop.Word.Application winword = new Microsoft.Office.Interop.Word.Application();
                            //Set status for word application is to be visible or not.
                            winword.Visible = false;
                            //Create a missing variable for missing value
                            object missing = System.Reflection.Missing.Value;
                            //Create a new document
                            Microsoft.Office.Interop.Word.Document document = winword.Documents.Add(ref missing, ref missing, ref missing, ref missing);
                            document.PageSetup.PaperSize = Microsoft.Office.Interop.Word.WdPaperSize.wdPaperA4;

                            // Create a paragraph.
                            Microsoft.Office.Interop.Word.Paragraph para = document.Paragraphs.Add(ref missing);
                            para.Range.Text = lines;

                            object filename = @saveFileName;
                            document.SaveAs(ref filename);
                            document.Close(ref missing, ref missing, ref missing);
                            document = null;
                            winword.Quit(ref missing, ref missing, ref missing);
                            winword = null;
                            /**END**/
                        }
                        else if (rbPlainText.Checked == true)
                        {
                            saveFileName = _saveTo + "//" + Path.GetFileName(_selectedFile) + ".txt";
                            // Write the string to a file.
                            System.IO.StreamWriter file = new System.IO.StreamWriter(@saveFileName);
                            file.WriteLine(lines);
                            file.Close();
                        }
                    }

                    // show success message box
                    MessageBox.Show(null, "Recognition Completed.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // open file in default application
                    Process.Start(@saveFileName);
                }
                catch (Exception ex)
                {
                    UpdateProgress("The file could not be read: \n" + ex.Message.ToString(), 0, true);
                    // error message
                    MessageBox.Show(null, "The file could not be read.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void backgroundWorkerOCRUsingTesseract_DoWork(object sender, DoWorkEventArgs e)
        {
            // run ocr process
            OCRUsingTesseract();
        }

        private void backgroundWorkerOCRUsingTesseract_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // update progress status
            UpdateProgress("OCR Completed...", 0, true);
            pbRecognitionProgress.Visible = false;

            string tessractDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\tesseract";
            tessractDirPath = string.Format(tessractDirPath.Replace("file:\\", ""));
            // read all text from file and display
            try
            {
                // if text file exists delete it
                if(File.Exists(tessractDirPath + @"\\out.txt"))
                {
                    File.Delete(tessractDirPath + @"\\out.txt");
                }

                // if pdf file exists delete it
                if (File.Exists(tessractDirPath + @"\\out.pdf"))
                {
                    File.Delete(tessractDirPath + @"\\out.pdf");
                }
            }
            catch (Exception ex)
            {
                UpdateProgress("The file could not be read: \n" + ex.Message, 0, true);
            }
        }

        /// <summary>
        /// function to update progress in progress bar
        /// </summary>
        /// <param name="text"></param>
        public void UpdateProgress(String text, int value, bool indeterminate)
        {
            //Check if invoke requied if so return - as i will be recalled in correct thread
            if (ControlInvokeRequired(pbRecognitionProgress, () => UpdateProgress(text, value, indeterminate))) return;
            if(indeterminate == true)
            {
                pbRecognitionProgress.Style = ProgressBarStyle.Marquee;
            }
            else
            {
                pbRecognitionProgress.Value = value;
            }

            // set string message to progressbar
            pbRecognitionProgress.Text = text;
        }

        /// <summary>
        /// Helper method to determin if invoke required, if so will rerun method on correct thread.
        /// if not do nothing.
        /// </summary>
        /// <param name="c">Control that might require invoking</param>
        /// <param name="a">action to preform on control thread if so.</param>
        /// <returns>true if invoke required</returns>
        public bool ControlInvokeRequired(Control c, Action a)
        {
            if (c.InvokeRequired) c.Invoke(new MethodInvoker(delegate { a(); }));
            else return false;

            return true;
        }

        private void backgroundWorkerRecognizaImage_DoWork(object sender, DoWorkEventArgs e)
        {
            // run ocr process
            OCRUsingTesseract(null, _language);
        }

        /// <summary>
        /// function to perform OCR using tesseract 
        /// </summary>
        public void OCRUsingTesseract(String file_path, String language)
        {
            string tessractDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\tesseract";
            tessractDirPath = string.Format(tessractDirPath.Replace("file:\\", ""));
            // tesseract data path
            String tessdataDirPath = tessractDirPath + "\\tessdata";

            // if file_path is not provided, set selected file from image preview pane
            if (_selectedFile != null)
            {
                String milliseconds = DateTimeOffset.Now.Millisecond.ToString();
                file_path = Path.Combine(Path.GetTempPath() + Path.GetFileName("ocr_doc_" + milliseconds + Path.GetExtension(_selectedFile)));

                // Perform Pre-processing
                Bitmap preprocessedImage = PreprocessImage((Bitmap)Image.FromFile(_selectedFile));
                // save image to temp path
                preprocessedImage.Save(file_path);
                // get temp file path
                out_file_path = Path.Combine(Path.GetTempPath(), Path.GetFileName(file_path));

                // set progress
                alert.DisplayOCRProgress("Recognizing... 5%");
            }
            else
            {
                return; // no processing; file not available
            }

            // set Nepali + English language mode
            if (language == "nep")
            {
                //language += "+eng";
            }

            // set progress
            alert.DisplayOCRProgress("Recognizing... (10%)");
            // set arguments 
            String arguments = "\"" + file_path + "\" \"" + out_file_path + "\" -l " + _language + " --psm 1 --oem 1";

            // Perform a time consuming operation and report progress.
            //backgroundWorkerOCRUsingTesseract.ReportProgress(5);

            // create image acquisition process
            var proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    WorkingDirectory = tessractDirPath,
                    FileName = tessractDirPath + "\\tesseract.exe",
                    Arguments = arguments,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };

            // run acquisition process
            proc.Start();

            // set progress
            alert.DisplayOCRProgress("Recognizing... (20%)");

            // Perform a time consuming operation and report progress.
            //backgroundWorkerOCRUsingTesseract.ReportProgress(50);
            int progress = 20;
            while (!proc.StandardOutput.EndOfStream)
            {
                string line = proc.StandardOutput.ReadLine();
                // do something with line
                // set progress
                alert.DisplayOCRProgress("Recognizing... (" + (progress + 10) + "%)");
            }

            // Free resources associated with process.
            proc.Close();
            // Recognition END

            // delete temp files
            if (File.Exists(file_path)) { File.Delete(file_path); }

            // set progress
            alert.DisplayOCRProgress("Recognizing... (80%)");
            // OCR process end
        }

        public Bitmap PreprocessImage(Bitmap bmp)
        {
            // use Otsu method to grayscale
            OtsuMethod ot = new OtsuMethod();
            Bitmap grayImage = ot.MakeGrayscale(bmp);

            HoughTransformationDeskew sk = new HoughTransformationDeskew(grayImage);
            //imageDeskew sk = new imageDeskew(orgImg);
            double skewangle = sk.GetSkewAngle();
            Bitmap deskewedImage = ImageLab.RotateImage(grayImage, -skewangle);

            return deskewedImage;
        }

        private void backgroundWorkerRecognizaImage_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            string saveFileName = "";

            // read all text from file and display
            try
            {
                // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(out_file_path + ".txt"))
                {
                    // Read the stream to a string, and write the string to the console.
                    String lines = sr.ReadToEnd();

                    if (rbMSWord.Checked == true)
                    {
                        saveFileName = _out_dir + "//" + Path.GetFileName(_selectedFile) + ".docx";

                        /**WRITE TO WORD FILE**/
                        //Create an instance for word app
                        Microsoft.Office.Interop.Word.Application winword = new Microsoft.Office.Interop.Word.Application();
                        //Set status for word application is to be visible or not.
                        winword.Visible = false;
                        //Create a missing variable for missing value
                        object missing = System.Reflection.Missing.Value;
                        //Create a new document
                        Microsoft.Office.Interop.Word.Document document = winword.Documents.Add(ref missing, ref missing, ref missing, ref missing);
                        document.PageSetup.PaperSize = Microsoft.Office.Interop.Word.WdPaperSize.wdPaperA4;

                        // Create a paragraph.
                        Microsoft.Office.Interop.Word.Paragraph para = document.Paragraphs.Add(ref missing);
                        para.Range.Text = lines;

                        object filename = @saveFileName;
                        document.SaveAs(ref filename);
                        document.Close(ref missing, ref missing, ref missing);
                        document = null;
                        winword.Quit(ref missing, ref missing, ref missing);
                        winword = null;
                        /**END**/
                    }
                    else if (rbPlainText.Checked == true)
                    {
                        saveFileName = _saveTo + "//" + Path.GetFileName(_selectedFile) + ".txt";
                        // Write the string to a file.
                        System.IO.StreamWriter file = new System.IO.StreamWriter(@saveFileName);
                        file.WriteLine(lines);
                        file.Close();
                    }
                }

                // delete temp files
                if (File.Exists(out_file_path + ".txt")) { File.Delete(out_file_path + ".txt"); }

                // show success message box
                MessageBox.Show(null, "Recognition Completed.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

                // open file in default application
                Process.Start(@saveFileName);
            }
            catch (Exception ex)
            {
                UpdateProgress("The file could not be read: \n" + ex.Message.ToString(), 0, true);
                // error message
                MessageBox.Show(null, "The file could not be read.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void backgroundWorkerRecognizePDF_DoWork(object sender, DoWorkEventArgs e)
        {
            // run ocr process
            OCRPDFUsingTesseractIBI(_language);
        }

        /// <summary>
        /// function to perform OCR multiple pages using tesseract; Images one by one
        /// </summary>
        public void OCRPDFUsingTesseractIBI(String language)
        {
            // temporary file directory inside user's home directory
            String userHomeDir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            String tempDir = userHomeDir + "\\.neocr\\docs";

            /**Get number of pages in PDF document**/
            // ghostscript variables
            GhostscriptVersionInfo _lastInstalledVersion = GhostscriptVersionInfo.GetLastInstalledVersion(
                GhostscriptLicense.GPL | GhostscriptLicense.AFPL,
                GhostscriptLicense.GPL);
            GhostscriptRasterizer _rasterizer = new GhostscriptRasterizer();

            string _pdf_temp_path = Path.Combine(tempDir, "tmp_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf");
            File.Copy(_selectedFile, _pdf_temp_path);

            // get number of pages
            int totalPagesCount = GSRasterizer.GetNumberOfPages(_lastInstalledVersion, _rasterizer, _pdf_temp_path);

            // recognized page counter
            int recognizedPageCounter = 1;  // recognized pages counter initialization

            // recognize all pages
            // set tesseract directory path
            string tessractDirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\tesseract";
            tessractDirPath = string.Format(tessractDirPath.Replace("file:\\", ""));

            // get temp file path
            out_file_path = Path.GetTempPath() + "\\ocred-text-neocr";//Path.GetFileName(file_path);
            if (File.Exists(out_file_path + ".txt")) { File.Delete(out_file_path + ".txt"); }

            // recognize all selected images in single run  // _files_to_process
            for (int pageNum = 1; pageNum <= totalPagesCount; pageNum++)
            {
                // rasterize pdf page
                String filePath = GSRasterizer.RenderPDFToImage(_lastInstalledVersion, _rasterizer, _pdf_temp_path, tempDir, pageNum, 300, 300);

                // set progress
                alert.DisplayOCRProgress("Recognizing Page... (" + recognizedPageCounter + " of " + totalPagesCount + ")");

                #region -- perform pre-processing --
                // Perform Pre-processing
                // preprocess and save image temporarily
                Bitmap preproImage = (Bitmap)System.Drawing.Image.FromFile(filePath);
                Bitmap preprocessedImage = PreprocessImage(preproImage);
                // generate temporary image file path
                String tempImgPath = Path.Combine(Path.GetTempPath(), "neocr-" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + Path.GetExtension(filePath));

                if (File.Exists(tempImgPath)) { File.Delete(tempImgPath); }
                //save image
                preprocessedImage.Save(tempImgPath);
                #endregion

                String tessdataDirPath = tessractDirPath + "\\tessdata";
                // set arguments // output file will be of format _out_file_path + "_rs"
                String arguments = "\"" + tempImgPath + "\" \"" + out_file_path + "_rs\" -l " + language + " --psm 1 --oem 1";

                // create image recognition process
                var proc = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        WorkingDirectory = tessractDirPath,
                        FileName = tessractDirPath + "\\tesseract.exe",
                        Arguments = arguments,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        RedirectStandardInput = true,
                        CreateNoWindow = true
                    }
                };

                proc.OutputDataReceived += new DataReceivedEventHandler(
                    (s, e) =>
                    {
                        Console.Out.WriteLine(e.Data);
                    }
                );
                proc.ErrorDataReceived += new DataReceivedEventHandler((s, e) =>
                {
                    Console.Out.WriteLine(e.Data);
                });

                proc.Start();
                proc.BeginOutputReadLine();

                proc.WaitForExit();
                // Recognition END

                #region -- delete temporary image file --
                if (File.Exists(tempImgPath)) { File.Delete(tempImgPath); }
                #endregion

                // Perform a time consuming operation and report progress.
                //backgroundWorkerOCRAllPagesUsingTesseract.ReportProgress(100);

                // This text is always added, making the file longer over time
                // if it is not deleted.
                if (File.Exists(out_file_path + "_rs.txt"))
                {
                    using (StreamWriter sw = File.AppendText(out_file_path + ".txt"))
                    {
                        String text = File.ReadAllText(out_file_path + "_rs.txt");   // read all content of files and append
                        sw.WriteLine(text);
                        //delete temporary file
                        File.Delete(out_file_path + "_rs.txt");
                    }
                }

                // increase recognied pages counter
                recognizedPageCounter++;
            }

            _rasterizer.Close();
            // delete temp file
            if (File.Exists(_pdf_temp_path))
            {
                File.Delete(_pdf_temp_path);
            }
        }

        private void backgroundWorkerRecognizePDF_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            string saveFileName = "";

            // read all text from file and display
            try
            {
                // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(out_file_path + ".txt"))
                {
                    // Read the stream to a string, and write the string to the console.
                    String lines = sr.ReadToEnd();

                    if (rbMSWord.Checked == true)
                    {
                        saveFileName = _out_dir + "//" + Path.GetFileName(_selectedFile) + ".docx";

                        /**WRITE TO WORD FILE**/
                        //Create an instance for word app
                        Microsoft.Office.Interop.Word.Application winword = new Microsoft.Office.Interop.Word.Application();
                        //Set status for word application is to be visible or not.
                        winword.Visible = false;
                        //Create a missing variable for missing value
                        object missing = System.Reflection.Missing.Value;
                        //Create a new document
                        Microsoft.Office.Interop.Word.Document document = winword.Documents.Add(ref missing, ref missing, ref missing, ref missing);
                        document.PageSetup.PaperSize = Microsoft.Office.Interop.Word.WdPaperSize.wdPaperA4;

                        // Create a paragraph.
                        Microsoft.Office.Interop.Word.Paragraph para = document.Paragraphs.Add(ref missing);
                        para.Range.Text = lines;

                        object filename = @saveFileName;
                        document.SaveAs(ref filename);
                        document.Close(ref missing, ref missing, ref missing);
                        document = null;
                        winword.Quit(ref missing, ref missing, ref missing);
                        winword = null;
                        /**END**/
                    }
                    else if (rbPlainText.Checked == true)
                    {
                        saveFileName = _saveTo + "//" + Path.GetFileName(_selectedFile) + ".txt";
                        // Write the string to a file.
                        System.IO.StreamWriter file = new System.IO.StreamWriter(@saveFileName);
                        file.WriteLine(lines);
                        file.Close();
                    }
                }

                // delete temp files
                if (File.Exists(out_file_path + ".txt")) { File.Delete(out_file_path + ".txt"); }

                // show success message box
                MessageBox.Show(null, "Recognition Completed.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

                // open file in default application
                Process.Start(@saveFileName);
            }
            catch (Exception ex)
            {
                UpdateProgress("The file could not be read: \n" + ex.Message.ToString(), 0, true);
                // error message
                MessageBox.Show(null, "The file could not be read.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
