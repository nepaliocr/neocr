﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Text_Image_Segmenter.Libs;
using WIA;

namespace Text_Image_Segmenter
{
    public partial class ScanWizard : Form
    {
        public ScanWizard()
        {
            InitializeComponent();
        }

        private void btnScan_Click(object sender, EventArgs e)
        {
            ScanTIFF();
        }

        public void ScanTIFF()
        {
            // Select the dialog
            CommonDialogClass dlg = new CommonDialogClass();

            try
            {
                WIA.ImageFile wiaImage = null;
                wiaImage = dlg.ShowAcquireImage(WiaDeviceType.ScannerDeviceType, WiaImageIntent.UnspecifiedIntent, WiaImageBias.MaximizeQuality, WIA.FormatID.wiaFormatTIFF, true, true, true);

                //if (scanResult != null)
                if (wiaImage != null)
                {
                    // Save the image
                    //string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Data\scan.tif");
                    String userHomeDir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                    // generate name
                    DirectoryInfo taskDirectory = new DirectoryInfo(userHomeDir + "\\.neocr\\docs\\");
                    FileInfo[] taskFiles = taskDirectory.GetFiles("scan_*.tif");
                    List<int> nums = new List<int>();
                    foreach (FileInfo fileInfo in taskFiles)
                    {
                        String[] splits = fileInfo.Name.Split('_');
                        nums.Add(Convert.ToInt16(splits[splits.Length - 1].Replace(".tif", "")));
                    }

                    // get max counter
                    int maxCounter = 0;
                    if (nums.Count > 0)
                    {
                        maxCounter = nums.Max();    // geet maximum of number list
                    }

                    // create file path
                    String path = userHomeDir + "\\.neocr\\docs\\scan_" + (maxCounter + 1).ToString("D4") + ".tif";

                    // delete existing image
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                    // if scanning is successful save image
                    if (wiaImage != null)
                    {
                        wiaImage.SaveFile(path);
                        // add scan status
                        GlobalVariables.SCAN_STATUS = true;
                    }
                }
            }
            catch (COMException e)
            {
                // add scan status
                GlobalVariables.SCAN_STATUS = false;

                // Display the exception in the console.
                Console.WriteLine(e.ToString());

                uint errorCode = (uint)e.ErrorCode;

                // Catch 2 of the most common exceptions
                if (errorCode == 0x80210006)
                {
                    MessageBox.Show("The scanner is busy or isn't ready");
                }
                else if (errorCode == 0x80210064)
                {
                    MessageBox.Show("The scanning process has been cancelled.");
                }
                else
                {
                    MessageBox.Show("Scanner not detected, check the device", "Error", MessageBoxButtons.OK);
                }
            }
            finally
            {
                this.Dispose();
                //btnCancel.Text = "Finish";
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            // add scan status
            if(btnCancel.Text == "Finish")
                GlobalVariables.SCAN_STATUS = true;
            else
                GlobalVariables.SCAN_STATUS = false;

            this.Dispose();
        }
    }
}
