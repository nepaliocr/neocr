﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Text_Image_Segmenter.Libs
{
    class BorderNoiseRemoval
    {
        /// <summary>
        /// Description: this function analyzes image to eliminate border noise in scanned image document
        /// Algorithm:  If all connected pixels (8 neighbors) are of color black then change
        /// all connected pixels to white color and continue this
        /// process until whole document is covered
        /// </summary>
        /// <param name="bitmap">Bitmap image to analyze; binary image</param>
        public static Bitmap RemoveBorderNoise(Bitmap bitmap)
        {
            int img_height = bitmap.Height;
            int img_width = bitmap.Width;

            // visited pixels list
            List<Point> visitedPixelsList = new List<Point>();

            #region <-- find cluster of dots -->
            // Iterate through evaluation matrix to find clusters of dots (grid boxes)
            for (int i = 0; i < img_width; i++)
            {
                for (int j = 0; j < img_height; j++)
                {
                    /*
                    // if point is already visited continue
                    if(visitedPixelsList.Exists(x => x.X == i && x.Y == j)){
                        continue;
                    }
                    */

                    List<Point> neighbors = new List<Point>();
                    // if all connected pixels are black make them white
                    if (bitmap.GetPixel(i, j).R == 0 && bitmap.GetPixel(i, j).G == 0 && bitmap.GetPixel(i, j).B == 0)
                    {
                        neighbors.Add(new Point(i - 1, j - 1));
                        neighbors.Add(new Point(i - 1, j));
                        neighbors.Add(new Point(i - 1, j + 1));
                        neighbors.Add(new Point(i, j - 1));
                        neighbors.Add(new Point(i, j + 1));
                        neighbors.Add(new Point(i + 1, j - 1));
                        neighbors.Add(new Point(i + 1, j));
                        neighbors.Add(new Point(i + 1, j + 1));

                        // Remove invalid neighbors positions
                        for (int k = neighbors.Count - 1; k >= 0; k--)
                        {
                            if (neighbors[k].X < 0 || neighbors[k].Y < 0 || neighbors[k].X >= img_width || neighbors[k].Y >= img_height)
                            {
                                neighbors.RemoveAt(k);
                            }
                        }

                        int blackNeighbors = 0;
                        // check if all neighbors are black
                        for (int k = neighbors.Count - 1; k >= 0; k--)
                        {
                            if(bitmap.GetPixel(neighbors[k].X, neighbors[k].Y).R == 0 && bitmap.GetPixel(neighbors[k].X, neighbors[k].Y).G == 0 && bitmap.GetPixel(neighbors[k].X, neighbors[k].Y).B == 0)
                            {
                                blackNeighbors++;
                            }
                        }

                        // if number of blackneighbors equals number of neighbors make all white neighbors
                        if(blackNeighbors == neighbors.Count)
                        {
                            // make all neighbors white
                            for (int k = neighbors.Count - 1; k >= 0; k--)
                            {
                                bitmap.SetPixel(neighbors[k].X, neighbors[k].Y, Color.White);
                            }
                            // make itself white
                            bitmap.SetPixel(i, j, Color.White);
                        }

                        //visitedPixelsList.Add(new Point(i, j));
                    }
                    /*
                    else
                    {
                        visitedPixelsList.Add(new Point(i, j)); // continue
                    }
                    */
                }
            }
            #endregion <-- end noise removal region -->

            return bitmap;
        }

    }
}
