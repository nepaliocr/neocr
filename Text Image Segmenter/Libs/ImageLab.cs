﻿using AForge.Imaging;
using AForge.Imaging.Filters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Text_Image_Segmenter.Libs
{
    public static class ImageLab
    {
        /* AFORGE.NET IMAGE PROCESSING .. TIPS
         * // 1 - grayscaling
        UnmanagedImage grayImage = null;

        if ( image.PixelFormat == PixelFormat.Format8bppIndexed )
        {
            grayImage = image;
        }
        else
        {
            grayImage = UnmanagedImage.Create( image.Width, image.Height,
                PixelFormat.Format8bppIndexed );
            Grayscale.CommonAlgorithms.BT709.Apply( image, grayImage );
        }

        // 2 - Edge detection
        DifferenceEdgeDetector edgeDetector = new DifferenceEdgeDetector( );
        UnmanagedImage edgesImage = edgeDetector.Apply( grayImage );

        // 3 - Threshold edges
        Threshold thresholdFilter = new Threshold( 40 );
        thresholdFilter.ApplyInPlace( edgesImage );
         * 
         * 
         */

        /// <summary>
        /// Ref: https://stackoverflow.com/questions/11428724/how-to-create-inverse-png-image
        /// </summary>
        /// <param name="bitmapImage"></param>
        /// <returns></returns>
        public static Bitmap Invert(Bitmap bitmapImage)
        {
            var bitmapRead = bitmapImage.LockBits(new Rectangle(0, 0, bitmapImage.Width, bitmapImage.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppPArgb);
            var bitmapLength = bitmapRead.Stride * bitmapRead.Height;
            var bitmapBGRA = new byte[bitmapLength];
            Marshal.Copy(bitmapRead.Scan0, bitmapBGRA, 0, bitmapLength);
            bitmapImage.UnlockBits(bitmapRead);

            for (int i = 0; i < bitmapLength; i += 4)
            {
                bitmapBGRA[i] = (byte)(255 - bitmapBGRA[i]);
                bitmapBGRA[i + 1] = (byte)(255 - bitmapBGRA[i + 1]);
                bitmapBGRA[i + 2] = (byte)(255 - bitmapBGRA[i + 2]);
                //        [i + 3] = ALPHA.
            }

            var bitmapWrite = bitmapImage.LockBits(new Rectangle(0, 0, bitmapImage.Width, bitmapImage.Height), ImageLockMode.WriteOnly, PixelFormat.Format32bppPArgb);
            Marshal.Copy(bitmapBGRA, 0, bitmapWrite.Scan0, bitmapLength);
            bitmapImage.UnlockBits(bitmapWrite);

            return bitmapImage;
        }

        // scale bitmap to new dimension
        public static Bitmap ScaleBitmap(Bitmap bm, Size Dimension)
        {
            try
            {
                Bitmap b = new Bitmap(Dimension.Width, Dimension.Height);

                using (Graphics g = Graphics.FromImage(b))
                {
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    g.DrawImage(bm, 0, 0, Dimension.Width, Dimension.Height);
                }

                return b;

                //Bitmap resized = new Bitmap(bm, Dimension);

                //return resized;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Bitmap could not be resized.");
                return bm;
            }
        }

        // REMOVE SURROUNDING WHITESPACE
        public static Bitmap RemoveSurroundingWhitePixels(Bitmap bmp)
        {
            //Filter Added by nirajan_pant@yahoo.com .. 2015-08-19
            // binarize image using Accord.NET
            // Create a new Sauvola threshold:
            var threshold = new Accord.Imaging.Filters.SauvolaThreshold();

            // Compute the filter
            bmp = threshold.Apply(bmp);

            /*Source: http://stackoverflow.com/questions/248141/remove-surrounding-whitespace-from-an-image */

            int w = bmp.Width;
            int h = bmp.Height;

            Func<int, bool> allWhiteRow = row =>
            {
                for (int i = 0; i < w; ++i)
                    if (bmp.GetPixel(i, row).R != 255)
                        return false;
                return true;
            };

            Func<int, bool> allWhiteColumn = col =>
            {
                for (int i = 0; i < h; ++i)
                    if (bmp.GetPixel(col, i).R != 255)
                        return false;
                return true;
            };

            int topmost = 0;
            for (int row = 0; row < h; ++row)
            {
                if (allWhiteRow(row))
                    topmost = row;
                else break;
            }

            int bottommost = 0;
            for (int row = h - 1; row >= 0; --row)
            {
                if (allWhiteRow(row))
                    bottommost = row;
                else break;
            }

            int leftmost = 0, rightmost = 0;
            for (int col = 0; col < w; ++col)
            {
                if (allWhiteColumn(col))
                    leftmost = col;
                else
                    break;
            }

            for (int col = w - 1; col >= 0; --col)
            {
                if (allWhiteColumn(col))
                    rightmost = col;
                else
                    break;
            }

            if (rightmost == 0) rightmost = w; // As reached left
            if (bottommost == 0) bottommost = h; // As reached top.

            int croppedWidth = rightmost - leftmost;
            int croppedHeight = bottommost - topmost;

            if (croppedWidth == 0) // No border on left or right
            {
                leftmost = 0;
                croppedWidth = w;
            }

            if (croppedHeight == 0) // No border on top or bottom
            {
                topmost = 0;
                croppedHeight = h;
            }

            try
            {
                var target = new Bitmap(croppedWidth, croppedHeight);
                using (Graphics g = Graphics.FromImage(target))
                {
                    g.DrawImage(bmp,
                      new RectangleF(0, 0, croppedWidth, croppedHeight),
                      new RectangleF(leftmost, topmost, croppedWidth, croppedHeight),
                      GraphicsUnit.Pixel);
                }
                return target;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("Values are topmost={0} btm={1} left={2} right={3} croppedWidth={4} croppedHeight={5}", topmost, bottommost, leftmost, rightmost, croppedWidth, croppedHeight),
                  ex);
            }
        }

        public static Rectangle SurroundingWhitePixelsRemovedRectangle(Bitmap bmp)
        {
            //Filter Added by nirajan_pant@yahoo.com .. 2015-08-19
            // binarize image using Accord.NET
            // Create a new Sauvola threshold:
            var threshold = new Accord.Imaging.Filters.SauvolaThreshold();

            // Compute the filter
            bmp = threshold.Apply(bmp);

            /*Source: http://stackoverflow.com/questions/248141/remove-surrounding-whitespace-from-an-image */

            int w = bmp.Width;
            int h = bmp.Height;

            Func<int, bool> allWhiteRow = row =>
            {
                for (int i = 0; i < w; ++i)
                    if (bmp.GetPixel(i, row).R != 255)
                        return false;
                return true;
            };

            Func<int, bool> allWhiteColumn = col =>
            {
                for (int i = 0; i < h; ++i)
                    if (bmp.GetPixel(col, i).R != 255)
                        return false;
                return true;
            };

            int topmost = 0;
            for (int row = 0; row < h; ++row)
            {
                if (allWhiteRow(row))
                    topmost = row;
                else break;
            }

            int bottommost = 0;
            for (int row = h - 1; row >= 0; --row)
            {
                if (allWhiteRow(row))
                    bottommost = row;
                else break;
            }

            int leftmost = 0, rightmost = 0;
            for (int col = 0; col < w; ++col)
            {
                if (allWhiteColumn(col))
                    leftmost = col;
                else
                    break;
            }

            for (int col = w - 1; col >= 0; --col)
            {
                if (allWhiteColumn(col))
                    rightmost = col;
                else
                    break;
            }

            if (rightmost == 0) rightmost = w; // As reached left
            if (bottommost == 0) bottommost = h; // As reached top.

            int croppedWidth = rightmost - leftmost;
            int croppedHeight = bottommost - topmost;

            if (croppedWidth == 0) // No border on left or right
            {
                leftmost = 0;
                croppedWidth = w;
            }

            if (croppedHeight == 0) // No border on top or bottom
            {
                topmost = 0;
                croppedHeight = h;
            }

            try
            {
                return new Rectangle(leftmost, topmost, croppedWidth, croppedHeight);
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("Values are topmost={0} btm={1} left={2} right={3} croppedWidth={4} croppedHeight={5}", topmost, bottommost, leftmost, rightmost, croppedWidth, croppedHeight),
                  ex);
            }
        }

        public static Bitmap Blur(Bitmap image, Rectangle rectangle, Int32 blurSize)
        {
            /* blur Method Source: http://notes.ericwillis.com/2009/10/blur-an-image-with-csharp/ 
             * More on Method: Blurring an image with c# is pretty simple. I was messing around with something one night and needed to blur certain regions of JPGs
             * that I was processing. This method accepts an image, a blur region (rectangle), a blur size and returns a blurred bitmap.*/

            Bitmap blurred = new Bitmap(image.Width, image.Height);

            // make an exact copy of the bitmap provided
            using (Graphics graphics = Graphics.FromImage(blurred))
                graphics.DrawImage(image, new Rectangle(0, 0, image.Width, image.Height),
                    new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);

            // look at every pixel in the blur rectangle
            for (Int32 xx = rectangle.X; xx < rectangle.X + rectangle.Width; xx++)
            {
                for (Int32 yy = rectangle.Y; yy < rectangle.Y + rectangle.Height; yy++)
                {
                    Int32 avgR = 0, avgG = 0, avgB = 0;
                    Int32 blurPixelCount = 0;

                    // average the color of the red, green and blue for each pixel in the
                    // blur size while making sure you don't go outside the image bounds
                    for (Int32 x = xx; (x < xx + blurSize && x < image.Width); x++)
                    {
                        for (Int32 y = yy; (y < yy + blurSize && y < image.Height); y++)
                        {
                            Color pixel = blurred.GetPixel(x, y);

                            avgR += pixel.R;
                            avgG += pixel.G;
                            avgB += pixel.B;

                            blurPixelCount++;
                        }
                    }

                    avgR = avgR / blurPixelCount;
                    avgG = avgG / blurPixelCount;
                    avgB = avgB / blurPixelCount;

                    // now that we know the average for the blur size, set each pixel to that color
                    for (Int32 x = xx; x < xx + blurSize && x < image.Width && x < rectangle.Width; x++)
                        for (Int32 y = yy; y < yy + blurSize && y < image.Height && y < rectangle.Height; y++)
                            blurred.SetPixel(x, y, Color.FromArgb(avgR, avgG, avgB));
                }
            }

            return blurred;
        }

        public static Bitmap IterativeThreshold(Bitmap bmp)
        {
            // use of aforge method for threshold
            // load image
            UnmanagedImage image = UnmanagedImage.FromManagedImage(bmp);

            UnmanagedImage grayImage = null;

            if (image.PixelFormat == PixelFormat.Format8bppIndexed)
            {
                grayImage = image;
            }
            else
            {
                grayImage = UnmanagedImage.Create(image.Width, image.Height,
                    PixelFormat.Format8bppIndexed);
                Grayscale.CommonAlgorithms.BT709.Apply(image, grayImage);
            }

            // create filter
            IterativeThreshold filter = new IterativeThreshold(2, 128);
            // apply the filter
            Bitmap bm = filter.Apply(grayImage).ToManagedImage(true);

            // An indexed image is an image with a palette.  GDI+ doesn't support drawing to indexed images.  Draw your indexed image on a non-indexed image and then draw your text on the non-indexed image:
            Bitmap nonIndexedBitmap = new Bitmap(bm.Width, bm.Height);
            Graphics graphics = Graphics.FromImage(nonIndexedBitmap);
            graphics.DrawImage(bm, 0, 0);

            return nonIndexedBitmap;
        }

        public static Bitmap Threshold(Bitmap original, int thresh)
        {
            //Bitmap original = (Bitmap)this.inputPicBox.Image;
            ImageAttributes imgAttribute = new ImageAttributes();
            imgAttribute.SetThreshold(thresh);
            //MessageBox.Show("height=" + original.Height + "  width=" + original.Width);
            Bitmap bmp = new Bitmap(original.Width, original.Height); // Determining Width and Height of Source Image
            Graphics graphics = Graphics.FromImage(bmp);
            graphics.DrawImage(original, new Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, original.Width, original.Height, GraphicsUnit.Pixel, imgAttribute);
            graphics.Dispose();   // Releasing all resource used by graphics             
            
            return bmp;
        }

        public static void Cut(Bitmap img)
        { }

        public static void Erage(Bitmap img)
        { }

        public static void ZoomIn(Bitmap img)
        { }

        public static void ZoomOut(Bitmap img)
        { }

        public static Bitmap RotateImage(Bitmap bmp, double angle)
        {
            Graphics g = default(Graphics);
            Bitmap tmp = new Bitmap(bmp.Width, bmp.Height, PixelFormat.Format32bppRgb);

            tmp.SetResolution(bmp.HorizontalResolution, bmp.VerticalResolution);
            g = Graphics.FromImage(tmp);
            try
            {
                g.FillRectangle(Brushes.White, 0, 0, bmp.Width, bmp.Height);
                g.RotateTransform(Convert.ToSingle(angle));
                g.DrawImage(bmp, 0, 0);
            }
            finally
            {
                g.Dispose();
            }
            return tmp;
        }

        public static void Select(Bitmap img)
        { }

        public static Bitmap CropAtRectangle(Bitmap b, Rectangle r)
        {
            Bitmap nb = new Bitmap(r.Width, r.Height);
            Graphics g = Graphics.FromImage(nb);
            g.DrawImage(b, -r.X, -r.Y);
            return nb;
        }

        // DialateAndErodeFilter Source : http://softwarebydefault.com/2013/05/19/image-erosion-dilation/
        public enum MorphologyType
        {
            Dilation,
            Erosion
        }

        public static Bitmap DilateAndErodeFilter(
                                   this Bitmap sourceBitmap,
                                   int matrixSize,
                                   MorphologyType morphType,
                                   bool applyBlue = true,
                                   bool applyGreen = true,
                                   bool applyRed = true)
        {
            BitmapData sourceData =
                       sourceBitmap.LockBits(new Rectangle(0, 0,
                       sourceBitmap.Width, sourceBitmap.Height),
                       ImageLockMode.ReadOnly,
                       PixelFormat.Format32bppArgb);


            byte[] pixelBuffer = new byte[sourceData.Stride *
                                          sourceData.Height];


            byte[] resultBuffer = new byte[sourceData.Stride *
                                           sourceData.Height];


            Marshal.Copy(sourceData.Scan0, pixelBuffer, 0,
                                       pixelBuffer.Length);


            sourceBitmap.UnlockBits(sourceData);


            int filterOffset = (matrixSize - 1) / 2;
            int calcOffset = 0;


            int byteOffset = 0;


            byte blue = 0;
            byte green = 0;
            byte red = 0;


            byte morphResetValue = 0;


            if (morphType == MorphologyType.Erosion)
            {
                morphResetValue = 255;
            }


            for (int offsetY = filterOffset; offsetY <
                sourceBitmap.Height - filterOffset; offsetY++)
            {
                for (int offsetX = filterOffset; offsetX <
                    sourceBitmap.Width - filterOffset; offsetX++)
                {
                    byteOffset = offsetY *
                                 sourceData.Stride +
                                 offsetX * 4;


                    blue = morphResetValue;
                    green = morphResetValue;
                    red = morphResetValue;


                    if (morphType == MorphologyType.Dilation)
                    {
                        for (int filterY = -filterOffset;
                            filterY <= filterOffset; filterY++)
                        {
                            for (int filterX = -filterOffset;
                                filterX <= filterOffset; filterX++)
                            {
                                calcOffset = byteOffset +
                                             (filterX * 4) +
                                (filterY * sourceData.Stride);


                                if (pixelBuffer[calcOffset] > blue)
                                {
                                    blue = pixelBuffer[calcOffset];
                                }


                                if (pixelBuffer[calcOffset + 1] > green)
                                {
                                    green = pixelBuffer[calcOffset + 1];
                                }


                                if (pixelBuffer[calcOffset + 2] > red)
                                {
                                    red = pixelBuffer[calcOffset + 2];
                                }
                            }
                        }
                    }
                    else if (morphType == MorphologyType.Erosion)
                    {
                        for (int filterY = -filterOffset;
                            filterY <= filterOffset; filterY++)
                        {
                            for (int filterX = -filterOffset;
                                filterX <= filterOffset; filterX++)
                            {
                                calcOffset = byteOffset +
                                             (filterX * 4) +
                                (filterY * sourceData.Stride);


                                if (pixelBuffer[calcOffset] < blue)
                                {
                                    blue = pixelBuffer[calcOffset];
                                }


                                if (pixelBuffer[calcOffset + 1] < green)
                                {
                                    green = pixelBuffer[calcOffset + 1];
                                }


                                if (pixelBuffer[calcOffset + 2] < red)
                                {
                                    red = pixelBuffer[calcOffset + 2];
                                }
                            }
                        }
                    }


                    if (applyBlue == false)
                    {
                        blue = pixelBuffer[byteOffset];
                    }


                    if (applyGreen == false)
                    {
                        green = pixelBuffer[byteOffset + 1];
                    }


                    if (applyRed == false)
                    {
                        red = pixelBuffer[byteOffset + 2];
                    }


                    resultBuffer[byteOffset] = blue;
                    resultBuffer[byteOffset + 1] = green;
                    resultBuffer[byteOffset + 2] = red;
                    resultBuffer[byteOffset + 3] = 255;
                }
            }


            Bitmap resultBitmap = new Bitmap(sourceBitmap.Width,
                                             sourceBitmap.Height);


            BitmapData resultData =
                       resultBitmap.LockBits(new Rectangle(0, 0,
                       resultBitmap.Width, resultBitmap.Height),
                       ImageLockMode.WriteOnly,
                       PixelFormat.Format32bppArgb);


            Marshal.Copy(resultBuffer, 0, resultData.Scan0,
                                       resultBuffer.Length);


            resultBitmap.UnlockBits(resultData);


            return resultBitmap;
        }

        // Median filter source: http://softwarebydefault.com/2013/06/09/image-blur-filters/
        public static Bitmap MedianFilter(this Bitmap sourceBitmap,
                                   int matrixSize)
        {
            BitmapData sourceData =
                       sourceBitmap.LockBits(new Rectangle(0, 0,
                       sourceBitmap.Width, sourceBitmap.Height),
                       ImageLockMode.ReadOnly,
                       PixelFormat.Format32bppArgb);


            byte[] pixelBuffer = new byte[sourceData.Stride *
                                          sourceData.Height];


            byte[] resultBuffer = new byte[sourceData.Stride *
                                           sourceData.Height];


            Marshal.Copy(sourceData.Scan0, pixelBuffer, 0,
                                       pixelBuffer.Length);


            sourceBitmap.UnlockBits(sourceData);


            int filterOffset = (matrixSize - 1) / 2;
            int calcOffset = 0;


            int byteOffset = 0;


            List<int> neighbourPixels = new List<int>();
            byte[] middlePixel;


            for (int offsetY = filterOffset; offsetY <
                sourceBitmap.Height - filterOffset; offsetY++)
            {
                for (int offsetX = filterOffset; offsetX <
                    sourceBitmap.Width - filterOffset; offsetX++)
                {
                    byteOffset = offsetY *
                                 sourceData.Stride +
                                 offsetX * 4;


                    neighbourPixels.Clear();


                    for (int filterY = -filterOffset;
                        filterY <= filterOffset; filterY++)
                    {
                        for (int filterX = -filterOffset;
                            filterX <= filterOffset; filterX++)
                        {


                            calcOffset = byteOffset +
                                         (filterX * 4) +
                                         (filterY * sourceData.Stride);


                            neighbourPixels.Add(BitConverter.ToInt32(
                                             pixelBuffer, calcOffset));
                        }
                    }


                    neighbourPixels.Sort();

                    middlePixel = BitConverter.GetBytes(
                                       neighbourPixels[filterOffset]);


                    resultBuffer[byteOffset] = middlePixel[0];
                    resultBuffer[byteOffset + 1] = middlePixel[1];
                    resultBuffer[byteOffset + 2] = middlePixel[2];
                    resultBuffer[byteOffset + 3] = middlePixel[3];
                }
            }


            Bitmap resultBitmap = new Bitmap(sourceBitmap.Width,
                                             sourceBitmap.Height);


            BitmapData resultData =
                       resultBitmap.LockBits(new Rectangle(0, 0,
                       resultBitmap.Width, resultBitmap.Height),
                       ImageLockMode.WriteOnly,
                       PixelFormat.Format32bppArgb);


            Marshal.Copy(resultBuffer, 0, resultData.Scan0,
                                       resultBuffer.Length);


            resultBitmap.UnlockBits(resultData);


            return resultBitmap;
        }

        // Convolution filter source: http://softwarebydefault.com/2013/06/09/image-blur-filters/        
        private static Bitmap ConvolutionFilter(this Bitmap sourceBitmap,
                                                  double[,] filterMatrix,
                                                       double factor = 1,
                                                            int bias = 0)
        {
            BitmapData sourceData = sourceBitmap.LockBits(new Rectangle(0, 0,
                                     sourceBitmap.Width, sourceBitmap.Height),
                                                       ImageLockMode.ReadOnly,
                                                 PixelFormat.Format32bppArgb);


            byte[] pixelBuffer = new byte[sourceData.Stride * sourceData.Height];
            byte[] resultBuffer = new byte[sourceData.Stride * sourceData.Height];


            Marshal.Copy(sourceData.Scan0, pixelBuffer, 0, pixelBuffer.Length);
            sourceBitmap.UnlockBits(sourceData);


            double blue = 0.0;
            double green = 0.0;
            double red = 0.0;


            int filterWidth = filterMatrix.GetLength(1);
            int filterHeight = filterMatrix.GetLength(0);


            int filterOffset = (filterWidth - 1) / 2;
            int calcOffset = 0;


            int byteOffset = 0;


            for (int offsetY = filterOffset; offsetY <
                sourceBitmap.Height - filterOffset; offsetY++)
            {
                for (int offsetX = filterOffset; offsetX <
                    sourceBitmap.Width - filterOffset; offsetX++)
                {
                    blue = 0;
                    green = 0;
                    red = 0;


                    byteOffset = offsetY *
                                 sourceData.Stride +
                                 offsetX * 4;


                    for (int filterY = -filterOffset;
                        filterY <= filterOffset; filterY++)
                    {
                        for (int filterX = -filterOffset;
                            filterX <= filterOffset; filterX++)
                        {


                            calcOffset = byteOffset +
                                         (filterX * 4) +
                                         (filterY * sourceData.Stride);


                            blue += (double)(pixelBuffer[calcOffset]) *
                                    filterMatrix[filterY + filterOffset,
                                                        filterX + filterOffset];


                            green += (double)(pixelBuffer[calcOffset + 1]) *
                                     filterMatrix[filterY + filterOffset,
                                                        filterX + filterOffset];


                            red += (double)(pixelBuffer[calcOffset + 2]) *
                                   filterMatrix[filterY + filterOffset,
                                                      filterX + filterOffset];
                        }
                    }


                    blue = factor * blue + bias;
                    green = factor * green + bias;
                    red = factor * red + bias;


                    blue = (blue > 255 ? 255 :
                           (blue < 0 ? 0 :
                            blue));


                    green = (green > 255 ? 255 :
                            (green < 0 ? 0 :
                             green));


                    red = (red > 255 ? 255 :
                          (red < 0 ? 0 :
                           red));


                    resultBuffer[byteOffset] = (byte)(blue);
                    resultBuffer[byteOffset + 1] = (byte)(green);
                    resultBuffer[byteOffset + 2] = (byte)(red);
                    resultBuffer[byteOffset + 3] = 255;
                }
            }


            Bitmap resultBitmap = new Bitmap(sourceBitmap.Width, sourceBitmap.Height);


            BitmapData resultData = resultBitmap.LockBits(new Rectangle(0, 0,
                                     resultBitmap.Width, resultBitmap.Height),
                                                      ImageLockMode.WriteOnly,
                                                 PixelFormat.Format32bppArgb);


            Marshal.Copy(resultBuffer, 0, resultData.Scan0, resultBuffer.Length);
            resultBitmap.UnlockBits(resultData);


            return resultBitmap;
        }

        // ImageBlurFilter filter source: http://softwarebydefault.com/2013/06/09/image-blur-filters/        
        public enum BlurType
        {
            GaussianBlur3x3,
            GaussianBlur5x5,
            Mean3x3,
            Mean5x5,
            Mean7x7,
            Mean9x9,
            Median3x3,
            Median5x5,
            Median7x7,
            Median9x9,
            Median11x11,
            MotionBlur5x5,
            MotionBlur5x5At135Degrees,
            MotionBlur5x5At45Degrees,
            MotionBlur7x7,
            MotionBlur7x7At135Degrees,
            MotionBlur7x7At45Degrees,
            MotionBlur9x9,
            MotionBlur9x9At135Degrees,
            MotionBlur9x9At45Degrees
        }

        public static Bitmap ImageBlurFilter(this Bitmap sourceBitmap,
                                                     BlurType blurType)
        {
            Bitmap resultBitmap = null;


            switch (blurType)
            {
                case BlurType.Mean3x3:
                    {
                        resultBitmap = sourceBitmap.ConvolutionFilter(
                                       Matrix.Mean3x3, 1.0 / 9.0, 0);
                    } break;
                case BlurType.Mean5x5:
                    {
                        resultBitmap = sourceBitmap.ConvolutionFilter(
                                       Matrix.Mean5x5, 1.0 / 25.0, 0);
                    } break;
                case BlurType.Mean7x7:
                    {
                        resultBitmap = sourceBitmap.ConvolutionFilter(
                                       Matrix.Mean7x7, 1.0 / 49.0, 0);
                    } break;
                case BlurType.Mean9x9:
                    {
                        resultBitmap = sourceBitmap.ConvolutionFilter(
                                       Matrix.Mean9x9, 1.0 / 81.0, 0);
                    } break;
                case BlurType.GaussianBlur3x3:
                    {
                        resultBitmap = sourceBitmap.ConvolutionFilter(
                                Matrix.GaussianBlur3x3, 1.0 / 16.0, 0);
                    } break;
                case BlurType.GaussianBlur5x5:
                    {
                        resultBitmap = sourceBitmap.ConvolutionFilter(
                               Matrix.GaussianBlur5x5, 1.0 / 159.0, 0);
                    } break;
                case BlurType.MotionBlur5x5:
                    {
                        resultBitmap = sourceBitmap.ConvolutionFilter(
                                  Matrix.MotionBlur5x5, 1.0 / 10.0, 0);
                    } break;
                case BlurType.MotionBlur5x5At45Degrees:
                    {
                        resultBitmap = sourceBitmap.ConvolutionFilter(
                        Matrix.MotionBlur5x5At45Degrees, 1.0 / 5.0, 0);
                    } break;
                case BlurType.MotionBlur5x5At135Degrees:
                    {
                        resultBitmap = sourceBitmap.ConvolutionFilter(
                        Matrix.MotionBlur5x5At135Degrees, 1.0 / 5.0, 0);
                    } break;
                case BlurType.MotionBlur7x7:
                    {
                        resultBitmap = sourceBitmap.ConvolutionFilter(
                        Matrix.MotionBlur7x7, 1.0 / 14.0, 0);
                    } break;
                case BlurType.MotionBlur7x7At45Degrees:
                    {
                        resultBitmap = sourceBitmap.ConvolutionFilter(
                        Matrix.MotionBlur7x7At45Degrees, 1.0 / 7.0, 0);
                    } break;
                case BlurType.MotionBlur7x7At135Degrees:
                    {
                        resultBitmap = sourceBitmap.ConvolutionFilter(
                        Matrix.MotionBlur7x7At135Degrees, 1.0 / 7.0, 0);
                    } break;
                case BlurType.MotionBlur9x9:
                    {
                        resultBitmap = sourceBitmap.ConvolutionFilter(
                        Matrix.MotionBlur9x9, 1.0 / 18.0, 0);
                    } break;
                case BlurType.MotionBlur9x9At45Degrees:
                    {
                        resultBitmap = sourceBitmap.ConvolutionFilter(
                        Matrix.MotionBlur9x9At45Degrees, 1.0 / 9.0, 0);
                    } break;
                case BlurType.MotionBlur9x9At135Degrees:
                    {
                        resultBitmap = sourceBitmap.ConvolutionFilter(
                        Matrix.MotionBlur9x9At135Degrees, 1.0 / 9.0, 0);
                    } break;
                case BlurType.Median3x3:
                    {
                        resultBitmap = sourceBitmap.MedianFilter(3);
                    } break;
                case BlurType.Median5x5:
                    {
                        resultBitmap = sourceBitmap.MedianFilter(5);
                    } break;
                case BlurType.Median7x7:
                    {
                        resultBitmap = sourceBitmap.MedianFilter(7);
                    } break;
                case BlurType.Median9x9:
                    {
                        resultBitmap = sourceBitmap.MedianFilter(9);
                    } break;
                case BlurType.Median11x11:
                    {
                        resultBitmap = sourceBitmap.MedianFilter(11);
                    } break;
            }


            return resultBitmap;
        }

        public static Bitmap FillHoles(Bitmap bitmap, int MaxHoleWidth, int MaxHoleHeight)
        {           
            // create and configure the filter
            AForge.Imaging.Filters.FillHoles filter = new AForge.Imaging.Filters.FillHoles();
            filter.MaxHoleHeight = MaxHoleHeight;
            filter.MaxHoleWidth = MaxHoleWidth;
            filter.CoupledSizeFiltering = false;
            // apply the filter
            //Bitmap result = filter.Apply( image );

            // IF IMAGE IS NOT BINARIZED
            for (int i = 0; i < bitmap.Height; i++)
            {
                for (int j = 0; j < bitmap.Width; j++)
                {
                    Color color = bitmap.GetPixel(j, i);

                    // String value "0" for Black and "255" for white
                    if (!((color.R.ToString().Equals("0") && color.G.ToString().Equals("0") && color.B.ToString().Equals("0"))
                        || (color.R.ToString().Equals("255") && color.G.ToString().Equals("255") && color.B.ToString().Equals("255"))))
                    {
                        // CONVERT TO GRAYSCALE IMAGE
                        bitmap = new OtsuMethod().MakeGrayscale(bitmap);
                        
                        // THRESHOLDING 
                        bitmap = ImageLab.IterativeThreshold(bitmap);
                    }
                }
            }

            // change image pixel format
            AForge.Imaging.UnmanagedImage image = AForge.Imaging.UnmanagedImage.FromManagedImage(bitmap);
            AForge.Imaging.UnmanagedImage grayImage = null;

            if (image.PixelFormat == System.Drawing.Imaging.PixelFormat.Format8bppIndexed)
            {
                grayImage = image;
            }
            else
            {
                grayImage = AForge.Imaging.UnmanagedImage.Create(image.Width, image.Height,
                    System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
                AForge.Imaging.Filters.Grayscale.CommonAlgorithms.BT709.Apply(image, grayImage);
            }

            // apply the filter
            Bitmap bm = filter.Apply(grayImage).ToManagedImage(true);

            return ConvertToNonIndexedImage(bm);
        }

        public static Bitmap ConvertToNonIndexedImage(Bitmap bm)
        {
            // An indexed image is an image with a palette.  GDI+ doesn't support drawing to indexed images.  Draw your indexed image on a non-indexed image and then draw your text on the non-indexed image:
            Bitmap nonIndexedBitmap = new Bitmap(bm.Width, bm.Height);
            Graphics graphics = Graphics.FromImage(nonIndexedBitmap);
            graphics.DrawImage(bm, 0, 0);

            return nonIndexedBitmap;
        }

        public static Bitmap Thinning(Bitmap bitmap)
        {
            // create filter sequence
            FiltersSequence filterSequence = new FiltersSequence();
            // add 8 thinning filters with different structuring elements
            filterSequence.Add(new HitAndMiss(
                new short[,] { { 0, 0, 0 }, { -1, 1, -1 }, { 1, 1, 1 } },
                HitAndMiss.Modes.Thinning));
            filterSequence.Add(new HitAndMiss(
                new short[,] { { -1, 0, 0 }, { 1, 1, 0 }, { -1, 1, -1 } },
                HitAndMiss.Modes.Thinning));
            filterSequence.Add(new HitAndMiss(
                new short[,] { { 1, -1, 0 }, { 1, 1, 0 }, { 1, -1, 0 } },
                HitAndMiss.Modes.Thinning));
            filterSequence.Add(new HitAndMiss(
                new short[,] { { -1, 1, -1 }, { 1, 1, 0 }, { -1, 0, 0 } },
                HitAndMiss.Modes.Thinning));
            filterSequence.Add(new HitAndMiss(
                new short[,] { { 1, 1, 1 }, { -1, 1, -1 }, { 0, 0, 0 } },
                HitAndMiss.Modes.Thinning));
            filterSequence.Add(new HitAndMiss(
                new short[,] { { -1, 1, -1 }, { 0, 1, 1 }, { 0, 0, -1 } },
                HitAndMiss.Modes.Thinning));
            filterSequence.Add(new HitAndMiss(
                new short[,] { { 0, -1, 1 }, { 0, 1, 1 }, { 0, -1, 1 } },
                HitAndMiss.Modes.Thinning));
            filterSequence.Add(new HitAndMiss(
                new short[,] { { 0, 0, -1 }, { 0, 1, 1 }, { -1, 1, -1 } },
                HitAndMiss.Modes.Thinning));
            // create filter iterator for 10 iterations
            FilterIterator filter = new FilterIterator(filterSequence, 10);
            // apply the filter


            Bitmap originalBmp = new Bitmap(bitmap);

            UnmanagedImage bm = UnmanagedImage.FromManagedImage(originalBmp);

            using (MemoryStream myMemoryStream = new MemoryStream())
            {
                originalBmp.Save(myMemoryStream, ImageFormat.Gif);
                myMemoryStream.Position = 0;
                bm = getUnmanagedImage((Bitmap)System.Drawing.Image.FromStream(myMemoryStream));
            }

            UnmanagedImage newImage = filter.Apply(bm);

            return newImage.ToManagedImage();
        }

        public static UnmanagedImage getUnmanagedImage(Bitmap image)
        {
            // sample 1 - wrapping .NET image into unmanaged without
            // making extra copy of image in memory
            BitmapData imageData = image.LockBits(
                new Rectangle(0, 0, image.Width, image.Height),
                ImageLockMode.ReadWrite, image.PixelFormat);

            UnmanagedImage unmanagedImage;

            try
            {
                unmanagedImage = new UnmanagedImage(imageData);
                // apply several routines to the unmanaged image
            }
            finally
            {
                image.UnlockBits(imageData);
            }


            //// sample 2 - converting .NET image into unmanaged
            //UnmanagedImage unmanagedImage = UnmanagedImage.FromManagedImage( image );
            //// apply several routines to the unmanaged image
            //...
            //// conver to managed image if it is required to display it at some point of time
            //Bitmap managedImage = unmanagedImage.ToManagedImage( );

            return unmanagedImage;

        }

        public static Bitmap RotateImage(Bitmap bmp, float angle, Color backgroundColor)
        {
            /*
             * Source: http://stackoverflow.com/questions/14184700/how-to-rotate-image-x-degrees-in-c [Date Accessed: Mar 17, 2016]
             */

            angle = angle % 360;
            if (angle > 180)
                angle -= 360;

            System.Drawing.Imaging.PixelFormat pf = default(System.Drawing.Imaging.PixelFormat);
            if (backgroundColor == Color.Transparent)
            {
                pf = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
            }
            else
            {
                pf = bmp.PixelFormat;
            }

            float sin = (float)Math.Abs(Math.Sin(angle * Math.PI / 180.0)); // this function takes radians
            float cos = (float)Math.Abs(Math.Cos(angle * Math.PI / 180.0)); // this one too
            float newImgWidth = sin * bmp.Height + cos * bmp.Width;
            float newImgHeight = sin * bmp.Width + cos * bmp.Height;
            float originX = 0f;
            float originY = 0f;

            if (angle > 0)
            {
                if (angle <= 90)
                    originX = sin * bmp.Height;
                else
                {
                    originX = newImgWidth;
                    originY = newImgHeight - sin * bmp.Width;
                }
            }
            else
            {
                if (angle >= -90)
                    originY = sin * bmp.Width;
                else
                {
                    originX = newImgWidth - sin * bmp.Height;
                    originY = newImgHeight;
                }
            }

            Bitmap newImg = new Bitmap((int)newImgWidth, (int)newImgHeight, pf);
            Graphics g = Graphics.FromImage(newImg);
            g.Clear(backgroundColor);
            g.TranslateTransform(originX, originY); // offset the origin to our calculated values
            g.RotateTransform(angle); // set up rotate
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;
            g.DrawImageUnscaled(bmp, 0, 0); // draw the image at 0, 0
            g.Dispose();

            return newImg;
        }

        public static Bitmap HorizontalBlurring(Bitmap img, int radius = 2)
        {
            int img_width = img.Width;
            int img_height = img.Height;
            for (int i = 0; i < img_height; i++)
            {
                for (int j = 0; j < img_width; j++)
                {
                    float A = 0, R = 0, G = 0, B = 0;
                    int neighbors = 0;
                    // Calculate sum of surrounded pixels value
                    for (int k = 1; k <= radius; k++)
                    {
                        // get left pixel
                        if (j - k > 0)
                        {
                            A += img.GetPixel(j - k, i).A;
                            R += img.GetPixel(j - k, i).R;
                            G += img.GetPixel(j - k, i).G;
                            B += img.GetPixel(j - k, i).B;
                            neighbors++;
                        }

                        // get right pixel
                        if (j + k < img_width)
                        {
                            A += img.GetPixel(j + k, i).A;
                            R += img.GetPixel(j + k, i).R;
                            G += img.GetPixel(j + k, i).G;
                            B += img.GetPixel(j + k, i).B;
                            neighbors++;
                        }
                    }

                    A += img.GetPixel(j, i).A;
                    R += img.GetPixel(j, i).R;
                    G += img.GetPixel(j, i).G;
                    B += img.GetPixel(j, i).B;

                    // Minimize average pixel value by a little to make it more dark
                    A = A / (neighbors + 1);
                    R = R / (neighbors + 1);
                    G = G / (neighbors + 1);
                    B = B / (neighbors + 1);

                    img.SetPixel(j, i, Color.FromArgb((int)A, (int)R, (int)G, (int)B));
                }
            }

            return img;
        }

        public static Bitmap VerticalBlurring(Bitmap img, int radius = 2)
        {
            int img_width = img.Width;
            int img_height = img.Height;
            for (int i = 0; i < img_height; i++)
            {
                for (int j = 0; j < img_width; j++)
                {
                    float A = 0, R = 0, G = 0, B = 0;
                    int neighbors = 0;
                    // Calculate sum of surrounded pixels value
                    for (int k = 1; k <= radius; k++)
                    {
                        // get upper pixel
                        if (i - k > 0)
                        {
                            A += img.GetPixel(j, i - k).A;
                            R += img.GetPixel(j, i - k).R;
                            G += img.GetPixel(j, i - k).G;
                            B += img.GetPixel(j, i - k).B;
                            neighbors++;
                        }

                        // get lower pixel
                        if (i + k < img_width)
                        {
                            A += img.GetPixel(j, i + k).A;
                            R += img.GetPixel(j, i + k).R;
                            G += img.GetPixel(j, i + k).G;
                            B += img.GetPixel(j, i + k).B;
                            neighbors++;
                        }
                    }

                    A += img.GetPixel(j, i).A;
                    R += img.GetPixel(j, i).R;
                    G += img.GetPixel(j, i).G;
                    B += img.GetPixel(j, i).B;

                    // Minimize average pixel value by a little to make it more dark
                    A = A / (neighbors + 1);
                    R = R / (neighbors + 1);
                    G = G / (neighbors + 1);
                    B = B / (neighbors + 1);

                    img.SetPixel(j, i, Color.FromArgb((int)A, (int)R, (int)G, (int)B));
                }
            }

            return img;
        }

        public static Bitmap ChangeDPI(Bitmap bitmap, int xDPI, int yDPI)
        {
            Bitmap newBitmap = null;
            using (newBitmap = new Bitmap(bitmap))
            {
                newBitmap.SetResolution(xDPI, yDPI);
                //newBitmap.Save("file300.jpg", ImageFormat.Jpeg);
            }

            return newBitmap;
        }
    }
}
