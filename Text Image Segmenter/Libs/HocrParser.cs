﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Text_Image_Segmenter
{
    public partial class NeOCR_Main
    {
        string output_path;
        List<Rectangle> text_blocks;
        List<Rectangle> image_blocks;
        List<string> texts;
        float dpi = 0;
        float img_height = 0;
        float img_width = 0;

        //main funtion
        public Bitmap ParserStart()
        {
            text_blocks = new List<Rectangle>();
            image_blocks = new List<Rectangle>();
            texts = new List<string>();

            // get selected cell position
            int selectedRow = 0;
            selectedRow = dgvPages.SelectedCells[0].RowIndex;
            string file_path = _files[selectedRow];

            // get language
            _language = Regex.Match(cbTessOCRLanguages.Items[cbTessOCRLanguages.SelectedIndex].ToString(), @"\(([^)]*)\)").Groups[1].Value;

            GetHOCRText(file_path, _language);

            Bitmap layout_detected = (Bitmap)Image.FromFile(file_path);

            dpi = (int)Math.Ceiling(layout_detected.HorizontalResolution);
            img_height = layout_detected.Height;
            img_width = layout_detected.Width;

            Font font = new Font("Arial", 18, FontStyle.Regular, GraphicsUnit.Point);
            //Brush red_brush = Brushes.Red;
            Pen pen = new Pen(Color.Red, 1);

            using (var graphics = Graphics.FromImage(layout_detected))
            {
                int rec_count = 1;
                foreach (Rectangle rec in Blocks())
                {
                    graphics.DrawRectangle(pen, rec);
                    //  graphics.DrawString(rec_count.ToString(), font, red_brush, rec.X, rec.Y);
                    rec_count++;
                }
            }

            List<string> image_paths = saveImage((Bitmap)Image.FromFile(file_path));

            CreateDocument(image_paths);

            return layout_detected;
        }

        // ocr and write hocr to hocr.html
        public void GetHOCRText(string file_path, string language)
        {
            // set tesseract directory path
            string tessractDirPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\tesseract";
            tessractDirPath = string.Format(tessractDirPath.Replace("file:\\", ""));

            string tessdataDirPath = tessractDirPath + "\\tessdata";
            // get temp file path
            _out_file_path = Path.GetTempPath() + Path.GetFileName(file_path);

            // set arguments 
            string arguments = "\"" + file_path + "\" \"" + _out_file_path + "\" -l " + language + " --oem 1 hocr";

            // create image acquisition process
            var proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    WorkingDirectory = tessractDirPath,
                    FileName = tessractDirPath + "\\tesseract.exe",
                    Arguments = arguments,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };

            // run acquisition process
            proc.Start();

            while (!proc.StandardOutput.EndOfStream)
            {
                string line = proc.StandardOutput.ReadLine();
            }

            // Free resources associated with process.
            proc.Close();
            // Recognition END

            output_path = Path.GetTempPath() + Path.GetFileName("hocr.html");

            using (StreamReader reader = new StreamReader(_out_file_path + ".hocr"))
            {
                string line;
                if (!File.Exists(output_path))
                {
                    File.Create(output_path);
                }

                using (StreamWriter writer = new StreamWriter(output_path))
                {
                    while ((line = reader.ReadLine()) != null)
                    {
                        line = line.Replace("&#39;", "'");
                        writer.WriteLine(line);
                    }
                }

            }

        }

        // Function to get HOCR text using tesseract and try restoring layout
        public void GetParsedHOCRText(string file_path, string language)
        {
            // set tesseract directory path
            string tessractDirPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\tesseract";
            tessractDirPath = string.Format(tessractDirPath.Replace("file:\\", ""));

            string tessdataDirPath = tessractDirPath + "\\tessdata";
            // get temp file path
            _out_file_path = Path.GetTempPath() + Path.GetFileName(file_path);

            // set arguments 
            string arguments = "\"" + file_path + "\" \"" + _out_file_path + "\" -l " + language + " --oem 1 hocr";

            // create image acquisition process
            var proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    WorkingDirectory = tessractDirPath,
                    FileName = tessractDirPath + "\\tesseract.exe",
                    Arguments = arguments,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };

            // run acquisition process
            proc.Start();

            // Perform a time consuming operation and report progress.
            //backgroundWorkerOCRUsingTesseract.ReportProgress(50);
            while (!proc.StandardOutput.EndOfStream)
            {
                string line = proc.StandardOutput.ReadLine();
                //Console.Out.WriteLine(line);
                UpdateProgress(line);
                // do something with line
            }

            // Free resources associated with process.
            proc.Close();
            // Recognition END

            // output file path
            output_path = Path.GetTempPath() + Path.GetFileName("hocr.html");

            // read file content
            using (StreamReader reader = new StreamReader(_out_file_path + ".hocr"))
            {
                string line;
                if (!File.Exists(output_path))
                {
                    File.Create(output_path);
                }

                using (StreamWriter writer = new StreamWriter(output_path))
                {
                    while ((line = reader.ReadLine()) != null)
                    {
                        line = line.Replace("&#39;", "'");
                        writer.WriteLine(line);
                    }
                }
            }

            // focus on ocred text pane
            this.ActiveControl = rtbOCRedText;
        }

        public List<Rectangle> Blocks()
        {
            HtmlAgilityPack.HtmlDocument htmldoc = new HtmlAgilityPack.HtmlDocument();
            htmldoc.Load(output_path, true);

            List<Rectangle> nonempty_block_list = new List<Rectangle>();
            List<Rectangle> empty_block_list = new List<Rectangle>();
            List<Rectangle> block_list = new List<Rectangle>();

            HtmlNodeCollection nodes = htmldoc.DocumentNode.SelectNodes("//div[@class='ocr_carea']");
            rtbOCRedText.Clear();

            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex("[ ]{2,}", options);

            if (nodes != null)
            {
                foreach (HtmlNode div_node in nodes)
                {

                    string text = div_node.InnerText;
                    HtmlAttribute att = div_node.Attributes["title"];
                    string[] att_str = att.Value.Split(' ');
                    Rectangle rec = new Rectangle(int.Parse(att_str[1]), int.Parse(att_str[2]), int.Parse(att_str[3]) - int.Parse(att_str[1]), int.Parse(att_str[4]) - int.Parse(att_str[2]));

                    if (!string.IsNullOrWhiteSpace(text))
                    {
                        rtbOCRedText.Text = rtbOCRedText.Text + text;

                        string new_text = text.Replace("\r\n", "");
                        new_text = new_text.Replace("&quot;", "'");
                        texts.Add(regex.Replace(new_text, " "));

                        nonempty_block_list.Add(rec);
                        text_blocks.Add(rec);
                    }
                    else
                    {
                        empty_block_list.Add(rec);
                    }
                }
            }

            return filterBlocks(nonempty_block_list, empty_block_list);
        }

        public List<Rectangle> filterBlocks(List<Rectangle> nonempty, List<Rectangle> empty)
        {
            List<Rectangle> removeList = new List<Rectangle>();

            foreach (Rectangle erec in empty)
            {
                foreach (Rectangle nrec in nonempty)
                {
                    nrec.Intersect(erec);

                    if (!nrec.IsEmpty)
                    {
                        removeList.Add(erec);
                    }
                }
            }

            foreach (Rectangle r in removeList)
            {
                empty.Remove(r);
            }

            foreach (Rectangle re in empty)
            {
                nonempty.Add(re);
            }

            image_blocks = empty;

            return nonempty;
        }

        public List<string> saveImage(Bitmap image)
        {
            string curr_dir = Directory.GetCurrentDirectory();
            List<string> image_paths = new List<string>();

            try
            {
                foreach (Rectangle rec in image_blocks)
                {
                    Bitmap bmp = image.Clone(rec, image.PixelFormat);
                    string file_name = rec.X + "" + rec.Y + "" + rec.Width + "" + rec.Height + ".png";

                    image_paths.Add(curr_dir + "\\" + file_name);
                    bmp.Save(file_name, System.Drawing.Imaging.ImageFormat.Png);
                    bmp.Dispose();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return image_paths;
        }

        /// <summary>
        /// This function extracts text from hocr result
        /// </summary>
        /// <param name="hocr_text">hocr text- result of ocr</param>
        /// <returns>text extracted from hocr</returns>
        public string GetTextFromHOCR(string hocr_text)
        {
            var pageContent = hocr_text;
            var pageDoc = new HtmlDocument();
            pageDoc.LoadHtml(pageContent);

            var allDivNodes = pageDoc.DocumentNode.SelectNodes("//*[contains(@class,'ocr_carea')]");//"//div"
            
            var sb = new StringBuilder();
            foreach (var nodeDiv in allDivNodes)
            {
                //Console.Out.WriteLine(nodeDiv.Attributes["class"].Value);
                foreach (var node in nodeDiv.DescendantsAndSelf())
                {
                    if (!node.HasChildNodes)
                    {
                        string text = node.InnerText;
                        if (!string.IsNullOrEmpty(text))
                            sb.Append(" " + text.Trim());
                    }

                    // line break after paragraph
                    String val;
                    if (node.Attributes["class"] != null)
                    {
                        val = node.Attributes["class"].Value;
                        if (val.Equals("ocr_par")) { sb.AppendLine(); }
                    }
                }
            }

            // replace double spacing with single spacing
            String text_res = sb.ToString();
            text_res = text_res.Replace("   ", " ");
            text_res = text_res.Replace("&quot;", "'");
            text_res = text_res.Replace("&gt;", ">");
            text_res = text_res.Replace("\r\n  \r\n", "\r\n");

            // return final ocred result
            return text_res.Replace("  ", " ");
        }

        /// <summary>
        /// function to generate document layout from hocr result
        /// </summary>
        /// <param name="hocr_text">hocr text- result of ocr</param>
        /// <param name="image">image which is recognized; layout will be restored based on image and detected text box relation</param>
        /// <returns></returns>
        public void GenerateLayoutFromHOCR(string hocr_text, Bitmap layout_detect)
        {
            // parameter declaration
            text_blocks = new List<Rectangle>();
            image_blocks = new List<Rectangle>();
            texts = new List<string>();

            // get selected cell position
            int selectedRow = 0;
            selectedRow = dgvPages.SelectedCells[0].RowIndex;
            string file_path = _files[selectedRow];

            // get language
            _language = Regex.Match(cbTessOCRLanguages.Items[cbTessOCRLanguages.SelectedIndex].ToString(), @"\(([^)]*)\)").Groups[1].Value;

            //GetHOCRText(file_path, _language);

            dpi = (int)Math.Ceiling(layout_detect.HorizontalResolution);
            img_height = layout_detect.Height;
            img_width = layout_detect.Width;

            Font font = new Font("Arial", 18, FontStyle.Regular, GraphicsUnit.Point);
            //Brush red_brush = Brushes.Red;
            Pen pen = new Pen(Color.Red, 1);

            using (var graphics = Graphics.FromImage(layout_detect))
            {
                int rec_count = 1;
                foreach (Rectangle rec in Blocks())
                {
                    graphics.DrawRectangle(pen, rec);
                    //  graphics.DrawString(rec_count.ToString(), font, red_brush, rec.X, rec.Y);
                    rec_count++;
                }
            }

            List<string> image_paths = saveImage((Bitmap)Image.FromFile(file_path));
            CreateDocument(image_paths);

            //return layout_detect;
        }
    }
}
