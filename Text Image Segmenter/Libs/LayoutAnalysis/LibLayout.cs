﻿using AForge.Imaging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Text_Image_Segmenter.Libs.LayoutAnalysis
{
    /// <summary>
    /// Author: Nirajan Pant 2017-06-23
    /// Description: This code is a part of Nepali OCR Project; supported by NAB and developed by KU (Oct 2016-Aug-2017)
    /// Copyright: (c) Nirajan; This code can be used, modified and distributed freely without any warranty and with inclusion of this copyright information
    /// </summary>
    class LibLayout
    {
        /// <summary>
        /// This function used to start layout detection process
        /// </summary>
        /// <param name="input_img">Input image for layout analysis</param>
        /// <param name="hs_factor">Horizontal smearing factor; used for horizontal smearing</param>
        /// <param name="vs_factor">Vertical smearing factor; used for vertical smearing</param>
        /// <returns></returns>
        public static Blob[] StartLayoutDetection(Bitmap input_img, float hs_factor, float vs_factor)
        {
            // If global BufferedImage is not set, return
            if (input_img == null)
            {
                Console.Out.WriteLine("The Image is not set");
                return null;
            }

            // use Otsu method to grayscale
            OtsuMethod ot = new OtsuMethod();
            Bitmap grayImage = ot.MakeGrayscale(input_img);
            // apply threshold
            Bitmap thresholdImage = ImageLab.IterativeThreshold(grayImage);
            // border noise removal
            //thresholdImage = BorderNoiseRemoval.RemoveBorderNoise(thresholdImage);

            // apply Invert filter
            Bitmap inv_img = ImageLab.Invert(thresholdImage);

            //#### LAYOUT ANALYSIS STARTED ####//
            Layout layout = new Layout(inv_img);
            // layout.makeBinary(binarization);
            layout.DetectBlobs();
            // layout.removeBlobs(0.2);
            layout.removeImages();
            layout.DetectBlobs();
            layout.MarkBlobs();
            
            //Argument should be faction of size of font
            //This part ASSUMES NO IMAGES are left behind
            layout.SmearHorizontallySelectively(1);
            layout.SmearVerticallySelectively(1);

            layout.DetectBlobs();
            layout.MarkBlackSpace();

            /*
             * 
            // ArrayList<BufferedImage> segmentList = layout.getSegments();
            //layout.showBlob();

            List<Rectangle> blocks = BlobDetection.getBlocks(layout.getImage());
            */
            Bitmap bm = layout.Image;

            // apply Invert filter
            Bitmap inv = ImageLab.Invert(bm);

            inv.Save("x.png");
            return null;
        }
    }
}
