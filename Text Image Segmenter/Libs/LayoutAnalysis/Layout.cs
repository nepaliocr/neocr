﻿using AForge.Imaging;
using AForge.Imaging.Filters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Text_Image_Segmenter.Libs.LayoutAnalysis
{
    class Layout
    {
        private Bitmap original_image, image;
        private Blob[] blobs;
        List<Blob> blobList;

        public Bitmap Image
        {
            get
            {
                return this.image;
            }
            set
            {
                this.image = value;
            }
        }

        // Get The Image
        public Layout(Bitmap image)
        {
            if (image == null)
            {
                return;
            }
            // Save Original Image for Later Use
            this.original_image = image;

            // For Some Reason The Blob Finder doesn't seem to detect edges
            // So I am adding white padding..for this
            // Create an image with size greater than original by padding 0f 1 pixel
            /*
             * this.image=new
             * BufferedImage(image.getWidth()+2,image.getHeight()+2,BufferedImage
             * .TYPE_3BYTE_BGR); Graphics graphics=this.image.getGraphics();
             * graphics.setColor(Color.white); graphics.fillRect(0, 0,
             * image.getWidth(), image.getHeight()); //Draw the original image in
             * the new Image graphics.drawImage(image,1,1,null); //So we have a
             * padded image
             */

            this.image = this.original_image;
            // Copy the image;

            // this.image.getSubimage(1, 1,
            // image.getWidth()-1,image.getHeight()-1)=copyImage(image);
        }

        public void DetectBlobs()
        {
            // create instance of blob counter
            BlobCounter blobCounter = new BlobCounter();
            blobCounter.MinWidth = 2; // minimum blob size is 2x2
            blobCounter.MinHeight = 2;
            blobCounter.ObjectsOrder = ObjectsOrder.XY;

            // process input image
            blobCounter.ProcessImage(this.image);
            // get information about detected objects
            blobs = blobCounter.GetObjectsInformation();
            blobList = blobs.ToList();
        }

        public void removeImages()
        {
            List<Blob> toRemoveList = new List<Blob>();
            foreach (Blob blob in blobList)
            {
                int height = blob.Rectangle.Width;
                int width = blob.Rectangle.Height;

                if (IsText(blob) == false)
                {
                    toRemoveList.Add(blob);
                }

            }

            // remove unnecessary blobs
            Brush penTool1 = Brushes.Black;
            using (var graphics = Graphics.FromImage(image))
            {
                foreach (Blob blob in toRemoveList)
                {
                    graphics.FillRectangle(penTool1, blob.Rectangle);
                }
            }
        }

        public Boolean IsText(Blob blob)
        {
            List<KeyValuePair<String, int>> diagonal_analysis = CountPixel(blob);
            if (diagonal_analysis[0].Value > (blob.Rectangle.Width / 10))
            {
                return false;
            }
            return true;
        }

        public List<KeyValuePair<String, int>> CountPixel(Blob blob)
        {
            // create filter
            Crop filter = new Crop(blob.Rectangle);
            // apply the filter
            Bitmap takeimage = filter.Apply(image);

            int bCount = 0;
            int maxBcount = 0;
            int wCount = 0;
            int maxWcount = 0;
            int mWidth = takeimage.Width;
            int mHeight = takeimage.Height;
            int diagonal_length = mWidth > mHeight ? mHeight : mWidth;
            for (int i = 0; i < diagonal_length; i++)
            {
                Color mgetcolor = takeimage.GetPixel(i, i);
                if (mgetcolor.R == 0 && mgetcolor.G == 0 && mgetcolor.B == 0)
                { 
                    // blackpixel encounterd
                    bCount += 1;
                    if (bCount > maxBcount)
                    {
                        maxBcount = bCount;
                    }
                    wCount = 0;
                }
                else
                {
                    wCount += 1;
                    if (wCount > maxWcount)
                    {
                        maxWcount = wCount;
                        bCount = 0;
                    }
                }

            }

            KeyValuePair<String, int> white_diagonal = new KeyValuePair<string, int>("white_diagonal", maxWcount);
            KeyValuePair<String, int> black_diagonal = new KeyValuePair<string, int>("black_diagonal", maxBcount);

            Console.Out.WriteLine("WD: " + maxWcount + " WB:" + maxBcount);

            List<KeyValuePair<String, int>> retLis = new List<KeyValuePair<string, int>>();
            retLis.Add(white_diagonal);
            retLis.Add(black_diagonal);

            return retLis;
        } // CountPixel() ends here.

        public void MarkBlobs()
        {
            Pen redPenTool1 = new Pen(Color.Red, 1);
            Brush penTool1 = Brushes.Red;
            // A Handy Method that I wrote to dispaly the Blobs in the Image
            using (var graphics = Graphics.FromImage(image))
            {
                foreach (Blob blob in blobList)
                {
                    //graphics.FillRectangle(penTool, r);
                    graphics.FillRectangle(penTool1, blob.Rectangle);
                    //graphics.DrawString(recCount.ToString(), font1, penTool, r);
                }
            }
        }

        public void SmearHorizontallySelectively(int run_length)
        {
            int height = image.Height;
            int width = image.Width;
            for (int i = 0; i < height; i++)
            {
                int distance;
                Color cPrev = Color.Black;
                for (int j = 0; j < width; j++)
                {
                    Color c = image.GetPixel(j, i);
                    if (c.R != 0 && cPrev.R == 0)
                    {
                        distance = 1;
                        while (distance <= run_length && j + distance < width)
                        {
                            Color c1 = image.GetPixel(j + distance, i);
                            if (c1.R == 0)
                            {
                                // TODO improve efficiency by avoiding repeation
                                image.SetPixel(j, i, Color.White);
                                cPrev = Color.White;
                                break;
                            }
                            cPrev = c;
                            distance += 1;
                        }
                    }
                    else
                    {
                        cPrev = c;
                    }
                }
            }
        }

        public void SmearVerticallySelectively(int run_length)
        {
            int height = image.Height;
            int width = image.Width;
            for (int i = 0; i < width; i++)
            {
                int distance;
                Color cPrev = Color.Black;
                for (int j = 0; j < height; j++)
                {
                    Color c = image.GetPixel(i, j);

                    if (c.R != 0 && cPrev.R == 0)
                    {
                        distance = 1;
                        while (distance <= run_length && j + distance < height)
                        {
                            Color c1 = image.GetPixel(i, j + distance);
                            if (c1.R == 0)
                            {
                                // TODO improve efficiency by avoiding repeation
                                image.SetPixel(i, j, Color.White);
                                cPrev = Color.White;
                                break;
                            }
                            cPrev = c;
                            distance += 1;
                        }
                    }
                    else
                    {
                        cPrev = c;
                    }
                }
            }
        }

        public void MarkBlackSpace()
        {
            foreach (Blob blob in blobList)
            {
                int x = blob.Rectangle.X, y;
                while (x < blob.Rectangle.Right)
                {
                    y = blob.Rectangle.Y;
                    while (y < blob.Rectangle.Bottom)
                    {
                        image.SetPixel(x, y, Color.White);
                        y += 1;
                    }
                    x += 1;
                }
            }

        }
    }
}
