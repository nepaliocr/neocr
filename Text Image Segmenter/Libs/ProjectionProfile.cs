﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Text_Image_Segmenter.Libs
{
    class ProjectionProfile
    {
        public List<List<int>> LineSegmentation(List<int> row_pixels_list, Bitmap img)
        {
            List<List<int>> line_borders = new List<List<int>>();   //List for storing line borders .. contains line tops and line bottoms

            List<int> line_top_list = new List<int>(); //List of line top indicating rows
            List<int> line_bottom_list = new List<int>(); //List of line bottom indicating rows

            for (int m = 0; m < (row_pixels_list.Count - 1); m++) //Loop through List with for
            {
                // Scan the black pixels count list and mark upper and lower borders of line
                /*Explanation of Text Separation
                 * Start of Text Line: the line with no black pixels just before the first line with black pixels
                 * End of Text Line: the line with no black pixels just after the last line with black pixels
                 **/
                //Find line top 
                if ((row_pixels_list[m] == 0 && row_pixels_list[m + 1] != 0))
                {
                    //for (int l = 0; l < bimg.Width; l++) //Draw line representing the upper border of text line
                    //{
                    //    bimg.SetPixel(l, m, Color.Red);
                    //}
                    line_top_list.Add(m);

                }

                //Find line bottom
                if ((row_pixels_list[m] != 0 && row_pixels_list[m + 1] == 0))
                {
                    //for (int l = 0; l < bimg.Width; l++) //Draw line representing the lower border of text line
                    //{
                    //    bimg.SetPixel(l, m, Color.Red);
                    //}
                    line_bottom_list.Add(m);
                }
            }

            //Make the list with equal entries
            if (line_top_list.Count() > line_bottom_list.Count())
            {
                line_bottom_list.Add(img.Height - 1);
            }

            line_borders.Add(line_top_list);
            line_borders.Add(line_bottom_list);
            return LineRefine(line_borders);
        }

        public List<List<int>> LineRefine(List<List<int>> line_borders)
        {
            for (int j = (line_borders[0].Count - 1); j >= 0; j--)
            {
                if ((line_borders[1][j] - line_borders[0][j]) < 10)     // Assumes that line height must be more that 10 pixels ...
                {                                                       // This method is not perfect but .. work fine for removing false lines due to dots ...
                    line_borders[0].RemoveAt(j);
                    line_borders[1].RemoveAt(j);
                }
            }

            return line_borders;
        }

        public List<KeyValuePair<int, Rectangle>> WordSegmentation(List<List<int>> line_borders, Bitmap orgImg)
        {
            List<int> line_top_list = line_borders[0]; //List of line top indicating rows
            List<int> line_bottom_list = line_borders[1]; //List of line bottom indicating rows  

            //Vertical Projection on Detected Text Lines
            //Declare list of rectangles. Rectangle represents the part of image covered by a single word
            var wordlist = new List<KeyValuePair<int, Rectangle>>();

            //List for storing vertical projection data
            List<int> vp_data = new List<int>();
            //Vertical Projection to detect words in each text line
            //Assume that each pair of list data represents upper and lower border of text line
            int vp_black_pixels = 0;

            bool coordinates = false;   //If the rectangle coordinates calculated
            bool size = false;          //If the size of rectangle in calculated
            int word_count = 0;         //variable for storing Number of words
            int i = 0, j = 0;
            //Run for Loop for each line in the document
            for (int n = 0; n < (line_top_list.Count); n++)
            {
                //Get upper and lower border of line
                int u_border = line_top_list[n];
                int l_border = line_bottom_list[n];

                //Vertical Projection for each text line
                for (int o = 0; o < (orgImg.Width - 1); o++) //loop for projection
                {
                    for (j = u_border; j <= l_border; j++)
                    {
                        Color color = orgImg.GetPixel(o, j);
                        if (color.R.ToString().Equals("0") && color.G.ToString().Equals("0") && color.B.ToString().Equals("0"))
                            vp_black_pixels++;
                    }
                    vp_data.Add(vp_black_pixels);
                    vp_black_pixels = 0;
                }

                //MessageBox.Show("vp data count: " + vp_data.Count.ToString() + " Image width: " + orgImg.Width.ToString());

                int x_cor = 0, y_cor = 0, word_width = 0, word_height = l_border - u_border;

                //Find the coordinates and other parameters representing a rectangle surrounding the word
                for (int col_count = 0; col_count < (orgImg.Width - 2); col_count++) //scan the vertical projection data of one line
                {
                    if (vp_data[col_count] == 0 && vp_data[col_count + 1] != 0)  // find the word range
                    {
                        x_cor = col_count;
                        y_cor = line_top_list[n];
                        coordinates = true;

                        //for (int l1 = 0; l1 < word_height; l1++)
                        //{
                        //    bimg.SetPixel(col_count, y_cor + l1, Color.Blue);
                        //}
                    }

                    if (vp_data[col_count] != 0 && vp_data[col_count + 1] == 0)
                    {
                        word_width = col_count + 1 - x_cor;
                        size = true;

                        //for (int l1 = 0; l1 < word_height; l1++)
                        //{
                        //    bimg.SetPixel(col_count, y_cor + l1, Color.Blue);
                        //}
                    }

                    //Add rectangle covering the word
                    if (coordinates == true && size == true)
                    {
                        Rectangle word_rect = new Rectangle(x_cor, y_cor, word_width, word_height);
                        wordlist.Add(new KeyValuePair<int, Rectangle>(n, word_rect));
                        coordinates = false;
                        size = false;
                        word_count++;
                    }
                }

                vp_data.Clear();
            }

            return wordlist;
        }

        public List<List<KeyValuePair<int, KeyValuePair<int, Rectangle>>>> WordRefine(List<KeyValuePair<int, Rectangle>> wordlist, int num_of_line)
        {
            //List<int> line_top_list = line_borders[0];
            //var wordlist = new List<KeyValuePair<int, Rectangle>>();
            //Find true words section 
            //MessageBox.Show("num of lines: " + num_of_line);
            //Calculates true words based on average spacing caculated above
            var refined_words = new List<List<KeyValuePair<int, KeyValuePair<int, Rectangle>>>>();   //List for storing refined words areas

            for (int line_n = 0; line_n < num_of_line; line_n++)   //Loop for each detected line
            {

                var line_words = new List<KeyValuePair<int, Rectangle>>();  //List for storing word in a single line

                for (int wd_num = 0; wd_num < (wordlist.Count); wd_num++)    //Loop for calculating word distances between words in a line
                // Execute loop only for words with key value representing current line
                {
                    //Filter the word in a line
                    if (wordlist[wd_num].Key == line_n)
                    {
                        line_words.Add(wordlist[wd_num]);
                    }
                }

                //List for temporary storage of words in a line
                var newList = new List<KeyValuePair<int, KeyValuePair<int, Rectangle>>>();

                if (line_words.Count > 1)
                {
                    newList.Add(new KeyValuePair<int, KeyValuePair<int, Rectangle>>(line_n + 1, line_words[0]));

                    int wordNum = 0;

                    for (int wd = 1; wd < line_words.Count; wd++)
                    {
                        //calculate spacing between linewords[wd] and topmost word on newList if there spacing is not accepted merge and store in newList
                        int x1 = newList[newList.Count - 1].Value.Value.Right; //X value of right corner of first rectangle
                        int x2 = line_words[wd].Value.Left;        //X value of left corner of second rectangle
                        //MessageBox.Show("x1 = " + x1 + " x2 = " + x2);
                        if (Math.Abs(x2 - x1) < (line_words[wd].Value.Height / 10))
                        {
                            //Merge the words
                            int x_cor = 0, y_cor = 0, w_width = 0, W_height = 0;
                            //Calculate x_cor and y_cor from first rectangle
                            x_cor = newList[newList.Count - 1].Value.Value.X;
                            y_cor = newList[newList.Count - 1].Value.Value.Y;
                            //Calculate w_width from x_cor and right x_cor of second rectangle
                            w_width = line_words[wd].Value.Right - newList[newList.Count - 1].Value.Value.Left;
                            W_height = line_words[wd].Value.Height;
                            //w_height is same as previous
                            Rectangle newRectangle = new Rectangle(x_cor, y_cor, w_width, W_height);

                            //Add newRectangle to the newList
                            newList.RemoveAt(newList.Count - 1);
                            newList.Add(new KeyValuePair<int, KeyValuePair<int, Rectangle>>(line_n, new KeyValuePair<int, Rectangle>(wordNum, newRectangle)));
                            wordNum++;
                        }
                        else
                        {
                            newList.Add(new KeyValuePair<int, KeyValuePair<int, Rectangle>>(line_n, new KeyValuePair<int, Rectangle>(wordNum, line_words[wd].Value)));
                            wordNum++;
                        }
                    }

                    wordNum = 0;
                }
                else if (line_words.Count == 1)
                {
                    //copy single word
                    newList.Add(new KeyValuePair<int, KeyValuePair<int, Rectangle>>(line_n, new KeyValuePair<int, Rectangle>(0, line_words[0].Value)));
                }

                refined_words.Add(newList);
                //Find true words section end
            }
            return refined_words;
        }

        public List<KeyValuePair<int, Rectangle>> CharacterSegmentationFromSingleWord(Bitmap inputImg)
        {
            // Declare variable to store segmentated character lists...
            List<KeyValuePair<int, Rectangle>> charList = new List<KeyValuePair<int, Rectangle>>();   // line num, rectangle representing char segment

            // INPUT Array of Blobs

            List<KeyValuePair<int, Rectangle>> charSegment = new List<KeyValuePair<int, Rectangle>>();
            //List Declaration for storing character segments with line and word information
            //Inner KeyValuePair has word number and the rectangle representing character segment
            //Outer KeyValyePair has line number and inner 

            //Horizontal projection on Word Rectangle Part

            //Detect Header Line

            //Vertical Projection on Word Rectangle Part
            //Segment Characters or Block of Characters

            /*<---Modify Horizontal and Vertical Projection Methods such that methods will be able to projet the selected rectangular part of image--->*/

            //Manage List of Character Segments

            //Return List of Character Segments
            int wordNum = 0;

            // find starting coordinate, width and height of rectangle
            int xCorRec = 0;
            int yCorRec = 0;
            int recWidth = inputImg.Width;
            int recHeight = inputImg.Height;

            // List for storing number of black pixels in each word: pair[word_num][black pixel]
            List<int> hProjProfWordHeaderLine = new List<int>();

            int blackPixelCount = 0;
            int maxBlackPixels = 0;
            int yCorWithMaxBlackPixels = yCorRec;

            #region -- Detect headerline of each word --
            // start loop for horizontal projection profile for detecting header line
            for (int i = yCorRec; i < (yCorRec + recHeight); i++)
            {
                for (int j = xCorRec; j < (xCorRec + recWidth); j++)
                {
                    Color color = inputImg.GetPixel(j, i);
                    if (color.R.ToString().Equals("0") && color.G.ToString().Equals("0") && color.B.ToString().Equals("0"))
                    {
                        blackPixelCount++;
                        //MessageBox.Show("counting black pixels");
                    }
                }

                // compare and update maxumum number of black pixels and row consisting it
                if (maxBlackPixels < blackPixelCount)
                {
                    maxBlackPixels = blackPixelCount;
                    yCorWithMaxBlackPixels = i;
                }

                // Store number of black pixels in each row in word  // list.Add(new KeyValuePair<string, int>("Cat", 1));
                hProjProfWordHeaderLine.Add(blackPixelCount);
                //MessageBox.Show("max black pixels: " + maxBlackPixels);
                blackPixelCount = 0;
            }

            /* GET HEADER LINE RANGE*/
            // get 'dika' range
            int positionOfMaxBlaxPixel = hProjProfWordHeaderLine.IndexOf(hProjProfWordHeaderLine.Max());
            int topBorderOfDika = 0;
            int bottomBorderOfDika = hProjProfWordHeaderLine.Count / 2;

            int iLoop = positionOfMaxBlaxPixel;

            // get top border
            while (iLoop >= 0)
            {
                if (hProjProfWordHeaderLine[iLoop] >= maxBlackPixels * 90 / 100)
                {
                    topBorderOfDika = iLoop;
                }

                iLoop--;
            }

            // get bottom border
            iLoop = positionOfMaxBlaxPixel;
            while (iLoop <= (hProjProfWordHeaderLine.Count / 2))
            {
                if (hProjProfWordHeaderLine[iLoop] >= maxBlackPixels * 90 / 100)
                {
                    bottomBorderOfDika = iLoop;
                }

                iLoop++;
            }

            // increase a little hight of Dika (Headerline .. to cover headerline distortion a little)
            topBorderOfDika -= recHeight * 2 / 100;
            bottomBorderOfDika += recHeight * 2 / 100;
            if (topBorderOfDika < 0)
                topBorderOfDika = 0;
            if (bottomBorderOfDika >= recHeight)
                bottomBorderOfDika = recHeight - 1;
            /* GET HEADERLINE RANGE */

            #endregion


            //MessageBox.Show("maxblackpixel coordinate " + yCorWithMaxBlackPixels);


            #region <-- remove extra white space .. at word's top and bottom ... -->
            int top = 0;

            // remove white space at top of word
            while (top < hProjProfWordHeaderLine.Count && hProjProfWordHeaderLine[top] == 0)
            {
                yCorRec++;
                recHeight--;
                top++;
            }

            // remove white space at bottom of word
            int bottom = hProjProfWordHeaderLine.Count - 1;
            while (bottom >= 0 && hProjProfWordHeaderLine[bottom] == 0)
            {
                recHeight--;
                bottom--;
                if (bottom < 0) bottom = 0;
            }

            hProjProfWordHeaderLine = hProjProfWordHeaderLine.GetRange(top, bottom - top);

            #endregion

            #region -- Remove Headerline -->
            ////##################### REMOVE HEADER LINE #######################
            ////################################################################

            /* new code for headLineUpperRange and headLineLowerRange*/
            int headLineUpperRange = topBorderOfDika - top;
            int headLineLowerRange = bottomBorderOfDika - top;
            /*code end*/

            // Erase the header line (dika)
            for (int i = topBorderOfDika; i <= bottomBorderOfDika; i++)
            {
                for (int j = xCorRec; j < (xCorRec + recWidth); j++)
                {
                    inputImg.SetPixel(j, i, Color.White);
                }
            }


            // remove 'dika' i.e. pixels between headLineUpperRange and headLineLowerRange .. make white

            #region <-- vertical projection  -->
            // start loop for horizontal projection profile for detecting header line

            // projection below header line

            List<int> bPixelsVerticalProjection = new List<int>();
            int bpCount = 0;

            // count column wise pixels
            for (int j = xCorRec; j < (xCorRec + recWidth); j++)
            {
                //for (int i = yCorRec + headLineLowerRange + 1; i <= (yCorRec + recHeight); i++)

                for (int i = yCorRec; i < (yCorRec + recHeight); i++)
                {
                    Color color = inputImg.GetPixel(j, i);
                    if (color.R.ToString().Equals("0") && color.G.ToString().Equals("0") && color.B.ToString().Equals("0"))
                    {
                        bpCount++;
                        //MessageBox.Show("counting black pixels pos: ["+ j+ ","+ i + "]");
                    }
                }

                // Store number of black pixels in each row in word  // list.Add(new KeyValuePair<string, int>("Cat", 1));
                bPixelsVerticalProjection.Add(bpCount);
                //MessageBox.Show("max black pixels: " + maxBlackPixels);
                bpCount = 0;
            }
            #endregion

            #region -- remove dika --
            //int heightOfDika = headLineLowerRange - headLineUpperRange + 1;

            //for (int l = 0; l < bPixelsVerticalProjection.Count; l++)
            //{
            //    bPixelsVerticalProjection[l] = bPixelsVerticalProjection[l] - heightOfDika;
            //    // if value is negative make it equal to 0
            //    if (bPixelsVerticalProjection[l] < 0)
            //        bPixelsVerticalProjection[l] = 0;
            //}

            #endregion

            // chop at new char ...if there is a space between two characters below .. the headerline
            int x = xCorRec;
            int y = yCorRec;
            int pos = 0;

            List<int> inPos = new List<int>();
            List<int> endPos = new List<int>();

            //foreach (int val in bPixelsBelowHeadLine)
            for (int val = 0; val < bPixelsVerticalProjection.Count; val++)
            {
                //if (val == 0)
                if (bPixelsVerticalProjection[val] == 0)
                {
                    inPos.Add(val);
                    //for (int x1 = headLineUpperRange; x1 <= headLineLowerRange + 1; x1++)
                    //{
                    //    inputImg.SetPixel(xCorRec + pos, (yCorRec + x1), Color.White);           // y value of point is given by .. (yCorRec + x)
                    //}

                    while (val < bPixelsVerticalProjection.Count && bPixelsVerticalProjection[val] == 0)
                    {
                        val++;
                    }

                    endPos.Add(val - 1);
                }
                //pos++;
            }

            #endregion


            #region <-- Segmentation .. code added 2016-01-01 -->
            //####################### SEGMENTATION ###########################
            //################################################################

            #region <--- REGION FIND CUT POINTS -->
            // List to store cutpoints ....
            List<int> cutPointsList = new List<int>();
            // Variable to track start and end of valley
            int pos1 = 0, pos2 = 0;

            // Loop to detect zero values and track valley to find cut point
            for (int val = 0; val < bPixelsVerticalProjection.Count; val++)
            {
                // If zero value is found i.e. if valley is started
                if (bPixelsVerticalProjection[val] == 0)
                {
                    pos1 = val;

                    // Loop until there are zero values
                    while (val < bPixelsVerticalProjection.Count && bPixelsVerticalProjection[val] == 0)
                    {
                        val++;
                    }

                    pos2 = val;

                    if (!(pos2 == bPixelsVerticalProjection.Count || pos1 == 0))
                        cutPointsList.Add((pos1 + pos2) / 2); // Take middle point of valley as a cutpoint
                }
            }

            #endregion <-- find cutpoints -->


            #region <-- chop charactres -->
            // add characters to list

            if (cutPointsList.Count < 1) // if there is no cutpoint .. means single character 
            {
                charList.Add(new KeyValuePair<int, Rectangle>(wordNum, new Rectangle(xCorRec, yCorRec, recWidth, recHeight)));
            }
            else if (cutPointsList.Count == 1)  // if there is exactly 1 cutpoint means there are two characters
            {
                charList.Add(new KeyValuePair<int, Rectangle>(wordNum, new Rectangle(xCorRec, yCorRec, cutPointsList[0], recHeight)));
                charList.Add(new KeyValuePair<int, Rectangle>(wordNum, new Rectangle(cutPointsList[0] + 1, yCorRec, recWidth - cutPointsList[0] - 1, recHeight)));
            }
            else    // if there are more than one cutponts
            {
                // add word start and end as cutpoints to facilitate cutting process
                cutPointsList.Insert(0, 0);
                cutPointsList.Insert(cutPointsList.Count, recWidth);

                for (int r = 0; r <= cutPointsList.Count - 2; r++)
                {
                    if (new Rectangle(xCorRec + cutPointsList[r], yCorRec, cutPointsList[r + 1] - cutPointsList[r], recHeight).Width < new Rectangle(xCorRec + cutPointsList[r], yCorRec, cutPointsList[r + 1] - cutPointsList[r], recHeight).Height / 3)
                    {
                        // if width of character segment is less than 1/3rd of its height .. merge it to segment left to is
                        // remove last segment
                        if (charList.Count > 1)
                        {
                            KeyValuePair<int, Rectangle> se = charList[charList.Count - 1];
                            charList.RemoveAt(charList.Count - 1);

                            // merge to last segment and add to list
                            charList.Add(new KeyValuePair<int, Rectangle>(wordNum, Rectangle.Union(se.Value, new Rectangle(xCorRec + cutPointsList[r], yCorRec, cutPointsList[r + 1] - cutPointsList[r], recHeight))));
                        }
                        else
                        {
                            charList.Add(new KeyValuePair<int, Rectangle>(wordNum, new Rectangle(xCorRec + cutPointsList[r], yCorRec, cutPointsList[r + 1] - cutPointsList[r], recHeight)));
                        }
                    }
                    else
                    {
                        charList.Add(new KeyValuePair<int, Rectangle>(wordNum, new Rectangle(xCorRec + cutPointsList[r], yCorRec, cutPointsList[r + 1] - cutPointsList[r], recHeight)));
                    }

                }
            }

            #endregion <-- chop characters -->

            #endregion

            // Return Characters List
            return charList;
        }
    }
}
