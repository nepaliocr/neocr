﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Text_Image_Segmenter.Libs;

namespace Layout
{
    public class TestOrientation
    {
        List<int> wvalleycount = new List<int>();
        List<int> bvalleycount = new List<int>();
        int width, height, rows, cols, sizeofarray, tempb, getimageindex;
        int countblocks = 0;
        Bitmap mini_image, original;
        int[] horizontalprojection = new int[200];
        int[] verticalprojection = new int[200];
        bool checktextorientation;
        Bitmap[] imgs_clone;
        Bitmap image, binarized_image;

        public Bitmap DoEverything(Bitmap image)
        {
            //Console.WriteLine("Process for do everything has started.");
            //image = Image.FromFile(file_path) as Bitmap;
            original = image;

            width = image.Width;
            height = image.Height;
           
            rows = width / 200;
            cols = height / 200;

            int chunks = rows * cols;

            // image binarization
            binarized_image = ImageLab.IterativeThreshold(image);
           
            
            // determines the chunk width and height
            int chunkWidth = 200; 
            int chunkHeight = 200;
            int count = 0;

            // image array to hold image chunks
            Bitmap[] imgs = new Bitmap[chunks]; 
            imgs_clone = new Bitmap[chunks];

            int x_axis = 0;
            int y_axis = 0;

            Bitmap src = binarized_image;

            for (int x = 0; x < rows; x++)
            {
                x_axis = 0;
                for (int y = 0; y < cols; y++)
                {
                    // initialize the image array with image chunks
                    imgs[count] = new Bitmap(chunkWidth, chunkHeight);

                    // draws the image chunk
                    try
                    {
                        Rectangle cropRect = new Rectangle(x_axis, y_axis, chunkWidth, chunkHeight);
                        Bitmap target = new Bitmap(cropRect.Width, cropRect.Height);

                        using (Graphics g = Graphics.FromImage(target))
                        {
                            g.DrawImage(src, new Rectangle(0, 0, target.Width, target.Height),
                                             cropRect,
                                             GraphicsUnit.Pixel);
                        }
                        imgs_clone[count] = target;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                    
                    count++;
                    x_axis = x_axis + 200;
                }
                y_axis = y_axis + 200;
            }


            /** for counting contiguous white and black pixels
                Here we pass the image to CountPixel function
            **/

            Console.WriteLine("Process for counting pixel has started.");
            foreach (Bitmap bitmap in imgs_clone)
            {
                CountPixel(bitmap);
                countblocks += 1;
            }
 
            SelectImage();

            return original;

        } // doeverything() ends here

        public void CountPixel(Bitmap takeimage)
        {
            this.mini_image = takeimage;
            int bcount = 0;
            int maxbcount = 0;
            int wcount = 0;
            int maxwcount = 0;
            int mwidth = mini_image.Width;
            int mheight = mini_image.Height;
            for (int i = 0; i < mwidth; i++)
            {
                for (int j = 0; j < mheight; j++)
                {
                    if (i == j)
                    {
                        Color mgetcolor = mini_image.GetPixel(i, j);

                        if (mgetcolor.R == 0 && mgetcolor.G == 0 && mgetcolor.B == 0)
                        { //blackpixel encounterd
                            bcount += 1;
                            if (bcount > maxbcount)
                            {
                                maxbcount = bcount;
                            }
                            wcount = 0;
                        }
                        else
                        {
                            wcount += 1;
                            if (wcount > maxwcount)
                            {
                                maxwcount = wcount;
                                bcount = 0;
                            }
                        }

                    }
                }
            }
            wvalleycount.Add(maxwcount);
            bvalleycount.Add(maxbcount);
            sizeofarray = wvalleycount.Count;

        } //CountPixel() ends here.

        public void SelectImage()
        {
            Console.WriteLine("Process for selecting Image has Started.");
            List<int> tempwvalleycount = new List<int>(wvalleycount);
            List<int> tempbvalleycount = new List<int>(bvalleycount);

            if (sizeofarray == 0)
            {
                return;
            }

            for (int i = sizeofarray - 1; i > 0; i--)
            {
                if (bvalleycount[i] > 20)
                {
                    wvalleycount.RemoveAt(i);
                    bvalleycount.RemoveAt(i);
                    sizeofarray = bvalleycount.Count;

                }
            }

            for (int i = 0; i < sizeofarray; i++)
            {
                if (bvalleycount[i] > 20)
                {
                    wvalleycount.RemoveAt(i);
                    bvalleycount.RemoveAt(i);
                    sizeofarray = bvalleycount.Count;

                }
            }

            int small = wvalleycount[0];

            for (int i = 0; i < sizeofarray; i++)
            {
                if (wvalleycount[i] < small)
                {
                    small = wvalleycount[i];
                    tempb = bvalleycount[i];
                    getimageindex = i;

                }

            }

            getimageindex = 0;
            for (int i = 0; i < tempbvalleycount.Count; i++)
            {
                if (small == tempwvalleycount[i] && tempb == tempbvalleycount[i])
                {
                    getimageindex = i;

                }
            }

            Console.WriteLine("getimageindex = "+getimageindex);
            Projection();

        } //selectImage() ends here.

        public void Projection()
        {
            Console.WriteLine("Process for projection has started.");
            mini_image = imgs_clone[getimageindex+2];
            int miniwidth = mini_image.Width;
            int miniheight = mini_image.Height;

            // vertical projection starts here.
            for (int x = 0; x < 200; x++)
            { // vertical projection i.e. you count pixel with respect to x axis.
                int tempcount = 0;
                for (int y = 0; y < 200; y++)
                {
                    Color temp = mini_image.GetPixel(x, y);

                    if (temp.R == 0 && temp.G == 0 && temp.B == 0)
                    { // black pixel encountered.
                        tempcount += 1;
                    }

                }
                horizontalprojection[x] = tempcount;

            }// vertical projection ends.

            /** horizontal projection count starts here
                if the values less than 5 then we assume texts are in horizontal position
                else texts are in vertical position
            **/
            for (int x = 0; x < miniheight; x++)
            { // horizontal projection i.e. you count pixel with respect to x axis.
                int tempcount = 0;
                for (int y = 0; y < miniwidth; y++)
                {
                    Color tempcolor = mini_image.GetPixel(y, x);

                    if (tempcolor.R == 0 && tempcolor.G == 0 && tempcolor.B == 0)
                    { // black pixel encountered.
                        tempcount += 1;
                    }

                }
                verticalprojection[x] = tempcount;

            }

            // checking if the transition is vertical.
            int vchecktransition = 0;
            for (int x = 0; x < 200; x++)
            {

                if (verticalprojection[x] <= 3)
                {
                    vchecktransition += 1;

                }

            }

            // checking if the transition if horizontal
            int hchecktransition = 0;
            for (int x = 0; x < 200; x++)
            {

                if (horizontalprojection[x] <= 3)
                {
                    hchecktransition += 1;

                }

            }

            // Deciding which transition is correct.

            Console.WriteLine("h = "+hchecktransition+" v = "+vchecktransition);

            if (hchecktransition > vchecktransition)
            {
                checktextorientation = true;

            }
            else
            {
                checktextorientation = false;
            }

            /** horizontal projection ends here.
                to check if there is no of zero valleys i.e. to see if there exist white pixels.
            **/
            Console.WriteLine("Function Porjection is finished");
            Correctorientation();

        } // projection() ends here.

        public void Correctorientation()
        {
            Console.WriteLine("Process for correct orientation has started.");
            int a = 0;

            if (checktextorientation == false)
            { // condition for the horizontal orientation ie. up and down.

                Console.WriteLine("Image is either at correct position or positioned down.");
                int greatno = verticalprojection[0];
                int getindex = 0;

                for (int x = 0; x < 200; x++)
                {

                    if (verticalprojection[x] > greatno)
                    {
                        greatno = verticalprojection[x];
                        getindex = x;

                    }
                }

                int downcheck = 0;
                int upcheck = 0;
                for (int x = getindex; x < 200; x++)
                {
                    if (verticalprojection[x] > 8)
                    {
                        downcheck += 1;
                    }
                    else
                    {
                        break;

                    }

                }
       
                for (int x = getindex; x < 200; x--)
                {
                    if (x > 1)
                    {
                        if (verticalprojection[x] > 8 || verticalprojection[x] == verticalprojection[getindex])
                        {
                            upcheck += 1;

                        }
                        else
                        {
                            break;
                        }
                    }
                }
             
                if (downcheck > upcheck)
                {
                    // nothing to image as it is in its correct position.
                    a = 1;          
                }
                else
                {
                    // rotate image 180 degree.
                    a = 2;
                }

            }
            else
            { // This means just do for the left and the right orientation.

                Console.WriteLine("Either image is left rotated or right rotated");
                int greatno = horizontalprojection[0];
                int getindex = 0;

                for (int x = 0; x < 200; x++)
                {

                    if (horizontalprojection[x] > greatno)
                    {
                        greatno = horizontalprojection[x];
                        getindex = x;

                    }
                }
                
                int leftcheck = 0;
                int rightcheck = 0;

                for (int x = getindex; x < 200; x++)
                {
                    if (horizontalprojection[x] > 8)
                    {
                        rightcheck += 1;
                    }
                    else
                    {
                        break;

                    }

                }
                
                for (int x = getindex; x < 200; x--)
                {
                    if (x > 1)
                    {
                        if (horizontalprojection[x] > 8 || horizontalprojection[x] == horizontalprojection[getindex])
                        {
                            leftcheck += 1;

                        }
                        else
                        {
                            break;
                        }
                    }
                }

                if (rightcheck > leftcheck)
                {
                    // rotate image by 90 degree
                    a = 3;
                }
                else
                {
                    // rotate image 270 degree.
                    a = 4;
                }

            }

            switch (a)
            {
                case 1:
                    Console.WriteLine("Leave as it is");
                    break;
                case 2:
                    Console.WriteLine("180 degree rotation");
                    original.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    break;
                case 3:
                    Console.WriteLine("90  degree rotation");
                    original.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    break;
                case 4:
                    Console.WriteLine("270 degree rotation");
                    original.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    break;

            }
            original.Save("D:\\test.png");

        } // correctorientation() ends here.

    }


}