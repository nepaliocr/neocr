﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Text_Image_Segmenter.Libs
{
    /*
    * @Author: Nirajan Pant
    * Date: 3 November 2016
    */

    class LayoutAnalyzer
    {
        // Function to Detect and Remove Horizontal lines present in the image
        public Bitmap RemoveHorizontalLines(Bitmap bitmap)
        {
            return bitmap; 
        }

        // Function to Detect and Remove Vertical lines present in the image
        public Bitmap RemoveVerticalLines(Bitmap bitmap)
        {
            return bitmap;
        }

        // Function to remove boundaries of image
        public Bitmap RemoveBoundaries(Bitmap bitmap)
        {
            return bitmap;
        }

        // Function to segment prodived image into different rectangular regions
        public List<List<Point>> DissectIntoRectangularBlocks(Bitmap bitmap)
        {
            Console.Out.Write(DateTime.Now);
            Console.Out.WriteLine("DissectIntoRectangularBlocks Function calling...");

            int img_height = bitmap.Height;
            int img_width = bitmap.Width;

            /*
            // CHECK THE CONDITIONS LATER
            // bitmap should be binarized image for processing
            // if image is not completely divisible into 8x8 grids add some extra rows and columns 
            */

            // Divide image into 8x8 matrices
            int numOfRows = (int)Math.Ceiling((decimal)(img_height / 8));
            int numOfColumns = (int)Math.Ceiling((decimal)(img_width / 8));

            // Scale image to make it perfectly divisible into 8x8 grids
            Console.Out.Write(DateTime.Now);
            Console.Out.WriteLine("Calling scaleBitmap function...");
            bitmap = ImageLab.ScaleBitmap(bitmap, new Size(numOfColumns * 8, numOfRows * 8));

            // Calculate new height and width
            img_height = numOfRows * 8;
            img_width = numOfColumns * 8;

            // Define matrix 
            Console.Out.Write(DateTime.Now);
            Console.Out.WriteLine("Calling ImageGridToEvalutionMatrix...");
            int[,] gridBoxEvaluation = ImageGridToEvaluationMatrix(bitmap, numOfRows, numOfColumns);

            List<int> visitedBlocksList = new List<int>();
            List<List<int>> clustersList = new List<List<int>>();

            Console.Out.Write(DateTime.Now);
            Console.Out.WriteLine("Finding clusters...");
            #region <-- find cluster of dots -->
            // Iterate through evaluation matrix to find clusters of dots (grid boxes)
            for (int i = 0; i < numOfRows; i++){
                for (int j = 0; j < numOfColumns; j++){
                    // IF gridBoxEvaluation[i,j] IS ZERO ADD IT TO VISITED LIST AND DISCARD
                    if (gridBoxEvaluation[i, j] == 0){
                        visitedBlocksList.Add(i * numOfColumns + j);
                        continue;
                    }

                    // IF gridBoxEvaluation[i,j] IS IN VISITED BLOCKS LIST DISCARD IT
                    if (visitedBlocksList.Contains(i * numOfColumns + j)){
                        continue;
                    }

                    // CREATE CLUSTER OF 1s:: ADD FIRST ITEM TO CLUSTER
                    List<int> cluster = new List<int>();
                    cluster.Add(i * numOfColumns + j);

                    // GET NEIGHBOUR CELLS
                    List<int> neighbors = new List<int>();
                    int neighborsNum = neighbors.Count;

                    neighbors.Add((i - 1) * numOfColumns + (j - 1));
                    neighbors.Add((i - 1) * numOfColumns + j);
                    neighbors.Add((i - 1) * numOfColumns + (j + 1));
                    neighbors.Add(i * numOfColumns + (j - 1));
                    neighbors.Add(i * numOfColumns + (j + 1));
                    neighbors.Add((i + 1) * numOfColumns + (j - 1));
                    neighbors.Add((i + 1) * numOfColumns + j);
                    neighbors.Add((i + 1) * numOfColumns + (j + 1));

                    // Remove unnecessary block positions
                    for (int k = neighbors.Count - 1; k >= neighborsNum; k--){
                        if(neighbors[k] < 0 || neighbors[k] > (numOfRows - 1)*(numOfColumns - 1) || visitedBlocksList.Contains(neighbors[k])){
                            neighbors.RemoveAt(k);
                        }
                    }

                    // CHECK FOR EACH NEIGHBOR
                    while(neighbors.Count > 0){
                        // Get last neighbor's position
                        int neighborPosition = neighbors[neighbors.Count - 1];
                        // Remove from neighbors list
                        neighbors.RemoveAt(neighbors.Count - 1);

                        // Estimate i and j value from neighborPosition
                        int I = neighborPosition / numOfColumns;
                        int J = neighborPosition % numOfColumns;

                        // IF gridBoxEvaluation[I,J] IS ZERO ADD IT TO VISITED LIST AND DISCARD
                        if (gridBoxEvaluation[I, J] == 0){
                            visitedBlocksList.Add(I * numOfColumns + J);
                            continue;
                        }

                        // IF gridBoxEvaluation[I,J] IS IN VISITED BLOCKS LIST DISCARD IT
                        if (visitedBlocksList.Contains(I * numOfColumns + J)){
                            continue;
                        }

                        // Add it to cluster
                        cluster.Add(neighborPosition);
                        visitedBlocksList.Add(I * numOfColumns + J);

                        int numOfNeighbors = neighbors.Count;

                        // Variable to store partial data for calculating block position
                        int positionPart1 = (I - 1) * numOfColumns;
                        int positionPart2 = (I + 1) * numOfColumns;

                        // Get neighbors of neighbor
                        neighbors.Add(positionPart1 + (J - 1));
                        neighbors.Add(positionPart1 + J);
                        neighbors.Add(positionPart1 + (J + 1));
                        neighbors.Add(I * numOfColumns + (J - 1));
                        neighbors.Add(I * numOfColumns + (J + 1));
                        neighbors.Add(positionPart2 + (J - 1));
                        neighbors.Add(positionPart2 + J);
                        neighbors.Add(positionPart2 + (J + 1));

                        // Remove unnecessary block positions
                        for (int m = neighbors.Count - 1; m >= numOfNeighbors; m--){
                            if (neighbors[m] < 0 || neighbors[m] > (numOfRows - 1) * (numOfColumns - 1) || visitedBlocksList.Contains(neighbors[m])){
                                neighbors.RemoveAt(m);
                            }
                        }
                    }

                    // Add cluster to cluster list
                    clustersList.Add(cluster);
                }
            }
            #endregion <-- end find cluster region -->

            // Get cluster bounding rectangles
            //List<Rectangle> clusterBoundingRectangles = GetClusterBoundingRectangles(clustersList, numOfColumns);

            Console.Out.Write(DateTime.Now);
            Console.Out.WriteLine("Detecting convex hulls...");
            // Get cluster bounding convex hull
            List<List<Point>> convexHullList = new List<List<Point>>();
            // Iterate for each cluster in clusterList
            foreach (List<int> cluster in clustersList)
            {
                List<Point> curvedPoints = new List<Point>();
                foreach (int block in cluster)
                {
                    // Estimate i and j value from neighborPosition
                    int iVal = block / numOfColumns;
                    int jVal = block % numOfColumns;

                    // Create rectangle from block position
                    //Rectangle box = new Rectangle(jVal * 8, iVal * 8, 8, 8);
                    int xCor = jVal * 8;
                    int yCor = iVal * 8;
                    // Get vertices of rectangle
                    curvedPoints.Add(new Point(xCor, yCor));
                    curvedPoints.Add(new Point(xCor, yCor + 8));
                    curvedPoints.Add(new Point(xCor + 8, yCor));
                    curvedPoints.Add(new Point(xCor + 8, yCor + 8));
                }

                convexHullList.Add(ComputeConvexHull(curvedPoints));
            }

            Console.Out.Write(DateTime.Now);
            Console.Out.WriteLine("Task Finished.");

            return convexHullList;
            //return clusterBoundingRectangles;
        }

        // Divide binarized image into 8x8 submatrices, 
        // Check each submatix for black pixel density and give it value 1 or 0 based on pixel density, 
        // Finally, evaluate each submatrix as 1 or 0, and return evaluated matrix of 1 and 0s
        private int[,] ImageGridToEvaluationMatrix(Bitmap bitmap, int numOfRows, int numOfColumns, int blockDimension=8, double blackPixelsDensity=0.1)
        {
            // Define matrix 
            int[,] gridBoxEvaluation = new int[numOfRows, numOfColumns];

            // Iterate through grids
            for (int i = 0; i < numOfRows; i++)
            {
                for (int j = 0; j < numOfColumns; j++)
                {
                    // Get grid block in the form of rectangle
                    Rectangle gridBox = new Rectangle(j * blockDimension, i * blockDimension, blockDimension, blockDimension);

                    // Calculate coverage of points in a grid box
                    int blackPixelCount = new Histogram().BlackPixelCount(bitmap, gridBox);

                    /*
                    // if black pixel coverage is greater than 15% 
                    // Threshold value greater than zero not working with smaller font size
                    */

                    if (blackPixelCount > blackPixelsDensity * blockDimension * blockDimension) 
                    {
                        gridBoxEvaluation[i, j] = 1;
                    }
                    else
                    {
                        gridBoxEvaluation[i, j] = 0;
                    }
                }
            }

            return gridBoxEvaluation;
        }

        // Estimate cluster bounding rectangles
        private List<Rectangle> GetClusterBoundingRectangles(List<List<int>> clustersList, int numOfColumns)
        {
            #region <-- Get cluster's bounding rectangle -->
            // List to store cluster bounding rectangles
            List<Rectangle> clusterBoundingRectangles = new List<Rectangle>();

            // Get cluster's bounding rectangle
            foreach (List<int> cluster in clustersList)
            {
                Rectangle clusterBoundingBox = new Rectangle(0, 0, 0, 0);
                foreach (int block in cluster)
                {
                    // Estimate i and j value from neighborPosition
                    int iVal = block / numOfColumns;
                    int jVal = block % numOfColumns;

                    // Estimate box position in image
                    //Console.Out.WriteLine(iVal + " " + jVal + "\n\n");

                    // Create rectangle from block position
                    Rectangle box = new Rectangle(jVal * 8, iVal * 8, 8, 8);

                    // Merge rectangles in same cluster
                    if (clusterBoundingBox.IsEmpty){
                        clusterBoundingBox = box;
                    }else{
                        clusterBoundingBox = Rectangle.Union(clusterBoundingBox, box);
                    }
                }

                // Add cluster bounding rectangle to list
                clusterBoundingRectangles.Add(clusterBoundingBox);
            }
            #endregion <-- Get cluster's bounding rectangle -->

            return clusterBoundingRectangles;
        }

        // Estimate Convex Hull of points
        public List<Point> ComputeConvexHull(List<Point> points)
        {
            // sort points
            Point[] sortedPoints = points.OrderBy(p => p.X).ThenBy(p => p.Y).ToArray();

            // List to store convex hull points
            int numOfPoints = sortedPoints.Length;
            Point[] Hull = new Point[numOfPoints];

            // Call chain hall 2D methos to find convex hull
            int vertices = chainHull_2D(ref sortedPoints, numOfPoints, ref Hull);


            return Hull.ToList().GetRange(0,vertices);
        }

        #region <-- Implementation of the Chain Hull Algorithm -->
        /*
        // Implementation of the Chain Hull Algorithm
        // @Author: C# code ported by Nirajan Pant, 2016
        // Original Implementation in C++
        // Copyright 2001 softSurfer, 2012 Dan Sunday
        // This code may be freely used and modified for any purpose
        // Provising that this copyright notice is included with it.
        // SoftSurfer makes no warranty for this code, and annot be held
        // liable for any real or imagined damage resulting from its use.
        // Users of this code must verify correctness for their application.

        // Assume that a class is already given for the object:
        // Point with coordinates {float x, y;}
        */

        // ifLeft(): tests if a point is Left|On| Right of an infinite line.
        // Input: three points P0, P1, and P2
        // Return: >0 for P2 left of the line through P0 and P1
        //          =0 for P2 on the line
        //          <= for P2 right of the line
        private float isLeft(Point P0, Point P1, Point P2)
        {
            return (P1.X - P0.X)*(P2.Y - P0.Y) - (P2.X - P0.X) * (P1.Y - P0.Y);
        }

        // chainHull_2D(): Andrew's monotone chain 2D convex hull algorithm
        // Input: P[0] = an array of 2D points presorted by increasing x and y-coordinates
        //  n = the number of points in P[]
        // Output: H[] = an array of the convex hull vertices (max is n)
        // Return: the number of points in H[]
        private int chainHull_2D(ref Point[] P, int n, ref Point[] H)
        {
            if (P.Length <= 8)
                return P.Length;

            // the output array (list) H[] will be used as the stack
            int bot = 0, top = (-1);    // indices for bottom and top of the stack
            int i;  // array (list) scan index

            // Get the indices of ponts with min x-coord and min|max y-coord
            int minmin = 0, minmax;
            float xmin = P[0].X;
            for(i=0;i< n; i++)
                if (P[i].X != xmin) break;

            minmax = i - 1;

            if (minmax == n - 1)
            {
                // degenerate case: all x-coords == xmin
                H[++top] = P[minmin];
                if (P[minmax].Y != P[minmin].Y)  // a nontrivial segment
                    H[++top] = P[minmax];
                H[++top] = P[minmin];   // add polygon endpoint

                return top + 1;
            }

            // Get the indices of points with max x-coord and min|max y-coord
            int maxmin, maxmax = n - 1;
            float xmax = P[n - 1].X;
            for (i = n - 1; i >= 0; i--)
                if (P[i].X != xmax) break;
            maxmin = i + 1;

            // Compute the lower hull on the stack H
            H[++top] = P[minmin];   // push minmin point onto stack
            i = minmax;
            while(++i <= maxmin)
            {
                // the lower line joins P[minmin] with P[maxmin]
                if (isLeft(P[minmin], P[maxmin], P[i]) >= 0 && i < maxmin) continue;    
                // ignore P[i] above or on the lower line
                while(top > 0)  // there are at least 2 points on the stack
                {
                    // test if P[i] is left of the line at the stack top
                    if (isLeft(H[top - 1], H[top], P[i]) > 0)
                        break;    // P[i] is a new hull vertex
                    else
                        top--;  // pop top point off stack
                }

                H[++top] = P[i];    // push P[i] onto stack
            }

            // Next, compute the upper hull on the stack H above the bottom hull
            if (maxmax != maxmin)    // if distinct xmax points
                H[++top] = P[maxmax];   // push maxmax point onto stack
            bot = top;  // the bottom point of the upper hull stack
            i = maxmin;
            while (--i >= minmax)
            {
                // the upper line joins P[maxmax] with P[minmax]
                if (isLeft(P[maxmax], P[minmax], P[i]) >= 0 && i > minmax)
                    continue;   // ignore P[i] below or on the upper line

                while(top > bot)  // at least 2 points on the upper stack
                {
                    // test if P[i] is left of the line at the stack top 
                    if (isLeft(H[top - 1], H[top], P[i]) > 0)
                        break;  // P[i] is a new hull vertex
                    else
                        top--;  // pop top point off stack
                }

                H[++top] = P[i];    // push P[i] onto stack
            }

            if (minmax != minmin)
                H[++top] = P[minmin];   // push joining endpoint onto stack

            return top + 1;
        }

        #endregion <-- Implementation of the Chain Hull Algorithm -->

        // FUNCTION TO GET RECTANGLES OF WHITE SPACE
        public List<List<Rectangle>> GetWhitespaceRectangles(Bitmap bitmap, int blockDimension=8)
        {
            Console.Out.Write(DateTime.Now);
            Console.Out.WriteLine("GetWhitespaceRectangles Function called...");

            int img_height = bitmap.Height;
            int img_width = bitmap.Width;

            /*
            // CHECK THE CONDITIONS LATER
            // bitmap should be binarized image for processing
            // if image is not completely divisible into square grids add some extra rows and columns 
            */

            //int blockDimension = 8;
            // Divide image into squeare matrices
            int numOfRows = (int)Math.Ceiling((decimal)(img_height / blockDimension));
            int numOfColumns = (int)Math.Ceiling((decimal)(img_width / blockDimension));

            // Scale image to make it perfectly divisible into square grids
            Console.Out.Write(DateTime.Now);
            Console.Out.WriteLine("Calling scaleBitmap function...");
            bitmap = ImageLab.ScaleBitmap(bitmap, new Size(numOfColumns * blockDimension, numOfRows * blockDimension));

            // Calculate new height and width
            img_height = numOfRows * blockDimension;
            img_width = numOfColumns * blockDimension;

            // Define matrix 
            Console.Out.Write(DateTime.Now);
            Console.Out.WriteLine("Calling ImageGridToEvalutionMatrix...");
            int[,] gridBoxEvaluation = ImageGridToEvaluationMatrix(bitmap, numOfRows, numOfColumns, blockDimension, 0.0);

            List<List<int>> clustersListVert = new List<List<int>>();
            List<List<int>> clustersListHori = new List<List<int>>();
            List<Rectangle> listRecVert = new List<Rectangle>();
            List<Rectangle> listRecHori = new List<Rectangle>();

            Console.Out.Write(DateTime.Now);
            Console.Out.WriteLine("Finding clusters...");
            #region <-- find cluster of dots - vertical rectangle form -->
            // Iterate through evaluation matrix to find clusters of dots (grid boxes) - vertical
            for (int i = 0; i < numOfColumns; i++)
            {
                List<int> cluster = new List<int>() {0,0,0};
                for (int j = 0; j < numOfRows; j++)
                {
                    // IF gridBoxEvaluation[i,j] IS ZERO ADD IT TO VISITED LIST AND DISCARD
                    if (gridBoxEvaluation[j, i] == 0)
                    {
                        cluster[0] = i;
                        cluster[1] = j;

                        while (j < numOfRows && gridBoxEvaluation[j, i] == 0)
                        {
                            j++;
                        }

                        cluster[2]=j - 1;
                        // If rectangle size is greater than some threshold
                        if (cluster[2] - cluster[1] > 20)
                        {
                            // Add cluster to cluster list
                            clustersListVert.Add(cluster);
                        }

                        cluster = new List<int>() { 0, 0, 0 };
                    }
                }
            }

            foreach (List<int> clus in clustersListVert)
            {
                int xCor = clus[0] * blockDimension;
                int yCor = clus[1] * blockDimension;

                int yCor1 = clus[2] * blockDimension + blockDimension;

                Rectangle r = new Rectangle(xCor, yCor, blockDimension, yCor1 - yCor);
                listRecVert.Add(r);
            }
            #endregion <-- end find cluster region -->

            #region <-- find cluster of dots - horizontal rectangle form -->
            // Iterate through evaluation matrix to find clusters of dots (grid boxes) - vertical
            for (int i = 0; i < numOfRows; i++)
            {
                List<int> cluster = new List<int>() { 0, 0, 0 };
                for (int j = 0; j < numOfColumns; j++)
                {
                    // IF gridBoxEvaluation[i,j] IS ZERO ADD IT TO VISITED LIST AND DISCARD
                    if (gridBoxEvaluation[i, j] == 0)
                    {
                        cluster[0] = i;
                        cluster[1] = j;

                        while (j < numOfColumns && gridBoxEvaluation[i, j] == 0)
                        {
                            j++;
                        }

                        cluster[2] = j - 1;
                        
                        // If rectangle size is greater than some threshold
                        if (cluster[2] - cluster[1] > 20)
                        {
                            // Add cluster to cluster list
                            clustersListHori.Add(cluster);
                        }

                        cluster = new List<int>() { 0, 0, 0 };
                    }
                }
            }

            foreach (List<int> clus in clustersListHori)
            {
                int xCor = clus[1] * blockDimension;
                int yCor = clus[0] * blockDimension;

                int xCor1 = clus[2] * blockDimension + blockDimension;

                Rectangle r = new Rectangle(xCor, yCor, xCor1 - xCor, blockDimension);
                listRecHori.Add(r);
            }
            #endregion <-- end find cluster region -->
            Console.Out.Write(DateTime.Now);
            Console.Out.WriteLine("Task Finished.");

            //return clusterBoundingRectangles;
            //return clustersList;

            #region <-- Order and merge adjacent rectangles -->
            List<Rectangle> sortedRecsVert = listRecVert.OrderBy(r => r.Left).ThenBy(r => r.Top).ToList();
            List<Rectangle> sortedRecsHori = listRecHori.OrderBy(r => r.Left).ThenBy(r => r.Top).ToList();

            // CHECK VERTICAL RECTANGLES
            int vrCount = sortedRecsVert.Count - 1;
            for (int i = vrCount; i > 0; i--)
            {
                // Merge rectangles that are somewhat equal in height and they share a side
                if (sortedRecsVert[i].Left - sortedRecsVert[i - 1].Right <= 1 && sortedRecsVert[i].Height == sortedRecsVert[i - 1].Height)
                {
                    sortedRecsVert[i - 1] = Rectangle.Union(sortedRecsVert[i], sortedRecsVert[i - 1]);
                    sortedRecsVert.RemoveAt(i);
                }
            }

            for (int i = (sortedRecsVert.Count - 1); i > 0; i--)
            {
                if (sortedRecsVert[i].Left - sortedRecsVert[i - 1].Right <= 1)
                {
                    if (sortedRecsVert[i].Height < sortedRecsVert[i - 1].Height)
                        sortedRecsVert.RemoveAt(i);
                    else if (sortedRecsVert[i].Height > sortedRecsVert[i - 1].Height)
                        sortedRecsVert.RemoveAt(i - 1);
                }
            }

            // CHECK HORIZONTAL RECTANGLES
            int hrCount = sortedRecsHori.Count - 1;
            for (int i = hrCount; i > 0; i--)
            {
                // Merge rectangles that are somewhat equal in width and they share a side
                if (sortedRecsHori[i].Top - sortedRecsHori[i - 1].Bottom <= 1 && sortedRecsHori[i].Width == sortedRecsHori[i - 1].Width)
                {
                    sortedRecsHori[i - 1] = Rectangle.Union(sortedRecsHori[i], sortedRecsHori[i - 1]);
                    sortedRecsHori.RemoveAt(i);
                }
            }

            for (int i = (sortedRecsHori.Count - 1); i > 0; i--)
            {
                if (sortedRecsHori[i].Top - sortedRecsHori[i - 1].Bottom <= 1)
                {
                    if (sortedRecsHori[i].Width < sortedRecsHori[i - 1].Width)
                        sortedRecsHori.RemoveAt(i);
                    else if (sortedRecsHori[i].Width > sortedRecsHori[i - 1].Width)
                        sortedRecsHori.RemoveAt(i - 1);
                }
            }
            #endregion;

            return new List<List<Rectangle>>() { listRecVert, listRecHori };
        }

        // FUNCTION TO GET RECTANGLES OF BLACK SPACE
        public List<List<Rectangle>> GetBlackspaceRectangles(Bitmap bitmap, int blockDimension = 1)
        {
            Console.Out.Write(DateTime.Now);
            Console.Out.WriteLine("GetWhitespaceRectangles Function called...");

            int img_height = bitmap.Height;
            int img_width = bitmap.Width;

            /*
            // CHECK THE CONDITIONS LATER
            // bitmap should be binarized image for processing
            // if image is not completely divisible into square grids add some extra rows and columns 
            */

            // Divide image into square matrices
            int numOfRows = (int)Math.Ceiling((decimal)(img_height / blockDimension));
            int numOfColumns = (int)Math.Ceiling((decimal)(img_width / blockDimension));

            // Scale image to make it perfectly divisible into  grids
            Console.Out.Write(DateTime.Now);
            Console.Out.WriteLine("Calling scaleBitmap function...");
            bitmap = ImageLab.ScaleBitmap(bitmap, new Size(numOfColumns * blockDimension, numOfRows * blockDimension));

            // Calculate new height and width
            img_height = numOfRows * blockDimension;
            img_width = numOfColumns * blockDimension;

            // Define matrix 
            Console.Out.Write(DateTime.Now);
            Console.Out.WriteLine("Calling ImageGridToEvalutionMatrix...");
            int[,] gridBoxEvaluation = ImageGridToEvaluationMatrix(bitmap, numOfRows, numOfColumns, blockDimension, 0.99);

            List<List<int>> clustersListVert = new List<List<int>>();
            List<List<int>> clustersListHori = new List<List<int>>();
            List<Rectangle> listRecVert = new List<Rectangle>();
            List<Rectangle> listRecHori = new List<Rectangle>();

            Console.Out.Write(DateTime.Now);
            Console.Out.WriteLine("Finding clusters...");
            #region <-- find cluster of dots - vertical rectangle form -->
            // Iterate through evaluation matrix to find clusters of dots (grid boxes) - vertical
            for (int i = 0; i < numOfColumns; i++)
            {
                List<int> cluster = new List<int>() { 0, 0, 0 };
                for (int j = 0; j < numOfRows; j++)
                {
                    // IF gridBoxEvaluation[i,j] IS ZERO ADD IT TO VISITED LIST AND DISCARD
                    if (gridBoxEvaluation[j, i] == 1)
                    {
                        cluster[0] = i;
                        cluster[1] = j;

                        while (j < numOfRows && gridBoxEvaluation[j, i] == 1)
                        {
                            j++;
                        }

                        cluster[2] = j - 1;
                        // If rectangle size is greater than some threshold
                        if (cluster[2] - cluster[1] > 20)
                        {
                            // Add cluster to cluster list
                            clustersListVert.Add(cluster);
                        }

                        cluster = new List<int>() { 0, 0, 0 };
                    }
                }
            }

            foreach (List<int> clus in clustersListVert)
            {
                int xCor = clus[0] * blockDimension;
                int yCor = clus[1] * blockDimension;

                int yCor1 = clus[2] * blockDimension + blockDimension;

                Rectangle r = new Rectangle(xCor, yCor, blockDimension, yCor1 - yCor);
                listRecVert.Add(r);
            }
            #endregion <-- end find cluster region -->

            #region <-- find cluster of dots - horizontal rectangle form -->
            // Iterate through evaluation matrix to find clusters of dots (grid boxes) - vertical
            for (int i = 0; i < numOfRows; i++)
            {
                List<int> cluster = new List<int>() { 0, 0, 0 };
                for (int j = 0; j < numOfColumns; j++)
                {
                    // IF gridBoxEvaluation[i,j] IS ZERO ADD IT TO VISITED LIST AND DISCARD
                    if (gridBoxEvaluation[i, j] == 1)
                    {
                        cluster[0] = i;
                        cluster[1] = j;

                        while (j < numOfColumns && gridBoxEvaluation[i, j] == 1)
                        {
                            j++;
                        }

                        cluster[2] = j - 1;

                        // If rectangle size is greater than some threshold
                        if (cluster[2] - cluster[1] > 20)
                        {
                            // Add cluster to cluster list
                            clustersListHori.Add(cluster);
                        }

                        cluster = new List<int>() { 0, 0, 0 };
                    }
                }
            }

            foreach (List<int> clus in clustersListHori)
            {
                int xCor = clus[1] * blockDimension;
                int yCor = clus[0] * blockDimension;

                int xCor1 = clus[2] * blockDimension + blockDimension;

                Rectangle r = new Rectangle(xCor, yCor, xCor1 - xCor, blockDimension);
                listRecHori.Add(r);
            }
            #endregion <-- end find cluster region -->
            Console.Out.Write(DateTime.Now);
            Console.Out.WriteLine("Task Finished.");

            //return clusterBoundingRectangles;
            //return clustersList;
            return new List<List<Rectangle>>() { listRecVert, listRecHori };
        }

        // Function to find which region contains text and which one text
        public List<List<Rectangle>> SeparateTextAndImageBlocks(Bitmap bitmap, List<Rectangle> blocks)
        {
            return new List<List<Rectangle>>();
        }

        // Function to get text blocks in order
        public List<Rectangle> GetSequenceOfBlocks(List<Rectangle> blocks)
        {
            return new List<Rectangle>();
        }
    }
}
