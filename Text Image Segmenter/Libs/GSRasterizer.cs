﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

// required Ghostscript.NET namespaces
using Ghostscript.NET;
using Ghostscript.NET.Rasterizer;
using System.Windows.Forms;

namespace NeOCR.Libs
{
    public static class GSRasterizer
    {
        /*
        // sample function to render pdf to image
        public void Start()
        {
            // ghostscript variables
            GhostscriptVersionInfo _lastInstalledVersion = null;
            GhostscriptRasterizer _rasterizer = null;

            int desired_x_dpi = 300;
            int desired_y_dpi = 300;

            string inputPdfPath = @"E:\pdf\155.pdf";
            string outputPath = @"E:\_pdf_out\";

            _lastInstalledVersion =
                GhostscriptVersionInfo.GetLastInstalledVersion(
                        GhostscriptLicense.GPL | GhostscriptLicense.AFPL,
                        GhostscriptLicense.GPL);

            _rasterizer = new GhostscriptRasterizer();

            _rasterizer.Open(inputPdfPath, _lastInstalledVersion, false);

            for (int pageNumber = 1; pageNumber <= _rasterizer.PageCount; pageNumber++)
            {
                string pageFilePath = Path.Combine(outputPath, "Page-" + pageNumber.ToString() + ".png");

                Image img = _rasterizer.GetPage(desired_x_dpi, desired_y_dpi, pageNumber);
                img.Save(pageFilePath, ImageFormat.Png);

                Console.WriteLine(pageFilePath);
            }
        }
        */

        // function to renderr pdf to image
        public static String RenderPDFToImage(GhostscriptVersionInfo _lastInstalledVersion, GhostscriptRasterizer _rasterizer, string inputPdfPath, string outputPath, int pageNumber, int desired_x_dpi, int desired_y_dpi)
        {
            // rasterize and save image
            string pageFilePath = outputPath + Path.GetFileName(inputPdfPath) + "_page-" + pageNumber.ToString() + ".png";
            try
            {
                // open file with rasterizer
                _rasterizer.Open(@inputPdfPath, _lastInstalledVersion, false);

                Image img = _rasterizer.GetPage(desired_x_dpi, desired_y_dpi, pageNumber);
                img.Save(@pageFilePath, ImageFormat.Png);
            }
            catch(Exception ex)
            {
                MessageBox.Show("GS Error: " + ex.ToString());
            }

            // return filepath
            return pageFilePath;
        }

        // function to get page count in provided PDF document
        public static int GetNumberOfPages(GhostscriptVersionInfo _lastInstalledVersion, GhostscriptRasterizer _rasterizer, String inputPdfPath)
        {
            // open file with rasterizer
            _rasterizer.Open(inputPdfPath, _lastInstalledVersion, false);

            // return page count
            return _rasterizer.PageCount;
        }
    }
}