﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace Text_Image_Segmenter
{
    public partial class NeOCR_Main
    {
        public void CreateDocument(List<string> image_paths)
        {
            try
            {
                //Create an instance for word app
                Application winword = new Application();

                //Set status for word application is to be visible or not.
                winword.Visible = false;

                //Create a missing variable for missing value
                object missing = System.Reflection.Missing.Value;

                //Create a new document
                Document document = winword.Documents.Add(ref missing, ref missing, ref missing, ref missing);
                document.PageSetup.PaperSize = WdPaperSize.wdPaperA4;

                float doc_height = document.PageSetup.PageHeight;
                float doc_width = document.PageSetup.PageWidth;

                float x_scale = 0;
                float y_scale = 0;

                if (img_width > doc_width)
                {
                    x_scale = doc_width / img_width;
                }
                else
                {
                    x_scale = img_width / doc_width;
                }

                if (img_height > doc_height)
                {
                    y_scale = doc_height / img_height;
                }
                else
                {
                    y_scale = img_height / doc_height;
                }

                int counter = 0;
                foreach (System.Drawing.Rectangle rec in text_blocks)
                {
                    Shape textbox = document.Shapes.AddTextbox(Microsoft.Office.Core.
                    MsoTextOrientation.msoTextOrientationHorizontal, rec.X * x_scale, rec.Y * y_scale, rec.Width * x_scale, rec.Height * y_scale > 30 ? rec.Height * y_scale : 30);

                    textbox.TextFrame.TextRange.Text = texts[counter];

                    // Set the Range to the first paragraph. 
                    Range rng = textbox.TextFrame.TextRange;

                    // Change the formatting. To change the font size for a right-to-left language,
                    rng.Font.SizeBi = (int)Math.Ceiling((x_scale + y_scale) * rng.Font.Size);
                    rng.Font.Name = "Arial";
                    rng.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;

                    rng.Select();
                    textbox.Line.Visible = Microsoft.Office.Core.MsoTriState.msoFalse;
                    counter++;
                }

                int image_count = 0;
                foreach (System.Drawing.Rectangle img in image_blocks)
                {
                    Image im = Image.FromFile(image_paths[image_count]);

                    int resize_width = (int)Math.Floor(img.Width * x_scale);
                    int resize_height = (int)Math.Floor(img.Height * y_scale);

                    string file_name = ResizeImage(im, new Size(resize_width, resize_height), img);
                    im.Dispose();

                    Shape picbox = document.Shapes.AddShape(1, img.X * x_scale, img.Y * y_scale, img.Width * x_scale, img.Height * y_scale > 30 ? img.Height * y_scale : 30);
                    picbox.Line.Visible = Microsoft.Office.Core.MsoTriState.msoFalse;
                    picbox.Fill.UserPicture(file_name);
                    image_count++;
                }

                //Save the document
                System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog();
                saveFileDialog.InitialDirectory = @"C:\";
                saveFileDialog.RestoreDirectory = true;
                saveFileDialog.Title = "Save Your Documents";
                saveFileDialog.DefaultExt = "docx";

                if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    object filename = @saveFileDialog.FileName;
                    document.SaveAs(ref filename);
                    document.Close(ref missing, ref missing, ref missing);
                    document = null;
                    winword.Quit(ref missing, ref missing, ref missing);
                    winword = null;
                    System.Windows.Forms.MessageBox.Show("Document created successfully !");
                }

                foreach (string file_name in image_paths)
                {
                    if (File.Exists(file_name))
                    {
                        File.Delete(file_name);
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public string ResizeImage(Image imgToResize, Size size, System.Drawing.Rectangle rec)
        {
            Bitmap bmp = new Bitmap(imgToResize, size);
            string file_name = rec.X + "" + rec.Y + "" + rec.Width + "" + rec.Height + ".png";
            string file_path = Path.GetTempPath() + Path.GetFileName(file_name);

            bmp.Save(file_path, System.Drawing.Imaging.ImageFormat.Png);
            bmp.Dispose();

            return file_path;
        }
    }

}
