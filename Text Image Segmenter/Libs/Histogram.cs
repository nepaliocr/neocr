﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Text_Image_Segmenter.Libs
{
    class Histogram
    {
        // method to count rowwise black pixels within rectangle boundaries
        // inputBmp => image to extract pixel frequency data, rect => defines the part of image to projection profile
        public List<int> RowPixelCount(Bitmap inputBmp, Rectangle rect)
        {
            // find starting coordinate, width and height of rectangle
            int xCorRec = rect.X;
            int yCorRec = rect.Y;
            int recWidth = rect.Width;
            int recHeight = rect.Height;

            // List for storing number of black pixels in each row [black pixel]
            List<int> hProjProfList = new List<int>();

            int blackPixelCount = 0;

            // count rowwise black pixels within rectangle boundaries
            for (int i = yCorRec; i <= (yCorRec + recHeight); i++)
            {
                for (int j = xCorRec; j <= (xCorRec + recWidth); j++)
                {
                    Color color = inputBmp.GetPixel(j, i);
                    if (color.R.ToString().Equals("0") && color.G.ToString().Equals("0") && color.B.ToString().Equals("0"))
                    {
                        blackPixelCount++;
                        //MessageBox.Show("counting black pixels");
                    }
                }

                // Store number of black pixels in each row in word  // list.Add(...);
                hProjProfList.Add(blackPixelCount);

                blackPixelCount = 0;
            }

            return hProjProfList;
        }

        // method to count column wise black pixels within rectangle boundaries
        // input_bmp => image to extract pixel frequency data, rect => defines the part of image to projection profile
        public List<int> ColPixelCount(Bitmap inputBmp, Rectangle rect)
        {
            // find starting coordinate, width and height of rectangle
            int xCorRec = rect.X;
            int yCorRec = rect.Y;
            int recWidth = rect.Width;
            int recHeight = rect.Height;

            // List for storing number of black pixels in each column [black pixel]
            List<int> hProjProfList = new List<int>();

            int blackPixelsInCol = 0;

            // loop to extract black pixels information within rectangle boundaries
            for (int i = xCorRec; i < (xCorRec + recWidth); i++)
            {
                for (int j = yCorRec; j < (yCorRec + recHeight); j++)
                {
                    Color color = inputBmp.GetPixel(i, j);
                    if (color.R.ToString().Equals("0") && color.G.ToString().Equals("0") && color.B.ToString().Equals("0"))
                    {
                        blackPixelsInCol++;
                        //MessageBox.Show("increment black pixels.. v profile");
                    }
                }

                hProjProfList.Add(blackPixelsInCol);
                blackPixelsInCol = 0;
            }

            return hProjProfList;
        }

        public int BlackPixelCount(Bitmap inputBmp, Rectangle rect)
        {
            // find starting coordinate, width and height of rectangle
            int xCorRec = rect.X;
            int yCorRec = rect.Y;
            int recWidth = rect.Width;
            int recHeight = rect.Height;

            int blackPixelsCount = 0;

            // loop to extract black pixels information within rectangle boundaries
            for (int i = xCorRec; i < (xCorRec + recWidth); i++)
            {
                for (int j = yCorRec; j < (yCorRec + recHeight); j++)
                {
                    Color color = inputBmp.GetPixel(i, j);
                    if (color.R.ToString().Equals("0") && color.G.ToString().Equals("0") && color.B.ToString().Equals("0"))
                    {
                        blackPixelsCount++;
                    }
                }
            }

            return blackPixelsCount;
        }

        // method to count rowwise black pixels in entire image
        public List<int> RowPixelCount(Bitmap img)
        {
            List<int> pixels_list = new List<int>();

            int img_width = img.Width;
            int img_height = img.Height;
            int black_pixels = 0;
            int i, j;
            for (i = 0; i < img_height; i++)
            {
                for (j = 0; j < img_width; j++)
                {
                    Color color = img.GetPixel(j, i);
                    if (color.R.ToString().Equals("0") && color.G.ToString().Equals("0") && color.B.ToString().Equals("0"))
                        black_pixels++;
                }
                pixels_list.Add(black_pixels);
                black_pixels = 0;
            }
            return pixels_list;
        }

        // method to count colwise black pixels in entire image
        public List<int> ColPixelCount(Bitmap img)
        {
            List<int> pixels_list = new List<int>();
            int img_width = img.Width;
            int img_height = img.Height;
            int black_pixels = 0;
            int i, j;
            for (i = 0; i < img_width; i++)
            {
                for (j = 0; j < img_height; j++)
                {
                    Color color = img.GetPixel(i, j);
                    if (color.R.ToString().Equals("0") && color.G.ToString().Equals("0") && color.B.ToString().Equals("0"))
                        black_pixels++;
                }
                pixels_list.Add(black_pixels);
                black_pixels = 0;
            }
            return pixels_list;
        }

        //Drawing histogram with vertical orientation
        public Bitmap DrawHistogram(List<int> hist_values, Bitmap img_to_hist)
        {
            Color myColor = Color.Black;

            int[] myValues = hist_values.ToArray();

            Pen myPen = new Pen(myColor);

            System.Drawing.Image img_histogram = new Bitmap(img_to_hist.Width, img_to_hist.Height);
            using (Graphics g = Graphics.FromImage(img_histogram))
            {
                double max = myValues.Max();
                for (int i = 0; i < myValues.Length; i += 2)
                {
                    //We draw each line
                    // Draw line using Point structure
                    Point p1 = new Point(i / 2, 0);

                    int scaled_value = (int)(100 / (float)max * (float)myValues[i]);

                    Point p2 = new Point(i / 2, scaled_value);

                    g.DrawLine(myPen, p1, p2);

                }

                img_histogram.RotateFlip(RotateFlipType.Rotate180FlipX);    //Rotate Histogra Image
                img_histogram.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }

            myPen.Dispose();

            return (Bitmap)img_histogram;
        }

        // Function to locate minima in this histogram
        public List<int> LocateMinimaInTheHistogram(List<int> histogram, int valleyThreshold)
        {
            // variable to store valley points
            List<int> vallies = new List<int>();

            // get deep valley and high peack in the histogram
            int deepValleyValue = histogram.Min();
            int highPeak = histogram.Max();

            // the points with values less than valleyThreshold will be considered as valley region
            //int valleyThreshold = (int)(0.2 * highPeak);

            for(int i=0;i<histogram.Count;i++)
            {
                int valleyStart = 0;
                int valleyEnd = 0;

                if (histogram[i] < valleyThreshold)
                {
                    valleyStart = i;
                    while(i < histogram.Count && histogram[i] < valleyThreshold)
                    {
                        valleyEnd = i;
                        i++;
                    }

                    //vallies.Add((valleyStart+valleyEnd)/2);
                    vallies.Add(valleyStart);
                    vallies.Add(valleyEnd);
                }
            }

            return vallies;
        }

        // Cluster Histogram
        public List<int> ClusterHistogram(List<int> vallies)
        {
            List<int> clusterBoundaries = new List<int>();
            int avgDist = 0;
            for(int i = 0; i < vallies.Count-2;)
            {
                avgDist += vallies[i + 1] - vallies[i];
                i += 2;
            }

            avgDist = (int)((float)avgDist / (vallies.Count/2.0));

            clusterBoundaries.Add(vallies[1]);

            for (int i = 0; i < vallies.Count - 2;)
            {
                if(vallies[i + 1] - vallies[i] > 2*avgDist)
                {
                    clusterBoundaries.Add(vallies[i+1]);
                    clusterBoundaries.Add(vallies[i + 2]);
                }

                i += 2;
            }

            //clusterBoundaries.Add(vallies[vallies.Count - 1]);
            return clusterBoundaries;
        }
    }

}
