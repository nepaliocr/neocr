﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Text_Image_Segmenter.Libs
{
    class GlobalVariables
    {
        private static Boolean scan_status = false;

        // Declare a Code property of type string:
        public static Boolean SCAN_STATUS
        {
            get
            {
                return scan_status;
            }
            set
            {
                scan_status = value;
            }
        }
    }
}
