# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* NeOCR is a free software based on Tesseract (Open Source OCR Engine) for the Windows operating system. It provides an easy and user-friendly user interface to recognize texts contained in images as well as PDF documents and convert to editable text formats (.txt, .doc, .docx).
* Version 1.0.0
* Download Windows Executable (https://drive.google.com/open?id=0B8BnppTIW9YFMGozMjZnYW8tVUE)

### Installation Pre-requisites ###

.Net Framework 4.5, GhostScript 

### Credits ###

NeOCR v1.0.0:
The following software libraries have been used in the development of the NeOCR:

- Tesseract 4.00alpha (https://github.com/tesseract-ocr/tesseract)

Windows executables are provided by Mannheim University Library (UB Mannheim) (https://digi.bib.uni-mannheim.de/tesseract/)

- Ghostscript ( https://www.ghostscript.com/ )

- Aforge.Net ( http://www.aforgenet.com/ )

- Accord.Net ( http://accord-framework.net/ )

- Html Agility Pack ( http://www.htmlagilitypack.com/ )

Development Team:

Special thanks to the Information and Language Processing Research Lab (ILPRL), Kathmandu University who have developed this software.

Team Lead: Dr. Bal Krishna Bal

Developers: Nirajan Pant, Sanjeev Budha

Funding Support: We are thankful to the Direct Aid Program, Australian Embassy to Nepal for providing the funding support to develop this software.

For any queries on the software, please kindly contact us at:

Nepal Association of the Blind (NAB)
GPO BOX 9399
Rara Mithila Marga-4, Sukedhara Kathmandu, Nepal

### Who do I talk to? ###

Repo Maintained By
Nirajan Pant
Email: nirajan_pant@yahoo.com

Or you can also contact

Nepal Association of the Blind (NAB)
GPO BOX 9399
Rara Mithila Marga-4, Sukedhara Kathmandu, Nepal
Phone No : +977 1 4376580, 4376598
Fax : +977 1 4378622

Email: nepaliocr2017@gmail.com